# Performance/Optimization Tracker

Here I am tracking how the performance changes as I add new features and optimize various parts of the code. This started during the switch to triangle mesh based rendering. These performance metrics are relevant to the object-based renderer (not the ray-tracer).

The features introduced in this record include:
 - Rendering in terms of triangle meshes instead of raw triangles
 - Shading and coloring using vertex interpolation
 - Introducing transparency (again)
 - Rastering fragments with z-distance stored instead of painter's algorithm
 - Implementing multithreading in rasterization step
 - Implementing multithreading in drawing step

[[_TOC_]]

## Performance Metric

Initial test setup:
 - Transparency: no (later yes)
 - Resolution: 1000 x 1000
 - Scene: three pots on three boxes (6 meshes)
 - Number of triangles: 9396
 - Using optimization `-O3`

## Phase 0

This is the original performance which uses object-rendering and the painter's
algorithm. It can't interpolate vertex attributes. It has rendering artifacts
related to drawing order.

**Test Results** (FPS): 468, 458, 457

## Phase 1

Here we switch to rendering using:
 - triangle meshes
 - vertex interpolation
 - rasterizing fragments

However, this is still the painters algorithm because currently we only store
fragment with lowest z-distance. We also rasterize directly to the pixel buffer.
In other words, this implementation combines rasterizing and drawing.

### Results

||P0|P1 Baseline|Fix #1|Fix #2|Fix #3|Fix #4|
|---|-------|------------|------|------|------|------|
FPS 1|468|152|225|261|279|316|
FPS 2|458|151|224|261|267|310|
FPS 3|457|147|223|253|270|308|
Avg|461|150|224|258.3|272|311.3|
Improve factor|-|-|49%|15.3%|5.3%|14.5%|

### Fix Descriptions

[New Baseline](c8b4d9e11c232c8d64a7e3d77205a816797caffb)

 - introduces meshes and single-depth z-buffering.
 - Possible redundant or irrelevant calculations made on vertices.
 - Rasterize functions not optimized
 - No clipping.

[Fix #1](c118e4ccfb62b7ca602f1fd0c5b4a6a0af496234)
  - Changed rasterize to calculate how alpha, beta, gamma change with x/y
  - Consider all points on rectangle for drawing candidates.

[Fix #2](59ec2d86cb09b056e03012b85d55ca880c3c27a8) - Rasterization computes interpolated z and color using dz_dx, dcolor_dx, etc

[Fix #3](7c11d2b9f60717ddf01defb4122fdaf918b8980a) - Rasterization computes all interpolations in terms of dx, dy derivatives

[Fix #4](f899dc90c2441afe62c969659767f6e757969f1c) - Rasterization computes min x value to start drawing triangle in each row

## Phase 2 - z-buffering
We were able to achieve 310 FPS using a z-buffer with length 1. Unfortunately, this prevents us from
drawing transparency. To enable true transparency, we need to keep all the objects in the z-buffer.
This is going to be necessarily slower since we have to keep a copy of each fragment. Furthermore,
we have to later sort these fragments to figure out which to draw when. However, hopefully, with
some good optimizations, we can achieve a similar level of performance.

**Summary**

 - Introduces true z-buffering of fragments
 - Enables transparency
 - Splits rendering and rasterization into two steps

### Results

|             |P1 |P2 |Fix #1|Fix #2|Fix #3|
|-------------|---|---|------|------|------|
FPS 1         |316|145|144   |147   |155   |
FPS 2         |310|147|144   |145   |155   |
FPS 3         |308|143|143   |148   |158   |
Avg           |   |145|143.7 |146.7 |156   |
Improve factor| - | - |  -   |  -   | 7.6% |


### Fix Descriptions

[P2 Baseline](18588a79cff32f936d672ed0cc7a5404f62ed831)

 - To allow for composition, stores all fragments at a given pixel instead of closest (`vector<Fragment>` -> `vector<vector<Fragment>>`)
 - Drawing stage is now non-trival - has to sort the fragments at each pixel and draw from closest opaque to closest fragment.

[Fix #1](b4554d2fb92ca8ce40ec9ba2d5758fefb75dcd0e) - Minor changes to how shader computes stuff, some old things removed

[Fix #2](f1621636fd319b8205b13e34e0b52f8481b3f788) - Changed drawing code to check for # fragments = 1

[Fix #3](28bb4bcd39db0943ff10dfa753392c21c175c09d) - Changes fragment generation code during rasterization.

 - Before, we kept track of all fragments at each pixel. During drawing, we would sort the fragments and then draw them from the closest opaque fragment to the closest fragment
 - Now fragments are sorted as they are generated during rasterization using linear search
 - Any fragment which is behind an opaque fragment is not stored. This is achieved by keeping track of the closest opaque fragments in `Raster::closest_opaque`
 - This makes the drawing code much simpler because it doesn't have to sort anything or find the closest opaque fragment.
 - Also, perhaps most importantly, fragments are allocated using `emplace_back` instead of `push_back`


### Note - Static Allocation

I also tried using a static fragment buffer allocated with malloc. 
In other words, I added the members to `Raster`:
 - `vector<vector<Fragment>> fragments` -> `Fragment* fragments = (...)malloc(sizeof(Fragment)*width*hegight*FRAG_DEPTH)`
 -  `-` -> `size_t* frag_size = (...) malloc(...)`

Here `FRAG_DEPTH` is some constant which determines the maximum number of fragments that can be stored at a given pixel. `frag_size` keeps track of how many fragments are at each pixel in each render loop.

I did this because I noticed that clearing the raster buffer was weirdly slow (it has to call `vector<Fragment>::clear` for each pixel). With static allocation, clearing the buffer is much simpler: `memset(frag_size, 0, width * height)`.

At first it appeared to show a pretty decent performance benefit. However, once I added a check for buffer overflow in each raster cycle `if (frag_size[index] >= FRAG_DEPTH) print_warning()`, the performance increase disappeared.

I think I should revisit this idea later. I suspect the reason this doesn't achieve the performance gains I hoped is that most pixels have only a few fragments in them. The `vector<vector<>>` solution is more space efficient.

## Phase 3

This is ongoing. Not clear exactly what will be changed. 
Note that the benchmark scene has changed here to be more complicated. The framerates are expected to decrease automatically.

### New Performance Metric

Test scene is `src/examples/scenes.h` / `complex_pots2`. This scene features
 - multiple stacked transparent objects
 - opaque-transparent occlusion
 - color interpolation.

Parameters
 - Transparency: yes
 - Resolution: 1000 x 1000
 - Scene: 6 pots, 3 boxes, a floor
 - Number of trials: 2000
 - Number of triangles: 18764
 - Using optimization `-O3`

It looks like this:

![pot scene](examples/transparent-pot-scene-1.png)

### Results

|             |P2 |Fix #1|Fix #2|Fix #3\*|Fix #4|Fix #5|Fix #6|Fix #7      |   Fix #8   | Fix #9     |
|-------------|---|------|------|--------|------|------|------|------------|------------|------------|
|FPS          | 77| 99   | 107  | 124    | 125  | 140  | 158  | 162        | 199        | 210-230    |
|Clear Frac   |11%|12.8% |10.9% |12.5%   |14.0% |16.9% |20.4% |20.8%, 1226 | 2.8%,  142 | 3.0%, 142  |
|Raster Frac  |65%|66.4% |69.1% |64.5%   |58.2% |67.1% |61.9% |61.4%, 3786 |72.8%, 3650 |71.9%, 3366 |
|Draw Frac    |22%|18.5% |17.6% |20.2%   |24.7% |12.5% |13.7% |13.8%,  800 |19.6%,  981 |19.7%,  923 |

\*This fix will not be incorporated into main codebase due to issues.

[Fix #1](fa7ea7ff06c918a45d9603dd8ff84365b6ffc912) - Fragment stores color as uint8_t[3] instead of `Vec3`

 - To compute color, we use a version of line-drawing algorithm. We store fractional part of current color separately from current color. When it goes over 1, we increment the current color.

[Fix #2](d27bfcccc48ffdc5aac4745b432c85dd8d00e930) - Fragment z-data is stored as float instead of double.

 - Because of memory inlining, this brings `sizeof(Fragment)` from 16 to 8.

[Fix #3](7477c5552c26c6097644a88478ab9dd9a4b0d3bb) - Rasterization math exclusively uses ints

 - I'm not including this in main codebase. Instead I'm leaving the code there with preprocessor flags to turn it on.
 - This fix is implemented in `src/full_renderer/fast_raster.h`
 - Basically it converts all floats into ints by multiplying them by 2^16. This means the first two bytes represent the whole part and the last two bytes represent the fractional part.
 - It gives us a somewhat significant speed increase, but there are major downsides:
   - Code is brittle. Lots of obscure operations done which are hard to maintain
   - Leaves rendering artifacts. Occasional edges are absent due to imprecision
 - It's a nice idea but it's not worth the cost.

[Fix #4](6ef50fe353658d543982d3cd1d4cb6eec6e84784) - Introduces multithreaded rasterization

  - Basically the raster is protected by a grid of mutexes
  - During triangle rasterization, the raster automatically locks the relevant mutexes
  - It detects when the pixel it is drawing goes out of scope of the current mutex and grabs another one
  - This performance is for 6 threads, with a grid of 32 x 32 mutexes
  - The work is divided into threads by triangle (not mesh), which incurs some overhead.

[Fix #5](d130ac75dc9700ee2579cd24a3f65c72fd6eed44) - introduces multithreaded rendering (drawing)

  - This one is simpler than multithreaded rasterization because each pixel can be rendered independently so there is no need for mutexes.

[Fix #6](594f894b3559c7a38bc0dad08b98b5212a4023eb) - removed fractional/whole color calculation during rasterization

  - This one was surprising. I removed it because I discovered it was the cause of an interpolation bug.
  - I previously added it (Phase 3 Fix #1) because it seemed to improve performance.
  - However, when I removed it, performance actually went up

[Fix #7](80149e5f6286302a1743844f25df7809ddf279dd) - caused by work balancing in rasterization - side effect of introduction of recursive object meshes

[Fix #8](914efa2ea242fa60c0c3b30487172efd97e8161f) - changed how raster clearing works
  - Clear fragments from raster as you render them
    - boost: 163 -> 194
    - for multithreaded, increases FPS 19%
    - for singlethreaded, decreases FPS 13%
  - Realized fragment occlusion check was too strong (was opaque && z > closest_opaque) should just be (z > closest_opaque)
    - boost: 194 -> 198
  - Dynamic calculation of number of objects
    - boost: 198 -> 201 (probably just noise)
  - Changed branch order in shading to favor objects in front of camera

[Fix #9](63e401fd55187e5ccdca5c2b4b7ee71993d37278) - optimized rasterization loop by pre-computing the end of x drawing range
  - Basically tried to keep number of statements in inner critical loop to an absolute minimum.
  - The performance here is difficult to assess, I've seen everything from 210 to 230.

## Phase 4

In this phase I mostly plan to target occlusion and vectorization. We use a new benchmark scene which contains a large number of polygons that are offscreen.

### Phase 4 Performance Metric

Test scene is `src/examples/scenes.h` / `benchmark2`. This scene is a large landscape with fractal trees and transparent orbs. The camera is placed in the middle of the scene so just by viewport occlusion, only ~1/4 of primitives should be drawn.

Parameters
 - Transparency: yes
 - Number of threads: 6
 - Number of triangles: 216226
 - Resolution: 1280 x 820
 - Landscape extending 80 x 80
 - Many fractal trees of depth 3 dotting landscape.
 - Three "biodomes" (large transparent spheres).
  - One encompasses most of the scene so camera is on the insize
  - Two are in front of one another in front of the camera to test stacked transparency.

It looks like this:

![benchmark2](examples/SR-performance-2.png)

### Baseline

At present there are likely many bugs in how its rendered related to integer overflow in raster.cc. This is the first piece of code that should be investigated. Next, it would be wise to try using vectorizing floating point operations. Finally, we should perform viewport and backface culling.

### Results

All numbers are the averagage number of microseconds per cycle (except FPS)

|             | P3  |
|-------------|-----|
|FPS          |   41|
|Clear        |  199|
|Projection   | 2028|
|Shading      | 1792|
|Rasterize    |15169|
|Drawing      | 4753|

## Misc ToDos

  1. Try loop unrolling.
  2. Try changing z = sqrt see if that fixes interpolation.
  3. Make calling pattern with StaticMesh consistent:
  - Option 1: wrap all StaticMesh in shared_ptr
      - this is okay because StaticMesh::children is vector of pointers
  - Option 2: Make all calls through references for consistency.
  4. When iterating through vertices/triangles, use ptr increment pattern.
  5. Dynamic raster thread size computation (can figure out number of objects during vertex processing)
  6. In raster thread queue, use static list of items.
    - After each item is processed, set pointer to null
    - May be faster than clearing vectors.
  7. In rasterization, order statements to improve data locality (register optimization)
