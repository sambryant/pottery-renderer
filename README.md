# Renderer

This is a simple real-time 3D graphics engine I wrote from scratch in C++ for practice. It runs fully on the CPU. It renders scenes of 100k triangles at about 200 FPS. The early work on this was done in Rust and python. The C++ version was introduced [here](dba1c18b8dfaf191da4a386251e26dac0191c987). One goal of this project is to use minimal outside resources - no tutorials, no OpenGL, etc. There are two rendering implementations.

1. [FullRenderer](src/full_renderer) - Uses object-based rasterization
  - This is the engine I'm actively developing.
  - It is fully multithreaded
  - It can handle occlusion loops, transparency, interpolated shading, and polygon intersections.
  - See [`performance.md`](performance.md) to see current state of performance
2. [ComplexRenderer](src/complex_renderer) - Uses ray tracing.
  - Currently it's defunct and not compatible with the rest of the code

The engine can either output a static image (in ppm format) or run a real-time interactive scene (using SDL). In interaction mode, the user can navigate around the scene and move the camera. The most important parts of the code are [FullRenderer](src/full_renderer/full_renderer.h), [Raster](src/full_renderer/raster.h), [Engine](src/scene/engine.h), and [StaticMesh](src/core/static_mesh.h).

The only dependency of this project is SDL, which is used to generate the window. You can still generate static images without install SDL. To make the code work without installing SDL, you need to 1) remove all the files in `src/animation`, 2) remove the include statement in `main.cc`, 3) modify `Makefile` / `env` to remove all SDL linking.

## Examples

Procedurally generated landscape meshes with a series of fractal tree meshes (originally rendered at 1000 x 1000). This rendered at 205 FPS on a MacBook Air.

![fractal tree](examples/landscape.png)

3D tree fractal rendered as a 20k triangle mesh (originally rendered at 1000 x 1000)

![fractal tree](examples/tree-fractal.png)

Scene with several transparent pots rendered as triangle meshes (originally rendered at 1000 x 1000)

![pot scene](examples/transparent-pot-scene-1.png)

## To Dos

  1. ~~Switch from triangles to triangle meshes to reduce duplicate vertex projection calls~~
  2. ~~For scan renderer - use raster with vertex attributes to resolve occlusion loops~~
  3. ~~For scan renderer - perform rasterization using multithreading - will require mutexes around z-buffer~~
  4. ~~Optimize mathematical operations by reducing copy constructors~~
  5. Optimizing rasterization mutex strategy to avoid thread hangs
  6. Thread reuse and work queues
  7. Implement basic culling techniques - back face, occlusion, etc.
  8. Optimize triangle drawing algorithm.
  9. Add in basic physics and interactions
  10. Consistent use of namespaces!
  11. Actual GPU use? (Currently all on CPU)
