#include "builder.h"

#include <optional>
#include <memory>
#include <iostream>

#include "../core/pmath.h"
#include "../core/color.h"
#include "../core/exception.h"

#include "scene.h"
#include "lighting.h"
#include "camera.h"

using namespace std;

SceneBuilder::SceneBuilder()
{
  this->lighting = nullptr;
  this->camera = nullptr;
  this->viewport = {DEFAULT_VP_WIDTH, DEFAULT_VP_HEIGHT};
  this->background = BLACK;
}

SceneBuilder &SceneBuilder::with_camera(std::shared_ptr<Camera> camera)
{
  this->camera = camera;
  return *this;
}

SceneBuilder &SceneBuilder::with_light_source(Vec3 source, precision_t ambient)
{
  this->lighting = make_shared<Lighting>(source, ambient);
  return *this;
}

SceneBuilder &SceneBuilder::with_screen(uint pixel_width, uint pixel_height, precision_t fov_x)
{
  this->screen.width = pixel_width;
  this->screen.height = pixel_height;
  precision_t aspect_ratio = ((precision_t) pixel_width) / ((precision_t) pixel_height);
  precision_t vp_width = 2 * CAMERA_VP_DISTANCE * tan((3.14159 / 180.0) * fov_x / 2);
  precision_t vp_height = vp_width / aspect_ratio;
  this->viewport[0] = vp_width;
  this->viewport[1] = vp_height;
  return *this;
}

SceneBuilder &SceneBuilder::with_background(Color background)
{
  this->background = background;
  return *this;
}

std::shared_ptr<Scene> SceneBuilder::build()
{
  if (this->camera) {
    return make_shared<Scene>(this->camera, this->lighting, this->viewport, this->screen, this->background);
  } else {
    throw core::GenericException("Must define camera first!");
  }
}
