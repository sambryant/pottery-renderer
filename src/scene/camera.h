#ifndef CAMERA
#define CAMERA

#include <vector>
#include <memory>
#include <optional>

#include "../core/pmath.h"
#include "../core/triangle.h"
#include "../core/color.h"

#define CAMERA_VP_DISTANCE 1.0 // DONT CHANGE
#define DEFAULT_CAMERA_POS {0.0, 0.0, 0.0}
#define DEFAULT_CAMERA_DIR {0.0, 0.0, 1.0}

/**
 * Represents the camera looking at the 3D scene.
 *
 * Note: here I'll describe why all the pixels are flipped in both x and y. It's not caused by just
 * this class, but rather how a few different classes work together. But this seems as good of a
 * place as any.
 *
 * When writing to the pixel output, the x and y pixels must both be flipped, but for different
 * reasons. The y pixel must be inverted because of how arrays are stored in memory. The lowest row
 * in a multi-dimensional array corresponds to the top of the rendered picture. This is
 * straightforward and not unique to this implementation.
 *
 * The x-pixel must also be inverted for a much more subtle reason. This rendering engine works by
 * transforming everything into a coordinate system where the camera is looking in the +z direction
 * and the viewport is located at z=0. However, when looking in the +z direction, the negative
 * x-axis is on the **right** hand side. We could probably switch to an implementation that aligns
 * the camera with the negative z-axis, but the assumptions are baked into a few parts of the code
 * so this might not be straightforward.
 */
class Camera
{
public:
  /// Location in 3D space of the camera.
  Vec3 pos;
private:
  // Angle between direction of view and y-axis (like polar angle but for y)
  precision_t angle1;
  // Angle between xz direction of view and z-axis (like azimuthal angle but for zx)
  precision_t angle2;
  // The left-right, up-down, and forward-back normalized viewing vectors
  Mat3 directions;
public:
  Camera(Vec3 pos, Vec3 dir)
  {
    this->set(pos, dir);
  }
  Camera()
  {
    // this->set(DEFAULT_CAMERA_POS, DEFAULT_CAMERA_DIR);
  }
public:
  void print_info() const;
  void move_pos(size_t axis, precision_t inc);
  void move_pos(precision_t inc_x, precision_t inc_y, precision_t inc_z);
  void move_camera(precision_t pan_inc, precision_t tilt_inc);
  void serialize(serializer::Serializer* out) const;
private:
  void set_view_from_angles(precision_t angle1, precision_t angle2);
  void set(const Vec3 &pos, const Vec3 &dir);
public:
  friend class Scene;
  friend class Engine;
};

#endif
