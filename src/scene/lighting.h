#ifndef SCENE_LIGHTING
#define SCENE_LIGHTING

#include "../core/pmath.h"

class Lighting {
public:
  const Vec3 source;
  precision_t ambient;
public:
  Lighting(const Vec3& source_, precision_t ambient_) : source(source_), ambient(ambient_) {};
};

#endif
