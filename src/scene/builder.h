#ifndef SCENE_BUILDER
#define SCENE_BUILDER

#include <optional>
#include <memory>

#include "../core/pmath.h"
#include "../core/color.h"
#include "../core/exception.h"

#include "scene.h"
#include "lighting.h"
#include "camera.h"

class SceneBuilder
{
private:
  std::shared_ptr<Lighting> lighting;
  std::shared_ptr<Camera> camera;
  Vec2 viewport;
  Screen screen;
  Color background;
public:
  SceneBuilder();
  SceneBuilder &with_camera(std::shared_ptr<Camera> camera);
  SceneBuilder &with_light_source(Vec3 source, precision_t ambient);
  SceneBuilder &with_screen(uint pixel_width, uint pixel_height, precision_t fov_x);
  SceneBuilder &with_background(Color background);
  std::shared_ptr<Scene> build();
};

#endif