#ifndef SCENE
#define SCENE

#include <vector>
#include <memory>
#include <optional>
#include <iostream>

#include "../core/pmath.h"
#include "../core/static_mesh.h"
#include "../core/color.h"
#include "../core/vertex.h"
#include "../core/serializer.h"

#include "screen.h"
#include "camera.h"
#include "lighting.h"
#include "engine.h"

#define DEFAULT_VP_WIDTH 2.0
#define DEFAULT_VP_HEIGHT 2.0

class Scene
{
public:
  std::shared_ptr<Camera> camera;
  const std::shared_ptr<Lighting> lighting;
  const Vec2 viewport;
  const Screen screen;
  const Color background;
  Engine engine;
  StaticMesh* root_object = NULL;
public:
  Scene(
    std::shared_ptr<Camera> camera_,
    std::shared_ptr<Lighting> lighting_,
    Vec2 viewport_,
    Screen screen_,
    Color background_) :
    camera(camera_),
    lighting(lighting_),
    viewport(viewport_),
    screen(screen_),
    background(background_),
    engine(Engine(screen_, viewport_)) {}
  Scene() = delete;
  ~Scene()
  {
    if (this->root_object)
      delete this->root_object;
  }
public:
  /**
   * Sets the root object for this scene and takes ownership of the passed pointer.
   */
  void set_root_object(std::unique_ptr<StaticMesh> obj)
  {
    if (this->root_object)
      delete this->root_object;
    this->root_object = obj.release();
  }

  size_t get_max_depth() const
  {
    if (this->root_object)
      return this->root_object->get_max_depth();
    else
      return 0;
  }

  size_t get_num_triangles() const
  {
    if (this->root_object)
      return this->root_object->get_num_triangles();
    else
      return 0;
  }

  inline void start_render_loop()
  {
    if (this->root_object == NULL)
    {
      std::cerr << "Scene has no root object! You must set this before rendering.\n";
      exit(1);
    }
    this->engine.update(this->camera);
  }

  void serialize(serializer::Serializer* out) const;

};

using SceneP = std::shared_ptr<Scene>;

#endif
