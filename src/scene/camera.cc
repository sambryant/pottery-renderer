#include "camera.h"

#include <cmath>
#include <memory>
#include <optional>

#include "../core/pmath.h"

using namespace std;

void Camera::set(const Vec3 &pos, const Vec3 &dir_)
{
  this->pos = pos;
  Vec3 dir = vec3_normalize(dir_);
  precision_t angle1 = atan2(dir[0], dir[2]);
  precision_t angle2 = asin(dir[1]);
  this->set_view_from_angles(angle1, angle2);
}

void Camera::print_info() const {
  cout << "Camera info:\n";
  cout << "\tpos = " << this->pos << '\n'
       << "\tth1 = " << this->angle1 << '\n'
       << "\tth2 = " << this->angle2 << '\n';
}

void Camera::move_pos(precision_t inc_x, precision_t inc_y, precision_t inc_z)
{
  this->pos = this->pos
            + this->directions[0] * inc_x
            + this->directions[1] * inc_y
            + this->directions[2] * inc_z;
}

void Camera::move_pos(size_t axis, precision_t inc)
{
  this->pos = this->pos + (this->directions[axis] * inc);
}

void Camera::move_camera(precision_t pan_inc, precision_t tilt_inc) {
  this->set_view_from_angles(this->angle1 + pan_inc, this->angle2 + tilt_inc);
}

void Camera::set_view_from_angles(precision_t angle1, precision_t angle2)
{
  Mat3 inverse_rotation = get_rotation_y( angle1) * get_rotation_x(-angle2);
  mat3_transpose_to(inverse_rotation, this->directions);
  this->angle1 = angle1;
  this->angle2 = angle2;
}

void Camera::serialize(serializer::Serializer* out) const
{
  out->serialize_arr("pos", pos);
  out->serialize_prm("angle1", angle1);
  out->serialize_prm("angle2", angle2);
}
