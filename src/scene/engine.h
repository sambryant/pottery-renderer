/**
 * Class which handles vertex math. Specifically, it computes the 2D projection of 3D vertices onto
 * the screen and computes the shading at each vertex.
 */
#ifndef SCENE_ENGINE
#define SCENE_ENGINE

#include <iostream>
#include <memory>

#include "../core/pmath.h"
#include "../core/vertex.h"
#include "../core/static_mesh.h"

#include "camera.h"
#include "lighting.h"
#include "screen.h"

/**
 * Does vertex processing math (see `notes/notes.pdf`).
 *
 * Note: this is not thread safe (due to mutable `num_valid_objects`).
 */
class Engine
{
private:
  const precision_t sx_vx;
  const precision_t sy_vy;
  const precision_t half_sx;
  const precision_t half_sy;
  Mat3 rotation;
  Vec3 translation;
  Vec3 camera_pos;
  mutable size_t num_valid_objects;
public:
  Engine(const Screen &screen, const Vec2 &viewport) :
   sx_vx(((precision_t) screen.width) / viewport[0]),
   sy_vy(((precision_t) screen.height) / viewport[1]),
   half_sx((precision_t) screen.width / 2),
   half_sy((precision_t) screen.height / 2) {}
  Engine() = delete;
public:
  size_t get_num_valid_objects() { return this->num_valid_objects; }
  void update(std::shared_ptr<Camera> camera);
  void process_vertex(Vertex &v) const;
  void process_object(StaticMesh* obj) const;
  void shade_object(StaticMesh* obj, const std::shared_ptr<Lighting> lighting) const;
protected:
  void vertex_to_normal(Vertex &v) const;
  void vertex_to_screen(Vertex &v) const;
};



#endif
