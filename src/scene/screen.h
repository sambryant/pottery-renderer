#ifndef SCREEN
#define SCREEN

#define DEFAULT_RESOLUTION 400

using screen_t = unsigned int;

struct Screen {
  screen_t width;
  screen_t height;
public:
  Screen() : width(DEFAULT_RESOLUTION), height(DEFAULT_RESOLUTION) {};
  Screen(screen_t w, screen_t h) : width(w), height(h) {};
};

#endif
