#include "engine.h"

#include <memory>
#include <cmath>

#include "camera.h"

#include "../core/pmath.h"
#include "../core/static_mesh.h"

using namespace std;

void Engine::shade_object(StaticMesh* obj, const shared_ptr<Lighting> lighting) const
{
  const Vec3& light_source = lighting->source;

  // Compute z-distance and light displacement vectors first.
  Vec3 vertex_camera_displacement;
  for (size_t i = 0; i < obj->num_vertices; i++)
  {
    Vertex& v = obj->vertices[i];
    vertex_camera_displacement = this->camera_pos - v.v;
    v.z = vec3_dot(vertex_camera_displacement, vertex_camera_displacement);
    v.light_displacement = vec3_normalize(light_source - v.v);
  }

  // Next compute which face of each triangle we are looking at
  for (size_t i = 0; i < obj->num_triangles; i++)
  {
    STriangle& t = obj->triangles[i];
    if (t.valid){
      this->num_valid_objects++;
      // Only compute shading when object is not marked as being illuminated.
      if (!obj->illuminated) {
        Vec3 obj_camera_displacement = this->camera_pos - t.v[0]->v;
        t.visible_face = vec3_dot(t.normal, obj_camera_displacement) > 0.0;

        for (int j=0; j<3; j++) {
          precision_t light = vec3_dot(t.v[j]->light_displacement, t.normal);
          if (t.visible_face) {
            t.shading[j] = max((0.5f * (1.0f + light)), lighting->ambient);
          } else {
            t.shading[j] = max((0.5f * (1.0f - light)), lighting->ambient);
          }
        }
      }
    }
  }

  // Finally, recursively call on the children
  for (auto child : obj->children) {
    this->shade_object(child, lighting);
  }
}

void Engine::process_object(StaticMesh* obj) const
{
  for (size_t i = 0; i < obj->num_vertices; i++)
  {
    this->process_vertex(obj->vertices[i]);
  }
  for (size_t i = 0; i < obj->num_triangles; i++)
  {
    STriangle& t = obj->triangles[i];
    t.valid = t.v[0]->valid && t.v[1]->valid && t.v[2]->valid;
  }
  for (auto child : obj->children)
  {
    this->process_object(child);
  }
}

void Engine::vertex_to_normal(Vertex &v) const
{
  for (int i=0; i<3; i++) {
    v.v_trans[i] = this->translation[i];
    for (int j=0; j<3; j++) {
      v.v_trans[i] += this->rotation[i][j] * v.v[j];
    }
  }
}

void Engine::vertex_to_screen(Vertex &v) const
{
  if (v.v_trans[2] > 0) {
    v.valid = true;
    precision_t factor = 1.0 / v.v_trans[2];
    v.v_screen[0] = this->half_sx - 1 - this->sx_vx * factor * v.v_trans[0];
    v.v_screen[1] = this->half_sy - 1 - this->sy_vy * factor * v.v_trans[1];
  } else {
    v.valid = false;
  }
}

void Engine::process_vertex(Vertex &v) const
{
  this->vertex_to_normal(v);
  this->vertex_to_screen(v);
}

void Engine::update(std::shared_ptr<Camera> camera)
{
  this->num_valid_objects = 0;
  this->camera_pos = camera->pos;
  this->rotation = get_rotation_x(camera->angle2) * get_rotation_y(-camera->angle1);
  Vec3 p = (this->rotation * camera->pos);
  for (int i=0; i<3; i++) {
    this->translation[i] = -p[i];
  }
}
