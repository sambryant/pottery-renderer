#include "scene.h"

void Scene::serialize(serializer::Serializer* out) const
{
  out->serialize_prm("screen_width", screen.width);
  out->serialize_prm("screen_height", screen.height);
  out->serialize_arr("background", background);
  out->serialize_arr("viewport", viewport);
  out->serialize_obj("camera", *camera);
  out->serialize_obj("root_object", *root_object);
}
