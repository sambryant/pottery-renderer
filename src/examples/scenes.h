#ifndef EXAMPLES_SCENES
#define EXAMPLES_SCENES

#include <memory>
#include <iostream>
#include <vector>

#include "../core/color.h"
#include "../core/pmath.h"
#include "../core/triangle.h"
#include "../core/mesh_builder.h"
#include "../core/output.h"
#include "../core/pixmap.h"
#include "../scene/builder.h"
#include "../shapes/pot.h"
#include "../performance.h"

namespace examples {

  namespace scenes {

    SceneP triangle_simple(uint res_x = 400, uint res_y = 400, precision_t fov_x = 90.0);
    SceneP triangles_ordered(uint res_x = 400, uint res_y = 400, precision_t fov_x = 90.0);
    SceneP tetrahedrons(uint res_x = 800, uint res_y = 600, precision_t fov_x = 90.0);
    SceneP cube(uint res_x = 800, uint res_y = 600, precision_t fov_x = 90.0, bool transparancy = false);
    SceneP complex_pots(uint res_x = 600, uint res_y = 400, precision_t fov_x = 90.0, bool transparancy = false);
    SceneP complex_pots2(uint res_x = 600, uint res_y = 400, precision_t fov_x = 90.0);
    SceneP panels(uint res_x = 800, uint res_y = 400, precision_t fov_x = 90.0, bool transparancy = false);
    SceneP broken_interpolation(uint res_x = 600, uint res_y = 400, precision_t fov_x = 90.0);
    SceneP perpendicular_surfaces(uint res_x = 600, uint res_y = 400, precision_t fov_x = 90.0);
    SceneP icosahedron(uint res_x, uint res_y, precision_t fov_x = 120.0);
    SceneP sphere(uint res_x, uint res_y, precision_t fov_x = 120.0);
    SceneP house1(uint res_x = 400, uint res_y = 400, precision_t fov_x = 90.0);
    SceneP tree1(uint res_x = 400, uint res_y = 400, precision_t fov_x = 90.0, uint depth=4);
    SceneP landscape1(uint res_x = 600, uint res_y = 400, precision_t fov_x = 100.0);
    SceneP landscape2(uint res_x = 1280, uint res_y = 720, precision_t fov_x = 100.0, unsigned seed=13);

    /**
     * This scene is used to benchmark performance.
     *
     * It shows a series of pots and boxes, with the pots being transparent.
     */
    inline SceneP benchmark1()
    {
      return complex_pots2(1000, 1000, 90.0);
    }

    /**
     * This scene is used to benchmark performance.
     *
     * It is a landscape with several features we would like to test optimization for:
     *  - High resolution.
     *  - Many objects behind the camera which should be occluded.
     *  - Many objects in front of camera but offscreen which should not be rendered.
     *  - Many objects occluded behind hills and trees.
     */
    inline SceneP benchmark2()
    {
      return landscape2(1280, 820, 100.0, 13);
    }


  }

}

#endif
