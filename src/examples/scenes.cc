#include "scenes.h"

#include <memory>
#include <iostream>
#include <vector>

#include "../core/color.h"
#include "../core/pmath.h"
#include "../core/mesh_builder.h"
#include "../core/vertex.h"
#include "../core/output.h"
#include "../core/pixmap.h"
#include "../scene/builder.h"
#include "../shapes/pot.h"
#include "../shapes/basic.h"
#include "../shapes/landscape.h"
#include "../shapes/house.h"
#include "../shapes/tree.h"

using namespace std;

namespace examples {
  namespace scenes {

    SceneP triangle_simple(uint res_x, uint res_y, precision_t fov_x)
    {
      Vec3 pos = {0.0, 0.0, -2.0};
      Vec3 dir = {0.0, 0.0, 1.0};
      Vec3 light = {20.0, 20.0, 10.0};
      SceneP scene = SceneBuilder()
        .with_camera(make_shared<Camera>(pos, dir))
        .with_light_source(light, 0.5)
        .with_screen(res_x, res_y, fov_x)
        .build();
      MeshBuilder root;
      Vertex* v0 = &root.add({1, 0, 0})->with_color(BLUE);
      Vertex* v1 = &root.add({-0.5, 0.866025, 0})->with_color(BLUE);
      Vertex* v2 = &root.add({-0.5, -0.866025, 0})->with_color(BLUE);
      root.add(v0, v1, v2);
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    SceneP perpendicular_surfaces(uint res_x, uint res_y, precision_t fov_x)
    {
      Vec3 pos = {0.05, 0.0, -1.0};
      Vec3 dir = {-0.01, 0.0, 1.0};
      Vec3 light = {20.0, 20.0, 10.0};
      SceneP scene = SceneBuilder()
        .with_camera(make_shared<Camera>(pos, dir))
        .with_light_source(light, 0.5)
        .with_screen(res_x, res_y, fov_x)
        .build();

      MeshBuilder mesh;
      MeshBuilder* squares[2];
      Color colors[] = {RED, BLUE};
      auto w = 1.0;
      auto d = 1.0;
      for (int i=0; i<2; i++) {
        MeshBuilder* dst = mesh.add_child();
        auto v0 = dst->add({-w/2, 0, -d/2});
        auto v1 = dst->add({-w/2, 0, +d/2});
        auto v2 = dst->add({+w/2, 0, -d/2});
        auto v3 = dst->add({+w/2, 0, +d/2});
        dst->add(v0, v1, v2);
        dst->add(v1, v2, v3);
        dst->with_opacity(true)
            .with_color(colors[i]);
        squares[i] = dst;
      }
      auto rx = get_rotation_x(M_PI/2);
      auto rz = get_rotation_z(M_PI/2);
      squares[0]->transform(rx);
      squares[1]->transform(rz)
                 .translate({0, 0, +d/2 + 0.0001});
      scene->set_root_object(unique_ptr<StaticMesh>(mesh.to_static_mesh()));
      return scene;
    }

    SceneP triangles_ordered(uint res_x, uint res_y, precision_t fov_x)
    {
      Vec3 pos = {0.0, 0.0, -2.0};
      Vec3 dir = {0.0, 0.0, 1.0};
      Vec3 light = {20.0, 20.0, 10.0};
      SceneP scene = SceneBuilder()
        .with_camera(make_shared<Camera>(pos, dir))
        .with_light_source(light, 0.5)
        .with_screen(res_x, res_y, fov_x)
        .build();
      vector<Color> colors = {RED, YELLOW, BLUE, PURPLE, WHITE, GREEN};
      Vec3 tris[6][3] = {
        {{2., 0., 1.}, {-1., 1.73205, 1.0}, {-1., -1.73205, 1.0}},
        {{1.5, 0., 0.5}, {-0.75, 1.29904, 0.5}, {-0.75, -1.29904, 0.5}},
        {{1., 0., 0.}, {-0.5, 0.866025, 0.}, {-0.5, -0.866025, 0.}},
        {{0.5, 0., -0.5}, {-0.25, 0.433013, -0.5}, {-0.25, -0.433013, -0.5}},
        {{1., 0., -3.0}, {-0.5, 0.866025, -3.0}, {-0.5, -0.866025, -3.0}},
        {{0.5, 0., -3.5}, {-0.25, 0.433013, -3.5}, {-0.25, -0.433013, -3.5}},
      };
      MeshBuilder root;
      for (size_t i=0; i<6; i++) {
        MeshBuilder* mesh = root.add_child();
        array<Vertex*, 3> vertices;
        for (int j=0; j<3; j++) {
          vertices[j] = mesh->add(tris[i][j]);
        }
        mesh->add(vertices[0], vertices[1], vertices[2]);
        mesh->with_color(colors[i]);
      }
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    SceneP tetrahedrons(uint res_x, uint res_y, precision_t fov_x)
    {
      Vec3 pos = {+0.2, 4.0, 0.0};
      Vec3 dir = {0.0, -1.0, +0.0};
      Vec3 light = {20.0, 20.0, 10.0};
      SceneP scene = SceneBuilder()
        .with_camera(make_shared<Camera>(pos, dir))
        .with_light_source(light, 0.5)
        .with_screen(res_x, res_y, fov_x)
        .build();

      Color colors[] = {RED, YELLOW, BLUE, PURPLE};
      vector<Vec3> offsets = {{
        {-2.0, 0.0, 0.0},
        { 0.0, 0.0, 0.0},
        { 2.0, 0.0, 0.0},
      }};
      MeshBuilder root;

      for (auto &offset: offsets) {
        MeshBuilder* obj = root.add_child();
        shapes::make_tetrahedron(*obj)
          .scale(0.9)
          .translate(offset);
        for (int i=0; i<4; i++)
          obj->vertices[i]->with_color(colors[i]);
      }
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    SceneP cube(uint res_x, uint res_y, precision_t fov_x, bool transparancy)
    {
      Vec3 pos = {0.0, 3.0, 5.0};
      Vec3 dir = {0.0, -0.3, -1.0};
      Vec3 light = {20.0, 20.0, 50.0};
      SceneP scene = SceneBuilder()
          .with_camera(make_shared<Camera>(pos, dir))
          .with_light_source(light, 0.5)
          .with_screen(res_x, res_y, fov_x)
          .build();

      // Make platforms
      precision_t size = 3.0;
      precision_t height = 2.0;
      MeshBuilder root;
      shapes::make_box(root)
        .scale(size, height, size)
        .translate({0, height/2, 0})
        .with_color(RED)
        .with_opacity(!transparancy);
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    SceneP complex_pots(uint res_x, uint res_y, precision_t fov_x, bool transparancy)
    {
      Vec3 pos = {0.0, 6.0, 6.0};
      Vec3 dir = {0.0, -1.0, -1.0};
      Vec3 light = {20.0, 20.0, 50.0};
      SceneP scene = SceneBuilder()
          .with_camera(make_shared<Camera>(pos, dir))
          .with_light_source(light, 0.5)
          .with_screen(res_x, res_y, fov_x)
          .build();

      // Make platforms
      vector<Vec3> offsets = {{
        {-4.0, 0, 0},
        { 0.0, 0, 0},
        { 4.0, 0, 0},
      }};
      vector<Color> colors = {RED, YELLOW, BLUE};

      precision_t box_size = 1.5;
      precision_t box_height = 3.0;
      precision_t pot_height = 3.0;
      shapes::PotFactory pot_builder = shapes::PotFactory(40, 40)
        .with_height(pot_height)
        .with_profile(&shapes::PotFactory::profile1);
      MeshBuilder root;
      int k = 0;
      for (auto offset : offsets)
      {
        pot_builder.make(*root.add_child())
          .with_opacity(!transparancy)
          .with_color(colors[k])
          .translate(offset);
        shapes::make_box(*root.add_child())
          .with_opacity(true)
          .with_color(RED)
          .scale(box_size, box_height, box_size)
          .translate(offset)
          .translate({0, box_height/2, -4.0});
        k++;
      }
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    SceneP complex_pots2(uint res_x, uint res_y, precision_t fov_x)
    {
      Vec3 pos = {0.0, 6.0, 6.0};
      Vec3 dir = {0.0, -1.0, -1.0};
      Vec3 light = {10.0, 10.0, 0.0};
      SceneP scene = SceneBuilder()
          .with_camera(make_shared<Camera>(pos, dir))
          .with_light_source(light, 0.5)
          .with_screen(res_x, res_y, fov_x)
          .build();

      // There are three columns of items
      // In each column, (from furthest to closest), there is a transparent pot, a solid box, and then a transparent pot

      vector<Vec3> x_offsets = {{
        {-4.0, 0, 0},
        { 0.0, 0, 0},
        { 4.0, 0, 0},
      }};

      vector<Vec3> z_offsets = {{
        { 0.0, 0, -8.0},
        { 0.0, 0, -4.0},
        { 0.0, 0,  0.0},
      }};
      vector<Color> colors = {RED, YELLOW, BLUE};

      precision_t box_size = 1.5;
      precision_t box_height = 2.0;
      precision_t pot_height = 3.0;
      const Vec3 pot_center = {0.0, 0.0, 0.0};
      shapes::PotFactory pot_builder = shapes::PotFactory(40, 40)
        .with_height(pot_height)
        .with_profile(&shapes::PotFactory::profile1);

      MeshBuilder root;

      int k = 0;
      for (auto x_off: x_offsets)
      {
        pot_builder.make(*root.add_child())
          .with_opacity(false)
          .with_color(colors[k])
          .translate(pot_center + x_off + z_offsets[0]);
        shapes::make_box(*root.add_child())
          .with_opacity(true)
          .with_color(colors[(k + 1)%3])
          .scale(box_size, box_height, box_size)
          .translate(x_off + z_offsets[1])
          .translate({0, box_height/2, 0});
        pot_builder.make(*root.add_child())
          .with_opacity(false)
          .with_color(colors[(k + 2)%3])
          .translate(pot_center + x_off + z_offsets[2]);
        k++;
      }
      shapes::make_floor(-8.0, 8.0, -10.0, 2.0, 0.0, PURPLE, WHITE, *root.add_child());
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    SceneP broken_interpolation(uint res_x, uint res_y, precision_t fov_x)
    {
      // This scene used to render incorrectly, but it's since been fixed.
      Vec3 pos = {0.0, 6.0, 6.0};
      Vec3 dir = {0.0, -1.0, -1.0};
      Vec3 light = {10.0, 10.0, 0.0};
      SceneP scene = SceneBuilder()
          .with_camera(make_shared<Camera>(pos, dir))
          .with_light_source(light, 0.5)
          .with_screen(res_x, res_y, fov_x)
          .build();
      MeshBuilder root;
      shapes::make_quad({+8.0, 0.0,  2.0}, {+8.0, 0.0, -10.0}, {-8.0, 0.0,  2.0}, {-8.0, 0.0, -10.0}, root);
      root.vertices[0]->with_color(PURPLE);
      root.vertices[1]->with_color(PURPLE);
      root.vertices[2]->with_color(WHITE);
      root.vertices[3]->with_color(WHITE);
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    SceneP panels(uint res_x, uint res_y, precision_t fov_x, bool transparancy)
    {
      Vec3 pos = {0.0, 0.0, 6.0};
      Vec3 dir = {0.0, 0.0, -1.0};
      Vec3 light = {20.0, 20.0, 50.0};
      SceneP scene = SceneBuilder()
          .with_camera(make_shared<Camera>(pos, dir))
          .with_light_source(light, 0.5)
          .with_screen(res_x, res_y, fov_x)
          .build();

      precision_t x1 = -6.0;
      precision_t x2 = -4.0;
      precision_t y1 = 0.0;
      precision_t y2 = 6.0;
      MeshBuilder root;
      shapes::make_quad({x1, y1, 0}, {x1, y2, 0}, {x2, y1, 0}, {x2, y2, 0}, *root.add_child())
        .with_color(RED);

      x1 = -1.0;
      x2 =  1.0;
      shapes::make_quad({x1, y1, 0}, {x1, y2, 0}, {x2, y1, 0}, {x2, y2, 0}, *root.add_child())
        .with_color(YELLOW);

      x1 = 6.0;
      x2 = 4.0;
      shapes::make_quad({x1, y1, 0}, {x1, y2, 0}, {x2, y1, 0}, {x2, y2, 0}, *root.add_child())
        .with_color(BLUE);

      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    SceneP icosahedron(uint res_x, uint res_y, precision_t fov_x)
    {
      Vec3 pos = {0.0, 0.0, 2.0};
      Vec3 dir = {0.0, 0.0, -1.0};
      Vec3 light = {20.0, 20.0, 10.0};
      SceneP scene = SceneBuilder()
        .with_camera(make_shared<Camera>(pos, dir))
        .with_light_source(light, 0.5)
        .with_screen(res_x, res_y, fov_x)
        .build();
      MeshBuilder root;
      shapes::make_icosahedron(root)
        .with_color(YELLOW);
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    SceneP sphere(uint res_x, uint res_y, precision_t fov_x)
    {
      Vec3 pos = {0.0, 0.0, 2.0};
      Vec3 dir = {0.0, 0.0, -1.0};
      Vec3 light = {20.0, 20.0, 10.0};
      SceneP scene = SceneBuilder()
        .with_camera(make_shared<Camera>(pos, dir))
        .with_light_source(light, 0.5)
        .with_screen(res_x, res_y, fov_x)
        .build();
      MeshBuilder root;
      shapes::make_sphere(6, root)
        .with_color(YELLOW);
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    SceneP tree1(uint res_x, uint res_y, precision_t fov_x, uint depth)
    {
      Vec3 pos = {0.0, 5.0, -20.0};
      Vec3 dir = {0.0, -0.3, 1.0};
      Vec3 light = {0.0, 10.0, -1.0};
      SceneP scene = SceneBuilder()
        .with_camera(make_shared<Camera>(pos, dir))
        .with_light_source(light, 0.5)
        .with_screen(res_x, res_y, fov_x)
        .build();
      using namespace shapes;
      TreeBuilder gen {};
      gen.with_height(8.0)
        .with_sampling_rate(0.4)
        .with_color_jitter(10)
        .with_vertex_jitter(
          TreeBuilder::VertexJitterMethod::xz,
          TreeBuilder::VertexJitterDist::uniform,
          0.09)
        .with_depth(depth);

      MeshBuilder root;
      make_floor(-10.0, 10.0, -10.0, 10.0, -0.1, GREEN, GREEN, *root.add_child());
      gen.generate(*root.add_child());
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    SceneP house1(uint res_x, uint res_y, precision_t fov_x)
    {
      Vec3 pos = {0.0, 10.0, 20.0};
      Vec3 dir = {0.0, -0.3, -1.0};
      Vec3 light = {0.0, 10.0, -1.0};
      SceneP scene = SceneBuilder()
        .with_camera(make_shared<Camera>(pos, dir))
        .with_light_source(light, 0.5)
        .with_screen(res_x, res_y, fov_x)
        .build();
      shapes::HouseBuilder builder;
      MeshBuilder root;
      shapes::make_floor(-20.0, 20.0, -20.0, 20.0, -0.1, GREEN, GREEN, *root.add_child());
      builder.generate(*root.add_child());
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    SceneP landscape1(uint res_x, uint res_y, precision_t fov_x)
    {
      Vec3 pos = {0.0, 3.0, 0.0};
      Vec3 dir = {0.0, 0.0, -1.0};
      Vec3 light = {20.0, 20.0, -30.0};
      shared_ptr<Scene> scene = SceneBuilder()
          .with_camera(make_shared<Camera>(pos, dir))
          .with_light_source(light, 0.2)
          .with_screen(res_x, res_y, fov_x)
          .with_background(BLUE)
          .build();
      shapes::LandscapeBuilder builder;
      MeshBuilder root;
      builder.generate(*root.add_child());
      // Add a sphere for the sun
      shapes::make_sphere(3, *root.add_child())
        .scale(2.0)
        .translate(light)
        .with_color(YELLOW)
        .with_illuminated(true)
        ;
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

    /**
     * This scene is used to benchmark performance.
     *
     * It is a landscape with several features we would like to test optimization for:
     *  - High resolution.
     *  - Many objects behind the camera which should be occluded.
     *  - Many objects in front of camera but offscreen which should not be rendered.
     *  - Many objects occluded behind hills and trees.
     */
    SceneP landscape2(uint res_x, uint res_y, precision_t fov_x, unsigned seed)
    {
      Vec3 pos = {0.0, 3.0, 0.0};
      Vec3 dir = {0.0, 0.0, -1.0};
      Vec3 light = {20.0, 20.0, -30.0};
      shared_ptr<Scene> scene = SceneBuilder()
          .with_camera(make_shared<Camera>(pos, dir))
          .with_light_source(light, 0.2)
          .with_screen(res_x, res_y, fov_x)
          .with_background(BLUE)
          .build();
      shapes::LandscapeBuilder builder { seed };
      shapes::TreeBuilder tree_gen { seed };

      MeshBuilder root;
      builder.size = 80.0;
      builder.generate(*root.add_child());

      // Add a sphere for the sun
      shapes::make_sphere(3, *root.add_child())
        .scale(2.0)
        .translate(light)
        .with_color(YELLOW)
        .with_illuminated(true)
        ;

      // Add some "bio-domes" to test stacked transparency.
      shapes::make_sphere(6, *root.add_child())
        .scale(40.0)
        .translate({0.0, 0.0, -2.0})
        .with_color({250, 250, 250})
        .with_opacity(false)
        ;
      shapes::make_sphere(6, *root.add_child())
        .scale(4.0)
        .translate({0.0, 0.0, -8.0})
        .with_color({250, 250, 250})
        .with_opacity(false)
        ;
      shapes::make_sphere(6, *root.add_child())
        .scale(4.0)
        .translate({0.0, 0.0, -20.0})
        .with_color({250, 250, 250})
        .with_opacity(false)
        ;
      scene->set_root_object(unique_ptr<StaticMesh>(root.to_static_mesh()));
      return scene;
    }

  }
}
