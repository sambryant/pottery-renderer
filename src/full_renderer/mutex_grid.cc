#include "mutex_grid.h"

#include <vector>
#include <algorithm>
#include <iostream>

#include "base.h"
#include <array>
#include <vector>
#include <cassert>
#include <limits>
#include <thread>
#include <mutex>

using namespace std;

using namespace full_renderer;

void MutexGrid::reserve(int x, int y, MutexGrid::MutexWrapper &lock, int *valid_until)
{
  int x_ind = x / this->col_size;
  int y_ind = y / this->row_size;
  mutex* m = this->mutexes[y_ind * this->num_cols + x_ind];
  lock.set_mutex(m);
  *valid_until = (x_ind + 1) * this->col_size;
}

