#ifndef FULL_RENDERER_FAST_RASTER
#define FULL_RENDERER_FAST_RASTER

#include "base.h"

#include <array>
#include <vector>
#include <cassert>
#include <limits>
#include <cstring>

#include "../core/color.h"
#include "../core/pmath.h"
#include "../core/pixmap.h"
#include "../core/static_mesh.h"
#include "../core/vertex.h"
#include "../scene/screen.h"

#include "mutex_grid.h"
#include "raster.h"

namespace full_renderer {

using fast_float_t = signed int;

using FastVec3 = std::array<fast_float_t, 3>;

inline fast_float_t float_to_fast_float(precision_t v)
{
  return (fast_float_t) (v * 65536);
}

inline FastVec3 vec_to_fast_vec(const Vec3 &v)
{
  return {full_renderer::float_to_fast_float(v[0]), full_renderer::float_to_fast_float(v[1]), full_renderer::float_to_fast_float(v[2])};
}

inline float fast_float_to_float(fast_float_t l)
{
  float frac = ((float) (l & 65535)) / 65536;
  float whole = (float) (l >> 16);
  return whole + frac;
}

inline uint8_t fast_float_to_uint8(fast_float_t l)
{
  return (uint8_t) ((l >> 16) & 255);
}

struct FastFragment {
  uint8_t color[3];
  bool opaque;
  z_distance_t z;
public:
  FastFragment(const fast_float_t *color, bool opaque_, z_distance_t z_) : opaque(opaque_), z(z_) {
    this->color[0] = fast_float_to_uint8(color[0]);
    this->color[1] = fast_float_to_uint8(color[1]);
    this->color[2] = fast_float_to_uint8(color[2]);
  }
};

/**
 * See raster.h
 *
 * This is the exact same thing, except instead of using floats, it uses floats encoded as ints with
 * last two bytes representing fractional part.
 */
class FastRaster {
public:
  const int width;
  const int height;
  bool using_multiple_threads;

  std::vector<std::vector<FastFragment>> data;
  std::vector<z_distance_t> closest_opaque;
  std::unique_ptr<MutexGrid> mutex;
public:
  FastRaster(const Screen &screen, bool using_multiple_threads_ = true) :
    width(screen.width),
    height(screen.height),
    using_multiple_threads(using_multiple_threads_)
  {
    this->mutex.reset(new MutexGrid(screen.width, screen.height));
    for (int i=0; i<this->height; i++) {
      for (int j=0; j<this->width; j++) {
        this->data.push_back(std::vector<FastFragment>());
        this->closest_opaque.push_back(std::numeric_limits<z_distance_t>::max());
      }
    }
  }
  ~FastRaster() {}

  inline void add_fragment(size_t index, const fast_float_t *colorl, bool opaque, fast_float_t zl)
  {
    z_distance_t z = fast_float_to_float(zl);
    if (opaque && z >= closest_opaque[index]) {
      return;
    }
    auto &dst = this->data[index];

    // TODO: This is linear search. Optimal when number of conflicting fragments is small. Because
    // we auto clip fragments hidden by opaque textures, this should usually be fine. However, to
    // render scenes with many layered transparencies, we probably want to do binary search.
    auto it = dst.begin();
    while (it < dst.end()) {
      if (z < it->z) {
        break;
      }
      it++;
    }
    dst.emplace(it, colorl, opaque, z);
    if (opaque) {
      this->closest_opaque[index] = z;
    }
  }

  // Rasterizes a triangle by computing its pixel fragments.
  void rasterize_triangle(const STriangle *tri);

  // Renders a raster to a pixel buffer.
  void render_to(core::PixMap &pixmap);

  /**
   * Draws the fragment buffer to the pixel map for a subset of the rows in the pixel map.
   *
   * Specifically, it operates on rows with stride `y_stride` starting at `y0`, e.g. the rows
   *    `y0`, `y0 + y_stride`, `y0 + 2 * y_stride`, ...
   * This is used to render in a multithreaded environment where each thread handles a different
   * offset row value `y0`.
   */
  void render_to_partial(core::PixMap &pixmap, size_t y0, size_t y_stride);
};

}

#endif
