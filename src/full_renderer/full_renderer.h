/**
 * High-level implementation of raster/render loop. This class owns all of the components necessary
 * to render a scene including the:
 *  - `Scene` - holds all of the objects, defines the camera orientation/position, and viewport.
 *  - `Engine` - handles computing vertex projections into 2D and calculating shading.
 *  - `Raster` - performs low-level rasterization math by drawing triangles to fragment buffers.
 *  - `PixMap` - holds the final pixel output of the rendering loop.
 *
 * This class does not perform any of the mathematical operations directly.
 */
#ifndef SCAN_RENDERER
#define SCAN_RENDERER

#include "base.h"

#include <exception>
#include <array>
#include <algorithm>
#include <list>
#include <memory>
#include <vector>
#include <cstdlib>

#include "../scene/scene.h"
#include "../scene/screen.h"
#include "../core/pmath.h"
#include "../core/color.h"
#include "../core/pixmap.h"
#include "../core/renderer.h"
#include "../core/static_mesh.h"

#include "raster.h"
#include "fast_raster.h"
#include "raster_thread_queue.h"

namespace full_renderer
{

class Renderer : public core::BaseRenderer
{
protected:
  std::shared_ptr<Scene> scene;
  std::unique_ptr<core::PixMap> pixmap;
  std::unique_ptr<RasterThreadQueue> raster_thread_queue;
  RasterT* raster;
  const uint num_threads;
  const bool multithreaded;
public:
  Renderer(std::shared_ptr<Scene> scene_, uint num_threads_ = DEFAULT_NUM_THREADS)
    : scene(scene_),
      num_threads( num_threads_ == 0 ? 1 : num_threads_),
      multithreaded(num_threads > 1)
  {
    this->pixmap.reset(new core::PixMap(this->scene->screen.width, this->scene->screen.height));
    this->pixmap->set_background(this->scene->background);
    this->raster = new RasterT(this->scene->screen, num_threads > 1);
    size_t num_objects = this->scene->get_num_triangles();
    this->raster_thread_queue.reset(new RasterThreadQueue(num_threads, num_objects));
  }
  ~Renderer() {
    delete this->raster;
  }
  Renderer() = delete;
  Renderer(const Renderer& rhs) = delete;
  Renderer& operator=(const Renderer&) = delete;
public:
  SceneP get_scene() const { return scene; }
  core::PixMap &get_pixmap() const { return *this->pixmap; }
  void render();
  void render_perf(Performance &perf);
protected:
  void test_report_objects(const StaticMesh* mesh) const;

  /**
   * Computes the projection of each vertex onto the screen.
   */
  void project_objects();

  /**
   * Computes shading for each vertex.
   */
  void compute_shading();

  /**
   * Rasterizes object mesh to fragments.
   */
  void rasterize();

  /**
   * Recursively rasterizes object mesh to fragments - only used in single-threaded contexts.
   */
  void rasterize_mesh(StaticMesh* obj);

  /**
   * Draws fragments from rasterization to the pixel buffer.
   */
  void draw();
};

}


#endif
