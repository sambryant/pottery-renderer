#ifndef FULL_RENDERER_RASTER_THREAD_QUEUE
#define FULL_RENDERER_RASTER_THREAD_QUEUE

#include "base.h"

#include <exception>
#include <array>
#include <list>
#include <vector>

#include "../scene/scene.h"
#include "../core/pmath.h"
#include "../core/static_mesh.h"

#include "raster.h"
#include "fast_raster.h"

namespace full_renderer
{

class RasterThreadQueue
{
public:
  const uint num_threads;
protected:
  size_t num_objects_per_thread;
  std::vector<std::vector<STriangle*>> thread_work_queue;
  size_t cur_thread_index;
  size_t cur_thread_size;
public:
  RasterThreadQueue(uint num_threads_, size_t num_objects) :
    num_threads(num_threads_)
  {
    this->set_num_valid_objects(num_objects);
    for (uint i=0; i<num_threads; i++) {
      this->thread_work_queue.emplace_back();
      this->thread_work_queue.back().reserve(this->num_objects_per_thread);
    }
  }
  void set_num_valid_objects(size_t num_objects);
  void clear();
  void rasterize(RasterT *raster);
  void build_queue(StaticMesh* obj);
};

}

#endif
