
Fragment constructor:
  stdcpy      : 97 (total = 20494732), (rater = 13654683)
  forloop     : 95 (total = 21004894), (raster = 14182511)
  unroll      : 99 (total = 20158736), (raster = 1338581)
render_pixel_inner (transparent on):
  for loop : 99 (render total = 3694830)     **
  std::copy: 93
  unrolled : 97 (total = 20491281) (render total = 3888381)
  ro/it ref: 98 (total = 20388895) (render total = 3862319)
  unroll full: 97 (render = 3870357)
  unroll part: 98 (render total = 3766284)

z distance:
  double                   : 98  (raster = 13565586), (render = 3774720)
  float                    : 107 (raster = 12845490), (render = 3278991)
  raster double, frag float: 106 (raster = 13049068), (render = 3274280)



multi-threading

base:
  32*32 mutexes
  6 threads
  work split by object.
  results: FPS: 117, 126, 118

thread-queue
  32*32 mutexes
  6 threads
  work split by object.
  results: FPS: 125, 126, 125
