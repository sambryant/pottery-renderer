/**
 * Explaination of code.
 *
 * At the time of writing this (23/06/01), this works just like `raster.cc`. However, all float
 * operations are avoided within rasterization loop. This is accomplished by converting all floats
 * to `fast_float`'s, which are stored as ints. A fast float stores the whole part of the float in
 * the first two bytes and the fractional part of the float in the last two bytes. Ultimately, the
 * rasterization only cares about whole numbers (e.g. which pixel, what 255 color, etc) and relative
 * z-distances (which can be compared directly in fast float form). This means that it is very
 * efficient to convert a fast float into its final needed value.
 *
 * However, the reduced precision leads to rendering artifacts and the code is somewhat arcane.
 */
#include "raster.h"
#include "fast_raster.h"

#include <vector>
#include <algorithm>
#include <iostream>

#include "base.h"
#include <array>
#include <vector>
#include <cassert>
#include <limits>

#include "../core/color.h"
#include "../core/pmath.h"
#include "../core/static_mesh.h"
#include "../core/vertex.h"
#include "../scene/screen.h"

#include "mutex_grid.h"

using namespace std;

using namespace full_renderer;

inline void fast_render_compute_abc(
  const Vec2 &a, const Vec2 &b, const Vec2 &c, const Vec2 &p,
  Vec3 &abc, Vec3 &dabc_dx, Vec3 &dabc_dy)
{
  Vec2 ba = b - a;
  Vec2 ca = c - a;
  Vec2 pa = p - a;
  precision_t denom = ba[1]*ca[0] - ba[0]*ca[1];
  abc[1] = (ca[0]*pa[1] - ca[1]*pa[0]) / denom; // beta
  abc[2] = (ba[1]*pa[0] - ba[0]*pa[1]) / denom; // gamma
  abc[0] = 1 - abc[1] - abc[2];

  // Compute derivatives of barycentric coordinates.
  dabc_dx[1] = - ca[1] / denom;
  dabc_dx[2] = + ba[1] / denom;
  dabc_dx[0] = - dabc_dx[1] - dabc_dx[2];
  dabc_dy[1] = + ca[0] / denom;
  dabc_dy[2] = - ba[0] / denom;
  dabc_dy[0] = - dabc_dy[1] - dabc_dy[2];
}

inline void fast_render_pixel_inner(vector<FastFragment> &fragments, vector<uint8_t>::iterator pixel_it)
{
  int i = fragments.size() - 1;
  while (i >= 0)
  {
    FastFragment& fragment = fragments.back();
    if (fragment.opaque) {
      for (int j=0; j<3; j++) {
        pixel_it[j] = fragment.color[j];
      }
    } else {
      // Note (uint8_t + uint8_t)/2 might look dangerous - in terms of overflow, but the math is fine.
      // because of how uint8_t works, (255 + 255)/2 = 255.
      for (int j=0; j<3; j++) {
        *(pixel_it + j) = ((fragment.color[j] + *(pixel_it + j))/2);
      }
    }
    // Clear fragment array as we process it: **much** faster than clearing in one go.
    fragments.pop_back();
    i--;
  }
}

void FastRaster::render_to(core::PixMap &pixmap)
{
  const auto max_distance = std::numeric_limits<z_distance_t>::max();

  auto pixel_it = pixmap.pixels.begin();
  size_t index = 0;
  for (auto &fragments: this->data) {
    fast_render_pixel_inner(fragments, pixel_it);
    this->closest_opaque[index] = max_distance;
    pixel_it += 4;
    index++;
  }
}

void FastRaster::render_to_partial(core::PixMap &pixmap, size_t y0, size_t y_stride)
{
  // Value we reset closest_opaque to for next raster cycle.
  const auto max_distance = std::numeric_limits<z_distance_t>::max();

  auto pixel_it = pixmap.pixels.begin() + 4 * (y0 * this->width);
  size_t index = y0 * this->width;

  // Since we are rendering with a stride, after each row, update indices by length * (stride - 1)
  size_t row_pixel_increment = 4 * (this->width * (y_stride - 1));
  size_t row_index_increment = (this->width * (y_stride - 1));
  for (int y = y0; y < this->height; y += y_stride) {
    for (int x = 0; x < this->width; x++) {
      vector<FastFragment> &fragments = this->data[index];
      fast_render_pixel_inner(fragments, pixel_it);
      this->closest_opaque[index] = max_distance;
      index++;
      pixel_it += 4;
    }
    index += row_index_increment;
    pixel_it += row_pixel_increment;
  }
}

inline void full_renderer_fast_raster_rasterize_triangle_inner(
  FastRaster* raster,
  const bool multi_threaded,
  const bool opaque,
  const int xmin,
  const int xmax,
  const int ymin,
  const int ymax,
  const FastVec3 abc0,
  const FastVec3 dabc_dx,
  const FastVec3 dabc_dy,
  const fast_float_t z0,
  const fast_float_t dz_dx,
  const fast_float_t dz_dy,
  const FastVec3 color0,
  const FastVec3 dcolor_dx,
  const FastVec3 dcolor_dy
)
{
  auto abc_row = abc0;
  auto clr_row = color0;
  auto z_row = z0;
  MutexGrid::MutexWrapper lock;
  int lock_valid_until;
  for (int y = ymin; y <= ymax; y++) {
    // These are the abc, color, and z values which are updated each column. They are incremented by
    // their respective dx amounts.
    auto abc = abc_row;
    auto clr = clr_row;
    auto z = z_row;
    auto index = (y * raster->width + xmin);

    // Discover true xmin
    int xmin_act = xmin;
    int skip_x = 0;
    for (int i=0; i<3; i++) {
      if (abc[i] < 0) {
        if (dabc_dx[i] <= 0) {
          skip_x = (xmax - xmin);
          break;
        }
        else
          skip_x = max(skip_x, (int) ((-abc[i] + dabc_dx[i] - 1) / dabc_dx[i]));
      }
    }
    if (skip_x > 0) {
      z += dz_dx * skip_x;
      for (int i=0; i<3; i++) {
        abc[i] += skip_x * dabc_dx[i];
        clr[i] += skip_x * dcolor_dx[i];
      }
      xmin_act += skip_x;
      index += skip_x;
    }

    if (xmin_act <= xmax) {

      // Lock mutex protecting the target raster area
      if (multi_threaded)
        raster->mutex->reserve(xmin_act, y, lock, &lock_valid_until);
      else
        lock_valid_until = -1;
      for (int x = xmin_act; x <= xmax; x++) {
        if (x == lock_valid_until)
          raster->mutex->reserve(x, y, lock, &lock_valid_until);
        if (abc[0] >= 0 && abc[1] >= 0 && abc[2] >= 0)
          raster->add_fragment(index, &(clr[0]), opaque, z);
        else
          break;
        z += dz_dx;
        for (int i=0; i<3; i++) {
          abc[i] = abc[i] + dabc_dx[i];
        }
        for (int i=0; i<3; i++) {
          clr[i] = clr[i] + dcolor_dx[i];
        }
        index++;
      }
      for (int i=0; i<3; i++) {
        abc_row[i] += dabc_dy[i];
        clr_row[i] += dcolor_dy[i];
      }
      z_row += dz_dy;

    }
  }
}

void FastRaster::rasterize_triangle(const STriangle *tri)
{
  const Vertex &a = *(tri->v[0]);
  const Vertex &b = *(tri->v[1]);
  const Vertex &c = *(tri->v[2]);

  // Compute the shaded color at each vertex.
  const Color color_a = shade_color(a.color, tri->shading[0]);
  const Color color_b = shade_color(b.color, tri->shading[1]);
  const Color color_c = shade_color(c.color, tri->shading[2]);

  int xmin = this->width - 1;
  int xmax = 0;
  int ymin = this->height - 1;
  int ymax = 0;
  for (int i=0; i<3; i++) {
    xmin = min((int) ceil(tri->v[i]->v_screen[0]), xmin);
    xmax = max((int) floor(tri->v[i]->v_screen[0]), xmax);
    ymin = min((int) ceil(tri->v[i]->v_screen[1]), ymin);
    ymax = max((int) floor(tri->v[i]->v_screen[1]), ymax);
  }
  xmin = max(xmin, 0);
  xmax = min(xmax, this->width - 1);
  ymin = max(ymin, 0);
  ymax = min(ymax, this->height - 1);

  // Compute alpha, beta, gamma at the (xmin, ymin) corner
  // Compute the change in alpha, beta, gamma for each pixel movement in x and y
  Vec3 abc0;
  Vec3 dabc_dx;
  Vec3 dabc_dy;
  z_distance_t dz_dx;
  z_distance_t dz_dy;
  z_distance_t z0;
  Vec3 color0;
  Vec3 dcolor_dx;
  Vec3 dcolor_dy;
  {
    Vec2 p = {(precision_t) xmin, (precision_t) ymin};
    fast_render_compute_abc(a.v_screen, b.v_screen, c.v_screen, p, abc0, dabc_dx, dabc_dy);

    z0 = abc0[0] * a.z + abc0[1] * b.z + abc0[2] * c.z;
    dz_dx = a.z * dabc_dx[0] + b.z * dabc_dx[1] + c.z * dabc_dx[2];
    dz_dy = a.z * dabc_dy[0] + b.z * dabc_dy[1] + c.z * dabc_dy[2];

    for (int i=0; i<3; i++) {
      color0[i] = abc0[0]*color_a[i] + abc0[1]*color_b[i] + abc0[2]*color_c[i];
      dcolor_dx[i] = dabc_dx[0]*color_a[i] + dabc_dx[1]*color_b[i] + dabc_dx[2]*color_c[i];
      dcolor_dy[i] = dabc_dy[0]*color_a[i] + dabc_dy[1]*color_b[i] + dabc_dy[2]*color_c[i];
    }
  }

  // Convert all data into fast floats.
  FastVec3 iabc0 = vec_to_fast_vec(abc0);
  FastVec3 idabc_dx = vec_to_fast_vec(dabc_dx);
  FastVec3 idabc_dy = vec_to_fast_vec(dabc_dy);

  fast_float_t iz0 = float_to_fast_float(z0);
  fast_float_t idz_dx = float_to_fast_float(dz_dx);
  fast_float_t idz_dy = float_to_fast_float(dz_dy);
  FastVec3 icolor0 = vec_to_fast_vec(color0);
  FastVec3 idcolor_dx = vec_to_fast_vec(dcolor_dx);
  FastVec3 idcolor_dy = vec_to_fast_vec(dcolor_dy);

  // Call triangle rasterization code.
  full_renderer_fast_raster_rasterize_triangle_inner(
    this,
    this->using_multiple_threads,
    tri->opaque,
    xmin,
    xmax,
    ymin,
    ymax,
    iabc0,
    idabc_dx,
    idabc_dy,
    iz0,
    idz_dx,
    idz_dy,
    icolor0,
    idcolor_dx,
    idcolor_dy);
}
