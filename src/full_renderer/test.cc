#include "test.h"

#include <vector>
#include <chrono>
#include <memory>
#include <thread>

#include "../core/color.h"
#include "../core/pmath.h"
#include "../core/output.h"
#include "../examples/scenes.h"
#include "../performance.h"

#include "full_renderer.h"
#include "base.h"

using namespace std;
using namespace full_renderer;

namespace full_renderer::tests
{

void run_test(string output, SceneP scene, uint num_threads = DEFAULT_NUM_THREADS)
{
  cout << "Running full renderer test: " << output << "\n";

  Renderer renderer(scene, num_threads);
  auto start = std::chrono::high_resolution_clock::now();
  renderer.render();
  auto end = std::chrono::high_resolution_clock::now();
  auto duration = chrono::duration_cast<chrono::milliseconds>(end - start);
  core::save_render(renderer.get_pixmap(), output);
  cout << "\tFinished rendering in " << duration.count() << "ms\n";
}

void run_performance_test(string output, SceneP scene, uint num_trials, uint num_threads)
{
  cout << "\nRunning full renderer performance test\n";

  Renderer renderer(scene, num_threads);

  Performance perf;
  cout << "\tResolution  : " << renderer.get_scene()->screen.width << " x " << renderer.get_scene()->screen.height << '\n'
       << "\t# Triangles : " << renderer.get_scene()->get_num_triangles() << '\n'
       << "\tMax depth   : " << renderer.get_scene()->get_max_depth() << '\n'
       << "\t# Trials    : " << num_trials << '\n'
       << "\t# Threads   : " << num_threads << '\n';

  long int total_time = 0;
  for (uint i=0; i<num_trials; i++) {
    auto start = std::chrono::high_resolution_clock::now();
    renderer.render_perf(perf);
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::microseconds>(end - start);
    total_time += duration.count();
  }
  core::save_render(renderer.get_pixmap(), output);
  cout << "Finished performance test\n";
  cout << "\tnumber trials  : " << num_trials << '\n';
  cout << "\ttotal time (us): " << total_time << '\n';
  cout << "\ttime/frame     : " << (total_time / num_trials) << '\n';
  cout << "\tframerate      : " << (1000000 / (total_time / num_trials)) << '\n';
  cout << "\nPerformance breakdown\n";
  perf.report(num_trials);
}

void test_0()
{
  run_test("output/SR-simplest.ppm", examples::scenes::triangle_simple());
}

void test_ordering()
{
  run_test("output/SR-ordering.ppm", examples::scenes::triangles_ordered());
}

void test_1()
{
  run_test("output/SR-tetrahedron.ppm", examples::scenes::tetrahedrons());
}

void test_2()
{
  run_test("output/SR-pot-scene.ppm", examples::scenes::complex_pots(600, 400, 90.0, false));
}

void test_3()
{
  run_test("output/SR-pot-transparent.ppm", examples::scenes::complex_pots(600, 400, 90.0, true));
}

void test_panels(unsigned int resolution=400)
{
  run_test("output/SR-panels.ppm", examples::scenes::panels());
}

void test_4_transparency()
{
  run_test("output/SR-cube-transparent.ppm", examples::scenes::cube(800, 600, 90.0, true));
}

void test_5_pot_scene_2()
{
  run_test("output/SR-pot-scene-2.ppm", examples::scenes::complex_pots2(1000, 1000, 90.0));
}

void test_6_color_interpolation()
{
  run_test("output/interpolation/SR-color-interpolation.ppm", examples::scenes::broken_interpolation(1000, 1000, 90.0), 1);
}

void test_7_tree1()
{
  run_test("output/trees/tree-2.ppm", examples::scenes::tree1(1000, 1000), 6);
}

void test_8_house1()
{
  run_test("output/house-1.ppm", examples::scenes::house1(1000, 1000), 6);
}

void test_9_perpendicular_surfaces()
{
  run_test("output/test-9-perpendicular.ppm", examples::scenes::perpendicular_surfaces(1000, 1000), 6);
}

void test_10_icosahedron()
{
  run_test("output/test-10-icosahedron.ppm", examples::scenes::icosahedron(1000, 1000), 6);
}

void test_11_sphere() {
  run_test("output/test-11-sphere.ppm", examples::scenes::sphere(1000, 1000), 6);
}

void test_12_landscape() {
  run_test("output/test-12-landscape.ppm", examples::scenes::landscape1(1000, 1000, 100.0), 6);
}

void test_13_landscape2() {
  auto scene = examples::scenes::landscape2(1280, 720, 100.0);
  run_test("output/SR-landscape2.ppm", scene, 1);
}

void test_performance_1()
{
  auto scene = examples::scenes::benchmark1();
  run_performance_test("output/SR-performance.ppm", scene, 2000, 6);
}

void test_performance_2()
{
  auto scene = examples::scenes::benchmark2();
  run_performance_test("output/SR-performance-2.ppm", scene, 200, 6);
}

}

using namespace full_renderer::tests;

void full_renderer::test() {
  // test_0();
  // test_1();
  // test_ordering();
  // test_2();
  // test_3();
  // test_panels();
  // test_4_transparency();
  // test_5_pot_scene_2();
  // test_6_color_interpolation();
  // test_7_tree1();
  // test_8_house1();
  // test_9_perpendicular_surfaces();
  // test_10_icosahedron();
  // test_11_sphere();
  // test_12_landscape();
  // test_13_landscape2();
  // test_performance_1();
  test_performance_2();
}

