#ifndef FULL_RENDERER_MUTEX_GRID
#define FULL_RENDERER_MUTEX_GRID

#include "base.h"

#include <array>
#include <vector>
#include <cassert>
#include <limits>
#include <cstring>
#include <thread>
#include <mutex>

namespace full_renderer {

class MutexGrid {
public:
  class MutexWrapper;
public:
  const int num_cols;
  const int num_rows;
  const int col_size;
  const int row_size;
protected:
  std::mutex** mutexes;
public:
  MutexGrid(int width, int height, int num_cols_ = DEFAULT_NUM_MUTEXES, int num_rows_ = DEFAULT_NUM_MUTEXES) :
    num_cols(num_cols_),
    num_rows(num_rows_),
    col_size((width + num_cols_ - 1)/ num_cols_),
    row_size((height + num_rows_ -1)/ num_rows_)
  {
    this->mutexes = (std::mutex**) malloc(sizeof(void *) * this->num_rows * this->num_cols);
    for (int i=0; i<this->num_rows; i++) {
      for (int j=0; j<this->num_cols; j++) {
        this->mutexes[i*this->num_cols + j] = new std::mutex();
      }
    }
  }
  ~MutexGrid() {
    for (int i=0; i<this->num_rows; i++) {
      for (int j=0; j<this->num_cols; j++) {
        delete this->mutexes[i*this->num_cols + j];
      }
    }
    free(this->mutexes);
  }
public:
  void reserve(int x, int y, MutexWrapper &lock, int *valid_until);
};

class MutexGrid::MutexWrapper {
private:
  std::mutex* mutex;
public:
  MutexWrapper()
  {
    this->mutex = NULL;
  }
  ~MutexWrapper()
  {
    this->release();
  }
  void set_mutex(std::mutex* m)
  {
    this->release();
    m->lock();
    this->mutex = m;
  }
  void release()
  {
    std::mutex* m = this->mutex;
    this->mutex = NULL;
    if (m != NULL) {
      m->unlock();
    }
  }
};

}

#endif
