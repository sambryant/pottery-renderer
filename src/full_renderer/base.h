#ifndef SCAN_RENDERER_BASE
#define SCAN_RENDERER_BASE

#include <array>
#include <thread>

#include "../core/pmath.h"
#include "../core/color.h"

namespace full_renderer {

  class Raster;

  class FastRaster;

  using RasterT = Raster;

  const uint DEFAULT_NUM_MUTEXES = 32;

  const uint DEFAULT_NUM_THREADS = std::thread::hardware_concurrency();

}

#endif
