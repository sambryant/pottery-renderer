/**
 * Class which handles low-level rasterization of triangles.
 *
 * In reality, this class performs two distinct operations:
 *  1) Rasterization - converting each triangle objects into pixel fragments
 *  2) Drawing - compositing all of the fragments at each pixel into a single color value.
 *
 * Both operate across multiple threads. This is more complicated for rasterization because it is
 * object-based (as opposed to pixel based), so it is inherently non-local. We use a `MutexGrid` to
 * deal with this complication which divides the screen into a grid, each protected by its own
 * mutex.
 *
 * This class is the critical-path. Its operations compose the bulk of the rendering time.
 * Therefore, it makes heavy use of inlined functions. The code in `raster.cc` is ugly and verbose
 * because it is optimized for efficiency.
 */
#ifndef SCAN_RENDERER_RASTER
#define SCAN_RENDERER_RASTER

#include "base.h"

#include <array>
#include <vector>
#include <cassert>
#include <limits>
#include <cstring>

#include "../core/color.h"
#include "../core/pmath.h"
#include "../core/pixmap.h"
#include "../core/static_mesh.h"
#include "../core/vertex.h"
#include "../scene/screen.h"

#include "mutex_grid.h"

namespace full_renderer {

using z_distance_t = float;

struct Fragment {
  uint8_t color[3];
  bool opaque;
  z_distance_t z;
public:
  Fragment(const Vec3 &color, bool opaque_, z_distance_t z_) : opaque(opaque_), z(z_) {
    this->color[0] = (uint8_t) color[0];
    this->color[1] = (uint8_t) color[1];
    this->color[2] = (uint8_t) color[2];
  }
  Fragment(z_distance_t z_) : opaque(true), z(z_) {
    this->color[0] = 0;
    this->color[1] = 0;
    this->color[2] = 0;
  }
};

class Raster {
public:
  const uint width;
  const uint height;
  bool using_multiple_threads;
  // List of fragments for each pixel in the scene.
  std::vector<std::vector<Fragment>> data;
  // The z-distance of the closest opaque texture for each pixel in the scene. This is used to clip
  // fragments which are hidden by opaque textures.
  std::vector<z_distance_t> closest_opaque;
  std::unique_ptr<MutexGrid> mutex;
public:
  Raster(const Screen &screen, bool using_multiple_threads_ = true) :
    width(screen.width),
    height(screen.height),
    using_multiple_threads(using_multiple_threads_)
  {
    this->mutex.reset(new MutexGrid(screen.width, screen.height));
    for (uint i=0; i<this->height; i++) {
      for (uint j=0; j<this->width; j++) {
        this->data.push_back(std::vector<Fragment>());
        this->closest_opaque.push_back(std::numeric_limits<z_distance_t>::max());
      }
    }
  }
  ~Raster() {}

  /**
   * Adds a fragment at given pixel index. If fragment is behind opaque texture, this does nothing.
   *
   * This is not really a public method, but it's called from an inlined function defined in raster.cc.
   */
  inline void add_fragment(size_t index, const Vec3 &color, bool opaque, z_distance_t z)
  {
    if (z >= closest_opaque[index]) {
      return;
    }
    auto &dst = this->data[index];

    // TODO: This is linear search. Optimal when number of conflicting fragments is small. Because
    // we auto clip fragments hidden by opaque textures, this should usually be fine. However, to
    // render scenes with many layered transparencies, we probably want to do binary search.
    auto it = dst.begin();
    while (it < dst.end()) {
      if (z < it->z) {
        break;
      }
      it++;
    }
    dst.emplace(it, color, opaque, z);
    if (opaque) {
      this->closest_opaque[index] = z;
    }
  }

  /**
   * Adds a fragment at given pixel index. If fragment is behind opaque texture, this does nothing.
   *
   * This is not really a public method, but it's called from an inlined function defined in raster.cc.
   */
  inline void add_outline_fragment(size_t index, z_distance_t z)
  {
    if (z >= closest_opaque[index]) {
      return;
    }
    auto &dst = this->data[index];

    // TODO: This is linear search. Optimal when number of conflicting fragments is small. Because
    // we auto clip fragments hidden by opaque textures, this should usually be fine. However, to
    // render scenes with many layered transparencies, we probably want to do binary search.
    auto it = dst.begin();
    while (it < dst.end()) {
      if (z < it->z) {
        break;
      }
      it++;
    }
    dst.emplace(it, z);
    this->closest_opaque[index] = z;
  }

  /**
   * Rasterizes a single triangle by computing its fragments via interpolation.
   */
  void rasterize_triangle(const STriangle *tri);

  /**
   * Draws the fragment buffer to the pixel map.
   */
  void render_to(core::PixMap &pixmap);

  /**
   * Draws the fragment buffer to the pixel map for a subset of the rows in the pixel map.
   *
   * Specifically, it operates on rows with stride `y_stride` starting at `y0`, e.g. the rows
   *    `y0`, `y0 + y_stride`, `y0 + 2 * y_stride`, ...
   * This is used to render in a multithreaded environment where each thread handles a different
   * offset row value `y0`.
   */
  void render_to_partial(core::PixMap &pixmap, size_t y0, size_t y_stride);
};

}

#endif
