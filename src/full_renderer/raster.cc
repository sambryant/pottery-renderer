#include "raster.h"
#include "base.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <iostream>
#include <limits>
#include <vector>

#include "../core/color.h"
#include "../core/pmath.h"
#include "../core/static_mesh.h"
#include "../core/vertex.h"
#include "../scene/screen.h"

#include "mutex_grid.h"

#define DRAW_OUTLINES false

using namespace std;

using namespace full_renderer;

/**
 * Collection of critical-path inlined functions relevant to Raster.
 */
namespace full_renderer::raster_util {

  inline void render_pixel_inner(vector<Fragment> &fragments, vector<uint8_t>::iterator pixel_it)
  {
    int i = fragments.size() - 1;
    while (i >= 0)
    {
      Fragment& fragment = fragments.back();
      if (fragment.opaque) {
        for (int j=0; j<3; j++) {
          pixel_it[j] = fragment.color[j];
        }
      } else {
        // Note (uint8_t + uint8_t)/2 might look dangerous - in terms of overflow, but the math is fine.
        // because of how uint8_t works, (255 + 255)/2 = 255.
        for (int j=0; j<3; j++) {
          *(pixel_it + j) = ((fragment.color[j] + *(pixel_it + j))/2);
        }
      }
      // Clear fragment array as we process it: **much** faster than clearing in one go.
      fragments.pop_back();
      i--;
    }
  }

  /**
   * Computes the barycentric coordinates of point `p` relative to the triangle (`a`,`b`,`c`).
   *
   * This is done by solving a system of (two) linear equations:
   *    p = a + bt * (b - a) + gm * (c - a).
   */
  inline void compute_abc(
    const Vec2 &a, const Vec2 &b, const Vec2 &c, const Vec2 &p,
    Vec3 &abc, Vec3 &dabc_dx, Vec3 &dabc_dy)
  {
    Vec2 ba = b - a;
    Vec2 ca = c - a;
    Vec2 pa = p - a;
    precision_t denom = ba[1]*ca[0] - ba[0]*ca[1];
    abc[1] = (ca[0]*pa[1] - ca[1]*pa[0]) / denom; // beta
    abc[2] = (ba[1]*pa[0] - ba[0]*pa[1]) / denom; // gamma
    abc[0] = 1 - abc[1] - abc[2];

    // Compute derivatives of barycentric coordinates.
    dabc_dx[1] = - ca[1] / denom;
    dabc_dx[2] = + ba[1] / denom;
    dabc_dx[0] = - dabc_dx[1] - dabc_dx[2];
    dabc_dy[1] = + ca[0] / denom;
    dabc_dy[2] = - ba[0] / denom;
    dabc_dy[0] = - dabc_dy[1] - dabc_dy[2];
  }

  /**
   * Determines what subset of x-pixels in the current row are contained in the triangle.
   * This is used to determine where we should start/stop rasterizing the triangle.
   *
   * `x_range` - the maximum possible x-range defined by the bounding box in form [xmin, xmax). This
   *    will be both read and written to by this function.
   * `act_start` - the offset from xmin where the row should start. This is only written to.
   * `abc_row` - the barycentric coordinates for the first pixel in the current row.
   * `dabc_dx` - the derivative of the barycentric coordinates with respect to the x-pixel.
   */
  inline void find_x_range_in_bounds(
    uint* x_range, // exclusive
    uint* act_start,
    const Vec3 &abc,
    const Vec3 &dabc)
  {
    uint first_pos = 0;
    uint first_neg = *x_range;
    for (int i=0; i<3; i++) {
      if (abc[i] < 0) {
        if (dabc[i] <= 0) {
          *x_range = 0;
          return;
        } else {
          first_pos = max(first_pos, (uint) ceil(- abc[i] / dabc[i]));
        }
      } else {
        if (dabc[i] >= 0) {
          continue;
        } else {
          double tmp = floor(-abc[i] / dabc[i]);
          if (tmp < 100000)
            first_neg = min(first_neg, (uint) tmp + 1);
        }
      }
    }
    if (first_pos >= first_neg) {
      *x_range = 0;
      *act_start = 0;
    } else {
      *act_start = first_pos;
      *x_range = first_neg - first_pos;
    }
  }

  /**
   * Performs rasterization of a triangle.
   *
   * How this works:
   *  - The rectangular bounds of the triangle are given by (xmin, xmax) x (ymin, ymax).
   *  - The value of alpha, beta, and gamma (the baryocentric coordinates) are given for (xmin, xmax).
   *  - For each attribute which must be interpolated, three quantities must be supplied:
   *     1. attr0 - the value of the attribute at the pixel (xmin, xmax)
   *     2. dattr_dx - the change in the attribute from one pixel column to the next.
   *     2. dattr_dy - the change in the attribute from one pixel row to the next.
   *  - As the triangle is drawn, the attributes at each pixel are quickly computed by incrementing by
   *    their respective derivatives.
   *
   * For example, consider the color attribute.
   *  - color0 gives the theoretical interpolated color at (xmin, xmax)
   *  - dcolor_dx indicates how the R, G, B channels each change as you increase x
   *  - dcolor_dy indicates how the R, G, B channels each change as you increase y.
   * Then for the ith row in the bounding box, the color starts at color0 + i * dcolor_dy. After each
   * column in that row is drawn, we increment that value by dcolor_dx.
   *
   * We can determine which pixels in the box are within the triangle by examining the baryocentric
   * coordinate attribute (`abc0`, `abc`, `dabc_dx`, etc). When any of its entries are less than 0,
   * the triangle is out of bounds.
   */
  inline void rasterize_triangle_inner(
    Raster*raster,
    const bool multi_threaded,
    const bool opaque,
    const uint xmin,
    const uint xmax,
    const uint ymin,
    const uint ymax,
    // Barycentric coordinate attributes
    const Vec3 abc0,
    const Vec3 dabc_dx,
    const Vec3 dabc_dy,
    // z-depth attributes
    const z_distance_t z0,
    const z_distance_t dz_dx,
    const z_distance_t dz_dy,
    // Color attributes.
    const Vec3 color0,
    const Vec3 dcolor_dx,
    const Vec3 dcolor_dy
  )
  {
    // Define row-attributes: values of each attribute at the start of each row. These must be
    // incremented at the end of the row loop.
    auto abc_row = abc0;
    auto clr_row = color0;
    auto z_row = z0;
    MutexGrid::MutexWrapper lock;
    int lock_valid_until;

    // Iterate through each row in the bounding box.
    for (uint y = ymin; y <= ymax; y++) {
      // Determine the sub-range of x values in (xmin, xmax) where this row should be drawn. This is
      // calculated based on what values of x would make the barycentric coordinates negative.
      uint x_offset = 0;
      uint x_range = (xmax - xmin + 1);
      full_renderer::raster_util::find_x_range_in_bounds(
        &x_range,
        &x_offset,
        abc_row,
        dabc_dx);

      if (x_range > 0)
      {
        // Define pixel attributes: values of each attribute for each pixel. These are initialized to be
        // the row-attributes and then incremented after every pixel.
        auto abc = abc_row;
        auto z = z_row;
        auto clr = clr_row;
        auto index = (y * raster->width + xmin);

        // Advance attributes based on how many columns we are skipping.
        if (x_offset > 0)
        {
          z += dz_dx * x_offset;
          abc += dabc_dx * x_offset;
          clr += dcolor_dx * x_offset;
          index += x_offset;
        }
        int x = xmin + x_offset;
        int x_end = x + x_range;

        // This is the critical loop so we minimize the number of statements in it. During each
        // iteration, we have to check for two things: 1) whether our mutex still protects our x
        // value, and 2) whether we have exceeded the x range. We can accomplish this with an inner
        // and an outer loop. The inner loop checks `limit` which is set to be whichever condition
        // happens first.
        int limit = x_end;
        while (x < x_end)
        {
          // Lock mutex protecting target area.
          if (multi_threaded) {
            raster->mutex->reserve(x, y, lock, &lock_valid_until);
            limit = min(lock_valid_until, x_end);
          }
          while (x < limit)
          {
            #if DRAW_OUTLINES
            // Drawing outlines - not optimized.
            if (x == xmin + x_offset || x == x_end - 1 || y == ymin || y == ymax)
              raster->add_outline_fragment(index, z);
            else
              raster->add_fragment(index, clr, opaque, z);
            #else
            raster->add_fragment(index, clr, opaque, z);
            #endif
            // Increment all attributes by their x-pixel derivatives.
            z += dz_dx;
            abc += dabc_dx;
            clr += dcolor_dx;
            index++;
            x++;
          }
        }
        lock.release();
      }
      // Increment all row-attributes by their y-pixel derivatives.
      z_row += dz_dy;
      abc_row += dabc_dy;
      clr_row += dcolor_dy;
    }
  }
}

void Raster::render_to(core::PixMap &pixmap)
{
  const auto max_distance = std::numeric_limits<z_distance_t>::max();

  auto pixel_it = pixmap.pixels.begin();
  size_t index = 0;
  for (auto &fragments: this->data) {
    full_renderer::raster_util::render_pixel_inner(fragments, pixel_it);
    this->closest_opaque[index] = max_distance;
    pixel_it += 4;
    index++;
  }
}

void Raster::render_to_partial(core::PixMap &pixmap, size_t y0, size_t y_stride)
{
  // Value we reset closest_opaque to for next raster cycle.
  const auto max_distance = std::numeric_limits<z_distance_t>::max();

  auto pixel_it = pixmap.pixels.begin() + 4 * (y0 * this->width);
  size_t index = y0 * this->width;

  // Since we are rendering with a stride, after each row, update indices by length * (stride - 1)
  size_t row_pixel_increment = 4 * (this->width * (y_stride - 1));
  size_t row_index_increment = (this->width * (y_stride - 1));
  for (uint y = y0; y < this->height; y += y_stride) {
    for (uint x = 0; x < this->width; x++) {
      vector<Fragment> &fragments = this->data[index];
      full_renderer::raster_util::render_pixel_inner(fragments, pixel_it);
      this->closest_opaque[index] = max_distance;
      index++;
      pixel_it += 4;
    }
    index += row_index_increment;
    pixel_it += row_pixel_increment;
  }
}

/**
 * Rasterizes triangle.
 *
 * This works by computing how all of the relevant attributes change with the pixel column and row.
 * This method computes all the attribute derivatives and then passes it to
 * `full_renderer::raster_util::rasterize_triangle_inner`, which handles actually rasterizing the
 * triangle.
 */
void Raster::rasterize_triangle(const STriangle *tri)
{
  const Vertex &a = *(tri->v[0]);
  const Vertex &b = *(tri->v[1]);
  const Vertex &c = *(tri->v[2]);

  // Compute the shaded color at each vertex.
  const Color color_a = shade_color(a.color, tri->shading[0]);
  const Color color_b = shade_color(b.color, tri->shading[1]);
  const Color color_c = shade_color(c.color, tri->shading[2]);

  // Compute the rectangular bounds which contain this triangle entirely.
  // TODO: Bounds check is buggy. Consider doing this during vertex processing.
  int xmin = this->width - 1;
  int xmax = 0;
  int ymin = this->height - 1;
  int ymax = 0;
  for (int i=0; i<3; i++) {
    xmin = min((int) ceil(tri->v[i]->v_screen[0]), xmin);
    xmax = max((int) floor(tri->v[i]->v_screen[0]), xmax);
    ymin = min((int) ceil(tri->v[i]->v_screen[1]), ymin);
    ymax = max((int) floor(tri->v[i]->v_screen[1]), ymax);
  }
  xmin = max(xmin, 0);
  xmax = min(xmax, (int) (this->width - 1));
  ymin = max(ymin, 0);
  ymax = min(ymax, (int) (this->height - 1));
  if (xmin >= this->width || ymin >= this->height)
    return;

  // For each vertex, compute its theoretical interpolated value at the pixel (xmin, ymin). Also
  // compute how it changes across each column and row (x-pixel derivative and y-pixel derivative).
  Vec3 abc0;
  Vec3 dabc_dx;
  Vec3 dabc_dy;
  z_distance_t dz_dx;
  z_distance_t dz_dy;
  z_distance_t z0;
  Vec3 color0;
  Vec3 dcolor_dx;
  Vec3 dcolor_dy;
  {
    // First compute the barycentric coordinates at (xmin, ymin).
    Vec2 p = {(precision_t) xmin, (precision_t) ymin};
    full_renderer::raster_util::compute_abc(a.v_screen, b.v_screen, c.v_screen, p, abc0, dabc_dx, dabc_dy);

    // Compute min-value and derivatives for attribute: z-depth.
    z0 = abc0[0] * a.z + abc0[1] * b.z + abc0[2] * c.z;
    dz_dx = a.z * dabc_dx[0] + b.z * dabc_dx[1] + c.z * dabc_dx[2];
    dz_dy = a.z * dabc_dy[0] + b.z * dabc_dy[1] + c.z * dabc_dy[2];

    // Compute min-value and derivatives for attribute: color.
    for (int i=0; i<3; i++) {
      color0[i] = abc0[0]*color_a[i] + abc0[1]*color_b[i] + abc0[2]*color_c[i];
      dcolor_dx[i] = dabc_dx[0]*color_a[i] + dabc_dx[1]*color_b[i] + dabc_dx[2]*color_c[i];
      dcolor_dy[i] = dabc_dy[0]*color_a[i] + dabc_dy[1]*color_b[i] + dabc_dy[2]*color_c[i];
    }
  }
  // Pass all of this information to inlined function which handles actual raster loop.
  full_renderer::raster_util::rasterize_triangle_inner(
    this,
    this->using_multiple_threads,
    tri->opaque,
    (uint) xmin,
    (uint) xmax,
    (uint) ymin,
    (uint) ymax,
    abc0,
    dabc_dx,
    dabc_dy,
    z0,
    dz_dx,
    dz_dy,
    color0,
    dcolor_dx,
    dcolor_dy);
}
