#include "raster_thread_queue.h"

#include <algorithm>
#include <vector>

#include "base.h"
#include "full_renderer.h"
#include "raster.h"

#include "../scene/scene.h"
#include "../scene/screen.h"
#include "../core/pmath.h"
#include "../core/color.h"
#include "../core/pixmap.h"
#include "../performance.h"
#include "../core/renderer.h"
#include "../core/static_mesh.h"

using namespace std;

using namespace full_renderer;

void RasterThreadQueue::clear() {
  for (auto &queue : this->thread_work_queue) {
    queue.clear();
  }
  this->cur_thread_index = 0;
  this->cur_thread_size = 0;
}

void RasterThreadQueue::set_num_valid_objects(size_t num_objects)
{
  this->num_objects_per_thread = (num_objects + num_threads - 1)/num_threads;
}

/**
 * Recursively assigns each thread which triangles it should render.
 *
 * TODO: Recursion may be inefficient for deeply nested objects. We should run some tests and
 * consider using a flat queue method.
 */
void RasterThreadQueue::build_queue(StaticMesh* obj)
{
  for (size_t i = 0; i < obj->num_triangles; i++)
  {
    STriangle& tri = obj->triangles[i];
    if (tri.valid)
    {
      this->thread_work_queue[this->cur_thread_index].push_back(&tri);
      this->cur_thread_size++;
    }
    if (this->cur_thread_size == this->num_objects_per_thread) {
      this->cur_thread_size = 0;
      this->cur_thread_index++;
    }
  }
  for (auto child : obj->children)
    this->build_queue(child);
}

void RasterThreadQueue::rasterize(RasterT *raster)
{
  vector<thread> threads;
  for (auto &items : this->thread_work_queue) {
    threads.push_back(thread([items, raster] {
      for (auto tri_p : items) {
        raster->rasterize_triangle(tri_p);
      }
    }));
  }
  for (auto &thread : threads) {
    thread.join();
  }
}
