#include "full_renderer.h"

#include "base.h"
#include <algorithm>
#include <vector>

#include "../scene/scene.h"
#include "../scene/screen.h"
#include "../core/pmath.h"
#include "../core/color.h"
#include "../core/pixmap.h"
#include "../performance.h"
#include "../core/renderer.h"
#include "../core/static_mesh.h"

using namespace std;

using namespace full_renderer;

void Renderer::render()
{
  this->scene->start_render_loop();
  this->pixmap->clear();
  this->raster_thread_queue->clear();
  this->project_objects();
  this->compute_shading();
  this->rasterize();
  this->draw();
}

void Renderer::render_perf(Performance &perf)
{
  this->scene->start_render_loop();
  auto t0 = std::chrono::high_resolution_clock::now();
  this->pixmap->clear();
  this->raster_thread_queue->clear();
  auto t1 = std::chrono::high_resolution_clock::now();
  this->project_objects();
  auto t2 = std::chrono::high_resolution_clock::now();
  this->compute_shading();
  auto t3 = std::chrono::high_resolution_clock::now();
  this->rasterize();
  auto t4 = std::chrono::high_resolution_clock::now();
  this->draw();
  auto t5 = std::chrono::high_resolution_clock::now();
  perf.log("clear buffers", t0, t1);
  perf.log("vertex projection", t1, t2);
  perf.log("vertex shading", t2, t3);
  perf.log("rasterize", t3, t4);
  perf.log("drawing", t4, t5);
}

void Renderer::test_report_objects(const StaticMesh* mesh) const
{
  if (!mesh)
    return;
  cout << "Mesh: " << (*mesh) << '\n';
  for (size_t i=0; i<mesh->num_triangles; i++) {
    STriangle& tri = mesh->triangles[i];
    cout << "\t Triangle " << (i + 1) << ": " << tri.valid << '\n';
    if (tri.valid) {
      for (int k=0; k<3; k++) {
        cout << "\t\tVertex " << (k+1) << '\n';
        cout << "\t\t\tShading: " << tri.shading[k] << '\n';
        cout << "\t\t\tColor  : " << tri.v[k]->color << '\n';
        cout << "\t\t\tPos    : " << tri.v[k]->v << '\n';
        cout << "\t\t\tScreen : " << tri.v[k]->v_screen << '\n';
      }
    }
  }
  for (auto c: mesh->children)
  {
    this->test_report_objects(c);
  }
}

void Renderer::project_objects()
{
  this->scene->engine.process_object(this->scene->root_object);
}

void Renderer::compute_shading()
{
  if (this->scene->lighting != nullptr) {
    this->scene->engine.shade_object(this->scene->root_object, this->scene->lighting);
  }
}

void Renderer::rasterize() {
  if (this->multithreaded) {
    this->raster_thread_queue->set_num_valid_objects(this->scene->engine.get_num_valid_objects());
    this->raster_thread_queue->build_queue(this->scene->root_object);
    this->raster_thread_queue->rasterize(this->raster);
  } else {
    this->rasterize_mesh(this->scene->root_object);
  }
}

void Renderer::rasterize_mesh(StaticMesh* obj) {
  for (size_t i=0; i<obj->num_triangles; i++) {
    if (obj->triangles[i].valid)
      this->raster->rasterize_triangle(&(obj->triangles[i]));
  }
  for (auto child : obj->children) {
    this->rasterize_mesh(child);
  }
}

void Renderer::draw() {
  if (!this->multithreaded) {
    this->raster->render_to(*this->pixmap);
  } else {
    const uint num_threads = this->num_threads;
    vector<thread> all_threads;
    for (uint i=0; i < num_threads; i++) {
      all_threads.push_back(thread([this, i, num_threads] {
        this->raster->render_to_partial(*this->pixmap, i, num_threads);
      }));
    }
    for (auto &t : all_threads) {
      t.join();
    }
  }
}
