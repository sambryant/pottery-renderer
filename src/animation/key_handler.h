#ifndef ANIMATION_KEY_HANDLER
#define ANIMATION_KEY_HANDLER

#include <map>
#include <memory>
#include <SDL2/SDL.h>
#include <iostream>

#include "options.h"
#include "../scene/scene.h"
#include "../core/pmath.h"

#define KH_X_NEG SDL_SCANCODE_A
#define KH_X_POS SDL_SCANCODE_D
#define KH_Y_NEG SDL_SCANCODE_LSHIFT
#define KH_Y_POS SDL_SCANCODE_SPACE
#define KH_Z_NEG SDL_SCANCODE_S
#define KH_Z_POS SDL_SCANCODE_W

namespace animation {

class KeyHandler
{
protected:
  precision_t increment;
  std::map<SDL_Scancode, bool> key_states;
public:
  KeyHandler(std::shared_ptr<Options> options) {
    this->increment = options->speed / options->fps;
    this->key_states[KH_X_NEG] = false;
    this->key_states[KH_X_POS] = false;
    this->key_states[KH_Y_NEG] = false;
    this->key_states[KH_Y_POS] = false;
    this->key_states[KH_Z_NEG] = false;
    this->key_states[KH_Z_POS] = false;
  };
  virtual ~KeyHandler() {}
public:
  virtual bool poll(std::shared_ptr<Scene> scene) = 0;
};

class KeyHandlerBasic : public KeyHandler
{
public:
  KeyHandlerBasic(std::shared_ptr<Options> options) : KeyHandler(options) {}
public:
  bool poll(std::shared_ptr<Scene> scene) override;
};

}


#endif
