#ifndef ANIMATION
#define ANIMATION

#include <memory>
#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>

#include "options.h"
#include "key_handler.h"
#include "../core/renderer.h"

namespace animation {

class Animation
{
public:
  std::unique_ptr<core::BaseRenderer> renderer;
  std::unique_ptr<KeyHandler> key_handler;
private:
  std::shared_ptr<Options> options;
  bool use_lock_textures;
  SDL_Window* window;
  SDL_Renderer* cxt;
  SDL_Texture* texture;
public:
  Animation(
    std::unique_ptr<core::BaseRenderer> &renderer,
    std::unique_ptr<KeyHandler> &key_handler,
    std::shared_ptr<Options> options
  );
  ~Animation();
public:
  void run();
private:
  void update_texture();
  void write_buffer();
};

}

#endif
