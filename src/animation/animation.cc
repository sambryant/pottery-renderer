#include "animation.h"

#include <algorithm>
#include <chrono>
#include <memory>
#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>

#include "options.h"
#include "../core/renderer.h"
#include "../core/pixmap.h"

using namespace std;

using namespace animation;

Animation::Animation(
  unique_ptr<core::BaseRenderer> &renderer,
  unique_ptr<KeyHandler> &key_handler,
  shared_ptr<Options> options
)
{
  this->options = options;
  this->renderer.reset(renderer.release());
  this->key_handler.reset(key_handler.release());
  core::PixMap& pixmap = this->renderer->get_pixmap();
  this->window = SDL_CreateWindow(
    "Render Engine",
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    pixmap.width,
    pixmap.height,
    0);
  this->cxt = SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED);
  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1" );

  this->texture = SDL_CreateTexture(
    this->cxt,
    SDL_PIXELFORMAT_RGBA32,
    SDL_TEXTUREACCESS_STREAMING,
    pixmap.width,
    pixmap.height);
}

Animation::~Animation()
{
  SDL_DestroyRenderer(this->cxt);
  SDL_DestroyWindow(this->window);
}

void Animation::run()
{
  int close = 0;

  int mouse_x, mouse_y;
  {
    core::PixMap& pixmap = this->renderer->get_pixmap();
    mouse_x = pixmap.width / 2;
    mouse_y = pixmap.height / 2;
  }

  uint frame_count = 0;
  long int frame_time = 0;

  while (!close) {
    auto t0 = std::chrono::high_resolution_clock::now();

    if (!this->key_handler->poll(this->renderer->get_scene())) {
      close = 1;
    }

    // Reset mouse position
    SDL_WarpMouseInWindow(this->window, mouse_x, mouse_y);

    this->renderer->render();
    this->update_texture();
    SDL_RenderPresent(this->cxt);
    auto t1 = std::chrono::high_resolution_clock::now();
    frame_count++;
    frame_time += std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();
    SDL_Delay(1000.0 / this->options->fps);
  }
  double ft = ((double) frame_time) / ((double) frame_count);
  cout << "Total number of frames: " << frame_count << '\n';
  cout << "Average performance   : " << ft << " ms / frame  (" << (1000.0/ft) << " fps)\n";
}

void Animation::update_texture()
{
  core::PixMap& pixmap = this->renderer->get_pixmap();
  if (this->options->use_lock_textures) {
    unsigned char* locked_pixels = nullptr;
    int pitch = 0;
    SDL_LockTexture(this->texture, nullptr, reinterpret_cast<void**>(&locked_pixels), &pitch);
    std::copy_n(
      pixmap.pixels.data(),
      pixmap.pixels.size(),
      locked_pixels);
    SDL_UnlockTexture(this->texture);
  } else {
    SDL_UpdateTexture(
      this->texture,
      nullptr,
      pixmap.pixels.data(),
      pixmap.width * 4);
  }
  SDL_RenderCopy(this->cxt, this->texture, nullptr, nullptr );
}


