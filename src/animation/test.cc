#include "test.h"

#include <optional>
#include <memory>
#include <limits>
#include <map>
#include <vector>
#include <cstdlib>
#include <chrono>
#include <SDL2/SDL.h>

#include "animation.h"
#include "../core/color.h"
#include "../core/pmath.h"
#include "../core/renderer.h"
#include "../scene/scene.h"
#include "../scene/screen.h"
#include "../scene/builder.h"
#include "../full_renderer/full_renderer.h"

#include "../examples/scenes.h"

using namespace std;
using namespace full_renderer;
using namespace animation;

namespace animation::tests {

void run_animation(SceneP scene, uint num_threads = DEFAULT_NUM_THREADS)
{
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
    printf("error initializing SDL: %s\n", SDL_GetError());
  }

  cout << "Running animation test with full renderer\n";

  shared_ptr<Options> options = make_shared<Options>();
  unique_ptr<KeyHandler> key_handler(new KeyHandlerBasic(options));
  full_renderer::Renderer *r = new Renderer(scene, num_threads);
  unique_ptr<core::BaseRenderer> renderer(r);
  Animation animation(renderer, key_handler, options);
  animation.run();
  SDL_Quit();
}

void test_2()
{
  run_animation(examples::scenes::complex_pots2(600, 400, 90.0), 6);
}

void test_tree()
{
  run_animation(examples::scenes::tree1(600, 400, 90.0), 6);
}

void test_house()
{
  run_animation(examples::scenes::house1(600, 400, 90.0), 6);
}

void test_perpendicular()
{
  run_animation(examples::scenes::perpendicular_surfaces(600, 400, 90.0), 6);
}

}

using namespace animation::tests;

void animation::test() {
  run_animation(examples::scenes::landscape2(600, 400, 90.0));
  // test_tree();
  // test_2();
  // test_tree();
  // test_house();
  // test_perpendicular();
}
