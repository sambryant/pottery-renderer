#ifndef ANIMATION_OPTIONS
#define ANIMATION_OPTIONS

#include <memory>

#include "../core/pmath.h"

#define ANIMATION_FPS 60
#define ANIMATION_SPEED 10.0
#define ANIMATION_USE_LOCK_TEXTURES true

namespace animation {

struct Options
{
  precision_t fps;
  precision_t speed;
  bool use_lock_textures;
  Options(precision_t fps=ANIMATION_FPS, precision_t speed=ANIMATION_SPEED, bool use_lock_textures=ANIMATION_USE_LOCK_TEXTURES)
  {
    this->fps = fps;
    this->speed = speed;
    this->use_lock_textures = use_lock_textures;
  }

};

}


#endif
