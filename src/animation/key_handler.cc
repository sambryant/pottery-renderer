#include "key_handler.h"

#include <memory>
#include <SDL2/SDL.h>
#include "../scene/scene.h"
#include "../core/pmath.h"

using namespace std;
using namespace animation;

bool KeyHandlerBasic::poll(shared_ptr<Scene> scene)
{
  SDL_Event event;

  std::map<SDL_Scancode, bool> &key_states = this->key_states;

  while (SDL_PollEvent(&event)) {
    switch (event.type) {

      case SDL_QUIT:
        return false;
        break;

      case SDL_MOUSEMOTION:
        scene->camera->move_camera(
          -event.motion.xrel * 0.01,
          -event.motion.yrel * 0.01);
        break;

      case SDL_KEYDOWN:
        key_states[event.key.keysym.scancode] = true;
        break;
      case SDL_KEYUP:
        key_states[event.key.keysym.scancode] = false;
        break;
    }
  }
  precision_t inc = this->increment;

  // We have to invert x-direction (see `Camera`)
  scene->camera->move_pos(
    +inc * key_states[KH_X_NEG] - inc * key_states[KH_X_POS],
    -inc * key_states[KH_Y_NEG] + inc * key_states[KH_Y_POS],
    -inc * key_states[KH_Z_NEG] + inc * key_states[KH_Z_POS]);
  return true;
}
