#ifndef CORE_UTIL
#define CORE_UTIL

#include <chrono>

namespace core
{
  /**
   * Indicates if byte ordering of the system is little endian.
   */
  constexpr bool is_little_endian()
  {
    return true;
    // const uint value = 0x01;
    // const void* address = (const void*) &value;
    // const unsigned char* first_byte = (const unsigned char*) address;
    // return (*first_byte == 0x01);
  }

  inline uint get_seed()
  {
    return std::chrono::system_clock::now().time_since_epoch().count();
  }

}


#endif