#include "pmath.h"

#include <cmath>
#include <iostream>
#include <iomanip>

using namespace std;

Mat3 get_rotation_x(precision_t th)
{
  precision_t s = sin(th);
  precision_t c = cos(th);
  return {{
    { 1.0,  0.0,  0.0},
    { 0.0,  c, -s},
    { 0.0,  s,  c},
  }};
}

Mat3 get_rotation_y(precision_t th)
{
  precision_t s = sin(th);
  precision_t c = cos(th);
  return {{
    { c  ,  0.0,  s  },
    { 0.0,  1.0,  0.0},
    {-s  ,  0.0,  c  }
  }};
}

Mat3 get_rotation_z(precision_t th)
{
  precision_t s = sin(th);
  precision_t c = cos(th);
  return {{
    { c  , -s  ,  0.0},
    { s  ,  c  ,  0.0},
    { 0.0,  0.0,  1.0}
  }};
}

std::ostream&
operator<<(std::ostream& out, const Vec3& v)
{
  out << "{" << setw(3)
      << v[0] << ", "
      << v[1] << ", "
      << v[2] << "}";
  return out;
}

std::ostream&
operator<<(std::ostream& out, const std::array<int, 3>& v)
{
  out << "{" << setw(3)
      << v[0] << ", "
      << v[1] << ", "
      << v[2] << "}";
  return out;
}

std::ostream&
operator<<(std::ostream& out, const Vec2& v)
{
  out << "{" << setw(3)
      << v[0] << ", "
      << v[1] << "}";
  return out;
}

