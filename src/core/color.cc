#include "color.h"

#include <iostream>
#include <iomanip>
#include <cstdlib>

#include "pmath.h"

using namespace std;

void color_copy(const Color &src, Color &dst)
{
  dst[0] = src[0];
  dst[1] = src[1];
  dst[2] = src[2];
}

Color shade_color(const Color &color, precision_t light)
{
  return {
    (uint8_t) (color[0] * light),
    (uint8_t) (color[1] * light),
    (uint8_t) (color[2] * light)
  };
}

Color blend_color(const Vec3 &abc, const Color &ca, const Color &cb, const Color &cc)
{
  return {
    (uint8_t) ((abc[0] * ca[0] + abc[1] * cb[0] + abc[2] * cc[0])),
    (uint8_t) ((abc[0] * ca[1] + abc[1] * cb[1] + abc[2] * cc[1])),
    (uint8_t) ((abc[0] * ca[2] + abc[1] * cb[2] + abc[2] * cc[2]))
  };
}

std::ostream&
operator<<(std::ostream& out, const Color& c)
{
  out << (unsigned short int) c[0] << ' '
      << (unsigned short int) c[1] << ' '
      << (unsigned short int) c[2];
  return out;
}
