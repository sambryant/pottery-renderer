#include "mesh_builder.h"

#include <vector>
#include <array>
#include <limits>
#include <map>

#include "static_mesh.h"
#include "vertex.h"
#include "pmath.h"

using namespace std;

MeshBuilder::~MeshBuilder()
{
  for (auto v : this->vertices)
    delete v;
  for (auto v : this->triangles)
    delete v;
  for (auto v : this->children)
    delete v;
}

MeshBuilder* MeshBuilder::add_child()
{
  MeshBuilder* mb = new MeshBuilder();
  this->children.push_back(mb);
  return mb;
}

Vertex* MeshBuilder::add(const Vec3 &vertex)
{
  Vertex* v = new Vertex(vertex);
  this->vertices.push_back(v);
  return v;
}

BTriangle* MeshBuilder::add(Vertex* a, Vertex* b, Vertex* c)
{
  BTriangle* t = new BTriangle(a, b, c);
  this->triangles.push_back(t);
  return t;
}

size_t MeshBuilder::get_num_triangles() const
{
  size_t num = this->triangles.size();
  for (auto mesh : this->children) {
    num += mesh->get_num_triangles();
  }
  return num;
}

size_t MeshBuilder::get_max_depth() const
{
  size_t max_depth = 1;
  for (auto mesh : this->children) {
    max_depth = max(max_depth, 1 + mesh->get_max_depth());
  }
  return max_depth;
}

MeshBuilder& MeshBuilder::translate(const Vec3& v, bool skip_children)
{
  for (auto vt: this->vertices) {
    vt->v += v;
  }
  if (!skip_children) {
    for (auto c : this->children) {
      c->translate(v, skip_children);
    }
  }
  return *this;
}

MeshBuilder& MeshBuilder::transform(const Mat3& m, bool skip_children)
{
  for (auto vt: this->vertices)
  {
    vt->v = m * vt->v;
  }
  if (!skip_children) {
    for (auto c : this->children) {
      c->transform(m, skip_children);
    }
  }
  return *this;
}

MeshBuilder& MeshBuilder::scale(precision_t scale, bool skip_children)
{
  if (scale == 1.0)
    return *this;
  for (auto vt: this->vertices)
  {
    vt->v[0] *= scale;
    vt->v[1] *= scale;
    vt->v[2] *= scale;
  }
  if (!skip_children) {
    for (auto c : this->children) {
      c->scale(scale, skip_children);
    }
  }
  return *this;
}

MeshBuilder& MeshBuilder::scale(precision_t scale_x, precision_t scale_y, precision_t scale_z, bool skip_children)
{
  for (auto vt: this->vertices)
  {
    vt->v[0] *= scale_x;
    vt->v[1] *= scale_y;
    vt->v[2] *= scale_z;
  }
  if (!skip_children) {
    for (auto c : this->children) {
      c->scale(scale_x, scale_y, scale_z, skip_children);
    }
  }
  return *this;
}

MeshBuilder& MeshBuilder::flip_x(bool skip_children)
{
  for (auto vt: this->vertices)
  {
    vt->v[0] *= -1.0;
  }
  if (!skip_children) {
    for (auto c : this->children) {
      c->flip_x(skip_children);
    }
  }
  return *this;
}

MeshBuilder& MeshBuilder::flip_y(bool skip_children)
{
  for (auto vt: this->vertices)
  {
    vt->v[1] *= -1.0;
  }
  if (!skip_children) {
    for (auto c : this->children) {
      c->flip_y(skip_children);
    }
  }
  return *this;
}

MeshBuilder& MeshBuilder::flip_z(bool skip_children)
{
  for (auto vt: this->vertices)
  {
    vt->v[2] *= -1.0;
  }
  if (!skip_children) {
    for (auto c : this->children) {
      c->flip_z(skip_children);
    }
  }
  return *this;
}

MeshBuilder& MeshBuilder::with_opacity(bool opaque, bool skip_children)
{
  for (auto t : this->triangles)
    t->opaque = opaque;
  if (!skip_children) {
    for (auto c : this->children) {
      c->with_opacity(opaque, skip_children);
    }
  }
  return *this;
}

MeshBuilder& MeshBuilder::with_color(Color color, bool skip_children)
{
  for (auto t : this->vertices)
    t->color = color;
  if (!skip_children) {
    for (auto c : this->children) {
      c->with_color(color, skip_children);
    }
  }
  return *this;
}

MeshBuilder& MeshBuilder::with_illuminated(bool illuminated, bool skip_children)
{
  this->illuminated = illuminated;
  if (!skip_children) {
    for (auto c : this->children) {
      c->with_illuminated(illuminated, skip_children);
    }
  }
  return *this;
}

StaticMesh* MeshBuilder::to_static_mesh()
{
  map<Vertex*, Vertex*> reassignment_map;
  auto mesh = new StaticMesh(this->vertices.size(), this->triangles.size(), this->illuminated);
  for (size_t i = 0; i < this->vertices.size(); i++)
  {
    mesh->vertices[i] = *(this->vertices[i]);
    reassignment_map[this->vertices[i]] = &(mesh->vertices[i]);
  }
  for (size_t i = 0; i < this->triangles.size(); i++)
  {
    mesh->triangles[i].opaque = this->triangles[i]->opaque;
    for (int j=0; j<3; j++) {
      mesh->triangles[i].v[j] = reassignment_map[this->triangles[i]->v[j]];
      mesh->triangles[i].shading[j] = 1.0;
    }
    mesh->triangles[i].compute_normal();
  }
  for (auto child_p : this->children)
    mesh->children.push_back(child_p->to_static_mesh());
  return mesh;
}

std::ostream& operator<<(std::ostream& out, const MeshBuilder& t)
{
  out << "Mesh Builder (V=" << t.vertices.size() << ", T=" << t.triangles.size() << ", C=" << t.children.size() << ")";
  return out;
}

std::ostream& operator<<(std::ostream& out, const BTriangle& t)
{
  out << "BTriangle (opaque = " << t.opaque << ")\n"
      << "\tA: " << *(t.v[0]) << "\n"
      << "\tB: " << *(t.v[0]) << "\n"
      << "\tC: " << *(t.v[0]) << "\n";
  return out;
}
