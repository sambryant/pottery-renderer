#include "pixmap.h"

#include <algorithm>
#include <bit>
#include <iostream>
#include <vector>

#include "../performance.h"

#include "pmath.h"
#include "vertex.h"
#include "util.h"

using namespace std;

void core::PixMap::set_background(Color background)
{
  this->background = background;
  // Here we encode the pixel array [background[0], background[1], background[2], alpha = 255] as a
  // uint so we can rapidly clear the pixmap using std::fill. This technique is endian-dependent. We
  // need the bytes to appear in the correct order.
  if (core::is_little_endian()) {
    this->background_uint = (((uint) background[0])) + (((uint) background[1]) << 8) + (((uint) background[2]) << 16) + (255 << 24);
  } else {
    this->background_uint = (((uint) background[0]) << 24) + (((uint) background[1]) << 16) + (((uint) background[2]) << 8) + 255;
  }
}

void core::PixMap::initialize()
{
  for (int i=0; i<this->height; i++) {
    for (int j=0; j<this->width; j++) {
      this->pixels.push_back(0);
      this->pixels.push_back(0);
      this->pixels.push_back(0);
      this->pixels.push_back(255);
    }
  }
}

void core::PixMap::clear()
{
  // We fill the pixel array with the background color by casting it to a uint array which allows us
  // to fill it with the scalar value `background_uint` which is just the background color encoded
  // as an int. This allows us to leverage the optimization of std::fill.
  uint* px = static_cast<uint*>(static_cast<void*>(pixels.data()));
  std::fill(px, px + this->width * this->height, background_uint);
}
