#ifndef CORE_EXCEPTION
#define CORE_EXCEPTION

#include <exception>
#include <string>

namespace core {
  struct GenericException : public std::exception {
    std::string msg;

    GenericException(const char* msg) {
      this->msg = msg;
    }

    const char * what() const throw () {
      return this->msg.c_str();
      // return "You must call 'prepare' before calling 'render'";
    }
  };
}

#endif
