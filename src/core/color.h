#ifndef CORE_COLOR
#define CORE_COLOR

#include <iostream>
#include <array>
#include <cstdlib>

#include "pmath.h"

typedef std::array<uint8_t, 3> Color;

const Color BLUE = {77, 142, 211};
const Color RED = {193, 56, 48};
const Color YELLOW = {242, 214, 77};
const Color PURPLE = {153, 114, 149};
const Color BLACK = {0, 0, 0};
const Color WHITE = {255, 255, 255};
const Color BROWN = {63, 48, 29};
const Color GREEN = {139, 145, 79};

void color_copy(const Color &src, Color &dst);

Color shade_color(const Color &color, precision_t light);

/// Testing only - this implementation is very slow - only to demonstrate / check correct behavior.
Color blend_color(const Vec3 &abc, const Color &ca, const Color &cb, const Color &cc);

std::ostream& operator<<(std::ostream& out, const Color& c);

#endif
