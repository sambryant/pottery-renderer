/**
 * Object used to build scenes from primitives. This is not used in rendering. Instead this is used
 * to construct a scene. It is then converted into a `StaticMesh`, which *is* used in rendering.
 * Since it's only used during setup, it's not critical that this be time or memory efficient.
 */
#ifndef CORE_MESH_BUILDER
#define CORE_MESH_BUILDER

#include <vector>
#include <array>
#include <iostream>

#include "vertex.h"
#include "pmath.h"
#include "static_mesh.h"

class BTriangle;

class MeshBuilder
{
public:
  std::vector<Vertex*> vertices;
  std::vector<BTriangle*> triangles;
  std::vector<MeshBuilder*> children;
  bool illuminated = false;
public:
  MeshBuilder() {}
  ~MeshBuilder();
  MeshBuilder& operator= (const MeshBuilder&) = delete;
public:
  /**
   * Determines total number of triangles in this mesh and all of its children.
   */
  size_t get_num_triangles() const;

  /**
   * Returns the maximum depth of the MeshBuilder tree (how deep its MeshBuilder children go).
   */
  size_t get_max_depth() const;

  // TRANSFORMATION FUNCTIONS
  /**
   * Translates each vertex in the mesh.
   */
  MeshBuilder& translate(const Vec3& v, bool skip_children = false);

  /**
   * Applies matrix transformation to each vertex in the mesh.
   */
  MeshBuilder& transform(const Mat3& m, bool skip_children = false);

  /**
   * Rescales size of the object mesh by scalar multiplication of vertices.
   *
   * This should be applied before translation (but commutes with transformations).
   */
  MeshBuilder& scale(precision_t scale, bool skip_children = false);
  MeshBuilder& scale(precision_t scale_x, precision_t scale_y, precision_t scale_z, bool skip_children = false);

  /**
   * Flips x coordinates of each vertex in mesh.
   */
  MeshBuilder& flip_x(bool skip_children = false);

  /**
   * Flips y coordinates of each vertex in mesh.
   */
  MeshBuilder& flip_y(bool skip_children = false);

  /**
   * Flips z coordinates of each vertex in mesh.
   */
  MeshBuilder& flip_z(bool skip_children = false);

  /**
   * Sets the opacity of each vertex in mesh.
   */
  MeshBuilder& with_opacity(bool opaque, bool skip_children = false);

  /**
   * Sets the color of each vertex in mesh.
   */
  MeshBuilder& with_color(Color color, bool skip_children = false);

  /**
   * Sets the illuminated flag (whether object receives shading calculations).
   */
  MeshBuilder& with_illuminated(bool illuminated, bool skip_children = false);

  /**
   * Adds a new child MeshBuilder (sub-object) and returns a pointer to the newly created object.
   * Cleanup of the memory is handled by this class's destructor.
   */
  MeshBuilder* add_child();

  /**
   * Adds a new vertex given by its 3D coordinates and returns a pointer to newly created object.
   * Cleanup of the memory is handled by this class's destructor.
   */
  Vertex* add(const Vec3 &vertex);

  /**
   * Adds a triangle to the mesh given by pointers to Vertex objects already added to the mesh.
   * If any of the Vertex pointers are not already in the mesh, this will caused undefined behavior.
   */
  BTriangle* add(Vertex* a, Vertex* b, Vertex* c);

  /**
   * Converts the mesh builder into a StaticMesh, which used fixed-allocted memory.
   */
  StaticMesh* to_static_mesh();

  /**
   * TODO: This is a would-be-nice function which detects when there are multiple vertices with the
   * same position and color. It would then remove the duplicates and redirect the pointer
   * references of the triangles.
   *
   * Pros: it could allow for mesh building without add vertex / add triangle separation. You could
   * simply add the coordinates for each triangle. They would later be collapsed.
   *
   * Cons: Potential for duplicate vertices if triangles are added in a way that introduces
   * numerical imprecisions.
   */
  void collapse_vertices()
  {
    std::cerr << "NIY\n";
    exit(1);
  }
};

/**
 * Represents a single triangle within a mesh builder. The triangle is represented by pointers to
 * the Vertex objects matching the pointers in `MeshBuilder::vertices`.
 */
class BTriangle
{
public:
  std::array<Vertex*, 3> v;
  bool opaque = true;
public:
  BTriangle(Vertex* a, Vertex* b, Vertex* c)
  {
    v[0] = a;
    v[1] = b;
    v[2] = c;
  }
  BTriangle* with_opacity(bool opaque) {
    this->opaque = opaque;
    return this;
  }
};

std::ostream& operator<<(std::ostream& out, const MeshBuilder& t);

std::ostream& operator<<(std::ostream& out, const BTriangle& t);

#endif
