#include "vertex.h"

#include <iostream>

void Vertex::serialize(serializer::Serializer* out) const
{
  out->serialize_arr("v", this->v);
  out->serialize_arr("color", this->color);
}

std::ostream& operator<<(std::ostream& out, const Vertex& v)
{
  out << "( v = {" << v.v << "}, clr = {" << v.color << "} )";
  return out;
}
