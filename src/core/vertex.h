#ifndef CORE_VERTEX
#define CORE_VERTEX

#include "pmath.h"
#include "color.h"
#include "serializer.h"

class Vertex
{
public:
  // Core data - does not change (for now...)

  Vec3 v;
  Color color = WHITE;

  // Computed data - recalculated every render loop.

  /**
   * Coordinates in system where the camera is at the origin and points in the +z direction.
   */
  Vec3 v_trans;

  /**
   * Vertex coordinates projected onto screen space (literal pixel coordinates).
   */
  Vec2 v_screen;

  /**
   * Indicates coordinate is in front of camera (TODO: double check!)
   */
  bool valid;

  /**
   * Vector pointing from the vertex to the light source.
   */
  Vec3 light_displacement;

  /**
   * Z-distance from vertex to camera.
   * TODO: I think its currently actually z^2. There are some issues with nearby planes being
   * rendered wrong. We should investigate this.
   */
  precision_t z;
public:
  Vertex() {}
  Vertex(const Vec3 &v)
  {
    this->v = v;
  }
  Vertex &with_color(const Color &color) {
    this->color = color;
    return *this;
  }
  void serialize(serializer::Serializer* out) const;
};

std::ostream& operator<<(std::ostream& out, const Vertex& v);

#endif
