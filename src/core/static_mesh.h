/**
 * Recursive representation of 3D models using meshes of triangles.
 */
#ifndef CORE_STATIC_MESH
#define CORE_STATIC_MESH

#include <vector>
#include <array>
#include <iostream>

#include "vertex.h"
#include "pmath.h"
#include "serializer.h"

class STriangle;

/**
 * Class representing a fixed triangle mesh. It uses construction-time allocation to ensure data
 * locality.
 */
class StaticMesh
{
public:
  Vertex* vertices;
  STriangle* triangles;
  std::vector<StaticMesh*> children;
  const size_t num_vertices;
  const size_t num_triangles;
  const bool illuminated;
public:
  StaticMesh(size_t num_vertices_, size_t num_triangles_, bool illuminated_ = false);
  ~StaticMesh();
  StaticMesh& operator= (const StaticMesh&) = delete;
public:
  size_t get_num_triangles() const;
  size_t get_max_depth() const;
  StaticMesh& translate(const Vec3& v, bool skip_children = false);
  StaticMesh& transform(const Mat3& m, bool skip_children = false);
  StaticMesh& scale(precision_t scale, bool skip_children = false);
  StaticMesh& with_opacity(bool opaque, bool skip_children = false);
  void serialize(serializer::Serializer* out) const;
};

class STriangle
{
public:
  // Core data
  std::array<Vertex*, 3> v;
  bool opaque = true;
  Vec3 normal;

  // Data recomputed on each render
  bool valid;
  bool visible_face;
  std::array<precision_t, 3> shading;
public:
  void compute_normal();
};

std::ostream& operator<<(std::ostream& out, const StaticMesh& t);

#endif
