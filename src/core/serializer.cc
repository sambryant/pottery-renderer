#include "serializer.h"

#include <iostream>
#include <map>
#include <string>
#include <fstream>

#include "vertex.h"

using namespace serializer;
using namespace std;

void Serializer::serialize_arr(const char* key_name, Vec2 v) {
  out << "\"" << key_name << "\":[" << v[0] << "," << v[1] << "],\n";
}

void Serializer::serialize_arr(const char* key_name, Vec3 v) {
  out << "\"" << key_name << "\":[" << v[0] << "," << v[1] << "," << v[2] << "],\n";
}

void Serializer::serialize_arr(const char* key_name, uint a, uint b, uint c) {
  out << "\"" << key_name << "\":[" << a << "," << b << "," << c << "],\n";
}

void Serializer::serialize_arr(const char* key_name, Color v) {
  out << "\"" << key_name << "\":[" << (uint) v[0] << "," << (uint) v[1] << "," << (uint) v[2] << "],\n";
}
