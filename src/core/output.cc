#include "output.h"

#include "base.h"

#include <vector>
#include <string>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <memory>

using namespace std;

void core::save_render(const PixMap &pixmap, std::string output, bool flip_y)
{
  ofstream myfile;
  myfile.open(output);
  myfile << "P3" << '\n'
         << pixmap.width << " " << pixmap.height << '\n'
         << "255" << '\n';
  for (int i=0; i<pixmap.height; i++) {
    int i0 = flip_y ? pixmap.height - i - 1 : i;
    for (int j=0; j<pixmap.width; j++) {
      const Color &c = pixmap.at(j, i0);
      myfile << (int) c[0] << " "
             << (int) c[1] << " "
             << (int) c[2] << " ";
    }
    myfile << '\n';
  }
  myfile.close();
  cout << "Saved " << pixmap.width << "x" << pixmap.height << " pixmap to " << output << '\n';
}
