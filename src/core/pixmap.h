/**
 * Class which encapsulates a 4-byte RGBA pixel array suitable for streaming to SDL graphics.
 */
#ifndef CORE_PIXMAP
#define CORE_PIXMAP

#include "base.h"

#include <array>
#include <vector>
#include <cassert>
#include <limits>

#include "color.h"
#include "pmath.h"

namespace core {

class PixMap {
public:
  const int width;
  const int height;
  std::vector<uint8_t> pixels;
protected:
  /**
   * Background color stored as [r,g,b] Color.
   */
  Color background;
  /**
   * Background color pixel [r,g,b,a] converted into endian-aware uint for fast clearing.
   */
  uint background_uint;
public:
  PixMap(uint width_, uint height_) : width(width_), height(height_) {
    this->initialize();
  }
  PixMap() = delete;
public:
  /**
   * Sets background color.
   */
  void set_background(Color background);

  /**
   * Refreshes the pixel map by setting all pixels to the background color.
   */
  void clear();

  /**
   * Returns the color at the given x and y pixel.
   *
   * This should not be used for any real-time rendering.
   */
  Color at(int px, int py) const
  {
    assert(px >= 0 && px < this->width);
    assert(py >= 0 && py < this->height);
    int ind = 4*(py * this->width + px);
    return {pixels[ind], pixels[ind + 1], pixels[ind + 2]};
  }
protected:
  void initialize();
};

}

#endif
