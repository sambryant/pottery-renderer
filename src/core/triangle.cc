#include <iostream>
#include <limits>

#include "pmath.h"
#include "triangle.h"

using namespace std;

void Triangle::set_vertices(const Vec3 &a, const Vec3 &b, const Vec3 &c)
{
  this->vertices[0].v = a;
  this->vertices[1].v = b;
  this->vertices[2].v = c;
  Vec3 ba = b - a;
  Vec3 ca = c - a;
  this->normal = vec3_normalize(vec3_cross(ba, ca));
  this->mid.v = (a + b + c) / 3.0;
}

void Triangle::translate(const Vec3 &v)
{
  for (int i=0; i<3; i++) {
    this->vertices[i].v = this->vertices[i].v + v;
  }
  this->mid.v = this->mid.v + v;
}
