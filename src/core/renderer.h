#ifndef CORE_RENDERER
#define CORE_RENDERER

#include "pixmap.h"
#include "../scene/scene.h"
#include <memory>
#include "../performance.h"

namespace core {

class BaseRenderer {
public:
  virtual core::PixMap &get_pixmap() const = 0;
  virtual void render() = 0;
  virtual void render_perf(Performance &perf) = 0;
  virtual std::shared_ptr<Scene> get_scene() const = 0;
  virtual ~BaseRenderer() {}
};

}

#endif
