#include "static_mesh.h"

#include <vector>
#include <array>
#include <limits>

#include "vertex.h"
#include "pmath.h"

using namespace std;

StaticMesh::StaticMesh(size_t num_vertices_, size_t num_triangles_, bool illuminated_)
  : num_vertices(num_vertices_),
    num_triangles(num_triangles_),
    illuminated(illuminated_)
{
  this->vertices = (Vertex*) malloc(sizeof(Vertex) * num_vertices);
  this->triangles = (STriangle*) malloc(sizeof(STriangle) * num_triangles);
}

StaticMesh::~StaticMesh() {
  free(this->vertices);
  free(this->triangles);
  for (auto child : this->children)
  {
    delete child;
  }
}

void STriangle::compute_normal()
{
  Vec3 ba = this->v[1]->v - this->v[0]->v;
  Vec3 ca = this->v[2]->v - this->v[0]->v;
  this->normal = vec3_normalize(vec3_cross(ba, ca));
}


size_t StaticMesh::get_num_triangles() const
{
  size_t num = this->num_triangles;
  for (auto mesh : this->children) {
    num += mesh->get_num_triangles();
  }
  return num;
}

size_t StaticMesh::get_max_depth() const
{
  size_t max_depth = 1;
  for (auto mesh : this->children) {
    max_depth = max(max_depth, 1 + mesh->get_max_depth());
  }
  return max_depth;
}

StaticMesh& StaticMesh::translate(const Vec3& v, bool skip_children)
{
  for (size_t i=0; i<this->num_vertices; i++)
    vertices[i].v += v;
  if (!skip_children) {
    for (auto c : this->children) {
      c->translate(v, skip_children);
    }
  }
  return *this;
}

StaticMesh& StaticMesh::transform(const Mat3& m, bool skip_children)
{
  for (size_t i=0; i<this->num_vertices; i++)
    vertices[i].v = m * vertices[i].v;
  if (!skip_children) {
    for (auto c : this->children) {
      c->transform(m, skip_children);
    }
  }
  return *this;
}

StaticMesh& StaticMesh::scale(precision_t scale, bool skip_children)
{
  if (scale == 1.0)
    return *this;
  for (size_t i=0; i<this->num_vertices; i++)
    vertices[i].v = vertices[i].v * scale;
  if (!skip_children) {
    for (auto c : this->children) {
      c->scale(scale, skip_children);
    }
  }
  return *this;
}

StaticMesh& StaticMesh::with_opacity(bool opaque, bool skip_children)
{
  for (size_t i=0; i<this->num_triangles; i++)
    triangles[i].opaque = opaque;
  if (!skip_children) {
    for (auto c : this->children) {
      c->with_opacity(opaque, skip_children);
    }
  }
  return *this;
}

std::ostream& operator<<(std::ostream& out, const StaticMesh& t)
{
  out << "Mesh (V=" << t.num_vertices << ", T=" << t.num_triangles << ", C=" << t.children.size() << ")";
  return out;
}

void StaticMesh::serialize(serializer::Serializer* out) const
{
  map<Vertex*, uint> v_map;
  for (uint i=0; i<this->num_vertices; i++) {
    v_map[&this->vertices[i]] = i;
  }
  out->serialize_prm("num_vertices", num_vertices);
  out->serialize_prm("num_triangles", num_triangles);
  out->serialize_prm("num_children", children.size());
  out->serialize_prm("illuminated", illuminated);
  out->serialize_obj_arr("vertices", this->vertices, this->num_vertices);
  out->arr_st("triangles");
  for (uint i=0; i<this->num_triangles; i++) {
    STriangle& t = this->triangles[i];
    out->out << "{\n";
    out->serialize_prm("opaque", t.opaque);
    out->serialize_arr("v", v_map[t.v[0]], v_map[t.v[1]], v_map[t.v[2]]);
    out->out << "},\n";
  }
  out->arr_ed();
  out->serialize_obj_arr("children", this->children);
}
