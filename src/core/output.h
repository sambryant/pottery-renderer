#ifndef CORE_OUTPUT
#define CORE_OUTPUT

#include "base.h"
#include <vector>
#include <string>
#include "pixmap.h"

namespace core {

void save_render(const PixMap &pixmap, std::string output, bool flip_y = false);

}

#endif
