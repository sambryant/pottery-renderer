#ifndef PMATH
#define PMATH

#include <array>
#include <iostream>
#include <cmath>

template <typename T>
using Vec3T = std::array<T, 3>;

template <typename T>
using Vec2T = std::array<T, 2>;

using precision_t = double;
using Vec2 = Vec2T<precision_t>;
using Vec3 = Vec3T<precision_t>;
using Mat3 = std::array<std::array<precision_t, 3>, 3>;
using RawTriangle = std::array<Vec3, 3>;
using RawProjTriangle = std::array<Vec2, 3>;
using PixelTriangle = std::array<Vec2, 3>;

template <typename T>
inline Vec2T<T> operator-(const Vec2T<T>& lhs, const Vec2T<T>& rhs)
{
  return {
    lhs[0] - rhs[0],
    lhs[1] - rhs[1],
  };
}

template <typename T>
inline Vec3T<T> operator+(const Vec3T<T>& lhs, const Vec3T<T>& rhs)
{
  return {
    lhs[0] + rhs[0],
    lhs[1] + rhs[1],
    lhs[2] + rhs[2],
  };
}

template <typename T>
inline void operator+=(Vec3T<T>& lhs, const Vec3T<T>& rhs)
{
  lhs[0] += rhs[0];
  lhs[1] += rhs[1];
  lhs[2] += rhs[2];
}

template <typename T>
inline Vec3T<T> operator-(const Vec3T<T>& lhs, const Vec3T<T>& rhs)
{
  return {
    lhs[0] - rhs[0],
    lhs[1] - rhs[1],
    lhs[2] - rhs[2],
  };
}

template <typename T, typename U>
inline Vec3T<T> operator*(const Vec3T<T>& lhs, U rhs)
{
  return {
    lhs[0] * rhs,
    lhs[1] * rhs,
    lhs[2] * rhs,
  };
}

template <typename T, typename U>
inline Vec3T<T> operator/(const Vec3T<T>& lhs, U rhs)
{
  return {
    lhs[0] / rhs,
    lhs[1] / rhs,
    lhs[2] / rhs,
  };
}

template <typename T>
inline T vec3_norm(const Vec3T<T>& v)
{
  return sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
}

template <typename T>
inline T vec3_dot(const Vec3T<T>& lhs, const Vec3T<T>& rhs)
{
  return lhs[0]*rhs[0]+lhs[1]*rhs[1]+lhs[2]*rhs[2];
}

template <typename T>
inline T vec3_distance2(const Vec3T<T>& lhs, const Vec3T<T>& rhs)
{
  Vec3T<T> dis = lhs - rhs;
  return vec3_dot(dis, dis);
}

template <typename T>
inline Vec3T<T> vec3_cross(const Vec3T<T>& lhs, const Vec3T<T>& rhs)
{
  Vec3T<T> ret;
  ret[0] = lhs[1]*rhs[2] - lhs[2]*rhs[1];
  ret[1] = lhs[2]*rhs[0] - lhs[0]*rhs[2];
  ret[2] = lhs[0]*rhs[1] - lhs[1]*rhs[0];
  return ret;
}

template <typename T>
inline Vec3T<T> vec3_normalize(const Vec3T<T>& lhs)
{
  T norm = 1.0 / sqrt(vec3_dot(lhs, lhs));
  return lhs * norm;
}

inline Vec3 operator*(const Mat3& lhs, const Vec3& rhs)
{
  Vec3 ret;
  for (int i=0; i<3; i++) {
    ret[i] = 0.0;
    for (int j=0; j<3; j++) {
      ret[i] += lhs[i][j] * rhs[j];
    }
  }
  return ret;
}

inline Mat3 operator*(const Mat3& lhs, const Mat3& rhs)
{
  Mat3 ret;
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {
      ret[i][j] = 0.0;
      for (int k=0; k<3; k++) {
        ret[i][j] += lhs[i][k] * rhs[k][j];
      }
    }
  }
  return ret;
}

inline Mat3 mat3_transpose(const Mat3& lhs)
{
  Mat3 ret;
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {
      ret[i][j] = lhs[j][i];
    }
  }
  return ret;
}

inline void mat3_transpose_to(const Mat3& src, Mat3& dst) {
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {
      dst[i][j] = src[j][i];
    }
  }
}

std::ostream& operator<<(std::ostream& out, const Vec3& v);
std::ostream& operator<<(std::ostream& out, const Vec2& v);
std::ostream& operator<<(std::ostream& out, const std::array<int, 3>& v);
Mat3 get_rotation_x(precision_t th);
Mat3 get_rotation_y(precision_t th);
Mat3 get_rotation_z(precision_t th);

#endif
