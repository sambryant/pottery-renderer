#ifndef TRIANGLE
#define TRIANGLE

#include <iostream>
#include <vector>
#include <iterator>

#include "pmath.h"
#include "color.h"
#include "vertex.h"

class Triangle
{
public:
  std::array<Vertex, 3> vertices;
  Vertex mid;
  Vec3 normal;
  Color color;
  bool valid;
  Color shading;
  uint8_t opacity;
  std::array<uint, 4> box_bounds; // Only used for ray-tracing.
  Triangle(const Vec3 &a, const Vec3 &b, const Vec3 &c) {
    this->color = BLUE;
    this->opacity = 255;
    this->set_vertices(a, b, c);
  }
  Triangle(const Vec3 &a, const Vec3 &b, const Vec3 &c, const Color &color) {
    this->color = color;
    this->opacity = 255;
    this->set_vertices(a, b, c);
  }
public:
  void translate(const Vec3 &v);
  Triangle &with_color(const Color &color)
  {
    this->color = color;
    return *this;
  }
  Triangle &with_opacity(uint8_t opacity)
  {
    this->opacity = opacity;
    return *this;
  }
protected:
  void set_vertices(const Vec3 &a, const Vec3 &b, const Vec3 &c);
};

class TriangleCollection
{
public:
  std::vector<Triangle> triangles;
public:
  TriangleCollection() {}
public:
  void add(const Triangle &triangle)
  {
    this->triangles.push_back(triangle);
  }
  void translate(const Vec3 &v)
  {
    for (std::size_t i=0; i<this->triangles.size(); i++) {
      this->triangles[i].translate(v);
    }
  }
};
#endif
