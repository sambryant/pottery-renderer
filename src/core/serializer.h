#ifndef CORE_SERIALIZER
#define CORE_SERIALIZER

#include <fstream>
#include <string>
#include <map>
#include <vector>

#include "pmath.h"
#include "color.h"

namespace serializer {

class Serializer;

class Serializer
{
public:
  std::ofstream out;
  Serializer(std::string output) {
    out.open(output);
  }
  ~Serializer() {
    out.close();
  }
public:
  void arr_st(const char* key_name)
  {
    out << "\"" << key_name << "\":[";
  }
  void arr_ed()
  {
    out << "],\n";
  }
  void serialize_arr(const char* key_name, Vec2 v);
  void serialize_arr(const char* key_name, Vec3 v);
  void serialize_arr(const char* key_name, uint a, uint b, uint c);
  void serialize_arr(const char* key_name, Color v);
  template <typename T>
  void serialize_prm(const char* key_name, T v)
  {
    out << "\"" << key_name << "\":" << v << ",\n";
  }
  template <typename T>
  void serialize_obj(const char* key_name, const T& obj)
  {
    out << "\"" << key_name << "\":{\n";
    obj.serialize(this);
    out << "},\n";
  }
  template <typename T>
  void serialize_obj_arr(const char* key_name, const T* objs, size_t num)
  {
    this->arr_st(key_name);
    for (size_t i=0; i<num; i++) {
      out << "{";
      objs[i].serialize(this);
      out << "},";
    }
    out << "],\n";
  }
  template <typename T>
  void serialize_obj_arr(const char* key_name, const std::vector<T> &arr)
  {
    this->arr_st(key_name);
    for (auto o : arr) {
      out << "{\n";
      o->serialize(this);
      out << "}\n,"; 
    }
    this->arr_ed();
  }
};

}

#endif
