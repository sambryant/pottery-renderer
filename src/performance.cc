#include "performance.h"

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>

using namespace std;

void Performance::log(const char *name, perf_time_t t1, perf_time_t t2)
{
  auto it = this->name_to_index.find(name);
  if (it == this->name_to_index.end()) {
    this->name_to_index[name] = this->names.size();
    this->names.push_back(name);
    this->times.push_back(0);
  }
  auto index = this->name_to_index[name];
  this->times[index] += std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
}

long int Performance::get_total_average(unsigned int num_trials)
{
  long int total_time(0);
  for (auto t: this->times) {
    total_time += t;
  }
  return total_time / num_trials;
}

void Performance::report(unsigned int num_trials) {
  cout << "\t" << left << setw(20) << "Description"
       << "  "
       << setw(15)
       << "Total time (us)"
       << "  "
       << setw(15)
       << "Avg time (us)"
       << "  "
       << setw(15)
       << "Fraction\n";
  long int total_time(0);
  for (auto t: this->times) {
    total_time += t;
  }
  cout << "\t"
       << left
       << setw(20) << "TOTAL"
       << "  "
       << setw(15)
       << (total_time)
       << "  "
       << setw(15)
       << ((double) total_time) / (((double) num_trials))
       << '\n';

  for (size_t i=0; i<this->names.size(); i++) {
    cout << "\t"
         << left
         << setw(20) << this->names[i]
         << "  "
         << setw(15)
         << (this->times[i])
         << "  "
         << setw(15)
         << ((double) this->times[i]) / (((double) num_trials))
         << "  "
         << setw(15)
         << 100.0 * ((double) this->times[i]) / total_time
         << '\n';
  }
}
