/**
 * Generates landscape of hills using pseudo-procedural techniques.
 *
 * The hills are created by adding Gaussian bumps of geometrically diminishing strength.
 */
#ifndef SHAPES_LANDSCAPE
#define SHAPES_LANDSCAPE

#include "../core/mesh_builder.h"
#include "../core/util.h"

#include "tree.h"

namespace shapes {

class LandscapeBuilder {
public:
  // Grid parameters
  precision_t size = 40.0;
  precision_t sample_density = 2.5; // samples / size
  const unsigned seed;

  // Hill generation parameters
  precision_t bumps_per_cycle_density = 0.005; // num / size^2
  uint num_bump_cycles = 100;
  precision_t bump_strength_initial = 1.5;
  precision_t bump_width2_initial = 10.0;
  precision_t bump_diminish_factor = 0.9;

  // Tree generation parameters
  TreeBuilder tree_gen;
  precision_t tree_density = 0.0075; // num / size^2
  precision_t tree_scale = 0.2;
  precision_t tree_scale_jitter = 0.05;
protected:
  mutable std::mt19937 rng_gen;
public:
  LandscapeBuilder(unsigned int seed_) : seed(seed_)
  {
    this->rng_gen = std::mt19937 { seed };
    this->initialize_tree_gen();
  }
  LandscapeBuilder() : seed(core::get_seed())
  {
    this->rng_gen = std::mt19937{ seed };
    this->initialize_tree_gen();
  }
  ~LandscapeBuilder() {}
public:
  MeshBuilder& generate(MeshBuilder& dst);
protected:
  void initialize_tree_gen() {
    this->tree_gen
      .with_seed(seed)
      .with_height(16.0)
      .with_sampling_rate(1.0)
      .with_color_jitter(10.0)
      .with_vertex_jitter(
        TreeBuilder::VertexJitterMethod::xz,
        TreeBuilder::VertexJitterDist::uniform,
        0.09)
      .with_depth(3);
  }

  /**
   * Adds some trees to the landscape at a fixed density.
   */
  MeshBuilder& add_trees(MeshBuilder& dst);

  /**
   * Adds Gaussian bumps to landscape of fixed strength and width.
   */
  MeshBuilder& add_bump_cycle(precision_t strength, precision_t width2, MeshBuilder& dst);

  /**
   * Generates triangular grid (Kagome lattice) covering [-0.5,0.5]x[-0.5,0.5] in xz plane.
   */
  MeshBuilder& generate_grid(MeshBuilder& dst);
};

}

#endif
