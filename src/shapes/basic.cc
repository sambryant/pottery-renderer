#include "basic.h"

#include <iostream>
#include <vector>

#include "../core/pmath.h"
#include "../core/triangle.h"
#include "../core/mesh_builder.h"

using namespace std;

MeshBuilder& shapes::make_box(MeshBuilder& mesh)
{
  Vertex* v000 = mesh.add({-1.0/2,-1.0/2,-1.0/2});
  Vertex* v001 = mesh.add({-1.0/2,-1.0/2,+1.0/2});
  Vertex* v010 = mesh.add({-1.0/2,+1.0/2,-1.0/2});
  Vertex* v011 = mesh.add({-1.0/2,+1.0/2,+1.0/2});
  Vertex* v100 = mesh.add({+1.0/2,-1.0/2,-1.0/2});
  Vertex* v101 = mesh.add({+1.0/2,-1.0/2,+1.0/2});
  Vertex* v110 = mesh.add({+1.0/2,+1.0/2,-1.0/2});
  Vertex* v111 = mesh.add({+1.0/2,+1.0/2,+1.0/2});

  // Logic: choose four base points (v000, v011, v101, v110)
  // For each base point, three adjacent points are obtained by flipping each x,y,z coordinate.
  mesh.add(v000, v100, v010); // flip x, flip y
  mesh.add(v000, v010, v001); // flip y, flip z
  mesh.add(v000, v001, v100); // flip z, flip x

  mesh.add(v011, v111, v001);
  mesh.add(v011, v001, v010);
  mesh.add(v011, v010, v111);

  mesh.add(v101, v001, v111);
  mesh.add(v101, v111, v100);
  mesh.add(v101, v100, v001);

  mesh.add(v110, v010, v100);
  mesh.add(v110, v100, v111);
  mesh.add(v110, v111, v010);

  return mesh;
}

MeshBuilder& shapes::make_tetrahedron(MeshBuilder& mesh)
{
  precision_t sqrt2 = sqrt(2);
  precision_t sqrt3 = sqrt(3);
  Vertex* a = mesh.add({2.0 * sqrt2 / 3.0, -1.0/3.0, 0});
  Vertex* b = mesh.add({-sqrt2 / 3.0, -1.0/3.0, sqrt2/sqrt3});
  Vertex* c = mesh.add({-sqrt2/3.0, -1.0/3.0, -sqrt2/sqrt3});
  Vertex* d = mesh.add({0, 1, 0});
  mesh.add(a, b, c);
  mesh.add(b, c, d);
  mesh.add(c, d, a);
  mesh.add(d, a, b);
  return mesh;
}

MeshBuilder& shapes::make_quad(const Vec3& a, const Vec3& b, const Vec3& c, const Vec3& d, MeshBuilder& mesh)
{
  Vertex* va = mesh.add(a);
  Vertex* vb = mesh.add(b);
  Vertex* vc = mesh.add(c);
  Vertex* vd = mesh.add(d);
  mesh.add(va, vb, vc);
  mesh.add(vb, vc, vd);
  return mesh;
}

MeshBuilder& shapes::make_icosahedron(MeshBuilder& mesh)
{
  std::vector<Vertex*> vs1;
  std::vector<Vertex*> vs2;
  Vec3 vr1[5];
  Vec3 vr2[5];
  // Solve some quadratic equations
  precision_t c = cos(2.0*M_PI/5.0);
  precision_t y = c / (1 - c);
  precision_t r = sqrt(1 - y*y);
  for (int i=0; i<5; i++)
  {
    vr1[i][0] = r*cos(i * 2.0*M_PI / 5.0);
    vr1[i][1] = y;
    vr1[i][2] = r*sin(i * 2.0*M_PI / 5.0);
    vr2[i][0] = r*cos((i + 0.5) * 2.0*M_PI / 5.0);
    vr2[i][1] = -y;
    vr2[i][2] = r*sin((i + 0.5) * 2.0*M_PI / 5.0);
  }
  for (int i=0; i<5; i++) {
    vs1.push_back(mesh.add(vr1[i]));
    vs2.push_back(mesh.add(vr2[i]));
  }
  vs1.push_back(mesh.add({0, 1, 0}));
  vs2.push_back(mesh.add({0, -1, 0}));

  for (int i=0; i<5; i++) {
    int i1 = i;
    int i2 = i < 4 ? i+1 : 0;
    mesh.add(vs1[5], vs1[i1], vs1[i2]);
    mesh.add(vs2[5], vs2[i1], vs2[i2]);
    mesh.add(vs1[i], vs1[i2], vs2[i]);
    mesh.add(vs2[i], vs2[i2], vs1[i2]);
  }
  return mesh;
}

MeshBuilder& shapes::make_sphere(uint max_depth, MeshBuilder& mesh, uint depth)
{
  if (depth > max_depth)
    return mesh;
  if (depth == 1)
    shapes::make_icosahedron(mesh);
  else {
    // Bisect every face, replacing each edge by two edges
    vector<array<Vertex*, 3>> new_tris;
    while (mesh.triangles.size() > 0)
    {
      BTriangle* tri = mesh.triangles.back();
      Vertex* a = tri->v[0];
      Vertex* b = tri->v[1];
      Vertex* c = tri->v[2];
      Vertex* ab = mesh.add((a->v + b->v)/2);
      Vertex* bc = mesh.add((b->v + c->v)/2);
      Vertex* ca = mesh.add((c->v + a->v)/2);
      new_tris.push_back({a, ab, ca});
      new_tris.push_back({b, bc, ab});
      new_tris.push_back({c, ca, bc});
      new_tris.push_back({ab, bc, ca});
      mesh.triangles.pop_back();
      delete tri;
    }

    for (auto new_tri : new_tris)
      mesh.add(new_tri[0], new_tri[1], new_tri[2]);

    for (auto v : mesh.vertices)
      v->v = vec3_normalize(v->v);
  }
  return shapes::make_sphere(max_depth, mesh, depth + 1);
}

MeshBuilder& shapes::make_floor(precision_t x1, precision_t x2, precision_t z1, precision_t z2, precision_t y, const Color &c1, const Color &c2, MeshBuilder& floor)
{
  precision_t x0 = (x1 + x2)/2;
  precision_t z0 = (z1 + z2)/2;
  Vertex* v0 = floor.add({x0, y, z0});
  Vertex* v1 = floor.add({x1, y, z1});
  Vertex* v2 = floor.add({x0, y, z1});
  Vertex* v3 = floor.add({x2, y, z1});
  Vertex* v4 = floor.add({x1, y, z0}); // 4
  Vertex* v5 = floor.add({x2, y, z0}); // 5
  Vertex* v6 = floor.add({x1, y, z2}); // 6
  Vertex* v7 = floor.add({x0, y, z2}); // 7
  Vertex* v8 = floor.add({x2, y, z2});
  floor.add(v1, v2, v0);
  floor.add(v2, v3, v0);
  floor.add(v3, v5, v0);
  floor.add(v5, v8, v0);
  floor.add(v8, v7, v0);
  floor.add(v7, v6, v0);
  floor.add(v6, v4, v0);
  floor.add(v4, v1, v0);
  floor.with_color(c1);
  v0->with_color(c2);
  return floor;
}
