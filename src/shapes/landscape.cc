#include "landscape.h"

#include <vector>
#include <random>
#include <cmath>

#include "../core/pmath.h"
#include "../core/mesh_builder.h"
#include "../shapes/tree.h"

using namespace std;
using namespace shapes;

MeshBuilder& LandscapeBuilder::generate(MeshBuilder& dst)
{
  MeshBuilder& hills = *dst.add_child();
  this->generate_grid(hills).scale(size);

  // Add bumps of diminishing strength
  precision_t bump_strength = bump_strength_initial;
  precision_t bump_width2 = bump_width2_initial;

  for (uint i=0; i<num_bump_cycles; i++) {
    this->add_bump_cycle(bump_strength, bump_width2, hills);
    bump_strength *= bump_diminish_factor;
    bump_width2 *= bump_diminish_factor;
  }
  hills.with_color(GREEN);

  add_trees(hills);

  return dst;
}

/**
 * Generates triangular grid (Kagome lattice) covering [-0.5,0.5]x[-0.5,0.5] in xz plane.
 */
MeshBuilder& LandscapeBuilder::generate_grid(MeshBuilder& dst)
{
  uint num_samples = (uint) (size * sample_density);
  float dim_x = 1.0;
  float dim_z = 1.0;
  float inc_x = dim_x / num_samples;
  float inc_z = dim_z / num_samples;

  vector<vector<Vertex*>> v_ptrs;

  float z = - dim_z / 2.0 + inc_z / 2;
  float x0;
  for (uint i = 0; i<num_samples; i++) {
    v_ptrs.push_back(vector<Vertex*>());
    if (i % 2 == 0)
      x0 = 0.0;
    else
      x0 = inc_x / 2;
    for (uint j = 0; j<num_samples; j++) {
      float x = -dim_x / 2.0 + x0 + inc_x * j;
      v_ptrs.back().push_back(dst.add({x, 0, z}));
    }
    z += inc_z;
  }

  // Close enough (missing a few triangles at end)
  for (uint i=0; i<num_samples-1; i++) {
    for (uint j=0; j<num_samples-1; j++) {
      if (i % 2 == 1) {
        dst.add(v_ptrs[i][j], v_ptrs[i+1][j], v_ptrs[i+1][j+1]);
        dst.add(v_ptrs[i][j], v_ptrs[i][j+1], v_ptrs[i+1][j+1]);
      } else {
        dst.add(v_ptrs[i][j], v_ptrs[i][j+1], v_ptrs[i+1][j]);
        dst.add(v_ptrs[i][j+1], v_ptrs[i+1][j], v_ptrs[i+1][j+1]);
      }
    }
  }
  return dst;
}

/**
 * Adds Gaussian bumps to landscape of fixed strength and width.
 */
MeshBuilder& LandscapeBuilder::add_bump_cycle(precision_t strength, precision_t width2, MeshBuilder& dst)
{
  normal_distribution<> dbump{0.0, strength};
  uniform_int_distribution<> dindex (0, dst.vertices.size());
  uint num = static_cast<uint>(size * size * bumps_per_cycle_density);

  for (uint i=0; i<num; i++) {
    float rand_y = dbump(rng_gen);
    int rand_index = dindex(rng_gen);
    Vertex& center = *dst.vertices[rand_index];

    // For each vertex, compute the XZ distance from the center and feed it to a gaussian
    for (auto v : dst.vertices)
    {
      Vec2 dis_xz = {v->v[0] - center.v[0], v->v[2] - center.v[2]};
      float dr2 = dis_xz[0]*dis_xz[0] + dis_xz[1]*dis_xz[1];
      // TODO: Try width^2 ???
      v->v[1] += rand_y * exp(-dr2 / width2);
    }
  }
  return dst;
}

/**
 * Adds some trees to the landscape at a fixed density.
 */
MeshBuilder& LandscapeBuilder::add_trees(MeshBuilder& dst)
{
  normal_distribution<> d_scale{tree_scale, tree_scale_jitter};
  uniform_int_distribution<> d_index (0, dst.vertices.size());

  uint count = static_cast<uint>(size * size * tree_density);
  for (uint i = 0; i < count; i++) {
    int index = d_index(rng_gen);
    Vertex& center = *dst.vertices[index];
    tree_gen.generate(*dst.add_child())
      .scale(d_scale(rng_gen))
      .translate(center.v);
  }
  return dst;
}
