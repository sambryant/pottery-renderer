#include "pot.h"

#include <cmath>
#include <array>
#include <map>
#include <vector>

#include "../core/pmath.h"
#include "../core/color.h"

using namespace std;

using namespace shapes;

precision_t PotFactory::profile1(precision_t y)
{
  precision_t z = 3.0 - y;
  return 1.0 + z * z * z * (-0.01 + 0.01 * z +0.035 * z * z - 0.011 * z * z * z - 0.0005 * z * z * z * z);
}

MeshBuilder& PotFactory::make(MeshBuilder& dst) const
{
  auto vertices = this->get_vertices();
  vector<vector<Vertex*> > vertex_ps;
  for (int i=0; i<this->n_ys; i++) {
    vertex_ps.emplace_back();
    for (int j=0; j<this->n_ths; j++) {
      vertex_ps[i].push_back(dst.add(vertices[i][j]));
    }
  }

  for (int i=0; i<this->n_ys - 1; i++) {
    for (int j=0; j<this->n_ths; j++) {
      int i2 = i + 1;
      int j2 = j == this->n_ths - 1 ? 0 : j + 1;
      dst.add(vertex_ps[i][j], vertex_ps[i][j2], vertex_ps[i2][j2]);
      dst.add(vertex_ps[i][j], vertex_ps[i2][j], vertex_ps[i2][j2]);
    }
  }
  return dst;
}

vector<vector<Vec3>> PotFactory::get_vertices() const
{
  precision_t y_inc = height / ((precision_t) this->n_ys);
  precision_t th_inc = 2.0 * 3.14159 / ((precision_t) this->n_ths);
  vector<vector<Vec3>> vertices;
  for (int i=0; i<this->n_ys; i++) {
    precision_t y = y_inc * i;
    vector<Vec3> vs;
    for (int j=0; j<this->n_ths; j++) {
      precision_t th = j * th_inc;
      precision_t r = pot_func(y);
      vs.push_back({r * cos(th), y, r * sin(th)});
    }
    vertices.push_back(vs);
  }
  return vertices;
}
