#ifndef SHAPES_HOUSE
#define SHAPES_HOUSE

#include "base.h"
#include "../core/mesh_builder.h"
#include "../core/color.h"

namespace shapes
{

  class HouseBuilder
  {
  public:
    precision_t width = 10.0;
    precision_t height = 4.0;
    precision_t depth = 7.0;
  public:
    HouseBuilder() {}
  public:
    void generate(MeshBuilder& dst) const;
  protected:
    MeshBuilder* generate_window(MeshBuilder* dst, precision_t height, precision_t width) const;
    MeshBuilder* generate_roof(MeshBuilder* dst) const;
  };

}

#endif