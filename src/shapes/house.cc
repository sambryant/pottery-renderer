#include "house.h"
#include "base.h"

#include <array>
#include <vector>
#include <iostream>
#include <cstdlib>

#include "../core/mesh_builder.h"
#include "../core/color.h"

using namespace std;

namespace shapes
{

MeshBuilder* HouseBuilder::generate_window(MeshBuilder* dst, precision_t height, precision_t width) const
{
  auto v0 = dst->add({-width/2, -height/2, 0});
  auto v1 = dst->add({-width/2, +height/2, 0});
  auto v2 = dst->add({+width/2, -height/2, 0});
  auto v3 = dst->add({+width/2, +height/2, 0});
  dst->add(v0, v1, v2);
  dst->add(v1, v2, v3);
  dst->with_opacity(false)
      .with_color(BLUE);
  return dst;
}

MeshBuilder* HouseBuilder::generate_roof(MeshBuilder* dst) const
{
  auto hang = 0.1 * this->width;
  auto angle = 0.4;
  auto w0 = this->width / 2;
  auto d0 = this->depth;
  auto w = w0 / cos(angle) + hang;
  auto d = d0 + hang;
  auto v0 = dst->add({-w/2, 0, -d/2});
  auto v1 = dst->add({-w/2, 0, +d/2});
  auto v2 = dst->add({+w/2, 0, -d/2});
  auto v3 = dst->add({+w/2, 0, +d/2});
  dst->add(v0, v1, v2);
  dst->add(v1, v2, v3);
  auto r1 = get_rotation_z(-0.4);
  dst->with_opacity(false)
      .with_color(BROWN)
      .translate({w/2, 0, 0})
      .translate({-(w - hang), 0, 0})
      .transform(r1)
      .translate({w0, this->height*1.1, 0})
      ;
  return dst;
}

void HouseBuilder::generate(MeshBuilder &dst) const
{
  precision_t w = this->width;
  precision_t h = this->height;
  precision_t d = this->depth;

  MeshBuilder* walls = dst.add_child();
  MeshBuilder* windows = dst.add_child();
  MeshBuilder* roof = dst.add_child();

  precision_t win_width = (1.5 / 11) * w;
  precision_t win_height = (2.0/3.0) * h;
  precision_t win_x = (2.5 / 11) * w;
  precision_t win_y = h/2;
  this->generate_window(windows->add_child(), win_height, win_width)
      ->translate({-win_x, win_y, -d/2});
  this->generate_window(windows->add_child(), win_height, win_width)
      ->translate({+win_x, win_y, -d/2});
  this->generate_window(windows->add_child(), win_height, win_width)
      ->translate({-win_x, win_y, +d/2});
  this->generate_window(windows->add_child(), win_height, win_width)
      ->translate({+win_x, win_y, +d/2});

  MeshBuilder* zwalls[4];
  for (int i=0; i<4; i++)
  {
    MeshBuilder* w1 = walls->add_child();
    precision_t xs[] = {0.0, win_x - win_width/2, win_x + win_width/2, w / 2};
    precision_t ys[] = {0.0, win_y - win_height/2, win_y + win_height/2, h};
    Vertex* vps[4][4];
    for (int i=0; i<4; i++) {
      for (int j=0; j<4; j++) {
        vps[i][j] = w1->add({xs[i], ys[j], -d/2});
      }
    }
    for (int i=0; i<3; i++) {
      for (int j=0; j<3; j++) {
        if (i==1 && j==1)
          continue;
        w1->add(vps[i][j], vps[i+1][j], vps[i+1][j+1]);
        w1->add(vps[i][j], vps[i][j+1], vps[i+1][j+1]);
      }
    }
    zwalls[i] = w1;
  }
  zwalls[1]->flip_x();
  zwalls[2]->flip_z();
  zwalls[3]->flip_x().flip_z();

  MeshBuilder* xwalls[4];
  auto xs = {-w/2, w/2};
  for (auto x : xs)
  {
    MeshBuilder* w1 = walls->add_child();
    precision_t zs[] = {-d/2, d/2};
    precision_t ys[] = {0.0, h};
    auto v0 = w1->add({x, 0, -d/2});
    auto v1 = w1->add({x, 0, +d/2});
    auto v2 = w1->add({x, h, -d/2});
    auto v3 = w1->add({x, h, +d/2});
    w1->add(v0, v1, v2);
    w1->add(v1, v2, v3);
  }
  walls->with_color(RED);

  this->generate_roof(roof->add_child());
  this->generate_roof(roof->add_child())->flip_x();
  roof->with_color(BROWN)
       .with_opacity(true);

}

}
