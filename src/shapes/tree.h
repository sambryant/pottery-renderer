#ifndef SHAPES_TREE
#define SHAPES_TREE

#include "base.h"

#include <random>

#include "../core/color.h"
#include "../core/mesh_builder.h"
#include "../core/util.h"

namespace shapes {

  /**
   * Function defining how a trunk's width "tapers" along the axis of the trunk.
   */
  using TaperFunc = precision_t(*)(precision_t);

  /**
   * Class used to construct a MeshBuilder for a fractionally generated tree.
   */
  class TreeBuilder {

  public:
    /**
     * Determines how the vertex randomizer samples random values.
     */
    enum class VertexJitterDist {
      normal,
      uniform
    };

    /**
     * Determines how the vertex randomizer transforms each vertex.
     */
    enum class VertexJitterMethod {
      // Indicates that the x and z coordinate of each vertex should each be noised separately.
      xz,
      // Indidcates that the radial coordinate of each vertex should be noised.
      radius
    };

    /**
     * Class used to generate the vertices along a trunk, particularly with respect to noise.
     */
    class VertexGen
    {
    public:
      TreeBuilder::VertexJitterMethod method = TreeBuilder::VertexJitterMethod::radius;
      TreeBuilder::VertexJitterDist dist = TreeBuilder::VertexJitterDist::uniform;
      precision_t strength = 0.09;
    protected:
      // We have to make both because they don't share a parent class
      mutable std::uniform_real_distribution<precision_t> *uniform = NULL;
      mutable std::normal_distribution<precision_t> *normal = NULL;
    public:
      VertexGen()
      {
        this->reset_dist();
      }
      ~VertexGen()
      {
        this->clear_dist();
      }
    public:
      Vec3 gen(std::mt19937& rng_gen, precision_t theta, precision_t y, precision_t radius=1.0) const;
      void reset_dist();
    protected:
      void clear_dist();
      precision_t gen(std::mt19937& rng_gen) const;
    };

    /**
     * The default tapering function
     */
    static precision_t taper1(precision_t y_frac);

  public:
    float height_width_ratio = 8.0;
    float sampling_rate = 0.4;
    int color_jitter = 10.0;
    Color base_color = BROWN;
    uint max_depth = 4;
    float branch_height_ratio = 0.8;
    float branching_angle = 5 * M_PI / 16.0;
  protected:
    unsigned seed;
    TaperFunc taper_func = &TreeBuilder::taper1;
    VertexGen vertex_gen;
    mutable std::mt19937 rng_gen;
  public:
    TreeBuilder(unsigned int seed_) : seed(seed_)
    {
      this->rng_gen = std::mt19937{seed};
    }
    TreeBuilder() : seed(core::get_seed())
    {
      this->rng_gen = std::mt19937{seed};
    }
  public:
    TreeBuilder& with_seed(unsigned seed) {
      this->seed = seed;
      this->rng_gen = std::mt19937{seed};
      return *this;
    }
    TreeBuilder& with_height(float height_width_ratio) {
      this->height_width_ratio = height_width_ratio;
      return *this;
    }
    TreeBuilder& with_sampling_rate(float sampling_rate) {
      this->sampling_rate = sampling_rate;
      return *this;
    }
    TreeBuilder& with_color(Color color) {
      this->base_color = color;
      return *this;
    }
    TreeBuilder& with_color_jitter(float color_jitter) {
      this->color_jitter = color_jitter;
      return *this;
    }
    TreeBuilder& with_vertex_jitter(VertexJitterMethod method, VertexJitterDist dist, precision_t strength)
    {
      this->vertex_gen.method = method;
      this->vertex_gen.dist = dist;
      this->vertex_gen.strength = strength;
      this->vertex_gen.reset_dist();
      return *this;
    }
    TreeBuilder& with_depth(uint depth)
    {
      this->max_depth = depth;
      return *this;
    }
    MeshBuilder& generate(MeshBuilder& dst) const;
  protected:
    /**
     * Recursive branch generation function. Creates a branch of the given size, then recursively
     * creates its children and joins them to the branch at an angle.
     */
    MeshBuilder& generate(uint depth, precision_t size, MeshBuilder& dst) const;

    /**
     * Function which handles computing the ObjectMesh for a single (non-recursive) branch.
     */
    MeshBuilder& generate_branch(MeshBuilder& dst) const;

    /**
     * Colors the vertices of the given tree using a base color and randomized noise.
     */
    MeshBuilder& apply_color(MeshBuilder& dst) const;
  };


};

#endif
