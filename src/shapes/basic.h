#ifndef SHAPES_BASIC
#define SHAPES_BASIC

#include "base.h"

#include "../core/pmath.h"
#include "../core/mesh_builder.h"

namespace shapes {

  /**
   * Constructs unit box centered at the origin *within* the given MeshBuilder.
   */
  MeshBuilder& make_box(MeshBuilder& mesh);

  /**
   * Constructs a unit tetrahedron centered at the origin *within* the given MeshBuilder.
   */
  MeshBuilder& make_tetrahedron(MeshBuilder& mesh);

  /**
   * Constructs a quadralateral. If the four points aren't aligned on a plane, output is unclear.
   */
  MeshBuilder& make_quad(const Vec3& a, const Vec3& b, const Vec3& c, const Vec3& d, MeshBuilder& mesh);

  /**
   * Constructs a unit icosahedron centered at the origin *within* the given MeshBuilder.
   */
  MeshBuilder& make_icosahedron(MeshBuilder& mesh);

  /**
   * Constructs a unit sphere approximation centered at the origin *within* the given MeshBuilder.
   *
   * The sphere is constructed by starting with an icosahedron and recursively doing bisect/project
   * on each triangle.
   *
   * TODO: This method currently creates duplicate vertices. We should fix this by definining the
   * vertex collapse method.
   */
  MeshBuilder& make_sphere(uint max_depth, MeshBuilder& mesh, uint depth=1);

  /**
   * A testing method that generates a "floor" composed of 4 sub-squares. The center vertex gets
   * a unique color.
   */
  MeshBuilder& make_floor(precision_t x1, precision_t x2, precision_t z1, precision_t z2, precision_t y, const Color &c1, const Color &c2, MeshBuilder& floor);

}

#endif
