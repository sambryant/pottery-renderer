#ifndef SHAPES_POT
#define SHAPES_POT

#include "base.h"

#include <vector>

#include "../core/color.h"
#include "../core/mesh_builder.h"

namespace shapes {

/**
 * Function defining a pot's profile.
 */
using PotFunc = precision_t(*)(precision_t);


class PotFactory
{
public:
  static precision_t profile1(precision_t y);
public:
  const int n_ths;
  const int n_ys;
  float height = 3.0;
  PotFunc pot_func = &PotFactory::profile1;
public:
  PotFactory(int nt, int ny): n_ths(nt), n_ys(ny) {}
public:
  PotFactory& with_height(precision_t height)
  {
    this->height = height;
    return *this;
  }
  PotFactory& with_profile(PotFunc pot_func)
  {
    this->pot_func = pot_func;
    return *this;
  }
  MeshBuilder& make(MeshBuilder& mesh) const;
private:
  std::vector<std::vector<Vec3>> get_vertices() const;
};

}

#endif
