#include "tree.h"

#include <vector>
#include <cmath>
#include <map>
#include <random>
#include <cstdlib>

#include "base.h"

#include "../core/color.h"
#include "../core/mesh_builder.h"
#include "../core/vertex.h"

using namespace std;

namespace shapes {


precision_t TreeBuilder::taper1(precision_t y_frac)
{
  if (y_frac < 0.3)
    return 1.0;
  auto y_rel = (y_frac - 0.3)/(1 - 0.3);
  return sqrt(1 - y_rel*y_rel);
}

MeshBuilder& TreeBuilder::generate(uint depth, precision_t size, MeshBuilder& branch) const
{
  // Add vertices for this branch.
  this->generate_branch(branch).scale(size);

  // Create the branches children and join them at correct angle.
  if (depth < this->max_depth) {
    Vec3 traslate = {0.0, this->branch_height_ratio * this->height_width_ratio * size, 0.0};

    auto r1 = get_rotation_z(+this->branching_angle);
    this->generate(depth + 1, size / 2, *branch.add_child())
      .transform(r1)
      .translate(traslate);

    auto r2 = get_rotation_z(-this->branching_angle);
    this->generate(depth + 1, size / 2, *branch.add_child())
      .transform(r2)
      .translate(traslate);
  }
  return branch;
}

MeshBuilder& TreeBuilder::generate(MeshBuilder &mesh) const
{
  this->generate(1, 1.0, mesh);
  return mesh;
}

MeshBuilder& TreeBuilder::generate_branch(MeshBuilder& mesh) const
{
  int num_angles = (int) ceil(2.0 * 3.1416 / this->sampling_rate);
  float d_theta = 2.0 * 3.1416 / num_angles;
  int num_slices = (int) ceil(this->height_width_ratio / this->sampling_rate);
  float d_slice = this->height_width_ratio / num_slices;

  // Generate vertices for a cylinder with staggered columns (Kagome lattice)
  float theta, y, r;
  vector<vector<Vertex*>> vertex_ptrs;
  for (int i=0; i<num_slices; i++) {
    y = i * d_slice;
    r = this->taper_func(y / this->height_width_ratio);
    vertex_ptrs.emplace_back();
    for (int j=0; j<num_angles; j++) {
      theta = (((float) j + (i % 2 == 2) * 0.5) * d_theta);
      vertex_ptrs[i].push_back(mesh.add(this->vertex_gen.gen(rng_gen, theta, y, r)));
    }
  }

  // Add triangles
  for (int i=0; i<num_slices - 1; i++) {
    int i2 = i + 1;
    for (int j=0; j<num_angles; j++) {
      int j2 = j == num_angles - 1 ? 0 : j + 1;
      mesh.add(vertex_ptrs[i][j], vertex_ptrs[i][j2], vertex_ptrs[i2][j]);
      mesh.add(vertex_ptrs[i2][j2], vertex_ptrs[i][j2], vertex_ptrs[i2][j]);
    }
  }

  // Close tree on top by connecting top ring of vertices to single point
  {
    auto y_top = num_slices * d_slice;
    auto top_vertex = mesh.add({0.0, y_top, 0.0});
    auto i = num_slices - 1;
    for (int j=0; j<num_angles; j++) {
      int j2 = j == num_angles - 1 ? 0 : j + 1;
      mesh.add(vertex_ptrs[i][j], vertex_ptrs[i][j2], top_vertex);
    }
  }

  // Close tree on bottom
  {
    auto bot_vertex = mesh.add({0.0, 0.0, 0.0});
    for (int j=0; j<num_angles; j++) {
      int j2 = j == num_angles - 1 ? 0 : j + 1;
      mesh.add(vertex_ptrs[0][j], vertex_ptrs[0][j2], bot_vertex);
    }
  }
  return this->apply_color(mesh);
}

uint8_t int_to_clr(int i)
{
  return (uint8_t) min(max(i, 0), 255);
}

MeshBuilder& TreeBuilder::apply_color(MeshBuilder& dst) const
{
  uniform_int_distribution<> distr(-this->color_jitter, this->color_jitter);

  for (auto vertex : dst.vertices) {
    auto off = distr(this->rng_gen);
    vertex->color[0] = int_to_clr(this->base_color[0] + off);
    vertex->color[1] = int_to_clr(this->base_color[1] + off);
    vertex->color[2] = int_to_clr(this->base_color[2] + off);
  }
  return dst;
}

void TreeBuilder::VertexGen::clear_dist()
{
  if (this->normal) {
    delete this->normal;
    this->normal = NULL;
  }
  if (this->uniform) {
    delete this->uniform;
    this->uniform = NULL;
  }
}

void TreeBuilder::VertexGen::reset_dist()
{
  this->clear_dist();
  switch(this->dist)
  {
    case TreeBuilder::VertexJitterDist::uniform:
      this->uniform = new uniform_real_distribution<>(-this->strength, this->strength);
      break;
    case TreeBuilder::VertexJitterDist::normal:
      this->normal = new normal_distribution<>(this->strength, this->strength);
      break;
    default:
      cerr << "Invalid vertex jitter distribution type " << ((int) this->dist) << '\n';
      exit(1);
  }
}

precision_t TreeBuilder::VertexGen::gen(std::mt19937& rng_gen) const
{
  switch(this->dist)
  {
    case TreeBuilder::VertexJitterDist::uniform:
      return (*this->uniform)(rng_gen);
    case TreeBuilder::VertexJitterDist::normal:
      return (*this->normal)(rng_gen);
    default:
      cerr << "Invalid vertex jitter distribution type " << ((int) this->dist) << '\n';
      exit(1);
  }
}

Vec3 TreeBuilder::VertexGen::gen(std::mt19937& rng_gen, precision_t theta, precision_t y, precision_t radius) const
{
  if (this->method == TreeBuilder::VertexJitterMethod::xz)
    return {radius*(cos(theta) + this->gen(rng_gen)), y, radius*(sin(theta) + this->gen(rng_gen))};
  else if (this->method == TreeBuilder::VertexJitterMethod::radius) {
    auto r = radius * (1.0 + this->gen(rng_gen));
    return { r*cos(theta), y, r*sin(theta)};
  } else {
    cerr << "Invalid vertex jitter distribution type " << ((int) this->dist) << '\n';
    exit(1);
  }
}

}
