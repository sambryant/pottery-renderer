#ifndef PERFORMANCE
#define PERFORMANCE

#include <iostream>
#include <vector>
#include <string>
#include <chrono>
#include <map>
#include <cstdlib>

using perf_time_t = std::chrono::time_point<std::chrono::high_resolution_clock>;

class Performance
{
public:
  std::map<std::string, size_t> name_to_index;
  std::vector<std::string> names;
  std::vector<long int> times;
public:
  void report(unsigned int num_trials);
  void log(const char *name, perf_time_t t1, perf_time_t t2);
  long int get_total_average(unsigned int num_trials);
};

#endif
