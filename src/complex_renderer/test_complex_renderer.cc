#include "test_complex_renderer.h"

#include <optional>
#include <memory>
#include <limits>
#include <map>
#include <vector>
#include <cstdlib>
#include <chrono>

#include "../scene/scene.h"
#include "../scene/screen.h"
#include "../scene/builder.h"
#include "../core/pmath.h"
#include "../core/color.h"
#include "complex_renderer.h"
#include "complex_renderer_threaded.h"
#include "../shapes/basic.h"
#include "../shapes/pot.h"
#include "../core/output.h"

#include "../examples/scenes.h"

using namespace std;
using namespace complex_renderer;

namespace complex_renderer::tests {

  // void test_1(unsigned int subbox_count=1)
  // {
  //   string output{"output/CR-tetrahedron.ppm"};
  //   cout << "Running complex renderer test 1\n";

  //   Renderer renderer(examples::scenes::triangle_simple(), subbox_count, subbox_count);

  //   auto start = std::chrono::high_resolution_clock::now();
  //   renderer.render();
  //   auto end = std::chrono::high_resolution_clock::now();
  //   auto duration = chrono::duration_cast<chrono::milliseconds>(end - start);
  //   cout << "\tFinished rendering in " << duration.count() << "ms\n";
  //   core::save_render(renderer.get_pixmap(), output);
  // }

  // void test_4(unsigned int subbox_count=16, unsigned int thread_count=4)
  // {
  //   string output{"output/CR-pot-scene.ppm"};
  //   RendererThreaded renderer(examples::scenes::complex_pots(600, 400, 90.0, false), subbox_count, subbox_count, thread_count);

  //   cout << "Running complex renderer test 4 (pot with scene multi-threaded)\n";
  //   cout << "\tNum Subboxes: " << subbox_count << '\n'
  //        << "\tNum threads : " << thread_count << '\n'
  //        << "\tNum objects : " << renderer.scene->objects.size() << '\n';

  //   auto start = std::chrono::high_resolution_clock::now();
  //   renderer.render();
  //   auto end = std::chrono::high_resolution_clock::now();
  //   auto duration = chrono::duration_cast<chrono::milliseconds>(end - start);

  //   cout << "\tFinished rendering in " << duration.count() << "ms\n";
  //   core::save_render(renderer.get_pixmap(), output);
  // }

  // void test_panels(unsigned int subbox_count=16, unsigned int resolution=400, unsigned int thread_count=4)
  // {
  //   string output{"output/CR-panels.ppm"};
  //   cout << "Running complex renderer test 5 (three panels)\n";

  //   RendererThreaded renderer(examples::scenes::panels(), subbox_count, subbox_count, thread_count);

  //   auto start = std::chrono::high_resolution_clock::now();
  //   renderer.render();
  //   auto end = std::chrono::high_resolution_clock::now();
  //   auto duration = chrono::duration_cast<chrono::milliseconds>(end - start);
  //   cout << "\tFinished rendering in " << duration.count() << "ms\n";
  //   core::save_render(renderer.get_pixmap(), output);
  // }

  // void test_performance(uint subbox_count, uint res_x=600, uint res_y=400, uint num_threads=4)
  // {
  //   string output{"output/CR-performance.ppm"};
  //   cout << "\nRunning complex renderer performance test\n";

  //   RendererThreaded renderer(examples::scenes::complex_pots(res_x, res_y, 90.0, false), subbox_count, subbox_count, num_threads);

  //   Performance perf;
  //   cout << "\tResolution: " << renderer.scene->screen.width << " x " << renderer.scene->screen.height << '\n'
  //        << "\t# Subbox  : " << subbox_count << '\n'
  //        << "\t# threads : " << num_threads << '\n'
  //        << "\t# Objects : " << renderer.scene->size() << '\n';

  //   long int total_time = 0;
  //   int ntrials = 200;
  //   for (int i=0; i<ntrials; i++) {
  //     auto start = std::chrono::high_resolution_clock::now();
  //     renderer.render_perf(perf);
  //     auto end = std::chrono::high_resolution_clock::now();
  //     auto duration = chrono::duration_cast<chrono::microseconds>(end - start);
  //     total_time += duration.count();
  //   }
  //   core::save_render(renderer.buffer->pixmap, output);
  //   cout << "Finished performance test\n";
  //   cout << "\tnumber trials  : " << ntrials << '\n';
  //   cout << "\ttotal time (us): " << total_time << '\n';
  //   cout << "\ttime/frame     : " << (total_time / ntrials) << '\n';
  //   cout << "\tframerate      : " << (1000000 / (total_time / ntrials)) << '\n';
  //   cout << "\nPerformance breakdown\n";
  //   perf.report(ntrials);
  // }
}

void complex_renderer::run()
{
  // complex_renderer::tests::test_1();
  // complex_renderer::tests::test_4();
  // complex_renderer::tests::test_panels();
  // complex_renderer::tests::test_performance(64, 1000, 1000, 6);
}

