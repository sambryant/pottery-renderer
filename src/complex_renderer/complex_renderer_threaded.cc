#include <optional>
#include <memory>
#include <limits>
#include <map>
#include <vector>
#include <cstdlib>
#include <thread>

#include "../performance.h"
#include "complex_renderer_threaded.h"
#include "complex_renderer.h"
#include "../scene/scene.h"
#include "../core/pmath.h"

// #define MEASURE_PERF

using namespace std;

using namespace complex_renderer;

void RendererThreaded::ray_trace(bool use_map, const ObjMap &tri_map)
{
  vector<thread> all_threads;
  for (unsigned int ii=0; ii<this->max_num_threads; ii++) {
    all_threads.push_back(thread([this, ii, tri_map, use_map] {this->ray_trace_inner(use_map, tri_map, ii, this->max_num_threads);}));
  }
  for (unsigned int ii=0; ii<this->max_num_threads; ii++) {
    all_threads[ii].join();
  }
}

void RendererThreaded::set_thread_count(unsigned int max_num_threads)
{
  this->max_num_threads = max_num_threads;
}
