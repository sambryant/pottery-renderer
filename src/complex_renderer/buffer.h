#ifndef COMPLEX_RENDERER_BUFFER
#define COMPLEX_RENDERER_BUFFER

#include <vector>
#include <map>
#include <optional>
#include <iostream>
#include <exception>

#include "../performance.h"
#include "../scene/scene.h"
#include "../scene/screen.h"
#include "../core/pmath.h"
#include "../core/pixmap.h"
#include "../core/color.h"
#include "base.h"

namespace complex_renderer
{

class Buffer
{
public:
  size_t num_valid;
  core::PixMap pixmap;
  /// The background color to draw when no object is displayed.
  Color background;
public:
  Buffer(size_t n_objects, const Screen &screen) : pixmap(screen.width, screen.height) {}
public:
  friend class Renderer;
};

}
#endif
