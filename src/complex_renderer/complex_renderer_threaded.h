#ifndef COMPLEX_RENDERER_THREADED
#define COMPLEX_RENDERER_THREADED

#include <vector>
#include <map>
#include <optional>
#include <iostream>
#include <memory>

#include "../performance.h"
#include "../scene/scene.h"
#include "../core/pmath.h"
#include "complex_renderer.h"

namespace complex_renderer
{

class RendererThreaded : public Renderer
{
public:
  unsigned int max_num_threads;
public:
  RendererThreaded(
    std::shared_ptr<Scene> scene_,
    unsigned int n_viewport_rows,
    unsigned int n_viewport_cols,
    unsigned int max_num_threads) : Renderer(scene_, n_viewport_rows, n_viewport_cols)
  {
    this->set_thread_count(max_num_threads);
  }
  void set_thread_count(unsigned int max_num_threads);
protected:
  void ray_trace(bool use_map, const ObjMap &tri_map);
};

}

#endif
