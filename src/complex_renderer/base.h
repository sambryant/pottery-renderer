#ifndef COMPLEX_RENDERER_BASE
#define COMPLEX_RENDERER_BASE

#include "../core/triangle.h"

namespace complex_renderer {

using box_index_t = unsigned int;

using obj_index_t = int;

using sub_box_t = std::array<box_index_t, 2>;

using ObjMap = std::map<sub_box_t, std::vector<Triangle*> >;

const std::array<Vec3, 3> TRAN_0 = {{{0,0,0}, {0,0,0}, {0,0,0}}};
const std::array<Vec2, 3> PROJ_0 = {{{0,0}, {0,0}, {0,0}}};
const Color COLOR_0 = {0, 0, 0};
const std::array<unsigned int, 4> BOUNDS_0 = {0, 0, 0, 0};

}

#endif
