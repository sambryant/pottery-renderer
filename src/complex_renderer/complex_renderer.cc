#include "complex_renderer.h"

#include <optional>
#include <memory>
#include <limits>
#include <map>
#include <array>
#include <vector>
#include <cstdlib>
#include <thread>
#include <chrono>
#include <exception>

#include "../performance.h"
#include "../scene/scene.h"
#include "../scene/screen.h"
#include "../core/pmath.h"
#include "../core/color.h"
#include "base.h"

using namespace std;

using namespace complex_renderer;

void Renderer::initialize()
{
  // Determine spacing in viewport between sub-boxes used to optimize collision detection.
  this->col_size = ceil(((float) this->scene->screen.width) / this->n_vp_cols);
  this->row_size = ceil(((float) this->scene->screen.height) / this->n_vp_rows);

  // Initialize the computation/rendering buffer based on number of objects and pixels.
  size_t n_objs = this->scene->get_num_triangles();
  this->buffer.reset(new Buffer(n_objs, this->scene->screen));
}

void Renderer::render_perf(Performance &perf)
{
  // this->scene->start_render_loop();
  // auto t1 = std::chrono::high_resolution_clock::now();
  // this->compute_projected_triangles();
  // auto t2 = std::chrono::high_resolution_clock::now();
  // this->compute_shading();
  // auto t3 = std::chrono::high_resolution_clock::now();
  // ObjMap box_map;
  // bool use_map = this->compute_triangle_subboxes(box_map);
  // auto t4 = std::chrono::high_resolution_clock::now();
  // this->ray_trace(use_map, box_map);
  // auto t5 = std::chrono::high_resolution_clock::now();

  // perf.log("proj triangles", t1, t2);
  // perf.log("comp shading", t2, t3);
  // perf.log("subbox map", t3, t4);
  // perf.log("ray trace", t4, t5);
}

void Renderer::render()
{
  // this->scene->start_render_loop();
  // this->compute_projected_triangles();
  // this->compute_shading();
  // ObjMap box_map;
  // bool use_map = this->compute_triangle_subboxes(box_map);
  // this->ray_trace(use_map, box_map);
}

void Renderer::compute_projected_triangles()
{
  // size_t num_valid = 0;
  // for (auto &obj: this->scene->objects) {
  //   this->scene->process_object(obj);
  //   if (obj.valid) {
  //     num_valid++;
  //   }
  // }
  // this->buffer->num_valid = num_valid;
}

void Renderer::compute_shading()
{
  // this->buffer->background = BLACK;
  // if (this->scene->lighting != nullptr) {
  //   for (auto &obj: this->scene->objects) {
  //     this->scene->engine.shade_object(obj, this->scene->lighting);
  //   }
  // }
}

bool Renderer::compute_triangle_subboxes(ObjMap &tri_map)
{
  // if (this->n_vp_cols == 1 && this->n_vp_rows == 1) {
  //   return false;
  // } else {
  //   for (auto &obj: this->scene->objects) {
  //     if (obj.valid) {
  //       this->compute_triangle_bounds(obj);
  //     }
  //   }
  //   this->build_box_map(tri_map);
  //   return true;
  // }
  return false;
}

// sub_box_t Renderer::determine_vertex_box(const Vec2 &screen)
// {
//   int i = (int) (screen[0] / this->col_size);
//   int j = (int) (screen[1] / this->row_size);
//   return {(uint) max(i, 0), (uint) max(j, 0)};
// }

// void Renderer::compute_triangle_bounds(Triangle &obj)
// {
//   unsigned int min_x = numeric_limits<unsigned int>::max();
//   unsigned int max_x = numeric_limits<unsigned int>::min();
//   unsigned int min_y = numeric_limits<unsigned int>::max();
//   unsigned int max_y = numeric_limits<unsigned int>::min();

//   for (unsigned int i=0; i<3; i++) {
//     auto box = this->determine_vertex_box(obj.vertices[i].v_screen);
//     min_x = min(min_x, box[0]);
//     max_x = max(max_x, box[0]);
//     min_y = min(min_y, box[1]);
//     max_y = max(max_y, box[1]);
//   }
//   obj.box_bounds[0] = min_x;
//   obj.box_bounds[1] = max_x;
//   obj.box_bounds[2] = min_y;
//   obj.box_bounds[3] = max_y;
// }

// void Renderer::build_box_map(ObjMap &box_map)
// {
//   for (unsigned int i=0; i<this->n_vp_cols + 1; i++) {
//     for (unsigned int j=0; j<this->n_vp_rows + 1; j++) {
//       box_map[{i, j}] = vector<Triangle*>();
//     }
//   }

//   for (auto &obj: this->scene->objects) {
//     if (obj.valid) {
//       for (uint i=obj.box_bounds[0]; i<= obj.box_bounds[1]; i++) {
//         for (uint j=obj.box_bounds[2]; j<= obj.box_bounds[3]; j++) {
//           box_map[{i, j}].push_back(&obj);
//         }
//       }
//     }
//   }
// }

void Renderer::ray_trace_inner(bool use_map, const ObjMap &tri_map, size_t py0, size_t y_stride)
{
  // const uint w = this->scene->screen.width;
  // const uint h = this->scene->screen.height;
  // for (uint py = py0; py < h; py += y_stride) {
  //   for (uint px = 0; px < w; px++) {
  //     Vec2 point = {(precision_t) px, (precision_t) py};
  //     vector<Triangle*> objects;
  //     vector<Vec3> collisions;
  //     Vec3 buf;
  //     if (use_map) {
  //       sub_box_t box_ind = {px / this->col_size, py / this->row_size};
  //       const vector<Triangle*> &objs = tri_map.at(box_ind);
  //       for (Triangle* obj: objs) {
  //         if (this->reverse_project_detect_to(obj, point, buf)) {
  //           objects.push_back(obj);
  //           collisions.push_back(buf);
  //         }
  //       }
  //     } else {
  //       for (Triangle& obj: this->scene->objects) {
  //         if (this->reverse_project_detect_to(&obj, point, buf)) {
  //           objects.push_back(&obj);
  //           collisions.push_back(buf);
  //         }
  //       }
  //     }
  //     Triangle* obj = this->render_point(objects, collisions);
  //     this->write_pixel(px, py, obj);
  //   }
  // }
}

void Renderer::ray_trace(bool use_map, const ObjMap &tri_map)
{
  this->ray_trace_inner(use_map, tri_map, 0, 1);
}


// bool Renderer::reverse_project_detect_to(const Triangle *obj, const Vec2 &screen_point, Vec3 &dst)
// {
//   const Vec2 &rel_bp = obj->vertices[1].v_screen - obj->vertices[0].v_screen;
//   const Vec2 &rel_cp = obj->vertices[2].v_screen - obj->vertices[0].v_screen;
//   const Vec2 &rel_pp = screen_point - obj->vertices[0].v_screen;
//   double denom = rel_bp[1] * rel_cp[0] - rel_bp[0] * rel_cp[1];
//   double t1 = -(rel_pp[0]*rel_cp[1] - rel_pp[1]*rel_cp[0]) / denom;
//   double t2 = (rel_pp[0]*rel_bp[1] - rel_pp[1]*rel_bp[0]) / denom;
//   if (t1 < 0.0 || t2 < 0.0 || (t1 + t2) > 1.0) {
//     return false;
//   } else {
//     for (int i=0; i<3; i++) {
//       dst[i] = obj->vertices[0].v[i]
//         + t1 * (obj->vertices[1].v[i] - obj->vertices[0].v[i])
//         + t2 * (obj->vertices[2].v[i] - obj->vertices[0].v[i]);
//     }
//     return true;
//   }
// }

// Triangle* Renderer::render_point(const std::vector<Triangle*> &objects, const std::vector<Vec3> &collisions)
// {
//   if (collisions.size() == 0) {
//     return NULL;
//   } else if (collisions.size() == 1) {
//     return objects[0];
//   } else {
//     double min_distance = numeric_limits<double>::max();
//     Triangle* min_collision = NULL;
//     for (size_t i=0; i<collisions.size(); i++) {
//       double dis = vec3_distance2(this->scene->camera->pos, collisions[i]);
//       if (dis < min_distance) {
//         min_distance = dis;
//         min_collision = objects[i];
//       }
//     }
//     return min_collision;
//   }
// }
