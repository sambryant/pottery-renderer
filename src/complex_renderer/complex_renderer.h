#ifndef COMPLEX_RENDERER
#define COMPLEX_RENDERER

#include <vector>
#include <map>
#include <optional>
#include <iostream>
#include <exception>

#include "../performance.h"
#include "../scene/scene.h"
#include "../scene/screen.h"
#include "../core/pmath.h"
#include "../core/color.h"
#include "../core/renderer.h"
#include "../core/pixmap.h"
#include "base.h"
#include "buffer.h"

namespace complex_renderer
{

class Renderer : public core::BaseRenderer
{
public:
  std::unique_ptr<Buffer> buffer;
  std::shared_ptr<Scene> scene;
  unsigned int n_vp_rows;
  unsigned int n_vp_cols;
protected:
  unsigned int row_size;
  unsigned int col_size;
public:
  Renderer(std::shared_ptr<Scene> scene_, unsigned int n_viewport_rows, unsigned int n_viewport_cols) : scene(scene_), n_vp_rows(n_viewport_rows), n_vp_cols(n_viewport_cols) {
    this->initialize();
  }
public:
  inline core::PixMap &get_pixmap() const
  {
    return this->buffer->pixmap;
  }
  inline std::shared_ptr<Scene> get_scene() const {
    return this->scene;
  }
  void render();
  void render_perf(Performance &perf);
protected:
  void initialize();
  void ray_trace_inner(bool use_map, const ObjMap &tri_map, size_t py0, size_t y_stride);
  void compute_projected_triangles();
  void compute_shading();
  bool compute_triangle_subboxes(ObjMap &tri_map);
  virtual
  void ray_trace(bool use_map, const ObjMap &tri_map);
  inline
  void write_pixel(uint px, uint py, Triangle* obj) {
    // if (obj != NULL) {
    //   this->buffer->pixmap.draw(obj->shading, px, py);
    // } else {
    //   this->buffer->pixmap.draw(this->buffer->background, px, py);
    // }
  }
  // bool reverse_project_detect_to(const Triangle *triangle, const Vec2 &proj_point, Vec3 &dst);
  // Triangle* render_point(const std::vector<Triangle*> &object, const std::vector<Vec3> &collisions);
  // sub_box_t determine_vertex_box(const Vec2 &projected);
  // void compute_triangle_bounds(Triangle &obj);
  // void build_box_map(ObjMap &box_map);
};

}
#endif
