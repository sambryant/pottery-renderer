use std::time::Instant;
use nalgebra::{SMatrix, SVector, Vector3, Matrix3};

pub const PI: f32 = std::f32::consts::PI;
pub const N_THS: u32 = 1000;
pub const N_XS: u32 = 1000;
pub const N_ZS: u32 = 1000;

pub const X_MAX: f32 = 10.0;
pub const Z_MAX: f32 = 10.0;

pub const X_INC: f32 = X_MAX / (N_XS as f32);
pub const Z_INC: f32 = Z_MAX / (N_ZS as f32);
pub const TH_INC: f32 = 2.0 * PI / (N_THS as f32);

pub const V0: [f32; 3] = [0.1, -0.5, 5.0];

pub mod raw {
  use super::*;
  type Vec = SVector<f32, 3>;
  type Mat = SMatrix<f32, 3, 3>;

  fn rotation_x(th: f32) -> Mat {
      let (sin, cos) = (th.sin(), th.cos());
      Mat::new(
        1.0, 0.0, 0.0,
        0.0, cos, -sin,
        0.0, sin, cos
      )
  }
  pub fn do_work() -> f32 {
    let mut sum = 0.0;
    for i in 0..N_THS {
      let th = TH_INC * (i as f32);
      let matrix = rotation_x(th);
      for j in 0..N_XS {
        for k in 0..N_ZS {
          let vector = Vec::new(V0[0] + X_INC * (j as f32), V0[1], V0[2] + Z_INC * (k as f32));
          let result = matrix * vector;
          sum += result.dot(&result);
        }
      }
    }
    sum
  }

  pub fn run_test() {
    let start = Instant::now();
    let result = do_work();
    let duration = start.elapsed();
    let micros = (duration.as_micros() as f64) / (1000000.0);
    println!("Time: {:.4}  | Data Structs: NAlg |  (result={})", micros, result);
  }

}

pub mod vec3 {
  use super::*;
  type Vec = Vector3<f32>;
  type Mat = Matrix3<f32>;

  fn rotation_x(th: f32) -> Mat {
      let (sin, cos) = (th.sin(), th.cos());
      Mat::new(
        1.0, 0.0, 0.0,
        0.0, cos, -sin,
        0.0, sin, cos
      )
  }

  pub fn do_work() -> f32 {
    let mut sum = 0.0;
    for i in 0..N_THS {
      let th = TH_INC * (i as f32);
      let matrix = rotation_x(th);
      for j in 0..N_XS {
        for k in 0..N_ZS {
          let vector = Vec::new(V0[0] + X_INC * (j as f32), V0[1], V0[2] + Z_INC * (k as f32));
          let result = matrix * vector;
          sum += result.dot(&result);
        }
      }
    }
    sum
  }

  pub fn run_test() {
    let start = Instant::now();
    let result = do_work();
    let duration = start.elapsed();
    let micros = (duration.as_micros() as f64) / (1000000.0);
    println!("Time: {:.4}  | Data Structs: NAlg |   (result={})", micros, result);
  }

}

pub fn go() {
  raw::run_test();
  // vec3::run_test();
}

