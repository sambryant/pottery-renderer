use nalgebra::{SMatrix, SVector};

pub mod hand_coded;
pub mod nalg;

type Vec = SVector<f32, 3>;
type Mat = SMatrix<f32, 3, 3>;


fn main() {
  // let v1 = Vec::new(1.0, 2.0, 3.0);
  // let v2 = Vec::new(4.0, 5.0, 6.0);
  // println!("Result: {:?}", v1.dot(&v2));

  // hand_coded::efficiency_test::go();
  nalg::efficiency_test::go();

  // hand_coded::efficiency_test::go();
  // hand_coded::efficiency_test_2::go();
  // hand_coded::efficiency_test_3::go();
}
