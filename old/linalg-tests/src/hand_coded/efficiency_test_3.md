# Efficiency Test 3

This one checks various methods of normalization.

## Methods

1. In-Place

Pass mutable references and modify values in place

2. By reference

Pass immutable references and return newly constructed vectors

3. By cloning
