// Test normalization in-place vs reference, vs cloning
use std::time::Instant;

pub const N: u32 = 1000;
pub const MAX: f32 = 10.0;
pub const INC: f32 = MAX / (N as f32);

pub type Vec = [f32; 3];


pub mod cloned {
  use super::*;

  fn normalize(v: Vec) -> Vec {
    let norm = 1.0 / (v[0] * v[0] + v[1] * v[1] + v[2] * v[2]).sqrt();
    [ norm * v[0], norm * v[1], norm * v[2] ]
  }

  fn do_work() -> f32 {
    let mut sum = 0.0;
    for i in 0..N {
      for j in 0..N {
        for k in 0..N {
          let v1 = [INC * (i as f32), INC * (j as f32), INC * (k as f32)];
          let v2 = normalize(v1.clone());
          sum += v2[0] - v2[1] + v2[2] - v1[0] - v1[1] + v1[2];
        }
      }
    }
    sum
  }

  pub fn run_test() {
    let start = Instant::now();
    let result = do_work();
    let duration = start.elapsed();
    let micros = (duration.as_micros() as f64) / (1000000.0);
    println!("Time: {:.4} | result: {}", micros, result);
  }
}

pub mod in_place {
  use super::*;

  fn normalize(v: &mut Vec) {
    let norm = 1.0 / (v[0] * v[0] + v[1] * v[1] + v[2] * v[2]).sqrt();
    v[0] *= norm;
    v[1] *= norm;
    v[2] *= norm;
  }

  fn do_work() -> f32 {
    let mut sum = 0.0;
    for i in 0..N {
      for j in 0..N {
        for k in 0..N {
          let mut vec = [INC * (i as f32), INC * (j as f32), INC * (k as f32)];
          normalize(&mut vec);
          sum += vec[0] - vec[1] + vec[2];
        }
      }
    }
    sum
  }

  pub fn run_test() {
    let start = Instant::now();
    let result = do_work();
    let duration = start.elapsed();
    let micros = (duration.as_micros() as f64) / (1000000.0);
    println!("Time: {:.4} | result: {}", micros, result);
  }
}

pub mod by_reference {
  use super::*;

  fn normalize(v: &Vec) -> Vec {
    let norm = 1.0 / (v[0] * v[0] + v[1] * v[1] + v[2] * v[2]).sqrt();
    [ norm * v[0], norm * v[1], norm * v[2] ]
  }

  fn do_work() -> f32 {
    let mut sum = 0.0;
    for i in 0..N {
      for j in 0..N {
        for k in 0..N {
          let v1 = [INC * (i as f32), INC * (j as f32), INC * (k as f32)];
          let v2 = normalize(&v1);
          sum += v2[0] - v2[1] + v2[2] - v1[0] - v1[1] + v1[2];
        }
      }
    }
    sum
  }

  pub fn run_test() {
    let start = Instant::now();
    let result = do_work();
    let duration = start.elapsed();
    let micros = (duration.as_micros() as f64) / (1000000.0);
    println!("Time: {:.4} | result: {}", micros, result);
  }
}

pub fn go() {
  // in_place::run_test()
  // cloned::run_test()
  by_reference::run_test()
}
