use std::time::Instant;

pub const PI: f32 = std::f32::consts::PI;

pub const N_THS: u32 = 1000;
pub const N_XS: u32 = 1000;
pub const N_ZS: u32 = 1000;

pub const X_MAX: f32 = 10.0;
pub const Z_MAX: f32 = 10.0;

mod raw {
  use super::*;

  pub type Mat = [[f32; 3]; 3];
  pub type Vec = [f32; 3];
  pub const V0: Vec = [0.1, -0.5, 5.0];

  /// Performs counterclockwise rotation in YZ plane
  pub fn rotation_x(th: f32) -> Mat {
    let (sin, cos) = (th.sin(), th.cos());
    [
      [ 1.0,  0.0,  0.0],
      [ 0.0,  cos, -sin],
      [ 0.0,  sin,  cos]
    ]
  }

  pub trait Ops {
    fn name() -> &'static str;
    fn mat_multiply(m: &Mat, v: &Vec) -> Vec;
    fn dot_product(v1: &Vec, v2: &Vec) -> f32;
  }

  pub struct OpsExplicit {}
  pub struct OpsLooped {}
  pub struct OpsLoopedMat {}

  impl Ops for OpsExplicit {
    fn name() -> &'static str {
      "Explicit ops"
    }
    fn mat_multiply(m: &Mat, v: &Vec) -> [f32; 3] {
      [
        m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2],
        m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2],
        m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2],
      ]
    }

    fn dot_product(v1: &Vec, v2: &Vec) -> f32 {
      v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
    }
  }

  impl Ops for OpsLoopedMat {
    fn name() -> &'static str {
      "Explicit dp, semi-looped mat mul"
    }
    fn mat_multiply(m: &Mat, v: &Vec) -> [f32; 3] {
      [
        (0..3).map(|j| m[0][j] * v[j]).sum(),
        (0..3).map(|j| m[1][j] * v[j]).sum(),
        (0..3).map(|j| m[2][j] * v[j]).sum(),
      ]
    }

    fn dot_product(v1: &Vec, v2: &Vec) -> f32 {
      v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
    }
  }

  impl Ops for OpsLooped {
    fn name() -> &'static str {
      "All Looped"
    }
    fn mat_multiply(m: &Mat, v: &Vec) -> [f32; 3] {
      let mut result = [0.0; 3];
      for i in 0..3 {
        result[i] = (0..3).map(|j| m[i][j] * v[j]).sum();
      }
      result
    }

    fn dot_product(v1: &Vec, v2: &Vec) -> f32 {
      (0..3).map(|i| v1[i] * v2[i]).sum()
    }
  }

  pub fn do_work<T: Ops>(n_ths: u32, n_xs: u32, n_zs: u32) -> f32 {
    let x_inc = X_MAX / (n_xs as f32);
    let z_inc = Z_MAX / (n_zs as f32);
    let th_inc = 2.0 * PI / (n_ths as f32);
    let mut sum = 0.0;
    for i in 0..n_ths {
      let th = th_inc * (i as f32);
      let matrix = rotation_x(th);
      for j in 0..n_xs {
        for k in 0..n_zs {
          let vector = [V0[0] + x_inc * (j as f32), V0[1], V0[2] + z_inc * (k as f32)];
          let result = T::mat_multiply(&matrix, &vector);
          sum += T::dot_product(&result, &result);
        }
      }
    }
    sum
  }

  pub fn run_test<T: Ops>() {
    let start = Instant::now();
    let result = do_work::<T>(N_THS, N_XS, N_ZS);
    let duration = start.elapsed();
    let micros = (duration.as_micros() as f64) / (1000000.0);
    println!("Time: {:.4}  | Data Structs: Raw | Ops: {},    (result={})", micros, T::name(), result);
  }
}

mod cloned {
  use super::*;

  pub type Mat = [[f32; 3]; 3];
  pub type Vec = [f32; 3];
  pub const V0: Vec = [0.1, -0.5, 5.0];

  /// Performs counterclockwise rotation in YZ plane
  pub fn rotation_x(th: f32) -> Mat {
    let (sin, cos) = (th.sin(), th.cos());
    [
      [ 1.0,  0.0,  0.0],
      [ 0.0,  cos, -sin],
      [ 0.0,  sin,  cos]
    ]
  }

  pub trait Ops {
    fn name() -> &'static str;
    fn mat_multiply(m: Mat, v: Vec) -> Vec;
    fn dot_product(v1: Vec, v2: Vec) -> f32;
  }

  pub struct OpsExplicit {}
  pub struct OpsLooped {}
  pub struct OpsLoopedMat {}

  impl Ops for OpsExplicit {
    fn name() -> &'static str {
      "Explicit ops"
    }
    fn mat_multiply(m: [[f32; 3]; 3], v: [f32; 3]) -> [f32; 3] {
      [
        m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2],
        m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2],
        m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2],
      ]
    }

    fn dot_product(v1: [f32; 3], v2: [f32; 3]) -> f32 {
      v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
    }
  }

  impl Ops for OpsLoopedMat {
    fn name() -> &'static str {
      "Explicit dp, semi-looped mat mul"
    }
    fn mat_multiply(m: [[f32; 3]; 3], v: [f32; 3]) -> [f32; 3] {
      [
        (0..3).map(|j| m[0][j] * v[j]).sum(),
        (0..3).map(|j| m[1][j] * v[j]).sum(),
        (0..3).map(|j| m[2][j] * v[j]).sum(),
      ]
    }

    fn dot_product(v1: [f32; 3], v2: [f32; 3]) -> f32 {
      v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
    }
  }

  impl Ops for OpsLooped {
    fn name() -> &'static str {
      "All Looped"
    }
    fn mat_multiply(m: [[f32; 3]; 3], v: [f32; 3]) -> [f32; 3] {
      let mut result = [0.0; 3];
      for i in 0..3 {
        result[i] = (0..3).map(|j| m[i][j] * v[j]).sum();
      }
      result
    }

    fn dot_product(v1: [f32; 3], v2: [f32; 3]) -> f32 {
      (0..3).map(|i| v1[i] * v2[i]).sum()
    }
  }

  pub fn do_work<T: Ops>(n_ths: u32, n_xs: u32, n_zs: u32) -> f32 {
    let x_inc = X_MAX / (n_xs as f32);
    let z_inc = Z_MAX / (n_zs as f32);
    let th_inc = 2.0 * PI / (n_ths as f32);
    let mut sum = 0.0;
    for i in 0..n_ths {
      let th = th_inc * (i as f32);
      let matrix = rotation_x(th);
      for j in 0..n_xs {
        for k in 0..n_zs {
          let vector = [V0[0] + x_inc * (j as f32), V0[1], V0[2] + z_inc * (k as f32)];
          let result = T::mat_multiply(matrix.clone(), vector.clone());
          sum += T::dot_product(result.clone(), result.clone());
        }
      }
    }
    sum
  }

  pub fn run_test<T: Ops>() {
    let start = Instant::now();
    let result = do_work::<T>(N_THS, N_XS, N_ZS);
    let duration = start.elapsed();
    let micros = (duration.as_micros() as f64) / (1000000.0);
    println!("Time: {:.4}  | Data Structs: Raw | Ops: {},    (result={})", micros, T::name(), result);
  }
}

mod thin_layer {
  use super::*;

  pub struct Mat(pub [[f32; 3]; 3]);
  pub struct Vec(pub [f32; 3]);
  const V0: Vec = Vec([0.1, -0.5, 5.0]);

  /// Performs counterclockwise rotation in YZ plane
  pub fn rotation_x(th: f32) -> Mat {
    let (sin, cos) = (th.sin(), th.cos());
    Mat([
      [ 1.0,  0.0,  0.0],
      [ 0.0,  cos, -sin],
      [ 0.0,  sin,  cos]
    ])
  }

  pub trait Ops {
    fn name() -> &'static str;
    fn mat_multiply(m: &Mat, v: &Vec) -> Vec;
    fn dot_product(v1: &Vec, v2: &Vec) -> f32;
  }

  pub struct OpsExplicit {}
  pub struct OpsLooped {}
  pub struct OpsLoopedMat {}

  impl Ops for OpsExplicit {
    fn name() -> &'static str {
      "Explicit ops"
    }
    fn mat_multiply(m: &Mat, v: &Vec) -> Vec {
      Vec([
        m.0[0][0] * v.0[0] + m.0[0][1] * v.0[1] + m.0[0][2] * v.0[2],
        m.0[1][0] * v.0[0] + m.0[1][1] * v.0[1] + m.0[1][2] * v.0[2],
        m.0[2][0] * v.0[0] + m.0[2][1] * v.0[1] + m.0[2][2] * v.0[2],
      ])
    }

    fn dot_product(v1: &Vec, v2: &Vec) -> f32 {
      v1.0[0] * v2.0[0] + v1.0[1] * v2.0[1] + v1.0[2] * v2.0[2]
    }
  }

  impl Ops for OpsLoopedMat {
    fn name() -> &'static str {
      "Explicit dp, semi-looped mat mul"
    }
    fn mat_multiply(m: &Mat, v: &Vec) -> Vec {
      Vec([
        (0..3).map(|j| m.0[0][j] * v.0[j]).sum(),
        (0..3).map(|j| m.0[1][j] * v.0[j]).sum(),
        (0..3).map(|j| m.0[2][j] * v.0[j]).sum(),
      ])
    }

    fn dot_product(v1: &Vec, v2: &Vec) -> f32 {
      v1.0[0] * v2.0[0] + v1.0[1] * v2.0[1] + v1.0[2] * v2.0[2]
    }
  }

  impl Ops for OpsLooped {
    fn name() -> &'static str {
      "All Looped"
    }
    fn mat_multiply(m: &Mat, v: &Vec) -> Vec {
      let mut result = [0.0; 3];
      for i in 0..3 {
        result[i] = (0..3).map(|j| m.0[i][j] * v.0[j]).sum();
      }
      Vec(result)
    }

    fn dot_product(v1: &Vec, v2: &Vec) -> f32 {
      (0..3).map(|i| v1.0[i] * v2.0[i]).sum()
    }
  }
  pub fn do_work<T: Ops>(n_ths: u32, n_xs: u32, n_zs: u32) -> f32 {
    let x_inc = X_MAX / (n_xs as f32);
    let z_inc = Z_MAX / (n_zs as f32);
    let th_inc = 2.0 * PI / (n_ths as f32);
    let mut sum = 0.0;
    for i in 0..n_ths {
      let th = th_inc * (i as f32);
      let matrix = rotation_x(th);
      for j in 0..n_xs {
        for k in 0..n_zs {
          let vector = Vec([V0.0[0] + x_inc * (j as f32), V0.0[1], V0.0[2] + z_inc * (k as f32)]);
          let result = T::mat_multiply(&matrix, &vector);
          sum += T::dot_product(&result, &result);
        }
      }
    }
    sum
  }

  pub fn run_test<T: Ops>() {
    let start = Instant::now();
    let result = do_work::<T>(N_THS, N_XS, N_ZS);
    let duration = start.elapsed();
    let micros = (duration.as_micros() as f64) / (1000000.0);
    println!("Time: {:.4}  | Data Structs: Thin | Ops: {},    (result={})", micros, T::name(), result);
  }
}

pub fn go() {
  // raw::run_test::<raw::OpsExplicit>()
  // raw::run_test::<raw::OpsLooped>()
  // raw::run_test::<raw::OpsLoopedMat>()

  // thin_layer::run_test::<thin_layer::OpsExplicit>()
  // thin_layer::run_test::<thin_layer::OpsLoopedMat>()

  // cloned::run_test::<cloned::OpsExplicit>()
  // cloned::run_test::<cloned::OpsLooped>()
  cloned::run_test::<cloned::OpsLoopedMat>()

}
