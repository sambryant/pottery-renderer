# Efficiency Tests

This document records the runtime efficiency of linear algebra operations with different under-the-
hood implementations. It's meant to roughly simulate the type of work done in ray-tracing to figure
out the best tradeoff of abstraction and runtime.


## The Test

Three integer variables: L, M, N (number of thetas, number of X's, number of Y's)

 - For each theta, generate a rotation matrix: R
 - For each x, z, generate a vector: v = v0 + (x, 0, z)
 - Perform matrix multiplication: v' = R v
 - Compute the norm2 of the result: d2 = v' * v'

For the results given below, we use:
  L = 1000
  M = 1000
  N = 1000

## Data Structure Types

Each method uses a different set of data structures with various degrees of abstraction

### DS #1 - Raw
Vectors: `[f32; 3]`;
Matrices: `[[f32; 3]; 3]`;

### DS #2 - Thin Struct
Vectors: `struct Vec(pub [f32; 3])`
Matrices: `struct Mat(pub [[f32; 3]; 3])`

### DS #3 - Cloned
Data structures same as raw but all data is cloned rather than passed by reference.

## Operation Method Types

Each method has to implement matrix multiplication and dot product

### Op #1 - Fully Explicit
Matrix multiplication:
```
  [ m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2], .... ]
```
Dot product:
```
  v[0] * v[0] + v[1] * v[1] + v[2] * v[2]
```

### Op #2 - Fully Loooped
Matrix Multiplication
```
    let mut result = [0.0; 3];
    for i in 0..3 {
      result[i] = (0..3).map(|j| m[i][j] * v[j]).sum()
    }
    result
```
Dot product:
```
  (0..3).map(|j| v[j] * v[j]).sum()
```

### Op #3 - Semi Looped Matrix Multiplication:
Matrix multiplication:
```
    [ (0..3).map(|j| m[0][j] * v[j]).sum(), ... ]
```
Dot product
```
  v[0] * v[0] + v[1] * v[1] + v[2] * v[2]
```

## Test Results

https://docs.google.com/spreadsheets/d/171eafAB207RFTcrm2HzL5UMixmKavoCigMWM8ezSWjo/edit#gid=0
