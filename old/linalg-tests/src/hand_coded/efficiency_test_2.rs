use std::time::Instant;

pub const PI: f32 = std::f32::consts::PI;

pub const N_THS: u32 = 100;
pub const N_XS: u32 = 1000;
pub const N_ZS: u32 = 1000;

pub const X_MAX: f32 = 10.0;
pub const Z_MAX: f32 = 10.0;

mod raw_pure {
  use super::*;

  pub type Mat = [[f32; 3]; 3];
  pub type Vec = [f32; 3];
  pub const V0: Vec = [0.1, -0.5, 5.0];

  pub fn rotation_x(th: f32) -> Mat {
    let (sin, cos) = (th.sin(), th.cos());
    [
      [ 1.0,  0.0,  0.0],
      [ 0.0,  cos, -sin],
      [ 0.0,  sin,  cos],
    ]
  }

  pub fn rotation_y(th: f32) -> Mat {
    let (sin, cos) = (th.sin(), th.cos());
    [
      [ cos,  0.0,  sin],
      [ 0.0,  1.0,  0.0],
      [-sin,  0.0,  cos],
    ]
  }

  pub trait Ops {
    fn name() -> &'static str;
    fn add_vec(v1: &Vec, v2: &Vec) -> Vec;
    fn mat_multiply(m: &Mat, v: &Vec) -> Vec;
    fn mat_mat_multiply(m1: &Mat, m2: &Mat) -> Mat;
    fn dot_product(v1: &Vec, v2: &Vec) -> f32;
  }

  pub struct OpsExplicit {}
  pub struct OpsOptimized {}

  impl Ops for OpsExplicit {
    fn name() -> &'static str {
      "Explicit ops"
    }

    fn add_vec(v1: &Vec, v2: &Vec) -> Vec {
      [v1[0] + v2[0], v1[1] + v2[1], v1[2] + v2[2]]
    }

    fn mat_multiply(m: &Mat, v: &Vec) -> Vec {
      [
        m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2],
        m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2],
        m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2],
      ]
    }

    fn mat_mat_multiply(m1: &Mat, m2: &Mat) -> Mat {
      [
        [
          m1[0][0]*m2[0][0]+m1[0][1]*m2[1][0]+m1[0][2]*m2[2][0],
          m1[0][0]*m2[0][1]+m1[0][1]*m2[1][1]+m1[0][2]*m2[2][1],
          m1[0][0]*m2[0][2]+m1[0][1]*m2[1][2]+m1[0][2]*m2[2][2],
        ],
        [
          m1[1][0]*m2[0][0]+m1[1][1]*m2[1][0]+m1[1][2]*m2[2][0],
          m1[1][0]*m2[0][1]+m1[1][1]*m2[1][1]+m1[1][2]*m2[2][1],
          m1[1][0]*m2[0][2]+m1[1][1]*m2[1][2]+m1[1][2]*m2[2][2],
        ],
        [
          m1[2][0]*m2[0][0]+m1[2][1]*m2[1][0]+m1[2][2]*m2[2][0],
          m1[2][0]*m2[0][1]+m1[2][1]*m2[1][1]+m1[2][2]*m2[2][1],
          m1[2][0]*m2[0][2]+m1[2][1]*m2[1][2]+m1[2][2]*m2[2][2],
        ],
      ]
    }

    fn dot_product(v1: &Vec, v2: &Vec) -> f32 {
      v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
    }
  }

  impl Ops for OpsOptimized {
    fn name() -> &'static str {
      "Optimized ops"
    }

    fn add_vec(v1: &Vec, v2: &Vec) -> Vec {
      [v1[0] + v2[0], v1[1] + v2[1], v1[2] + v2[2]]
    }

    fn mat_multiply(m: &Mat, v: &Vec) -> Vec {
      [
        (0..3).map(|i| m[0][i]*v[i]).sum(),
        (0..3).map(|i| m[1][i]*v[i]).sum(),
        (0..3).map(|i| m[2][i]*v[i]).sum(),
      ]
    }

    fn mat_mat_multiply(m1: &Mat, m2: &Mat) -> Mat {
      [
        [
          m1[0][0]*m2[0][0]+m1[0][1]*m2[1][0]+m1[0][2]*m2[2][0],
          m1[0][0]*m2[0][1]+m1[0][1]*m2[1][1]+m1[0][2]*m2[2][1],
          m1[0][0]*m2[0][2]+m1[0][1]*m2[1][2]+m1[0][2]*m2[2][2],
        ],
        [
          m1[1][0]*m2[0][0]+m1[1][1]*m2[1][0]+m1[1][2]*m2[2][0],
          m1[1][0]*m2[0][1]+m1[1][1]*m2[1][1]+m1[1][2]*m2[2][1],
          m1[1][0]*m2[0][2]+m1[1][1]*m2[1][2]+m1[1][2]*m2[2][2],
        ],
        [
          m1[2][0]*m2[0][0]+m1[2][1]*m2[1][0]+m1[2][2]*m2[2][0],
          m1[2][0]*m2[0][1]+m1[2][1]*m2[1][1]+m1[2][2]*m2[2][1],
          m1[2][0]*m2[0][2]+m1[2][1]*m2[1][2]+m1[2][2]*m2[2][2],
        ],
      ]
      // let mut result = [[0.0; 3]; 3];
      // for i in 0..3 {
      //   for j in 0..3 {
      //     result[i][j] = (0..3).map(|k| m1[i][k]*m2[k][j]).sum();
      //   }
      // }
      // result
    }

    fn dot_product(v1: &Vec, v2: &Vec) -> f32 {
      v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
    }
  }

  pub fn do_work<T: Ops>(n_ths: u32, n_xs: u32, n_zs: u32) -> f32 {
    let x_inc = X_MAX / (n_xs as f32);
    let z_inc = Z_MAX / (n_zs as f32);
    let th_inc = 2.0 * PI / (n_ths as f32);
    let mut sum = 0.0;
    for j in 0..n_xs {
      for k in 0..n_zs {
        for i in 0..n_ths {
          let th = th_inc * (i as f32);
          let v1 = [x_inc * (j as f32), 0.0, z_inc * (k as f32)];
          let r1 = rotation_x(th);
          let r2 = rotation_y(th + 0.3);
          let m = T::mat_mat_multiply(&r2, &r1);
          let v2 = T::add_vec(&V0, &v1);
          let result = T::mat_multiply(&m, &v2);
          sum += T::dot_product(&result, &result);
        }
      }
    }
    sum
  }

  pub fn run_test<T: Ops>() {
    let start = Instant::now();
    let result = do_work::<T>(N_THS, N_XS, N_ZS);
    let duration = start.elapsed();
    let micros = (duration.as_micros() as f64) / (1000000.0);
    println!("Time: {:.4}  | Data Structs: Raw (Pure) | Ops: {},    (result={})", micros, T::name(), result);
  }
}

mod raw_affine {
  use super::*;

  pub type Mat = [[f32; 4]; 4];
  pub type Vec = [f32; 4];
  pub const V0: Vec = [0.1, -0.5, 5.0, 1.0];

  pub fn translation(v: &Vec) -> Mat {
    [
      [ 1.0,  0.0,  0.0, v[0]],
      [ 0.0,  1.0,  0.0, v[1]],
      [ 0.0,  0.0,  1.0, v[2]],
      [ 0.0,  0.0,  0.0, 1.0]
    ]
  }

  pub fn rotation_x(th: f32) -> Mat {
    let (sin, cos) = (th.sin(), th.cos());
    [
      [ 1.0,  0.0,  0.0, 0.0],
      [ 0.0,  cos, -sin, 0.0],
      [ 0.0,  sin,  cos, 0.0],
      [ 0.0,  0.0,  0.0, 1.0]
    ]
  }

  pub fn rotation_y(th: f32) -> Mat {
    let (sin, cos) = (th.sin(), th.cos());
    [
      [ cos,  0.0,  sin, 0.0],
      [ 0.0,  1.0,  0.0, 0.0],
      [-sin,  0.0,  cos, 0.0],
      [ 0.0,  0.0,  0.0, 1.0],
    ]
  }

  pub trait Ops {
    fn name() -> &'static str;
    fn mat_multiply(m: &Mat, v: &Vec) -> Vec;
    fn mat_mat_multiply(m1: &Mat, m2: &Mat) -> Mat;
    fn dot_product(v1: &Vec, v2: &Vec) -> f32;
  }

  pub struct OpsExplicit {}

  impl Ops for OpsExplicit {
    fn name() -> &'static str {
      "Explicit ops"
    }

    fn mat_multiply(m: &Mat, v: &Vec) -> Vec {
      [
        m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2] + m[0][3] * v[3],
        m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2] + m[1][3] * v[3],
        m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2] + m[2][3] * v[3],
        1.0
      ]
    }

    fn mat_mat_multiply(m1: &Mat, m2: &Mat) -> Mat {
      [
        [
          m1[0][0]*m2[0][0]+m1[0][1]*m2[1][0]+m1[0][2]*m2[2][0]+m1[0][3]*m2[3][0],
          m1[0][0]*m2[0][1]+m1[0][1]*m2[1][1]+m1[0][2]*m2[2][1]+m1[0][3]*m2[3][1],
          m1[0][0]*m2[0][2]+m1[0][1]*m2[1][2]+m1[0][2]*m2[2][2]+m1[0][3]*m2[3][2],
          m1[0][0]*m2[0][3]+m1[0][1]*m2[1][3]+m1[0][2]*m2[2][3]+m1[0][3]*m2[3][3],
        ],
        [
          m1[1][0]*m2[0][0]+m1[1][1]*m2[1][0]+m1[1][2]*m2[2][0]+m1[1][3]*m2[3][0],
          m1[1][0]*m2[0][1]+m1[1][1]*m2[1][1]+m1[1][2]*m2[2][1]+m1[1][3]*m2[3][1],
          m1[1][0]*m2[0][2]+m1[1][1]*m2[1][2]+m1[1][2]*m2[2][2]+m1[1][3]*m2[3][2],
          m1[1][0]*m2[0][3]+m1[1][1]*m2[1][3]+m1[1][2]*m2[2][3]+m1[1][3]*m2[3][3],
        ],
        [
          m1[2][0]*m2[0][0]+m1[2][1]*m2[1][0]+m1[2][2]*m2[2][0]+m1[2][3]*m2[3][0],
          m1[2][0]*m2[0][1]+m1[2][1]*m2[1][1]+m1[2][2]*m2[2][1]+m1[2][3]*m2[3][1],
          m1[2][0]*m2[0][2]+m1[2][1]*m2[1][2]+m1[2][2]*m2[2][2]+m1[2][3]*m2[3][2],
          m1[2][0]*m2[0][3]+m1[2][1]*m2[1][3]+m1[2][2]*m2[2][3]+m1[2][3]*m2[3][3],
        ],
        [
          m1[3][0]*m2[0][0]+m1[3][1]*m2[1][0]+m1[3][2]*m2[2][0]+m1[3][3]*m2[3][0],
          m1[3][0]*m2[0][1]+m1[3][1]*m2[1][1]+m1[3][2]*m2[2][1]+m1[3][3]*m2[3][1],
          m1[3][0]*m2[0][2]+m1[3][1]*m2[1][2]+m1[3][2]*m2[2][2]+m1[3][3]*m2[3][2],
          m1[3][0]*m2[0][3]+m1[3][1]*m2[1][3]+m1[3][2]*m2[2][3]+m1[3][3]*m2[3][3],
        ],
      ]
    }

    fn dot_product(v1: &Vec, v2: &Vec) -> f32 {
      v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
    }
  }

  pub fn do_work<T: Ops>(n_ths: u32, n_xs: u32, n_zs: u32) -> f32 {
    let x_inc = X_MAX / (n_xs as f32);
    let z_inc = Z_MAX / (n_zs as f32);
    let th_inc = 2.0 * PI / (n_ths as f32);
    let mut sum = 0.0;
    for j in 0..n_xs {
      for k in 0..n_zs {
        for i in 0..n_ths {
          let th = th_inc * (i as f32);
          let vec = [x_inc * (j as f32), 0.0, z_inc * (k as f32), 1.0];
          let r1 = rotation_x(th);
          let r2 = rotation_y(th + 0.3);
          let t1 = translation(&vec);
          let m = T::mat_mat_multiply(&r2, &T::mat_mat_multiply(&r1, &t1));
          let result = T::mat_multiply(&m, &V0);
          sum += T::dot_product(&result, &result);
        }
      }
    }
    sum
  }

  pub fn run_test<T: Ops>() {
    let start = Instant::now();
    let result = do_work::<T>(N_THS, N_XS, N_ZS);
    let duration = start.elapsed();
    let micros = (duration.as_micros() as f64) / (1000000.0);
    println!("Time: {:.4}  | Data Structs: Raw (Affine) | Ops: {},    (result={})", micros, T::name(), result);
  }
}

mod cloned_pure {
  use super::*;

  pub type Mat = [[f32; 3]; 3];
  pub type Vec = [f32; 3];
  pub const V0: Vec = [0.1, -0.5, 5.0];

  pub fn rotation_x(th: f32) -> Mat {
    let (sin, cos) = (th.sin(), th.cos());
    [
      [ 1.0,  0.0,  0.0],
      [ 0.0,  cos, -sin],
      [ 0.0,  sin,  cos],
    ]
  }

  pub fn rotation_y(th: f32) -> Mat {
    let (sin, cos) = (th.sin(), th.cos());
    [
      [ cos,  0.0,  sin],
      [ 0.0,  1.0,  0.0],
      [-sin,  0.0,  cos],
    ]
  }

  pub trait Ops {
    fn name() -> &'static str;
    fn add_vec(v1: Vec, v2: Vec) -> Vec;
    fn mat_multiply(m: Mat, v: Vec) -> Vec;
    fn mat_mat_multiply(m1: Mat, m2: Mat) -> Mat;
    fn dot_product(v1: Vec, v2: Vec) -> f32;
  }

  pub struct OpsExplicit {}

  impl Ops for OpsExplicit {
    fn name() -> &'static str {
      "Explicit ops"
    }

    fn add_vec(v1: Vec, v2: Vec) -> Vec {
      [v1[0] + v2[0], v1[1] + v2[1], v1[2] + v2[2]]
    }

    fn mat_multiply(m: Mat, v: Vec) -> Vec {
      [
        m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2],
        m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2],
        m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2],
      ]
    }

    fn mat_mat_multiply(m1: Mat, m2: Mat) -> Mat {
      [
        [
          m1[0][0]*m2[0][0]+m1[0][1]*m2[1][0]+m1[0][2]*m2[2][0],
          m1[0][0]*m2[0][1]+m1[0][1]*m2[1][1]+m1[0][2]*m2[2][1],
          m1[0][0]*m2[0][2]+m1[0][1]*m2[1][2]+m1[0][2]*m2[2][2],
        ],
        [
          m1[1][0]*m2[0][0]+m1[1][1]*m2[1][0]+m1[1][2]*m2[2][0],
          m1[1][0]*m2[0][1]+m1[1][1]*m2[1][1]+m1[1][2]*m2[2][1],
          m1[1][0]*m2[0][2]+m1[1][1]*m2[1][2]+m1[1][2]*m2[2][2],
        ],
        [
          m1[2][0]*m2[0][0]+m1[2][1]*m2[1][0]+m1[2][2]*m2[2][0],
          m1[2][0]*m2[0][1]+m1[2][1]*m2[1][1]+m1[2][2]*m2[2][1],
          m1[2][0]*m2[0][2]+m1[2][1]*m2[1][2]+m1[2][2]*m2[2][2],
        ],
      ]
    }

    fn dot_product(v1: Vec, v2: Vec) -> f32 {
      v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
    }
  }

  pub fn do_work<T: Ops>(n_ths: u32, n_xs: u32, n_zs: u32) -> f32 {
    let x_inc = X_MAX / (n_xs as f32);
    let z_inc = Z_MAX / (n_zs as f32);
    let th_inc = 2.0 * PI / (n_ths as f32);
    let mut sum = 0.0;
    for j in 0..n_xs {
      for k in 0..n_zs {
        for i in 0..n_ths {
          let th = th_inc * (i as f32);
          let v1 = [x_inc * (j as f32), 0.0, z_inc * (k as f32)];
          let r1 = rotation_x(th);
          let r2 = rotation_y(th + 0.3);
          let m = T::mat_mat_multiply(r2.clone(), r1.clone());
          let v2 = T::add_vec(V0.clone(), v1.clone());
          let result = T::mat_multiply(m.clone(), v2.clone());
          sum += T::dot_product(result.clone(), result.clone());
        }
      }
    }
    sum
  }

  pub fn run_test<T: Ops>() {
    let start = Instant::now();
    let result = do_work::<T>(N_THS, N_XS, N_ZS);
    let duration = start.elapsed();
    let micros = (duration.as_micros() as f64) / (1000000.0);
    println!("Time: {:.4}  | Data Structs: Cloned (Pure) | Ops: {},    (result={})", micros, T::name(), result);
  }
}

pub fn go() {
  // raw_pure::run_test::<raw_pure::OpsExplicit>()
  raw_pure::run_test::<raw_pure::OpsOptimized>()
  // raw_affine::run_test::<raw_affine::OpsExplicit>()
  // cloned_pure::run_test::<cloned_pure::OpsExplicit>()
}
