# Efficiency Test Analysis

The results of these tests were somewhat surprising.

Things learned:

  1. Using raw arrays over thin structs imparts modest benefit. i.e.
    `[f32; 3]` is more efficient than `struct Vec([f32; 3])`
  Confusing: I thought rust had zero-cost abstractions...

  2. The optimized library `nalgebra` performs slightly worse than the naive hand-coded implementation.
  I guess its abstractions introduce a small amount of overhead and there aren't that many fancy
  optimizations you can do when your matrices are so small.

  3. In terms of operations, explicit vs looped is context dependent.

    For example, when doing matrix * vector, it seems the most efficient method is:
    ```
      [ (0..3).map(|i| m[0][i]*v[i]).sum(), ... ]
    ```
    For dot products, the explicit version is the fastest:
    ```
    v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
    ```
    And most, confusingly, for matrix * matrix, the explicit form also appears to win out:
    ```
    [[m1[0][0]*m2[0][0]+m1[0][1]*m2[1][0]+m1[0][2]*m2[2][0], ...
    ```


