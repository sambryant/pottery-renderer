use crate::{
  triangle::*,
  color::*,
  pmath::*,
};

/// Structure which turns a "pot" into a mesh of triangles.
pub struct PotMaker {
  pub n_ths: usize,
  pub n_ys: usize,
}

impl PotMaker {

  pub fn make_fancy_pot_mesh(&self, base: Vec3, height: f32, radius: fn(f32) -> f32, side_length: f32) -> Vec<Triangle> {
    let mut vertices = self.get_vertices(height, radius);

    fn pt_dis(v1: &Vec3, v2: &Vec3) -> f32 {
      ((v1[0] - v2[0])*(v1[0] - v2[0]) + (v1[1] - v2[1])*(v1[1] - v2[1]) + (v1[2] - v2[2])*(v1[2] - v2[2])).sqrt()
    }

    let mut y_distances = vec![0.0];
    let mut y_length = 0.0;
    for i in 0..(vertices.len() - 1) {
      y_length += pt_dis(&vertices[i][0], &vertices[i+1][0]);
      y_distances.push(y_length);
    }

    let mut th_distances = vec![0.0];
    let mut th_length = 0.0;
    for i in 0..(vertices[0].len() - 1) {
      th_length += pt_dis(&vertices[0][i], &vertices[0][i+1]);
      th_distances.push(th_length);
    }

    let mut vertex_colors = Vec::new();
    for i in 0..self.n_ys {
      vertex_colors.push(Vec::new());
      for j in 0..self.n_ths {
        let y_tst = ((y_distances[i] / side_length) as i32) % 2;
        let th_tst = ((th_distances[j] / side_length) as i32) % 2;
        if y_tst == th_tst {
          vertex_colors[i].push(1);
        } else {
          vertex_colors[i].push(0);
        }
      }
    }

    self.transform_vertices(&mut vertices, base);

    fn make_triangle(vertices: &Vec<Vec<Vec3>>, colors: &Vec<Vec<u8>>, (i1, j1): (usize, usize), (i2, j2): (usize, usize), (i3, j3): (usize, usize)) -> Triangle {
      let count = colors[i1][j1] + colors[i2][j2] + colors[i3][j3];
      let color = if count >= 2 {
        YELLOW
      } else {
        WHITE
      };
      Triangle::new([vertices[i1][j1], vertices[i2][j2], vertices[i3][j3]]).color(color)
    }

    let mut triangles = Vec::new();
    for i in 0..(self.n_ys - 1) {
      for j in 0..(self.n_ths) {
        let j2 = if j == self.n_ths - 1 {
          0
        } else {
          j + 1
        };
        triangles.push(make_triangle(&vertices, &vertex_colors, (i, j), (i, j2), (i+1, j2)));
        triangles.push(make_triangle(&vertices, &vertex_colors, (i, j), (i+1, j), (i+1, j2)));
      }
    }
    triangles


  }

  /// Returns [y][th]
  fn get_y_th_mesh(&self, height: f32) -> Vec<Vec<(f32, f32)>> {
    let y_inc = height / (self.n_ys as f32);
    let th_inc = 2.0 * PI / (self.n_ths as f32);

    (0..self.n_ys)
      .map(|i| y_inc * (i as f32))
      .map(|y| (0..self.n_ths).map(|j| (y, th_inc * (j as f32))).collect()).collect()
  }

  fn get_vertices(&self, height: f32, radius: fn(f32) -> f32) -> Vec<Vec<Vec3>> {
    self.get_y_th_mesh(height).into_iter()
      .map(|vs| vs.into_iter()
        .map(|(y, th)| {
          let r = radius(y);
          [r * th.cos(),  y, r * th.sin()]
        }).collect()).collect()
  }

  fn transform_vertices(&self, vertices: &mut Vec<Vec<Vec3>>, base: Vec3) {
    for i in 0..vertices.len() {
      for j in 0..vertices[i].len() {
        vertices[i][j][0] += base[0];
        vertices[i][j][1] += base[1];
        vertices[i][j][2] += base[2];
      }
    }

  }

  // Takes a base point, a height, and a function giving the radius as a function of the
  // y-coordinate and converts the given surface into a mesh of triangles.
  pub fn make_pot_mesh(&self, base: Vec3, height: f32, color: Color, radius: fn(f32) -> f32) -> Vec<Triangle> {
    let mut vertices = self.get_vertices(height, radius);
    self.transform_vertices(&mut vertices, base);

    println!("Computing mesh with {} x {} = {} vertices", self.n_ys, self.n_ths, self.n_ys * self.n_ths);

    let mut triangles = Vec::new();
    for i in 0..(self.n_ys - 1) {
      for j in 0..(self.n_ths) {
        let j2 = if j == self.n_ths - 1 {
          0
        } else {
          j + 1
        };
        triangles.push(Triangle::new([vertices[i][j], vertices[i][j2], vertices[i+1][j2]]).color(color));
        triangles.push(Triangle::new([vertices[i][j], vertices[i+1][j], vertices[i+1][j2]]).color(color));
      }
    }
    triangles
  }

}
