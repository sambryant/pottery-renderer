use crate::{
  complex_renderer::*,
  scene::*,
  triangle::*,
  color::*,
  pot::*,
};
use std::time::{Instant};
use super::graphics::*;

pub fn test() {
  // test_1();
  test_4();
  // test_5();
  // test_2();
}

/// Looks at positive z-axis. Expected output:
///             .     .
///
///          .     .
pub fn test_1() {
  let camera = Camera::new(1.0, [0.0, -0.5, -2.0], [0.0, 0.3, 1.0]);
  let mut s = Scene::new().with_camera(camera);
  let renderer = ComplexRenderer::new(&s, 2, 2);

  let vs = [
    [-0.5, 0.0, 0.0],
    [ 0.5, 0.0,  0.0],
    [0.0, 1.0,  0.0],
    [0.0, 0.0,  -0.5],
  ];
  s.objects.push(Triangle::new([vs[0], vs[1], vs[2]]).color(BLUE));
  s.objects.push(Triangle::new([vs[1], vs[2], vs[3]]).color(RED));
  s.objects.push(Triangle::new([vs[2], vs[3], vs[0]]).color(PURPLE));
  s.objects.push(Triangle::new([vs[3], vs[0], vs[1]]).color(YELLOW));

  s.light_source = Some([2.0, 10.0, 0.0]);

  let r = renderer.render(&s);
  draw_complex_render(r, "output/test-complex-2b.png");
}

// /// Looks at negative z-axis.
// pub fn test_2() {
//   let mut s = Scene::new();
//   let renderer = ComplexRenderer::new(&s, 2, 2);

//   s.camera_pos = [0.0, 0.75, 2.0];
//   s.camera_dir = pmath_vec3_normalize(&[0.0, -0.3, -1.0]);
//   s.light_source = Some([2.0, 0.0, 15.0]);
//   s.ambient_light = 0.5;

//   let vs = [
//     [-0.5, 0.0, 0.0],
//     [ 0.5, 0.0,  0.0],
//     [0.0, 1.0,  0.0],
//     [0.0, 0.0,  0.5],
//   ];
//   s.objects.push(Triangle::new([vs[0], vs[1], vs[2]]).color(RED));
//   s.objects.push(Triangle::new([vs[1], vs[2], vs[3]]).color(RED));
//   s.objects.push(Triangle::new([vs[2], vs[3], vs[0]]).color(RED));
//   s.objects.push(Triangle::new([vs[3], vs[0], vs[1]]).color(RED));


//   let r = renderer.render(&s);
//   draw_complex_render(r, "output/test-complex-3.png");
// }

// /// Looks at negative z-axis.
// pub fn test_3() {
//   let mut s = Scene::new();
//   let renderer = ComplexRenderer::new(&s, 8, 8);

//   s.camera_pos = [0.0, 2.00, 4.0];
//   s.camera_dir = pmath_vec3_normalize(&[0.0, -0.2, -1.0]);
//   s.light_source = Some([5.0, 15.0, 2.0]);
//   s.ambient_light = 0.0;

//   fn pot_function(z: f32) -> f32 {
//     let z = 3.0 - z;
//     1.0 + z * z * z * (-0.01 + 0.01 * z +0.035 * z * z - 0.011 * z * z * z - 0.0005 * z * z * z * z)
//   }
//   let height = 3.0;
//   let base = [0.0, 0.0, 0.0];
//   let color = BLUE;
//   let pm = PotMaker { n_ths: 40, n_ys: 20 };
//   let triangles = pm.make_pot_mesh(base, height, color, pot_function);
//   for t in triangles.into_iter() {
//     s.objects.push(t);
//   }

//   s.objects.push(Triangle::new([
//       [-4.0, 0.0, -4.0],
//       [ 4.0, 0.0, -4.0],
//       [-4.0, 0.0,  4.0]]
//     ).color(GRAY));
//   s.objects.push(Triangle::new([
//       [ 4.0, 0.0,  4.0],
//       [ 4.0, 0.0, -4.0],
//       [-4.0, 0.0,  4.0]]
//     ).color(GRAY));

//   s.objects.push(
//     Triangle::new([
//       [-1.0, 0.0, 0.0],
//       [-0.9, 0.0, 0.1],
//       [-0.9, 0.0, -0.1]]
//     ).color(RED));

//   let r = renderer.render(&s);
//   draw_complex_render(r, "output/test-complex-pot-1.png");
// }

pub fn test_4() {
  let camera = Camera::new(1.0, [0.0, 4.0, 5.0], [0.0, -0.3, -1.0]);
  let mut s = Scene::new().with_camera(camera);
  let renderer = ComplexRenderer::new(&s, 16, 16);
  s.light_source = Some([20.0, 20.0, 50.0]);
  s.ambient_light = 0.5;

  fn pot_function(z: f32) -> f32 {
    let z = 3.0 - z;
    1.0 + z * z * z * (-0.01 + 0.01 * z +0.035 * z * z - 0.011 * z * z * z - 0.0005 * z * z * z * z)
  }
  let height = 3.0;
  let base = [0.0, 0.0, 0.0];
  let color = BLUE;
  let pm = PotMaker { n_ths: 40, n_ys: 20 };
  let triangles = pm.make_pot_mesh(base, height, color, pot_function);
  for t in triangles.into_iter() {
    s.objects.push(t);
  }

  s.objects.push(Triangle::new([
      [-4.0, 0.0, -4.0],
      [ 4.0, 0.0, -4.0],
      [-4.0, 0.0,  4.0]]
    ).color(GRAY));
  s.objects.push(Triangle::new([
      [ 4.0, 0.0,  4.0],
      [ 4.0, 0.0, -4.0],
      [-4.0, 0.0,  4.0]]
    ).color(GRAY));

  s.objects.push(
    Triangle::new([
      [-4.1, 0.1, 0.0],
      [-3.9, 0.1, 0.2],
      [-3.9, 0.1, -0.2]]
    ).color(RED));

  let t0 = Instant::now();
  let r = renderer.render(&s);
  let elapsed = t0.elapsed().as_millis();

  println!("Finished rendering in {:?}ms", elapsed);

  draw_complex_render(r, "output/test-complex-pot-2b.png");
}

pub fn test_5() {
  let camera = Camera::new(1.0, [-5.0, 4.0, 0.0], [1.0, -0.3, 0.0]);
  let mut s = Scene::new().with_camera(camera);
  let renderer = ComplexRenderer::new(&s, 16, 16);
  s.light_source = Some([-50.0, 20.0, -20.0]);
  s.ambient_light = 0.5;

  fn pot_function(z: f32) -> f32 {
    let z = 3.0 - z;
    1.0 + z * z * z * (-0.01 + 0.01 * z +0.035 * z * z - 0.011 * z * z * z - 0.0005 * z * z * z * z)
  }
  let height = 3.0;
  let base = [0.0, 0.0, 0.0];
  let color = BLUE;
  let pm = PotMaker { n_ths: 40, n_ys: 20 };
  let triangles = pm.make_pot_mesh(base, height, color, pot_function);
  for t in triangles.into_iter() {
    s.objects.push(t);
  }

  s.objects.push(Triangle::new([
      [-4.0, 0.0, -4.0],
      [ 4.0, 0.0, -4.0],
      [-4.0, 0.0,  4.0]]
    ).color(GRAY));
  s.objects.push(Triangle::new([
      [ 4.0, 0.0,  4.0],
      [ 4.0, 0.0, -4.0],
      [-4.0, 0.0,  4.0]]
    ).color(GRAY));

  s.objects.push(
    Triangle::new([
      [-4.1, 0.1, 0.0],
      [-3.9, 0.1, 0.2],
      [-3.9, 0.1, -0.2]]
    ).color(RED));

  let r = renderer.render(&s);
  draw_complex_render(r, "output/test-complex-x-shader2.png");
}

