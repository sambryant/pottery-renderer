use crate::{
  complex_renderer_opt::{ComplexRendererOpt, ComplexRendererOptBuffer},
  scene::*,
  triangle::*,
  color::*,
  pot::*,
  perf::*,
};
use super::graphics::*;

pub fn test() {
  // test_1();
  // test_4();
  // test_5();
  test_6();
}

pub fn test_5() {
  let mut perf = Perf::new(true);
  let camera = Camera::new(1.0, [-5.0, 4.0, 0.0], [1.0, -0.3, 0.0]);
  let mut s = Scene::new().with_camera(camera);
  let renderer = ComplexRendererOpt::new(&s, 16, 16);
  s.light_source = Some([-50.0, 20.0, -20.0]);
  s.ambient_light = 0.5;

  fn pot_function(z: f32) -> f32 {
    let z = 3.0 - z;
    1.0 + z * z * z * (-0.01 + 0.01 * z +0.035 * z * z - 0.011 * z * z * z - 0.0005 * z * z * z * z)
  }
  let height = 3.0;
  let base = [0.0, 0.0, 0.0];
  let color = BLUE;
  let pm = PotMaker { n_ths: 40, n_ys: 20 };
  let triangles = pm.make_pot_mesh(base, height, color, pot_function);
  for t in triangles.into_iter() {
    s.objects.push(t);
  }

  s.objects.push(Triangle::new([
      [-4.0, 0.0, -4.0],
      [ 4.0, 0.0, -4.0],
      [-4.0, 0.0,  4.0]]
    ).color(GRAY));
  s.objects.push(Triangle::new([
      [ 4.0, 0.0,  4.0],
      [ 4.0, 0.0, -4.0],
      [-4.0, 0.0,  4.0]]
    ).color(GRAY));

  s.objects.push(
    Triangle::new([
      [-4.1, 0.1, 0.0],
      [-3.9, 0.1, 0.2],
      [-3.9, 0.1, -0.2]]
    ).color(RED));
  let mut buffer = ComplexRendererOptBuffer::allocate(&s);
  renderer.render(&s, &mut buffer, &mut perf);
  draw_complex_render_opt(&buffer, "output/test-complex-opt-x-shader2.png");
  perf.report();
}

pub fn test_6() {
  let mut perf = Perf::new(false);
  let camera = Camera::new(1.0, [-5.0, 4.0, 0.0], [1.0, -0.3, 0.0]);
  let mut s = Scene::new().with_camera(camera);
  let renderer = ComplexRendererOpt::new(&s, 64, 64);
  s.light_source = Some([-50.0, 20.0, -20.0]);
  s.ambient_light = 0.5;


  fn pot_function(z: f32) -> f32 {
    let z = 3.0 - z;
    1.0 + z * z * z * (-0.01 + 0.01 * z +0.035 * z * z - 0.011 * z * z * z - 0.0005 * z * z * z * z)
  }
  let side_length = 0.5;
  let height = 3.0;
  let base = [0.0, 0.0, 0.0];
  let color = BLUE;
  let pm = PotMaker { n_ths: 100, n_ys: 100 };
  let triangles = pm.make_fancy_pot_mesh(base, height, pot_function, side_length);
  for t in triangles.into_iter() {
    s.objects.push(t);
  }

  s.objects.push(Triangle::new([
      [-4.0, 0.0, -4.0],
      [ 4.0, 0.0, -4.0],
      [-4.0, 0.0,  4.0]]
    ).color(GRAY));
  s.objects.push(Triangle::new([
      [ 4.0, 0.0,  4.0],
      [ 4.0, 0.0, -4.0],
      [-4.0, 0.0,  4.0]]
    ).color(GRAY));

  println!("Rendering scene with {} triangles", s.objects.len());

  let mut buffer = ComplexRendererOptBuffer::allocate(&s);
  renderer.render(&s, &mut buffer, &mut perf);
  draw_complex_render_opt(&buffer, "output/test-complex-pot-fancy-color-1.png");
  perf.report();
}

