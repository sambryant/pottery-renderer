use image::{ImageBuffer, Rgb};

use crate::{
  color::clr_to_array,
  simple_renderer::SimpleRenderResult,
  complex_renderer::ComplexRenderResult,
  complex_renderer_opt::{ComplexRendererOptBuffer},
  fast_renderer::FastRenderer,
};

pub fn draw_simple_render(r: SimpleRenderResult, fname: &'static str) {
  let color: [u8; 3] = [255, 255, 255];

  let (bounds, screen, vertices, flip_x, flip_y) = (r.viewport_bounds, r.screen, r.vertices, r.flip_x, r.flip_y);
  let mut image = ImageBuffer::new(screen[0], screen[1]);

  let (x1, x2, y1, y2) = (bounds[0][0], bounds[0][1], bounds[1][0], bounds[1][1]);


  for v in vertices.into_iter() {
    let (x, y) = (v[0], v[1]);
    if x < x1 || x > x2 || y < y1 || y > y2 {
      continue;
    }
    let px_x = if flip_x {
      screen[0] - 1 - ((x - x1) / (x2 - x1) * (screen[0] as f32)) as u32
    } else {
      ((x - x1) / (x2 - x1) * (screen[0] as f32)) as u32
    };
    let px_y = if flip_y {
      screen[1] - 1 - (((y - y1) / (y2 - y1)) * (screen[1] as f32)) as u32
    } else {
      (((y - y1) / (y2 - y1)) * (screen[1] as f32)) as u32
    };
    image.put_pixel(px_x, px_y, Rgb::from(color));
  }
  image.save(fname).unwrap();
  println!("Saved image: {}", fname);
}

pub fn draw_complex_render(r: ComplexRenderResult, fname: &'static str) {
  let width = r.colors.len() as u32;
  let height = r.colors[0].len() as u32;

  let mut image = ImageBuffer::new(width, height);

  for i in 0..width {
    for j in 0..height {
      let iact = if r.flip_x { width - 1 - i } else { i };
      let jact = if r.flip_y { height - 1 - j } else { j };
      image.put_pixel(iact, jact, Rgb::from(clr_to_array(r.colors[i as usize][j as usize])));
    }
  }
  image.save(fname).unwrap();
  println!("Saved image: {}", fname);
}

pub fn draw_complex_render_opt(buffer: &ComplexRendererOptBuffer, fname: &'static str) {
  let width = buffer.rendered.len() as u32;
  let height = buffer.rendered[0].len() as u32;

  let mut image = ImageBuffer::new(width, height);

  for i in 0..width {
    for j in 0..height {
      let iact = if buffer.flip_x { width - 1 - i } else { i };
      let jact = if buffer.flip_y { height - 1 - j } else { j };
      image.put_pixel(iact, jact, Rgb::from(clr_to_array(buffer.rendered[i as usize][j as usize])));
    }
  }
  image.save(fname).unwrap();
  println!("Saved image: {}", fname);
}
