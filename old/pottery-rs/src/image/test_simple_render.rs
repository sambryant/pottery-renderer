use crate::{
  simple_renderer::*,
  scene::*,
  triangle::*,
  pmath::*,
  color::*,
  pot::*,
};
use super::graphics::*;

pub fn test() {
  // test_1();
  // test_2();
  // test_3();
  // test_4();
  // test_5();
  // test_6();
}

// /// Looks at positive x-axis. Expected output:
// ///             .     .
// ///
// ///          .     .
// pub fn test_1() {
//   let mut s = Scene::new();
//   let renderer = SimpleRenderer{};

//   s.camera_pos = [-2.0, 0.0, 0.0];
//   s.camera_dir = [1.0, 0.0, 0.0];
//   s.objects.push(Triangle::new([
//     [0.0, 0.0, -0.5],
//     [0.0, 0.0,  0.5],
//     [0.0, 1.0,  0.0],
//   ]));
//   s.objects.push(Triangle::new([
//     [0.0, 0.0,  0.5],
//     [0.0, 1.0,  0.0],
//     [0.0, 1.0,  1.0],
//   ]));

//   let r = renderer.render(&s);
//   draw_simple_render(r, "output/test-simple-1.png");
// }

// /// Looks at positive z-axis. Expected output:
// ///             .     .
// ///
// ///          .     .
// pub fn test_2() {
//   let mut s = Scene::new();
//   let renderer = SimpleRenderer{};

//   s.camera_pos = [0.0, 0.0, -2.0];
//   s.camera_dir = [0.0, 0.0, 1.0];
//   s.objects.push(Triangle::new([
//     [-0.5, 0.0, 0.0],
//     [ 0.5, 0.0,  0.0],
//     [0.0, 1.0,  0.0],
//   ]));
//   s.objects.push(Triangle::new([
//     [-0.5, 0.0,  0.0],
//     [0.0, 1.0,  0.0],
//     [-1.0, 1.0,  0.0],
//   ]));

//   let r = renderer.render(&s);
//   draw_simple_render(r, "output/test-simple-2.png");

//   // s.render(vertex);
// }

// /// Looks at negative x-axis. Expected output:
// ///             .     .
// ///
// ///          .     .
// pub fn test_3() {
//   let mut s = Scene::new();
//   let renderer = SimpleRenderer{};

//   s.camera_pos = [2.0, 0.0, 0.0];
//   s.camera_dir = [-1.0, 0.0, 0.0];
//   s.objects.push(Triangle::new([
//     [0.0, 0.0, -0.5],
//     [0.0, 0.0,  0.5],
//     [0.0, 1.0,  0.0],
//   ]));
//   s.objects.push(Triangle::new([
//     [0.0, 0.0,  -0.5],
//     [0.0, 1.0,  0.0],
//     [0.0, 1.0,  -1.0],
//   ]));

//   let r = renderer.render(&s);
//   draw_simple_render(r, "output/test-simple-3.png");

//   // s.render(vertex);
// }

// /// Looks at negative z-axis. Expected output:
// ///             .     .
// ///
// ///          .     .
// pub fn test_4() {
//   let mut s = Scene::new();
//   let renderer = SimpleRenderer{};

//   s.camera_pos = [0.0, 0.0, 2.0];
//   s.camera_dir = [0.0, 0.0, -1.0];
//   s.objects.push(Triangle::new([
//     [-0.5, 0.0, 0.0],
//     [ 0.5, 0.0,  0.0],
//     [0.0, 1.0,  0.0],
//   ]));
//   s.objects.push(Triangle::new([
//     [0.5, 0.0,  0.0],
//     [0.0, 1.0,  0.0],
//     [1.0, 1.0,  0.0],
//   ]));

//   let r = renderer.render(&s);
//   draw_simple_render(r, "output/test-simple-4.png");
// }

// /// Looks at negative z-axis. Expected output: tetrahedron
// pub fn test_5() {
//   let mut s = Scene::new();
//   let renderer = SimpleRenderer {};

//   s.camera_pos = [0.0, 0.5, 2.0];
//   s.camera_dir = pmath_vec3_normalize(&[0.0, -0.3, -1.0]);

//   let vs = [
//     [-0.5, 0.0, 0.0],
//     [ 0.5, 0.0,  0.0],
//     [0.0, 1.0,  0.0],
//     [0.0, 0.0,  0.5],
//   ];
//   // s.objects.push(Triangle::new([vs[0], vs[1], vs[2]]).color(BLUE));
//   s.objects.push(Triangle::new([vs[1], vs[2], vs[3]]).color(RED));
//   // s.objects.push(Triangle::new([vs[2], vs[3], vs[0]]).color(PURPLE));
//   // s.objects.push(Triangle::new([vs[3], vs[0], vs[1]]).color(YELLOW));

//   let r = renderer.render(&s);
//   draw_simple_render(r, "output/test-simple-5.png");
// }

// pub fn test_6() {
//   let mut s = Scene::new();
//   let renderer = SimpleRenderer {};

//   s.camera_pos = [0.0, 2.00, 4.0];
//   s.camera_dir = pmath_vec3_normalize(&[0.0, -0.2, -1.0]);
//   // s.light_source = Some([2.0, 0.0, 15.0]);

//   fn pot_function(z: f32) -> f32 {
//     let z = 3.0 - z;
//     1.0 + z * z * z * (-0.01 + 0.01 * z +0.035 * z * z - 0.011 * z * z * z - 0.0005 * z * z * z * z)
//   }
//   let height = 3.0;
//   let base = [0.0, 0.0, 0.0];
//   let color = BLUE;
//   let pm = PotMaker { n_ths: 40, n_ys: 20 };
//   let triangles = pm.make_pot_mesh(base, height, color, pot_function);
//   for t in triangles.into_iter() {
//     s.objects.push(t);
//   }

//   let r = renderer.render(&s);
//   draw_simple_render(r, "output/test-simple-pot-1.png");
// }

