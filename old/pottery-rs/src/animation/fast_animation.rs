use sdl2::EventPump;
use sdl2::render::Canvas;
use sdl2::video::Window;
use std::time::{Instant, Duration};

use crate::{
  scene::Scene,
  fast_renderer::FastRenderer,
};
use super::{
  graphics,
  key_handler,
};

pub struct Animation {
  pub canvas: Canvas<Window>,
  pub config: Config,
  pub scene: Scene,
  pub event_pump: EventPump,
  pub renderer: FastRenderer,
}

fn handle_events(scene: &mut Scene, ep: &mut EventPump) -> Result<(), ()> {
  use sdl2::event::Event;
  for event in ep.poll_iter() {
    // todo!()
    match event {
      Event::KeyDown { keycode, keymod, .. } => match keycode {
        Some(kc) => {
          if key_handler::handle_key_event(scene, kc, keymod).is_err() {
            return Err(())
          }
        },
        _ => {}
      }
      _ => {},
    }
  }
  Ok(())
}

/// Miscellaneous global parameters.
pub struct Config {
  /// Sleep time at end of each game loop, will eventually be removed in favor of a vsync-based
  /// solution, or at least one that takes into account the run time of the thread.
  pub sleep_time: Duration,
  pub loop_limit: Option<usize>,
}

/* Implementations */
impl Config {
  pub fn new(scene: &Scene) -> Config {
    Config {
      loop_limit: None,
      sleep_time: Duration::new(0, 1_000_000_000u32 / 10),
    }
  }
}

impl Animation {
  pub fn new(scene: Scene) -> Result<Self, String> {
    let sdl = sdl2::init().unwrap();

    let event_pump = sdl.event_pump().unwrap();

    let canvas = sdl.video()
      .expect("Failed to get video subsystem")
      .window("Game", scene.screen[0], scene.screen[1])
      .resizable()
      .build()
      .expect("Failed to get window")
      .into_canvas()
      .build()
      .expect("Failed to build canvas for window");

    let config = Config::new(&scene);
    let renderer = FastRenderer::new(&scene);

    Ok(Self {
      canvas, config, scene, event_pump, renderer
    })
  }

  pub fn run(&mut self) -> Result<(), String> {
    self.canvas.present();

    let loop_time = self.config.sleep_time;
    let mut total_run_time = Duration::new(0, 0);
    let mut total_sleep_time = Duration::new(0, 0);
    let mut loop_count = 0;

    let mut loop_start = Instant::now();
    let total_timer = Instant::now();
    loop {
      if handle_events(&mut self.scene, &mut self.event_pump).is_err() {
        break;
      }
      graphics::fast_render(
        &self.config,
        &mut self.canvas,
        &mut self.scene,
        &mut self.renderer,
      )?;

      let elapsed_time = loop_start.elapsed();

      if let Some(sleep_time) = loop_time.checked_sub(elapsed_time) {
        std::thread::sleep(sleep_time);
        total_run_time += elapsed_time;
        total_sleep_time += sleep_time;
      } else {
        total_run_time += elapsed_time;
        println!("Cant keep up!");
      }
      loop_start = Instant::now();

      loop_count += 1;
      if let Some(limit) = self.config.loop_limit {
        if loop_count == limit {
          break
        }
      }

    }
    let total_time = total_timer.elapsed();

    println!("Total number loops: {}", loop_count);
    println!("Total time        : {}", (total_time.as_nanos() as f64) / 1_000_000_000.0);
    println!("Avg framerate     : {}", ((loop_count as f64) / (total_time.as_nanos() as f64) * 1_000_000_000.0));

    println!("Finishing up");
    let run_time = total_run_time.as_nanos() as f64;
    let slp_time = total_sleep_time.as_nanos() as f64;
    let run_avg = run_time / (loop_count as f64);
    let slp_avg = slp_time / (loop_count as f64);
    let target_avg = loop_time.as_nanos() as f64;

    println!("number of loops: {}", loop_count);
    println!("ns per update: {}", run_avg);
    println!("ns per sleep : {}", slp_avg);
    println!("% budget used: {}", run_avg / target_avg);

    Ok(())
  }

}

