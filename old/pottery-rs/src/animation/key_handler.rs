use sdl2::keyboard::{
  Keycode,
  Mod,
};
use crate::scene::Scene;

pub fn handle_key_event(scene: &mut Scene, keycode: Keycode, modifier: Mod) -> Result<(), ()> {
  use Keycode::*;
  if keycode == Escape {
    return Err(())
  }

  match modifier {
    Mod::NOMOD => match keycode {
      Left => {
        scene.camera.move_pos(0, -0.1);
      },
      Right => {
        scene.camera.move_pos(0,  0.1);
      },
      Up => {
        scene.camera.move_pos(2,  0.1);
      },
      Down => {
        scene.camera.move_pos(2, -0.1);
      },
      Q => {
        scene.camera.move_pos(1, -0.1);
      },
      E => {
        scene.camera.move_pos(1,  0.1);
      },
      I => {
        scene.camera.print_info();
      },
      _ => {}
    },
    Mod::LSHIFTMOD => match keycode {
      Up => {
        scene.camera.tilt( 0.1);
      },
      Down => {
        scene.camera.tilt(-0.1);
      },
      Left => {
        scene.camera.pan( -0.1);
      },
      Right => {
        scene.camera.pan( 0.1);
      },
      _ => {}
    },
    _ => {
      println!("Key Event: {}, {}", modifier, keycode);
    }
  }
  Ok(())
}
