use std::time::Duration;
use crate::{
  scene::*,
  triangle::*,
  color::*,
  pot::*,
};
use super::fast_animation::Animation;

pub fn go() {
  let camera = Camera::new(1.0, [0.0, 4.0, 5.0], [0.0, -0.3, -1.0]);
  let mut s = Scene::new().with_camera(camera);
  s.light_source = Some([20.0, 20.0, 50.0]);
  s.ambient_light = 0.5;

  fn pot_function(z: f32) -> f32 {
    let z = 3.0 - z;
    1.0 + z * z * z * (-0.01 + 0.01 * z +0.035 * z * z - 0.011 * z * z * z - 0.0005 * z * z * z * z)
  }
  let height = 3.0;
  let base = [0.0, 0.0, 0.0];
  let color = BLUE;
  let pm = PotMaker { n_ths: 40, n_ys: 20 };
  let triangles = pm.make_pot_mesh(base, height, color, pot_function);
  for t in triangles.into_iter() {
    s.objects.push(t);
  }

  s.objects.push(Triangle::new([
      [-4.0, 0.0, -4.0],
      [ 4.0, 0.0, -4.0],
      [-4.0, 0.0,  4.0]]
    ).color(GRAY));
  s.objects.push(Triangle::new([
      [ 4.0, 0.0,  4.0],
      [ 4.0, 0.0, -4.0],
      [-4.0, 0.0,  4.0]]
    ).color(GRAY));

  let mut animation = Animation::new(s).unwrap();
  animation.run().unwrap();
}

pub fn test_performance() {
  let camera = Camera::new(1.0, [0.0, 4.0, 5.0], [0.0, -0.3, -1.0]);
  let mut s = Scene::new().with_camera(camera);
  s.light_source = Some([20.0, 20.0, 50.0]);
  s.ambient_light = 0.5;

  fn pot_function(z: f32) -> f32 {
    let z = 3.0 - z;
    1.0 + z * z * z * (-0.01 + 0.01 * z +0.035 * z * z - 0.011 * z * z * z - 0.0005 * z * z * z * z)
  }
  let height = 3.0;
  let base = [0.0, 0.0, 0.0];
  let color = BLUE;
  let pm = PotMaker { n_ths: 40, n_ys: 20 };
  let triangles = pm.make_pot_mesh(base, height, color, pot_function);
  for t in triangles.into_iter() {
    s.objects.push(t);
  }
  s.objects.push(Triangle::new([
      [-4.0, 0.0, -4.0],
      [ 4.0, 0.0, -4.0],
      [-4.0, 0.0,  4.0]]
    ).color(GRAY));
  s.objects.push(Triangle::new([
      [ 4.0, 0.0,  4.0],
      [ 4.0, 0.0, -4.0],
      [-4.0, 0.0,  4.0]]
    ).color(GRAY));

  let mut animation = Animation::new(s).unwrap();
  animation.config.sleep_time = Duration::new(0, 0);
  animation.config.loop_limit = Some(20000);
  animation.run().unwrap();
}


