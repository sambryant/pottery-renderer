use crate::complex_renderer_opt::ComplexRendererOptBuffer;
use sdl2::pixels::Color;
use sdl2::render::WindowCanvas;
use std::time::{Instant, Duration};

use crate::scene::Scene;
use super::animation::Config;
use crate::fast_renderer::FastRenderer;
use super::fast_animation::Config as FastConfig;
use crate::perf::Perf;

pub fn render(
  config: &Config,
  canvas: &mut WindowCanvas,
  scene: &mut Scene,
  buffer: &mut ComplexRendererOptBuffer,
  perf: &mut Perf) -> Result<(), String> {

  config.renderer.render(&scene, buffer, perf);

  let t0 = Instant::now();
  let width = buffer.rendered.len() as u32;
  let height = buffer.rendered[0].len() as u32;
  for i in 0..width {
    for j in 0..height {
      let iact = if buffer.flip_x { width - 1 - i } else { i };
      let jact = if buffer.flip_y { height - 1 - j } else { j };
      canvas.set_draw_color::<Color>(buffer.rendered[i as usize][j as usize].into());
      canvas.draw_point((iact as i32, jact as i32));
    }
  }
  canvas.present();
  perf.log("draw", t0.elapsed().as_nanos());
  Ok(())
}

pub fn fast_render(
  config: &FastConfig,
  canvas: &mut WindowCanvas,
  scene: &mut Scene,
  render: &mut FastRenderer) -> Result<(), String> {
  render.render(scene);
  let [width, height] = [scene.screen[0], scene.screen[1]];

  for (color, pixels) in render.pixel_triangles.iter() {
    canvas.set_draw_color::<Color>((*color).into());
    canvas.draw_line(pixels[0], pixels[1]);
    canvas.draw_line(pixels[1], pixels[2]);
    canvas.draw_line(pixels[2], pixels[0]);
  }

  canvas.present();
  Ok(())
}
