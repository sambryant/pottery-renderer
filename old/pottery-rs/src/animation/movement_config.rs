
pub struct MovementConfig {
  pub velocity_linear: f32,
  pub velocity_angular: f32,
}
