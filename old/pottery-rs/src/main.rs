pub mod animation;
pub mod color;
pub mod scene;
pub mod triangle;
pub mod pmath;
pub mod pot;
pub mod image;
pub mod perf;

pub mod fast_renderer;
pub mod complex_renderer;
pub mod complex_renderer_opt;

pub mod simple_renderer;

// Legacy
// pub mod v1;

fn main() {
  image::test_complex_render::test();
  // image::test_complex_render_opt::test();
  // image::test_complex_render::test();
  // scene::test_camera();
  // animation::test::go();
  // animation::test::test_performance();
  // animation::test_fast::test_performance();
}
