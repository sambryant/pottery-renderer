use crate::triangle::Triangle;
use std::collections::HashMap;
use std::time::{Instant, Duration};
use crate::{
  scene::*,
  pmath::*,
  color::*,
  perf::*,
};

pub struct ComplexRendererOpt {
  pub n_viewport_cols: u32,
  pub n_viewport_rows: u32,
  col_size: f32,
  row_size: f32,
}

pub struct ComplexRendererOptBuffer {
  /// Final output - color for each pixel
  pub rendered: Vec<Vec<Color>>,
  /// Triangle vertices in transformed coordinates.
  transformed_triangles: Vec<[Vec3; 3]>,
  /// 2D projection of vertices of each triangle onto viewport screen in transformed coordinates.
  projected_triangles: Vec<[Vec2; 3]>,
  /// Indicates if the triangle is in front of the camera.
  projected_valid: Vec<bool>,
  /// Shading value for each triangle
  has_shading: bool,
  shading: Vec<Color>,
  /// Bounding sub-boxes for each triangle
  triangle_box_bounds: Vec<[[u32; 2]; 2]>,
  pub flip_x: bool,
  pub flip_y: bool,
}

impl ComplexRendererOptBuffer {

  fn clear(&mut self, scene: &Scene, perf: &mut Perf) {
    // let t0 = Instant::now();
    for i in 0..scene.screen[0] {
      for j in 0..scene.screen[1] {
        self.rendered[i as usize][j as usize] = scene.background.clone();
      }
    }
    // perf.log("clear", t0.elapsed().as_nanos());
  }

  pub fn allocate(scene: &Scene) -> Self {
    let rendered = (0..scene.screen[0])
      .map(|_| (0..scene.screen[1])
        .map(|_| scene.background.clone()).collect()
      ).collect();

    let transformed_triangles = (0..scene.objects.len())
      .map(|_| [[0.0; 3]; 3]).collect();

    let projected_triangles = (0..scene.objects.len())
      .map(|_| [[0.0; 2]; 3]).collect();

    let projected_valid = (0..scene.objects.len())
      .map(|_| false).collect();

    let shading = (0..scene.objects.len())
      .map(|_| (0, 0, 0)).collect();

    let triangle_box_bounds = (0..scene.objects.len())
      .map(|_| [[0, 0], [0, 0]]).collect();
    ComplexRendererOptBuffer {
      rendered,
      projected_triangles,
      shading,
      has_shading: scene.light_source.is_some(),
      triangle_box_bounds,
      transformed_triangles,
      projected_valid,
      flip_x: true,
      flip_y: true,
    }
  }
}

impl ComplexRendererOpt {
  pub fn new(scene: &Scene, n_viewport_cols: u32, n_viewport_rows: u32) -> Self {
    let col_size = scene.viewport[0] / (n_viewport_cols as f32);
    let row_size = scene.viewport[1] / (n_viewport_rows as f32);
    Self {
      n_viewport_cols,
      n_viewport_rows,
      col_size,
      row_size,
    }
  }
}

type SubBox = (u32, u32);

type TriangleIndex = usize;

impl ComplexRendererOpt {

  /// For each triangle, computes the projection of each of its vertices onto the viewport screen.
  fn compute_projected_triangles(scene: &Scene, buffer: &mut ComplexRendererOptBuffer, perf: &mut Perf) {
    // let t0 = Instant::now();
    for (i, obj) in scene.objects.iter().enumerate() {
      scene.transform_triangle_to(&mut buffer.transformed_triangles[i], &obj.vs);
      scene.project_transformed_triangle_to(
        &mut buffer.projected_valid[i],
        &mut buffer.projected_triangles[i],
        &buffer.transformed_triangles[i]);
    }
    // perf.log("compute_projected_triangles", t0.elapsed().as_nanos());
  }

  fn compute_shading(&self, scene: &Scene, buffer: &mut ComplexRendererOptBuffer, perf: &mut Perf) {
    // let t0 = Instant::now();
    if let Some(light_source) = scene.light_source {
      buffer.has_shading = true;
      for (i, obj) in scene.objects.iter().enumerate() {
        if buffer.projected_valid[i] {
          // Vector pointing from midpoint of triangle to light source
          let obj_light_vec = pmath_vec3_normalize(&pmath_sub3(&light_source, &obj.mid));

          // Vector pointing from midpoint of triangle to camera
          let obj_camera_vec = pmath_sub3(&scene.camera.pos, &obj.mid);

          // Lighting on positive side of triangle given by dot product of v1 and normal
          let light = pmath_dot_vec3_vec3(&obj_light_vec, &obj.normal);

          // Determine which side of the triangle we are looking at by dotting v2 and normal
          let check_obj_side = pmath_dot_vec3_vec3(&obj.normal, &obj_camera_vec);
          if check_obj_side > 0.0 {
            let light = (0.5 * ( 1.0 + light)).max(scene.ambient_light);
            buffer.shading[i] = shade_color(&obj.color, light);
          } else {
            let light = (0.5 * ( 1.0 - light)).max(scene.ambient_light);
            buffer.shading[i] = shade_color(&obj.color, light);
          }

        }
      }
    } else {
      buffer.has_shading = false;
    }
    // perf.log("compute_shading", t0.elapsed().as_nanos());
 }

  /// For each subbox on the viewport, determines list of triangles which might intersect it.
  fn compute_triangle_subboxes(&self, scene: &Scene, buffer: &mut ComplexRendererOptBuffer, perf: &mut Perf) -> Option<HashMap<SubBox, Vec<TriangleIndex>>> {
    // let t0 = Instant::now();
    match (self.n_viewport_cols, self.n_viewport_rows) {
      (1, 1) => None,
      _ => {
        // Determine the set of bounding sub-boxes for each triangle. When ray tracing within this
        // subregion, we must check that triangle.
        for (i, proj) in buffer.projected_triangles.iter().enumerate() {
          if buffer.projected_valid[i] {
            self.determine_triangle_bounds(&scene, &mut buffer.triangle_box_bounds[i], &proj);
          }
        }
        // perf.log("compute_triangle_subboxes", t0.elapsed().as_nanos());
        Some(self.build_box_map(buffer))
      }
    }
  }

  fn ray_trace(&self, scene: &Scene, buffer: &mut ComplexRendererOptBuffer, subboxes: Option<HashMap<SubBox, Vec<TriangleIndex>>>, perf: &mut Perf) {
    // Step 4: Ray tracing
    //  - Iterate through each point in the screen.
    //  - Determine which sub box we are in.
    //  - Check for collisions for each triangle in that subbox
    // let t0 = Instant::now();
    if let Some(ref subbox_map) = subboxes {
      for i in 0..scene.screen[0] {
        for j in 0..scene.screen[1] {
          let box_ind = self.get_box_index(&scene, i, j);
          let tri_inds = subbox_map.get(&box_ind).unwrap();
          // let t1 = Instant::now();
          // let objects_to_check: Vec<TriangleIndex> = if let Some(ref subbox_map) = subboxes {

          //   subbox_map.get(&box_ind).unwrap().clone()
          // } else {
          //   (0..scene.objects.len()).collect()
          // };
          // perf.log("rt: obj_to_check", t1.elapsed().as_nanos());

          let t2 = Instant::now();
          let point = self.get_viewport_point(&scene, i, j);
          let mut collisions: Vec<(TriangleIndex, Vec3)> = Vec::new();
          for ind in tri_inds.into_iter() {
            let tri = &scene.objects[*ind];
            if let Some(pt) = self.reverse_project_detect(
              &tri, &buffer.projected_triangles[*ind], point.clone()
            ) {
              collisions.push((*ind, pt));
            }
          }
          // perf.log("rt: obj check", t2.elapsed().as_nanos());

          if let Some(color) = self.render_point(&scene, &buffer, collisions) {
            buffer.rendered[i as usize][j as usize] = color;
          }
        }
      }
    } else {
      todo!()
    }
    // perf.log("ray_trace", t0.elapsed().as_nanos());
  }

  /// Given a vertex projected onto the viewport, determines which sub-box it lies in.
  fn determine_vertex_box(&self, scene: &Scene, projected: &Vec2) -> SubBox {
    let (w, h) = (scene.viewport[0], scene.viewport[1]);
    let (px, py) = (projected[0] + w / 2.0, projected[1] + h / 2.0);
    let (i, j) = ((px / self.col_size) as u32, (py / self.row_size) as u32);
    (i, j)
  }

  /// Determines the indices of the sub-boxes which fully encapsulate a given triangle.
  /// These are the bounds of the sub-boxes which need to check for the triangle to be present.
  fn determine_triangle_bounds(&self, scene: &Scene, dst: &mut [[u32; 2]; 2], projected: &[Vec2; 3]) {
    let mut xs = [0; 3];
    let mut ys = [0; 3];
    for i in 0..3 {
      let (x, y) = self.determine_vertex_box(scene, &projected[i]);
      xs[i] = x;
      ys[i] = y;
    }
    fn get_min_max(arr: [u32; 3]) -> [u32; 2] {
      let (mut min, mut max) = (arr[0], arr[0]);
      for i in 1..3 {
        if arr[i] > max {
          max = arr[i];
        }
        if arr[i] < min {
          min = arr[i];
        }
      }
      [min, max]
    }
    dst[0] = get_min_max(xs);
    dst[1] = get_min_max(ys);
  }

  fn build_box_map(&self, buffer: &ComplexRendererOptBuffer) -> HashMap<(u32, u32), Vec<usize>> {
    let mut map = HashMap::new();
    for i in 0..self.n_viewport_cols {
      for j in 0..self.n_viewport_rows {
        map.insert((i, j), Vec::new());
      }
    }
    for (ind, bounds) in buffer.triangle_box_bounds.iter().enumerate() {
      if buffer.projected_valid[ind] {
        for i in bounds[0][0]..(bounds[0][1]+1) {
          for j in bounds[1][0]..(bounds[1][1]+1) {
            if let Some(b) = map.get_mut(&(i,j)) {
              b.push(ind)
            }
          }
        }
      }
    }
    map
  }

  fn get_box_index(&self, scene: &Scene, i: u32, j: u32) -> (u32, u32) {
    (i * self.n_viewport_cols / scene.screen[0], j * self.n_viewport_rows / scene.screen[1])
  }

  fn get_viewport_point(&self, scene: &Scene, i: u32, j: u32) -> Vec2 {
    [
      ((i as f32) / (scene.screen[0] as f32)) * scene.viewport[0] - scene.viewport[0]/2.0,
      ((j as f32) / (scene.screen[1] as f32)) * scene.viewport[1] - scene.viewport[1]/2.0
    ]
  }

  fn render_point(&self, scene: &Scene, buffer: &ComplexRendererOptBuffer, mut collisions: Vec<(usize, Vec3)>) -> Option<Color> {
    if collisions.len() == 0 {
      None
    } else if collisions.len() == 1 {
      if buffer.has_shading {
        Some(buffer.shading[collisions[0].0].clone())
      } else {
        Some(scene.objects[collisions[0].0].color.clone())
      }
    } else {
      let mut min_distance = None;
      let mut min_collision = None;
      while let Some((ind, pt)) = collisions.pop() {
        let dis = pmath_distance2_vec3_vec3(&scene.camera.pos, &pt);
        if let Some(prev_min) = min_distance {
          if dis < prev_min {
            min_distance = Some(dis);
            min_collision = Some(ind);
          }
        } else {
          min_distance = Some(dis);
          min_collision = Some(ind);
        }
      }
      min_collision.map(|ind| {
        if buffer.has_shading {
          buffer.shading[ind].clone()
        } else {
          scene.objects[ind].color.clone()
        }
      })
    }
  }

  /// Given a triangle in 3D, its projection onto VP, and a projected point P' in the viewport this
  /// computes the point P lying on the triangle in 3D whose projection is P'.
  /// This returns None when the point does not lie in the triangle.
  fn reverse_project_detect(&self, triangle: &Triangle, proj_triangle: &[Vec2; 3], proj_point: Vec2) -> Option<Vec3> {
    fn sub2(v1: &Vec2, v2: &Vec2) -> Vec2 {
      [v1[0] - v2[0], v1[1] - v2[1]]
    }
    let (ap, bp, cp) = (proj_triangle[0], proj_triangle[1], proj_triangle[2]);
    let (rel_bp, rel_cp) = (sub2(&bp, &ap), sub2(&cp, &ap));
    let rel_pp = sub2(&proj_point, &ap);

    let denom = rel_bp[1] * rel_cp[0] - rel_bp[0] * rel_cp[1];
    let t1 = -(rel_pp[0]*rel_cp[1] - rel_pp[1]*rel_cp[0]) / denom;
    let t2 = (rel_pp[0]*rel_bp[1] - rel_pp[1]*rel_bp[0]) / denom;

    if t1 < 0.0 || t2 < 0.0 || (t1 + t2) > 1.0 {
      None
    } else {
      let a = triangle.vs[0];
      let (ba, ca) = (triangle.rel_vs[0], triangle.rel_vs[1]);
      Some([
        a[0] + t1*ba[0] + t2 * ca[0],
        a[1] + t1*ba[1] + t2 * ca[1],
        a[2] + t1*ba[2] + t2 * ca[2],
      ])
    }
  }

  pub fn render(&self, scene: &Scene, buffer: &mut ComplexRendererOptBuffer, perf: &mut Perf) {
    buffer.clear(&scene, perf);

    // Step 1: Project all objects onto viewport screen in coordinate system where viewport at z=0
    Self::compute_projected_triangles(scene, buffer, perf);

    // Step 2: Compute shading for each object in scene.
    self.compute_shading(&scene, buffer, perf);

    // Step 3: For each subbox on the viewport, compute which triangles might intersect it.
    let triangle_subboxes = self.compute_triangle_subboxes(&scene, buffer, perf);

    // Step 4: Ray tracing
    self.ray_trace(scene, buffer, triangle_subboxes, perf);
  }
}
