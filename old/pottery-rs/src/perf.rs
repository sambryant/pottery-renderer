use std::collections::HashMap;

pub struct Perf {
  on: bool,
  calls: HashMap<&'static str, (usize, u128)>,
}

impl Perf {
  pub fn new(on: bool) -> Self {
    Self {
      on,
      calls: HashMap::new(),
    }
  }

  pub fn log(&mut self, name: &'static str, time: u128) {
    if self.on {
      match self.calls.get_mut(name) {
        Some(calls) => {
          calls.0 += 1;
          calls.1 += time;
        },
        None => {
          self.calls.insert(name, (1, time));
        }
      }
    }
  }

  pub fn report(self) {
    if self.on {
      println!("Performance report");
      println!("{:30} | {:10} | {} | {}", "method", "#", "total time (ns)", "avg (ns)");
      for (name, (calls, time)) in self.calls.into_iter() {
        let avg = (time as f64) / (calls as f64);
        println!("{:30} | {:10} | {} | {:.2}", name, calls, time, avg);
      }
    }
  }
}


