use crate::{
  pmath::*,
  triangle::*,
  color::*,
};

pub struct Scene {
  pub background: Color,
  pub camera: Camera,
  pub light_source: Option<Vec3>,
  pub ambient_light: f32,
  // pub camera_pos: Vec3,
  // pub camera_dir: Vec3,
  pub dir_horizontal: Vec3,
  pub dir_vertical: Vec3,
  pub viewport: Vec2, // width, height
  pub screen: [u32; 2],
  pub objects: Vec<Triangle>,
}

pub struct Camera {
  pub pos: Vec3,
  // Angle between direction of view and y-axis (like polar angle but for y)
  angle1: f32,
  // Angle between xz direction of view and z-axis (like azimuthal angle but for zx)
  angle2: f32,
  // The left-right, up-down, and forward-back normalized viewing vectors
  directions: [Vec3; 3],
  // Distance from camera eye to viewport
  viewport_distance: f32,
  // Transformer that converts points to coordinate system where viewport is at z=0
  transformer: CoordinateTransformer1,
}

/// Transforms vertices to system where viewport is at z=0 plane and parallel to x-axis/y-axis.
pub trait CoordinateTransformer<T> {
  fn transform_vertex(&self, v: &T) -> T;
  fn transform_direction(&self, v: &T) -> T;
}

pub struct CoordinateTransformer1 {
  rotation: Mat3,
  translation: Vec3,
}

pub fn test_camera() {
  let mut c = Camera::new(1.0, [0.0, 0.0, 0.0], [0.0, 0.0, 1.0]);
  c.set_view_from_angles(PI / 4.0, 0.0);
  c.set_view_from_angles(0.0, PI / 8.0);
  c.print_info();
}

impl Camera {

  pub fn new(viewport_distance: f32, pos: Vec3, dir: Vec3) -> Camera {
    let dir = pmath_vec3_normalize(&dir);
    let angle1 = dir[0].atan2(dir[2]);
    let angle2 = dir[1].asin();
    let rotation1 = pmath_mat3_get_rotation_y(-angle1);
    let rotation2 = pmath_mat3_get_rotation_x( angle2);
    let rotation = pmath_mul_mat3_mat3(&rotation2, &rotation1);
    let inverse_rotation = pmath_mul_mat3_mat3(
      &pmath_mat3_get_rotation_y( angle1),
      &pmath_mat3_get_rotation_x(-angle2));

    let pos_rotated = pmath_mul_mat3_vec3(&rotation, &pos);
    let translation = [-pos_rotated[0], -pos_rotated[1], -pos_rotated[2] - viewport_distance];
    let transformer = CoordinateTransformer1 {
      translation,
      rotation,
    };
    let mut directions = pmath_transpose_mat3(&inverse_rotation);
    for i in 0..3 {
      directions[0][i] = -directions[0][i];
    }
    Camera {
      viewport_distance,
      angle1,
      angle2,
      pos,
      directions,
      transformer,
    }
  }

  pub fn print_info(&self) {
    println!("Camera");
    println!("  ang1 = {};", self.angle1);
    println!("  ang2 = {};", self.angle2);
    println!("  pos = {};", fmt_vec3(&self.pos));
    for i in 0..3 {
      println!("  dir{} = {};", i, fmt_vec3(&self.directions[i]));
    }
  }

  pub fn move_pos(&mut self, axis: usize, inc: f32) {
    let pos = pmath_add3(&self.pos, &pmath_mul_vec3(inc, &self.directions[axis]));
    self.set_camera_pos(pos);
  }

  pub fn set_camera_pos(&mut self, pos: Vec3) {
    let pos_rotated = self.transformer.transform_direction(&pos);
    let translation = [-pos_rotated[0], -pos_rotated[1], -pos_rotated[2] - self.viewport_distance];
    self.transformer.translation = translation;
    self.pos = pos;
  }

  pub fn tilt(&mut self, inc: f32) {
    let angle2 = (self.angle2 + inc).max(-PI / 2.0).min(PI / 2.0);
    if angle2 != self.angle2 {
      self.set_view_from_angles(self.angle1, angle2);
    }
  }

  pub fn pan(&mut self, inc: f32) {
    let angle1 = (self.angle1 - inc) % (2.0 * PI);
    self.set_view_from_angles(angle1, self.angle2);
  }

  pub fn set_view_from_angles(&mut self, angle1: f32, angle2: f32) {
    let rotation1 = pmath_mat3_get_rotation_y(-angle1);
    let rotation2 = pmath_mat3_get_rotation_x( angle2);
    let rotation = pmath_mul_mat3_mat3(&rotation2, &rotation1);

    let inverse_rotation = pmath_mul_mat3_mat3(
      &pmath_mat3_get_rotation_y( angle1),
      &pmath_mat3_get_rotation_x(-angle2));

    let pos_rotated = pmath_mul_mat3_vec3(&rotation, &self.pos);
    let translation = [-pos_rotated[0], -pos_rotated[1], -pos_rotated[2] - self.viewport_distance];
    let transformer = CoordinateTransformer1 {
      translation,
      rotation,
    };
    self.directions = pmath_transpose_mat3(&inverse_rotation);
    for i in 0..3 {
      self.directions[0][i] = -self.directions[0][i];
    }
    self.angle1 = angle1;
    self.angle2 = angle2;
    self.transformer = transformer;

    let angle11 = self.directions[2][0].atan2(self.directions[2][2]);
    let angle22 = self.directions[2][1].asin();
  }

  pub fn set_view_from_vec(&mut self, dir: &Vec3) {
    let dir = pmath_vec3_normalize(dir);
    let angle1 = dir[0].atan2(dir[2]);
    let angle2 = dir[1].asin();
    self.set_view_from_angles(angle1, angle2);
  }
}

impl CoordinateTransformer<Vec3> for CoordinateTransformer1 {
  fn transform_vertex(&self, v: &Vec3) -> Vec3 {
    let v2 = pmath_mul_mat3_vec3(&self.rotation, &v);
    [v2[0] + self.translation[0], v2[1] + self.translation[1], v2[2] + self.translation[2]]
  }
  fn transform_direction(&self, v: &Vec3) -> Vec3 {
    pmath_mul_mat3_vec3(&self.rotation, &v)
  }
}

impl Scene {
  pub fn new() -> Self {
    // let viewport_distance = 1.0;
    Scene {
      background: (0, 0, 0),
      camera: Camera::new(1.0, [0.0, 0.0, 0.0], [0.0, 0.0, 1.0]),
      dir_horizontal: [1.0, 0.0, 0.0],
      dir_vertical: [1.0, 1.0, 0.0],
      // camera_pos: [0.0, 0.0, 0.0],
      // camera_dir: [0.0, 0.0, 1.0],
      viewport: [2.0, 2.0],
      screen: [400, 400],
      objects: Vec::new(),
      light_source: None,
      ambient_light: 0.5,
    }
  }

  pub fn with_camera(mut self, camera: Camera) -> Self {
    self.camera = camera;
    self
  }

  pub fn transform_triangle(&self, vs: &[Vec3; 3]) -> [Vec3; 3] {
    let mut trans = [[0.0; 3]; 3];
    for i in 0..3 {
      trans[i] = self.camera.transformer.transform_vertex(&vs[i]);
    }
    trans
  }

  pub fn transform_triangle_to(&self, dst: &mut[Vec3; 3], vs: &[Vec3; 3]) {
    for i in 0..3 {
      dst[i] = self.camera.transformer.transform_vertex(&vs[i]);
    }
  }

  pub fn project_transformed_triangle(&self, trans_vs: &[Vec3; 3]) -> Option<[Vec2; 3]> {
    let mut proj = [[0.0; 2]; 3];
    for i in 0..3 {
      if let Some(proj_vertex) = self.get_viewport_projection_vertex(trans_vs[i].clone()) {
        proj[i] = proj_vertex;
      } else {
        return None
      }
    }
    Some(proj)
  }

  pub fn project_transformed_triangle_to(&self, valid: &mut bool, dst: &mut [Vec2; 3], trans_vs: &[Vec3; 3]) {
    for i in 0..3 {
      if let Some(proj_vertex) = self.get_viewport_projection_vertex(trans_vs[i].clone()) {
        dst[i] = proj_vertex;
      } else {
        *valid = false;
        return;
      }
    }
    *valid = true;
  }
  /// Given a vertex transformed into coordinates where viewport is z=0, returns the [x, y]
  /// projection of that vertex onto the viewport.
  pub fn get_viewport_projection_vertex(&self, v_trans: Vec3) -> Option<[f32; 2]> {
    // The line from the camera (a) to the vertex (b) is given by:
    //   f = (b - a)*t + a
    // We want the value of t where line intersects z=0 viewport. This is given by:
    //   t = -az / (bz - az)
    let b = v_trans;
    let az = - self.camera.viewport_distance;
    let offset = b[2] - az;

    // Need offset to be non-zero and offset * az < 0
    let check = offset * az;
    if check < 0.0 {
      let t = -az / offset;
      Some([t * b[0], t * b[1]])
      // Some([-b[0] * t, -b[1] * t])
    } else {
      None
    }
  }

  pub fn get_viewport_projection_triangle(&self, triangle: &Triangle) -> Option<[[f32; 2]; 3]> {
    let mut projected_vertices = [[0.0, 0.0]; 3];
    for i in 0..3 {
      let v_trans = self.camera.transformer.transform_vertex(&triangle.vs[i]);
      if let Some(proj) = self.get_viewport_projection_vertex(v_trans) {
        projected_vertices[i] = proj;
      } else {
        return None
      }
    }
    Some(projected_vertices)
  }
}

