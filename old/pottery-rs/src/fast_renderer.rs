use crate::{
  scene::*,
  pmath::*,
  color::*,
};

pub struct FastRenderer {
  /// Sorted triangles by distance from eye
  pub sorted_triangles: Vec<(f32, usize)>,
  /// Triangle vertices in transformed coordinates.
  transformed_triangles: Vec<[Vec3; 3]>,
  /// 2D projection of vertices of each triangle onto viewport screen in transformed coordinates.
  projected_triangles: Vec<[Vec2; 3]>,
  /// 2D pixel coordinates of each triangle,
  pub pixel_triangles: Vec<(Color, [(i32, i32); 3])>, // SUBOPTIMAL
  /// Indicates if the triangle is in front of the camera.
  projected_valid: Vec<bool>,
  /// Shading value for each triangle
  has_shading: bool,
  shading: Vec<Color>,
  pub flip_x: bool,
  pub flip_y: bool,
}

impl FastRenderer {

  pub fn new(scene: &Scene) -> Self {
    let transformed_triangles = (0..scene.objects.len())
      .map(|_| [[0.0; 3]; 3]).collect();

    let projected_triangles = (0..scene.objects.len())
      .map(|_| [[0.0; 2]; 3]).collect();

    let pixel_triangles = (0..scene.objects.len())
      .map(|i| (scene.objects[i].color.clone(), [(0, 0); 3])).collect();

    let projected_valid = (0..scene.objects.len())
      .map(|_| false).collect();

    let sorted_triangles = (0..scene.objects.len())
      .map(|i| (0.0, i)).collect();

    let shading = (0..scene.objects.len())
      .map(|_| (0, 0, 0)).collect();

    Self {
      projected_triangles,
      shading,
      pixel_triangles,
      sorted_triangles,
      has_shading: scene.light_source.is_some(),
      transformed_triangles,
      projected_valid,
      flip_x: true,
      flip_y: true,
    }
  }

  /// For each triangle, computes the projection of each of its vertices onto the viewport screen.
  fn compute_projected_triangles(&mut self, scene: &Scene) {
    for (i, obj) in scene.objects.iter().enumerate() {
      scene.transform_triangle_to(&mut self.transformed_triangles[i], &obj.vs);
      scene.project_transformed_triangle_to(
        &mut self.projected_valid[i],
        &mut self.projected_triangles[i],
        &self.transformed_triangles[i]);
    }
  }

  fn compute_shading(&mut self, scene: &Scene) {
    if let Some(light_source) = scene.light_source {
      self.has_shading = true;
      for (i, obj) in scene.objects.iter().enumerate() {
        if self.projected_valid[i] {
          // Vector pointing from midpoint of triangle to light source
          let obj_light_vec = pmath_vec3_normalize(&pmath_sub3(&light_source, &obj.mid));

          // Vector pointing from midpoint of triangle to camera
          let obj_camera_vec = pmath_sub3(&scene.camera.pos, &obj.mid);

          // Sidenote: Compute distance here
          self.sorted_triangles[i] = (pmath_dot_vec3_vec3(&obj_camera_vec, &obj_camera_vec), i);

          // Lighting on positive side of triangle given by dot product of v1 and normal
          let light = pmath_dot_vec3_vec3(&obj_light_vec, &obj.normal);

          // Determine which side of the triangle we are looking at by dotting v2 and normal
          let check_obj_side = pmath_dot_vec3_vec3(&obj.normal, &obj_camera_vec);
          if check_obj_side > 0.0 {
            let light = (0.5 * ( 1.0 + light)).max(scene.ambient_light);
            self.shading[i] = shade_color(&obj.color, light);
          } else {
            let light = (0.5 * ( 1.0 - light)).max(scene.ambient_light);
            self.shading[i] = shade_color(&obj.color, light);
          }

        }
      }
    } else {
      self.has_shading = false;
    }
  }

  fn get_viewport_point(&self, scene: &Scene, i: u32, j: u32) -> Vec2 {
    [
      ((i as f32) / (scene.screen[0] as f32)) * scene.viewport[0] - scene.viewport[0]/2.0,
      ((j as f32) / (scene.screen[1] as f32)) * scene.viewport[1] - scene.viewport[1]/2.0
    ]
  }

  fn render_point(&mut self, scene: &Scene, mut collisions: Vec<(usize, Vec3)>) -> Option<Color> {
    if collisions.len() == 0 {
      None
    } else if collisions.len() == 1 {
      if self.has_shading {
        Some(self.shading[collisions[0].0].clone())
      } else {
        Some(scene.objects[collisions[0].0].color.clone())
      }
    } else {
      let mut min_distance = None;
      let mut min_collision = None;
      while let Some((ind, pt)) = collisions.pop() {
        let dis = pmath_distance2_vec3_vec3(&scene.camera.pos, &pt);
        if let Some(prev_min) = min_distance {
          if dis < prev_min {
            min_distance = Some(dis);
            min_collision = Some(ind);
          }
        } else {
          min_distance = Some(dis);
          min_collision = Some(ind);
        }
      }
      min_collision.map(|ind| {
        if self.has_shading {
          self.shading[ind].clone()
        } else {
          scene.objects[ind].color.clone()
        }
      })
    }
  }

  pub fn render(&mut self, scene: &Scene) {

    // Step 1: Project all objects onto viewport screen in coordinate system where viewport at z=0
    self.compute_projected_triangles(scene);

    // Step 2: Compute shading for each object in scene.
    self.compute_shading(&scene);

    // Step 3: Sort objects by their distance from the camera
    self.sorted_triangles.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());

    for (i, (_, ind)) in self.sorted_triangles.iter().enumerate() {
      if self.has_shading {
        self.pixel_triangles[i].0 = self.shading[*ind].clone();
      }
      for j in 0..3 {
        let [x, y] = self.projected_triangles[*ind][j];
        self.pixel_triangles[i].1[j] = (
          (((x - scene.viewport[0]/2.0) / scene.viewport[0]) * (scene.screen[0] as f32)) as i32,
          (((y - scene.viewport[1]/2.0) / scene.viewport[1]) * (scene.screen[1] as f32)) as i32,
        );
      }
    }
  }
}
