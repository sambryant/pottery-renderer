#![allow(unused_variables)]
pub type Mat4 = [[f32; 4]; 4];
pub type Mat3 = [[f32; 3]; 3];
pub type Vec4 = [f32; 4];
pub type Vec3 = [f32; 3];
pub type Vec2 = [f32; 2];

pub use std::f32::consts::PI;

#[inline]
pub fn fmt_vec3(a: &Vec3) -> String {
  format!("{{{}, {}, {}}}", a[0], a[1], a[2])
}

#[inline]
pub fn pmath_sub3(a: &Vec3, b: &Vec3) -> Vec3 {
  [a[0] - b[0], a[1] - b[1], a[2] - b[2]]
}

#[inline]
pub fn pmath_mul_vec3(a: f32, b: &Vec3) -> Vec3 {
  [a*b[0], a*b[1], a*b[2]]
}

#[inline]
pub fn pmath_add3(a: &Vec3, b: &Vec3) -> Vec3 {
  [a[0] + b[0], a[1] + b[1], a[2] + b[2]]
}

#[inline]
pub fn pmath_transpose_mat3(a: &Mat3) -> Mat3 {
  let mut b = [[0.0; 3]; 3];
  for i in 0..3 {
    for j in 0..3 {
      b[i][j] = a[j][i];
    }
  }
  b
}

#[inline]
pub fn pmath_vec3_normalize(a: &Vec3) -> Vec3 {
  let norm = 1.0 / (a[0]*a[0] + a[1]*a[1] + a[2]*a[2]).sqrt();
  [a[0] * norm, a[1] * norm, a[2] * norm]
}

/// Performs counterclockwise rotation in YZ plane
pub fn pmath_mat3_get_rotation_x(th: f32) -> Mat3 {
  let (sin, cos) = (th.sin(), th.cos());
  [
    [ 1.0,  0.0,  0.0],
    [ 0.0,  cos, -sin],
    [ 0.0,  sin,  cos]
  ]
}

/// Performs counterclockwise rotation in ZX plane
pub fn pmath_mat3_get_rotation_y(th: f32) -> Mat3 {
  let (sin, cos) = (th.sin(), th.cos());
  [
    [ cos,  0.0,  sin],
    [ 0.0,  1.0,  0.0],
    [-sin,  0.0,  cos]
  ]
}

/// Performs counterclockwise rotation in XY plane
pub fn pmath_mat3_get_rotation_z(th: f32) -> Mat3 {
  let (sin, cos) = (th.sin(), th.cos());
  [
    [ cos, -sin,  0.0],
    [ sin,  cos,  0.0],
    [ 0.0,  0.0,  1.0]
  ]
}

#[inline]
pub fn pmath_mul_mat3_mat3(a: &Mat3, b: &Mat3) -> Mat3 {
  [
    [
      a[0][0]*b[0][0]+a[0][1]*b[1][0]+a[0][2]*b[2][0],
      a[0][0]*b[0][1]+a[0][1]*b[1][1]+a[0][2]*b[2][1],
      a[0][0]*b[0][2]+a[0][1]*b[1][2]+a[0][2]*b[2][2],
    ],
    [
      a[1][0]*b[0][0]+a[1][1]*b[1][0]+a[1][2]*b[2][0],
      a[1][0]*b[0][1]+a[1][1]*b[1][1]+a[1][2]*b[2][1],
      a[1][0]*b[0][2]+a[1][1]*b[1][2]+a[1][2]*b[2][2],
    ],
    [
      a[2][0]*b[0][0]+a[2][1]*b[1][0]+a[2][2]*b[2][0],
      a[2][0]*b[0][1]+a[2][1]*b[1][1]+a[2][2]*b[2][1],
      a[2][0]*b[0][2]+a[2][1]*b[1][2]+a[2][2]*b[2][2],
    ],
  ]
}

#[inline]
pub fn pmath_mul_mat3_vec3(a: &Mat3, b: &Vec3) -> Vec3 {
  [
    (0..3).map(|j| a[0][j] * b[j]).sum(),
    (0..3).map(|j| a[1][j] * b[j]).sum(),
    (0..3).map(|j| a[2][j] * b[j]).sum(),
  ]
}

#[inline]
pub fn pmath_dot_vec3_vec3(a: &Vec3, b: &Vec3) -> f32 {
  a[0]*b[0] + a[1]*b[1] + a[2]*b[2]
}

#[inline]
pub fn pmath_distance2_vec3_vec3(a: &Vec3, b: &Vec3) -> f32 {
  (a[0] - b[0])*(a[0] - b[0]) +
  (a[1] - b[1])*(a[1] - b[1]) +
  (a[2] - b[2])*(a[2] - b[2])
}

#[inline]
pub fn pmath_cross_vec3_vec3(a: &Vec3, b: &Vec3) -> Vec3 {
  [
    a[1]*b[2] - a[2]*b[1],
    a[2]*b[0] - a[0]*b[2],
    a[0]*b[1] - a[1]*b[0]
  ]
}
