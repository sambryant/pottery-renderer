use crate::v1::{
  color,
  color::Color,
  object::*,
  pmath::*,
  pot::PotMaker,
  ray_collision::FastRayCollision,
  util,
};

/// Virtual rectangular frame in front of eye through which light rays are projected onto
/// Each pixel shown in final render is value on this frame.
struct ViewPort {
  pub dimensions: [f32; 3],
}

/// Camera eye represented by 3D position and orientation unit vector in direction of observation.
pub struct Camera {
  pub position: FastPoint,
  pub orientation: Orientation,
}

struct Screen {
  pub width: usize,
  pub height: usize,
}

struct Scene {
  ambient_light: f32,
  viewport: ViewPort,
  camera: Camera,
  screen: Screen,
  objects: Vec<Box<dyn Object>>,
  light_source: Option<FastPoint>,
}

impl Screen {
  pub fn new(vp: &ViewPort, pixels_per_unit: f32) -> Screen {
    let width = (vp.dimensions[0] * pixels_per_unit) as usize;
    let height = (vp.dimensions[1] * pixels_per_unit) as usize;
    Self { width, height }
  }
}

impl Scene {
  pub fn new(camera: Camera, viewport: ViewPort, screen: Screen) -> Self {
    Self {
      ambient_light: 0.0,
      camera,
      viewport,
      screen,
      objects: Vec::new(),
      light_source: None,
    }
  }

  pub fn add_object<T: Object + 'static>(&mut self, object: Box<T>) {
    self.objects.push(object);
  }

  pub fn set_light_source(&mut self, light: FastPoint) {
    self.light_source = Some(light);
  }

  pub fn set_ambient_light(&mut self, value: f32) {
    if value < 0.0 || value > 1.0 {
      panic!("Ambient light must be between 0 and 1");
    }
    self.ambient_light = value;
  }

  pub fn get_rendered_collision(p0: &FastPoint, mut collisions: Vec<FastRayCollision>) -> Option<FastRayCollision> {
    if collisions.len() == 0 {
      None
    } else if collisions.len() == 1 {
      Some(collisions.pop().unwrap())
    } else {
      let mut min_distance = None;
      let mut min_collision = None;
      while let Some(c) = collisions.pop() {
        let disp = [p0[0] - c.point[0], p0[1] - c.point[1], p0[2] - c.point[2]];
        let dis2 = disp[0]*disp[0] + disp[1] * disp[1] + disp[2] * disp[2];
        if let Some(prev_min) = min_distance {
          if dis2 < prev_min {
            min_distance = Some(dis2);
            min_collision = Some(c);
          }
        } else {
          min_distance = Some(dis2);
          min_collision = Some(c);
        }
      }
      min_collision
    }
  }

  fn render_collision(&self, p0: &FastPoint, c: FastRayCollision) -> Color {
    if let Some(ref light) = self.light_source {
      // Compute the cosine of the reflected light ray against the light source.
      let lvec = [light[0] - c.point[0], light[1] - c.point[1], light[2] - c.point[2]];
      let lvec = fast_vec_normalize(lvec);

      // Just use normal instead of reflections
      let normal = c.normal;

      let dir = [-c.point[0] + p0[0], -c.point[1] + p0[1], -c.point[2] + p0[2]];
      let check = dir[0] * normal[0] + dir[1] * normal[1] + dir[2] * normal[2];
      let cosine = if check < 0.0 {
        -(lvec[0] * normal[0] + lvec[1] * normal[1] + lvec[2] * normal[2])
      } else {
        lvec[0] * normal[0] + lvec[1] * normal[1] + lvec[2] * normal[2]
      };
      let light = (0.5 * (1.0 + cosine)).max(self.ambient_light);
      color::shade_color(&c.color, light)
      // println!("light: {:.2}", light);
      // let color = [(collision.color[0] * light) as u8, (collision.color[1] * light) as u8, (collision.color[2] * light) as u8];
      // println!("color: {:?} -> {:?}", collision.color, color);
      // color
    } else {
      c.color
    }
  }

  pub fn render(&self) -> Result<Vec<Vec<Color>>, ()> {
    // Indexed by [y][x]
    let mut rendered: Vec<Vec<Color>> = (0..self.screen.height)
      .map(|_| (0..self.screen.width)
        .map(|_| color::BLACK).collect()
      ).collect();

    // For each pixel on the screen, run ray-tracing.
    // We start in translated coordinates where camera is at origin and pointing at (0, 0, 1).
    // Before we send info to the collision detector, we have to translate to real coordinates.

    // Figure out top left corner of viewport.
    // If the camera is at origin and pointing at (0, 0, 1), this is easy
    let anchor_pt = [
      -self.viewport.dimensions[0]/2.0,
       self.viewport.dimensions[2],
      -self.viewport.dimensions[1]/2.0,
      1.0
    ];
    println!("Screen: {}, {}", self.screen.width, self.screen.height);

    let (p0, theta, phi) = (self.camera.position, self.camera.orientation.theta, self.camera.orientation.phi);
    // Define transformation which maps the +z axis to the camera direction.
    let rotate_x = Matrix::rotation_x(theta);
    let rotate_z = Matrix::rotation_z(phi);
    let trans = (&rotate_z) * (&rotate_x);
    let fast_trans = convert_matrix_to_fast(&trans);

    let stride_x = self.viewport.dimensions[0] / (self.screen.width as f32);
    let stride_y = self.viewport.dimensions[1] / (self.screen.height as f32);
    for i in 0..self.screen.height {
      for j in 0..self.screen.width {

        let trans_viewport_pt = [
          anchor_pt[0] + (j as f32) * stride_x,
          anchor_pt[1],
          anchor_pt[2] + (i as f32) * stride_y,
        ];

        let d0 = fast_matrix_mul_vec(&fast_trans, &trans_viewport_pt);
        // let d0 = fast_vec_normalize(d0);

        // Check for collision with each object in scene.
        let mut collisions = Vec::new();
        for obj in self.objects.iter() {
          if let Some(collision) = obj.fast_detect_collision(&p0, &d0) {
            collisions.push(collision);
          }
        }

        // Choose which object to show if there are multiple.
        if let Some(collision) = Self::get_rendered_collision(&p0, collisions) {
          rendered[i][j] = self.render_collision(&p0, collision);
        }
      }
    }
    Ok(rendered)
  }
}

// pub fn test_tetrahdron() {
//   let viewport = ViewPort {
//     dimensions: Vector::new(10.0, 5.0, 8.0)
//   };
//   let screen = Screen::new(&viewport, 50.0);
//   let camera = Camera {
//     position: (0.0, 0.0, 10.0).into(),
//     orientation: Orientation { theta: PI, phi: PI },
//     // direction: Vector::new(0.0, 0.0, -1.0).normalize(),
//   };
//   let mut scene = Scene::new(camera, viewport, screen);

//   // Construct a series of tetrahdeons at different locations
//   let triangle_points: Vec<Point> = vec![
//     [1.0, 0.0, 0.0].into(),
//     [-0.5, 0.866, 0.0].into(),
//     [-0.5, -0.866, 0.0].into(),
//     [0.0, 0.0, 1.0].into(),
//   ];
//   let triangle_colors: Vec<color::Color> = vec![
//     color::RED,
//     color::BLUE,
//     color::YELLOW,
//     color::PURPLE
//   ];
//   let tetrahderon_offsets: Vec<Vector> = vec![
//     [-3.0, 0.0, 0.0].into(),
//     [ 0.0, 0.0, 0.0].into(),
//     [ 3.0, 0.0, 0.0].into(),
//   ];
//   for translation in tetrahderon_offsets.into_iter() {
//     for i in 0..4 {
//       let mut tmp_pts = Vec::new();
//       for j in 0..4 {
//         if i == j {
//           continue;
//         } else {
//           tmp_pts.push(&triangle_points[j] + &translation);
//         }
//       }
//       // if i == 3 {
//         // continue;
//       // }
//       scene.add_object(Box::new(Triangle::from_pts(tmp_pts, triangle_colors[i].clone())))
//     }
//   }

//   let rendered = scene.render().expect("Failed to render scene!");
//   util::save_array_as_image(&rendered, "output/test-tetrahderon-1.png");
// }

// pub fn test_tetrahdron_lit() {
//   let viewport = ViewPort {
//     dimensions: Vector::new(10.0, 5.0, 8.0)
//   };
//   let viewport = ViewPort {
//     dimensions: Vector::new(4.0, 3.0, 4.0)
//   };
//   let screen = Screen::new(&viewport, 200.0);
//   let camera = Camera {
//     position: (0.0, 0.0, 8.0).into(),
//     orientation: Orientation { theta: PI, phi: PI },
//     // direction: Vector::new(0.0, 0.0, -1.0).normalize(),
//   };
//   let mut scene = Scene::new(camera, viewport, screen);

//   let light_source = (3.0, 0.0, 2.0).into();
//   scene.set_light_source(light_source);
//   scene.set_ambient_light(0.5);

//   // Construct a series of tetrahdeons at different locations
//   let triangle_points: Vec<Point> = vec![
//     [1.0, 0.0, 0.0].into(),
//     [-0.5, 0.866, 0.0].into(),
//     [-0.5, -0.866, 0.0].into(),
//     [0.0, 0.0, 1.0].into(),
//   ];
//   let triangle_colors: Vec<color::Color> = vec![
//     color::RED,
//     color::BLUE,
//     color::YELLOW,
//     color::PURPLE
//   ];
//   let tetrahderon_offsets: Vec<Vector> = vec![
//     [-3.0, 0.0, 0.0].into(),
//     [ 0.0, 0.0, 0.0].into(),
//     [ 3.0, 0.0, 0.0].into(),
//   ];
//   for translation in tetrahderon_offsets.into_iter() {
//     for i in 0..4 {
//       let mut tmp_pts = Vec::new();
//       for j in 0..4 {
//         if i == j {
//           continue;
//         } else {
//           tmp_pts.push(&triangle_points[j] + &translation);
//         }
//       }
//       scene.add_object(Box::new(Triangle::from_pts(tmp_pts.clone(), triangle_colors[i].clone())));
//     }
//   }

//   let rendered = scene.render().expect("Failed to render scene!");
//   util::save_array_as_image(&rendered, "output/test-tetrahderon-2-lighting.png");
// }


pub fn test_tetrahdron_lit2() {
  let viewport = ViewPort {
    dimensions: [10.0, 5.0, 5.0]
  };
  let screen = Screen::new(&viewport, 50.0);
  let camera = Camera {
    position: [0.0, -10.0, 5.0, 1.0],
    orientation: Orientation { theta: -PI / 8.0, phi: 0.0},
  };
  let mut scene = Scene::new(camera, viewport, screen);

  let light_source = [20.0, 20.0, 50.0, 1.0];
  scene.set_light_source(light_source);
  scene.set_ambient_light(0.5);

  let base_points: Vec<Point> = vec![
    [-50.0, -50.0, 0.0].into(),
    [-50.0, 50.0, 0.0].into(),
    [50.0, 50.0, 0.0].into(),
    [50.0, -50.0, 0.0].into(),
  ];
  scene.add_object(Box::new(
    Triangle::new(base_points[0].clone(), base_points[1].clone(), base_points[2].clone(), color::GRAY)));
  scene.add_object(Box::new(
    Triangle::new(base_points[2].clone(), base_points[3].clone(), base_points[0].clone(), color::GRAY)));

  // Construct a series of tetrahdeons at different locations
  let triangle_points: Vec<Point> = vec![
    [1.0, 0.0, 0.0].into(),
    [-0.5, 0.866, 0.0].into(),
    [-0.5, -0.866, 0.0].into(),
    [0.0, 0.0, 1.0].into(),
  ];
  let triangle_colors: Vec<color::Color> = vec![
    color::RED,
    color::BLUE,
    color::YELLOW,
    color::PURPLE
  ];
  let tetrahderon_offsets: Vec<Vector> = vec![
    [-3.0, 0.0, 0.0].into(),
    [ 0.0, 0.0, 0.0].into(),
    [ 3.0, 0.0, 0.0].into(),
  ];
  for translation in tetrahderon_offsets.into_iter() {
    for i in 0..4 {
      let mut tmp_pts = Vec::new();
      for j in 0..4 {
        if i == j {
          continue;
        } else {
          tmp_pts.push(&triangle_points[j] + &translation);
        }
      }
      scene.add_object(Box::new(Triangle::from_pts(tmp_pts.clone(), triangle_colors[i].clone())));
    }
  }

  let rendered = scene.render().expect("Failed to render scene!");
  util::save_array_as_image(&rendered, "output/test-fast-tetrahderon-3-lighting.png");
}

pub fn test_pot() {
  let viewport = ViewPort {
    dimensions: [5.0, 5.0, 2.0]
  };
  let screen = Screen::new(&viewport, 100.0);
  let camera = Camera {
    position: [0.0, -5.0, 4.0, 1.0],
    orientation: Orientation { theta: -PI / 8.0, phi: 0.0},
  };
  let mut scene = Scene::new(camera, viewport, screen);

  let light_source = [20.0, 20.0, 50.0, 1.0];
  scene.set_light_source(light_source);
  scene.set_ambient_light(0.5);

  let base_points: Vec<Point> = vec![
    [-50.0, -50.0, 0.0].into(),
    [-50.0, 50.0, 0.0].into(),
    [50.0, 50.0, 0.0].into(),
    [50.0, -50.0, 0.0].into(),
  ];
  scene.add_object(Box::new(
    Triangle::new(base_points[0].clone(), base_points[1].clone(), base_points[2].clone(), color::GRAY)));
  scene.add_object(Box::new(
    Triangle::new(base_points[2].clone(), base_points[3].clone(), base_points[0].clone(), color::GRAY)));

  fn pot_function(z: f32) -> f32 {
    let z = 3.0 - z;
    1.0 + z * z * z * (-0.01 + 0.01 * z +0.035 * z * z - 0.011 * z * z * z - 0.0005 * z * z * z * z)
  }
  fn clr_function(th: f32) -> [u8; 3] {
    let r = 0.5*(1.0+th.sin());
    let g = 0.5*(1.0+(th+2.0*PI/3.0).sin());
    let b = 0.5*(1.0+(th+4.0*PI/3.0).sin());
    [(255.0 * r) as u8, (255.0 * g) as u8, (255.0 * b) as u8]
  }
  let height = 3.0;
  let base = [0.0, 0.0, 0.0];
  // let color = color::BLUE;
  let pm = PotMaker { n_ths: 40, n_zs: 20 };
  let triangles = pm.make_fast_colored_pot_mesh(base, height, clr_function, pot_function);
  for t in triangles.into_iter() {
    scene.add_object(Box::new(t))
  }


  // // Construct a series of tetrahdeons at different locations
  // let triangle_points: Vec<Point> = vec![
  //   [1.0, 0.0, 0.0].into(),
  //   [-0.5, 0.866, 0.0].into(),
  //   [-0.5, -0.866, 0.0].into(),
  //   [0.0, 0.0, 1.0].into(),
  // ];
  // let triangle_colors: Vec<color::Color> = vec![
  //   color::RED,
  //   color::BLUE,
  //   color::YELLOW,
  //   color::PURPLE
  // ];
  // let tetrahderon_offsets: Vec<Vector> = vec![
  //   [-3.0, 0.0, 0.0].into(),
  //   [ 0.0, 0.0, 0.0].into(),
  //   [ 3.0, 0.0, 0.0].into(),
  // ];
  // for translation in tetrahderon_offsets.into_iter() {
  //   for i in 0..4 {
  //     let mut tmp_pts = Vec::new();
  //     for j in 0..4 {
  //       if i == j {
  //         continue;
  //       } else {
  //         tmp_pts.push(&triangle_points[j] + &translation);
  //       }
  //     }
  //     scene.add_object(Box::new(Triangle::from_pts(tmp_pts.clone(), triangle_colors[i].clone())));
  //   }
  // }

  let rendered = scene.render().expect("Failed to render scene!");
  util::save_array_as_image(&rendered, "output/test-fast-pot-2.png");
}

