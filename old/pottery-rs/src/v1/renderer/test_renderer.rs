use crate::v1::{
  color,
  color::Color,
  object::*,
  ray_collision::RayCollision,
  pmath::*,
  pot::PotMaker,
  util,
};

/// Virtual rectangular frame in front of eye through which light rays are projected onto.
/// Each pixel shown in final render is value on this frame.
struct ViewPort {
  pub dimensions: Vector,
}

/// Camera eye represented by 3D position and orientation unit vector in direction of observation.
pub struct Camera {
  pub position: Point,
  pub orientation: Orientation,
}

struct Screen {
  pub width: usize,
  pub height: usize,
}

struct Scene {
  ambient_light: f32,
  viewport: ViewPort,
  camera: Camera,
  screen: Screen,
  objects: Vec<Box<dyn Object>>,
  light_source: Option<Point>,
}

impl Screen {
  pub fn new(vp: &ViewPort, pixels_per_unit: f32) -> Screen {
    let width = (vp.dimensions[0] * pixels_per_unit) as usize;
    let height = (vp.dimensions[1] * pixels_per_unit) as usize;
    Self { width, height }
  }
}

impl Scene {
  pub fn new(camera: Camera, viewport: ViewPort, screen: Screen) -> Self {
    Self {
      ambient_light: 0.0,
      camera,
      viewport,
      screen,
      objects: Vec::new(),
      light_source: None,
    }
  }

  pub fn add_object<T: Object + 'static>(&mut self, object: Box<T>) {
    self.objects.push(object);
  }

  pub fn set_light_source(&mut self, light: Point) {
    self.light_source = Some(light);
  }

  pub fn set_ambient_light(&mut self, value: f32) {
    if value < 0.0 || value > 1.0 {
      panic!("Ambient light must be between 0 and 1");
    }
    self.ambient_light = value;
  }

  pub fn get_rendered_collision(virtual_vp_pt: &Point, mut collisions: Vec<RayCollision>) -> Option<RayCollision> {
    if collisions.len() == 0 {
      None
    } else {
      // Compute distance from camera eye to viewport. If collision distance is smaller than this,
      // we should *not* render it.
      let vp_distance = (virtual_vp_pt * virtual_vp_pt).sqrt();

      let mut min_distance = None;
      let mut min_collision = None;

      while let Some(collision) = collisions.pop() {
        // Exclude anything in front of viewport
        if collision.distance < vp_distance {
          continue;
        }

        if let Some(prev_min) = min_distance {
          if collision.distance < prev_min {
            min_distance = Some(collision.distance);
            min_collision = Some(collision);
          }
        } else {
          min_distance = Some(collision.distance);
          min_collision = Some(collision);
        }
      }
      min_collision
    }
  }

  fn render_collision(&self, collision: RayCollision) -> Color {
    if let Some(ref light_source) = self.light_source {
      // Compute the cosine of the reflected light ray against the light source.
      let light_vector = (light_source - &collision.point).normalize();

      // Just use normal instead of reflections
      if true {
        let mut normal = collision.normal.get_vector();
        if &collision.direction * &normal < 0.0 {
          normal = -&normal;
        }
        let cosine = &light_vector * &normal;
        let light = (0.5 * (1.0 + cosine)).max(self.ambient_light);
        color::shade_color(&collision.color, light)
      } else {
        todo!()
        // let cosine = &light_vector * &collision.direction_reflected;
        // let light = (0.5 * (1.0 + cosine)).max(self.ambient_light);
        // color::shade_color(&collision.color, light)
      }
      // println!("light: {:.2}", light);
      // let color = [(collision.color[0] * light) as u8, (collision.color[1] * light) as u8, (collision.color[2] * light) as u8];
      // println!("color: {:?} -> {:?}", collision.color, color);
      // color
    } else {
      collision.color
    }
  }

  pub fn render(&self) -> Result<Vec<Vec<Color>>, ()> {
    // Indexed by [y][x]
    let mut rendered: Vec<Vec<Color>> = (0..self.screen.height)
      .map(|_| (0..self.screen.width)
        .map(|_| color::BLACK).collect()
      ).collect();

    // For each pixel on the screen, run ray-tracing.
    // We start in translated coordinates where camera is at origin and pointing at (0, 0, 1).
    // Before we send info to the collision detector, we have to translate to real coordinates.

    // Figure out top left corner of viewport.
    // If the camera is at origin and pointing at (0, 0, 1), this is easy
    let anchor_pt = Point::new(
      -self.viewport.dimensions[0]/2.0,
       self.viewport.dimensions[2],
      -self.viewport.dimensions[1]/2.0,
       );

    println!("Screen: {}, {}", self.screen.width, self.screen.height);
    let (test_i, test_j) = (300, 390);

    // Define transformation which maps the +z axis to the camera direction.
    let rotate_x = Matrix::rotation_x(self.camera.orientation.theta);
    let rotate_z = Matrix::rotation_z(self.camera.orientation.phi);
    let trans = (&rotate_z) * (&rotate_x);

    let stride_x = self.viewport.dimensions[0] / (self.screen.width as f32);
    let stride_y = self.viewport.dimensions[1] / (self.screen.height as f32);
    for i in 0..self.screen.height {
      for j in 0..self.screen.width {

        let is_test_pt = i == test_i && j == test_j;

        let trans_viewport_pt = Point::new(
          anchor_pt.x + (j as f32) * stride_x,
          anchor_pt.y,
          anchor_pt.z + (i as f32) * stride_y);

        // let real_viewport_pt = (&trans_inv * &trans_viewport_pt) + &self.eye.position;

        let ray_direction = (&trans * &trans_viewport_pt).normalize();

        // TODO: Send reference to camera position instead of cloning it for each pixel!!
        let light_ray = VectorPoint::new(self.camera.position.clone(), ray_direction);

        // Check for collision with each object in scene.
        let mut collisions = Vec::new();
        for obj in self.objects.iter() {
          if let Some(collision) = obj.detect_collision(&light_ray) {
            collisions.push(collision);
          }
        }

        // Choose which object to show if there are multiple.
        if let Some(collision) = Self::get_rendered_collision(&trans_viewport_pt, collisions) {
          rendered[i][j] = self.render_collision(collision);
        }
      }
    }

    Ok(rendered)
  }
}

pub fn test_tetrahdron() {
  let viewport = ViewPort {
    dimensions: Vector::new(10.0, 5.0, 8.0)
  };
  let screen = Screen::new(&viewport, 50.0);
  let camera = Camera {
    position: (0.0, 0.0, 10.0).into(),
    orientation: Orientation { theta: PI, phi: PI },
    // direction: Vector::new(0.0, 0.0, -1.0).normalize(),
  };
  let mut scene = Scene::new(camera, viewport, screen);

  // Construct a series of tetrahdeons at different locations
  let triangle_points: Vec<Point> = vec![
    [1.0, 0.0, 0.0].into(),
    [-0.5, 0.866, 0.0].into(),
    [-0.5, -0.866, 0.0].into(),
    [0.0, 0.0, 1.0].into(),
  ];
  let triangle_colors: Vec<color::Color> = vec![
    color::RED,
    color::BLUE,
    color::YELLOW,
    color::PURPLE
  ];
  let tetrahderon_offsets: Vec<Vector> = vec![
    [-3.0, 0.0, 0.0].into(),
    [ 0.0, 0.0, 0.0].into(),
    [ 3.0, 0.0, 0.0].into(),
  ];
  for translation in tetrahderon_offsets.into_iter() {
    for i in 0..4 {
      let mut tmp_pts = Vec::new();
      for j in 0..4 {
        if i == j {
          continue;
        } else {
          tmp_pts.push(&triangle_points[j] + &translation);
        }
      }
      // if i == 3 {
        // continue;
      // }
      scene.add_object(Box::new(Triangle::from_pts(tmp_pts, triangle_colors[i].clone())))
    }
  }

  let rendered = scene.render().expect("Failed to render scene!");
  util::save_array_as_image(&rendered, "output/test-tetrahderon-1.png");
}

pub fn test_tetrahdron_lit() {
  let viewport = ViewPort {
    dimensions: Vector::new(10.0, 5.0, 8.0)
  };
  let viewport = ViewPort {
    dimensions: Vector::new(4.0, 3.0, 4.0)
  };
  let screen = Screen::new(&viewport, 200.0);
  let camera = Camera {
    position: (0.0, 0.0, 8.0).into(),
    orientation: Orientation { theta: PI, phi: PI },
    // direction: Vector::new(0.0, 0.0, -1.0).normalize(),
  };
  let mut scene = Scene::new(camera, viewport, screen);

  let light_source = (3.0, 0.0, 2.0).into();
  scene.set_light_source(light_source);
  scene.set_ambient_light(0.5);

  // Construct a series of tetrahdeons at different locations
  let triangle_points: Vec<Point> = vec![
    [1.0, 0.0, 0.0].into(),
    [-0.5, 0.866, 0.0].into(),
    [-0.5, -0.866, 0.0].into(),
    [0.0, 0.0, 1.0].into(),
  ];
  let triangle_colors: Vec<color::Color> = vec![
    color::RED,
    color::BLUE,
    color::YELLOW,
    color::PURPLE
  ];
  let tetrahderon_offsets: Vec<Vector> = vec![
    [-3.0, 0.0, 0.0].into(),
    [ 0.0, 0.0, 0.0].into(),
    [ 3.0, 0.0, 0.0].into(),
  ];
  for translation in tetrahderon_offsets.into_iter() {
    for i in 0..4 {
      let mut tmp_pts = Vec::new();
      for j in 0..4 {
        if i == j {
          continue;
        } else {
          tmp_pts.push(&triangle_points[j] + &translation);
        }
      }
      scene.add_object(Box::new(Triangle::from_pts(tmp_pts.clone(), triangle_colors[i].clone())));
    }
  }

  let rendered = scene.render().expect("Failed to render scene!");
  util::save_array_as_image(&rendered, "output/test-tetrahderon-2-lighting.png");
}


pub fn test_tetrahdron_lit2() {
  let viewport = ViewPort {
    dimensions: Vector::new(10.0, 5.0, 5.0)
  };
  let screen = Screen::new(&viewport, 50.0);
  let camera = Camera {
    position: (0.0, -10.0, 5.0).into(),
    orientation: Orientation { theta: -PI / 8.0, phi: 0.0},
  };
  let mut scene = Scene::new(camera, viewport, screen);

  let light_source = (20.0, 20.0, 50.0).into();
  scene.set_light_source(light_source);
  scene.set_ambient_light(0.5);


  let base_points: Vec<Point> = vec![
    [-50.0, -50.0, 0.0].into(),
    [-50.0, 50.0, 0.0].into(),
    [50.0, 50.0, 0.0].into(),
    [50.0, -50.0, 0.0].into(),
  ];
  scene.add_object(Box::new(
    Triangle::new(base_points[0].clone(), base_points[1].clone(), base_points[2].clone(), color::GRAY)));
  scene.add_object(Box::new(
    Triangle::new(base_points[2].clone(), base_points[3].clone(), base_points[0].clone(), color::GRAY)));

  // Construct a series of tetrahdeons at different locations
  let triangle_points: Vec<Point> = vec![
    [1.0, 0.0, 0.0].into(),
    [-0.5, 0.866, 0.0].into(),
    [-0.5, -0.866, 0.0].into(),
    [0.0, 0.0, 1.0].into(),
  ];
  let triangle_colors: Vec<color::Color> = vec![
    color::RED,
    color::BLUE,
    color::YELLOW,
    color::PURPLE
  ];
  let tetrahderon_offsets: Vec<Vector> = vec![
    [-3.0, 0.0, 0.0].into(),
    [ 0.0, 0.0, 0.0].into(),
    [ 3.0, 0.0, 0.0].into(),
  ];
  for translation in tetrahderon_offsets.into_iter() {
    for i in 0..4 {
      let mut tmp_pts = Vec::new();
      for j in 0..4 {
        if i == j {
          continue;
        } else {
          tmp_pts.push(&triangle_points[j] + &translation);
        }
      }
      scene.add_object(Box::new(Triangle::from_pts(tmp_pts.clone(), triangle_colors[i].clone())));
    }
  }

  let rendered = scene.render().expect("Failed to render scene!");
  util::save_array_as_image(&rendered, "output/test-tetrahderon-3-lighting.png");
}

pub fn test_pot() {
  let viewport = ViewPort {
    dimensions: Vector::new(5.0, 5.0, 2.0)
  };
  let screen = Screen::new(&viewport, 100.0);
  let camera = Camera {
    position: (0.0, -5.0, 4.0).into(),
    orientation: Orientation { theta: -PI / 8.0, phi: 0.0},
  };
  let mut scene = Scene::new(camera, viewport, screen);

  let light_source = (20.0, 20.0, 50.0).into();
  scene.set_light_source(light_source);
  scene.set_ambient_light(0.5);


  let base_points: Vec<Point> = vec![
    [-50.0, -50.0, 0.0].into(),
    [-50.0, 50.0, 0.0].into(),
    [50.0, 50.0, 0.0].into(),
    [50.0, -50.0, 0.0].into(),
  ];
  scene.add_object(Box::new(
    Triangle::new(base_points[0].clone(), base_points[1].clone(), base_points[2].clone(), color::GRAY)));
  scene.add_object(Box::new(
    Triangle::new(base_points[2].clone(), base_points[3].clone(), base_points[0].clone(), color::GRAY)));

  fn pot_function(z: f32) -> f32 {
    let z = 3.0 - z;
    1.0 + z * z * z * (-0.01 + 0.01 * z +0.035 * z * z - 0.011 * z * z * z - 0.0005 * z * z * z * z)
  }
  fn clr_function(th: f32) -> [u8; 3] {
    let r = 0.5*(1.0+th.sin());
    let g = 0.5*(1.0+(th+2.0*PI/3.0).sin());
    let b = 0.5*(1.0+(th+4.0*PI/3.0).sin());
    [(255.0 * r) as u8, (255.0 * g) as u8, (255.0 * b) as u8]
  }
  let height = 3.0;
  let base = (0.0, 0.0, 0.0).into();
  // let color = color::BLUE;
  let pm = PotMaker { n_ths: 40, n_zs: 20 };
  let triangles = pm.make_colored_pot_mesh(base, height, clr_function, pot_function);
  for t in triangles.into_iter() {
    scene.add_object(Box::new(t))
  }


  // // Construct a series of tetrahdeons at different locations
  // let triangle_points: Vec<Point> = vec![
  //   [1.0, 0.0, 0.0].into(),
  //   [-0.5, 0.866, 0.0].into(),
  //   [-0.5, -0.866, 0.0].into(),
  //   [0.0, 0.0, 1.0].into(),
  // ];
  // let triangle_colors: Vec<color::Color> = vec![
  //   color::RED,
  //   color::BLUE,
  //   color::YELLOW,
  //   color::PURPLE
  // ];
  // let tetrahderon_offsets: Vec<Vector> = vec![
  //   [-3.0, 0.0, 0.0].into(),
  //   [ 0.0, 0.0, 0.0].into(),
  //   [ 3.0, 0.0, 0.0].into(),
  // ];
  // for translation in tetrahderon_offsets.into_iter() {
  //   for i in 0..4 {
  //     let mut tmp_pts = Vec::new();
  //     for j in 0..4 {
  //       if i == j {
  //         continue;
  //       } else {
  //         tmp_pts.push(&triangle_points[j] + &translation);
  //       }
  //     }
  //     scene.add_object(Box::new(Triangle::from_pts(tmp_pts.clone(), triangle_colors[i].clone())));
  //   }
  // }

  let rendered = scene.render().expect("Failed to render scene!");
  util::save_array_as_image(&rendered, "output/test-pot-2.png");
}

