use crate::v1::pmath::*;


/// Represents a traced ray starting at the camera eye and pointing outwards towards viewport.
pub type LightRay = VectorPoint;
