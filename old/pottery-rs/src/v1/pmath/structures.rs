use std::ops::Index;

// pub type Matrix = [[f32; 3]; 3];

pub const PI: f32 = std::f32::consts::PI;

pub type FastPoint = [f32; 4];

pub type FastVector = [f32; 3];

pub type FastMatrix = [[f32; 4]; 4];

pub fn make_translation_matrix(x: f32, y: f32, z: f32) -> FastMatrix {
  [[1.0, 0.0, 0.0, x],
   [0.0, 1.0, 0.0, y],
   [0.0, 0.0, 1.0, z],
   [0.0, 0.0, 0.0, 1.0]]
}

pub fn fast_cross_product(v: &FastVector, w: &FastVector) -> FastVector {
  [
    v[1] * w[2] - v[2] * w[1],
    v[2] * w[0] - v[0] * w[2],
    v[0] * w[1] - v[1] * w[0]
  ]
}

pub fn fast_sub(v: &FastVector, w: &FastVector) -> FastVector {
  [ v[0] - w[0], v[1] - w[1], v[2] - w[2]]
}

pub fn fast_matrix_mul(m1: &FastMatrix, m2: &FastMatrix) -> FastMatrix {
  let mut m = [[0.0; 4]; 4];
  for i in 0..4 {
    for j in 0..4 {
      m[i][j] = m1[i][0] * m2[0][j] + m1[i][1] * m2[1][j] + m1[i][2] * m2[2][j] + m1[i][3] * m2[3][j];
    }
  }
  m
}

pub fn fast_matrix_mul_vec(m: &FastMatrix, p: &FastVector) -> FastVector {
  let mut p2 = [0.0; 3];
  for i in 0..3 {
    p2[i] = m[i][0]*p[0] + m[i][1]*p[1] + m[i][2]*p[2];
  }
  p2
}

pub fn fast_matrix_mul_pt(m: &FastMatrix, p: &FastPoint) -> FastPoint {
  let mut p2 = [0.0; 4];
  for i in 0..4 {
    p2[i] = m[i][0]*p[0] + m[i][1]*p[1] + m[i][2]*p[2]+m[i][3]*p[3];
  }
  p2
}

pub fn convert_matrix_to_fast(mat: &Matrix) -> FastMatrix {
  let mut fm = [[0.0; 4]; 4];
  for i in 0..3 {
    for j in 0..3 {
      fm[i][j] = mat._data[i][j];
    }
  }
  fm[3][3] = 1.0;
  fm
}

pub fn fast_vec_normalize(a: FastVector) -> FastVector {
  let n = 1.0 / (a[0]*a[0] + a[1]*a[1] + a[2]*a[2]).sqrt();
  [a[0] * n, a[1] * n, a[2] * n]
}

pub fn fast_get_vec_dis2(a: &FastVector, b: &FastVector) -> f32 {
  (a[0] - b[0])*(a[0] - b[0]) +
  (a[1] - b[1])*(a[1] - b[1]) +
  (a[2] - b[2])*(a[2] - b[2])
}

pub fn fast_get_pt_dis2(a: &FastPoint, b: &FastPoint) -> f32 {
  (a[0] - b[0])*(a[0] - b[0]) +
  (a[1] - b[1])*(a[1] - b[1]) +
  (a[2] - b[2])*(a[2] - b[2])
}

#[derive(Clone)]
pub struct Matrix {
  pub _data: [[f32; 3]; 3],
}

#[derive(Clone)]
pub struct Orientation {
  /// Polar angle (angle to z axis).
  pub theta: f32,
  /// Azimuthal angle (angle of vector projected onto xy axis)
  pub phi: f32,
}

#[derive(Clone)]
pub struct Vector {
  pub x: f32,
  pub y: f32,
  pub z: f32,
}

/// Represents a position with a direction (i.e. an actual vector) like a camera.
#[derive(Clone)]
pub struct VectorPoint {
  pub position: Point,
  pub direction: UnitVector,
}

#[derive(Clone)]
pub struct UnitVector {
  /// Whenever this is constructed directly, it is the caller's responsibility to ensure normalized.
  pub _protected: (),
  pub x: f32,
  pub y: f32,
  pub z: f32,
}

pub type Point = Vector;

impl Matrix {

  pub fn empty() -> Matrix {
    Matrix { _data: [[0.0; 3]; 3]}
  }

  /// Performs counterclockwise rotation in YZ plane
  pub fn rotation_x(th: f32) -> Matrix {
    let (sin, cos) = (th.sin(), th.cos());
    Self { _data: [
      [ 1.0,  0.0,  0.0],
      [ 0.0,  cos, -sin],
      [ 0.0,  sin,  cos]
    ] }
  }

  /// Performs counterclockwise rotation in ZX plane
  pub fn rotation_y(th: f32) -> Matrix {
    let (sin, cos) = (th.sin(), th.cos());
    Self { _data: [
      [ cos,  0.0,  sin],
      [ 0.0,  1.0,  0.0],
      [-sin,  0.0,  cos]
    ] }
  }

  /// Performs counterclockwise rotation in XY plane
  pub fn rotation_z(th: f32) -> Matrix {
    let (sin, cos) = (th.sin(), th.cos());
    Self { _data: [
      [ cos, -sin,  0.0],
      [ sin,  cos,  0.0],
      [ 0.0,  0.0,  1.0]
    ] }
  }

}

pub const IDENTITY: Matrix = Matrix { _data: [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]]};

pub trait VectorLike: Index<usize, Output=f32> {

  fn reflect_x(&self) -> Self;

  fn cross(&self, rhs: &Self) -> Vector;

}

impl Orientation {
  pub fn get_vector(&self) -> UnitVector {
    let sin_th = self.theta.sin();
    UnitVector {
      _protected: (),
      x: sin_th * self.phi.cos(),
      y: sin_th * self.phi.sin(),
      z: self.theta.cos()
    }
  }
}

impl UnitVector {
  pub fn get_orientation(&self) -> Orientation {
    let theta = self.z.acos();
    let phi = self.y.atan2(self.x);
    Orientation { theta, phi }
  }
}

impl Vector {

  pub fn new(x: f32, y: f32, z: f32) -> Self {
    Vector { x, y, z }
  }

  pub fn normalize(&self) -> UnitVector {
    UnitVector::from(self)
  }

  /// Converts vector to unit vector without normalizing. The caller must be sure the vector is
  /// already normalized.
  pub fn is_unit_vector(self) -> UnitVector {
    UnitVector { _protected: (), x: self.x, y: self.y, z: self.z }
  }

}

impl VectorPoint {
  pub fn new(position: Point, direction: UnitVector) -> Self {
    Self { position, direction }
  }
}

impl VectorLike for Vector {

  fn reflect_x(&self) -> Vector {
    Self { x: -self.x, y: self.y, z: self.z }
  }

  fn cross(&self, rhs: &Self) -> Vector {
    (
      self.y * rhs.z - self.z * rhs.y,
      self.z * rhs.x - self.x * rhs.z,
      self.x * rhs.y - self.y * rhs.x
    ).into()
  }

}

impl VectorLike for UnitVector {

  fn reflect_x(&self) -> UnitVector {
    Self { _protected: (), x: -self.x, y: self.y, z: self.z }
  }

  fn cross(&self, rhs: &Self) -> Vector {
    (
      self.y * rhs.z - self.z * rhs.y,
      self.z * rhs.x - self.x * rhs.z,
      self.x * rhs.y - self.y * rhs.x
    ).into()
  }

}

impl From<[[f32; 3]; 3]> for Matrix {
  fn from(d: [[f32; 3]; 3]) -> Self {
    Matrix { _data: d }
  }
}

impl From<&UnitVector> for Orientation {
  fn from(v: &UnitVector) -> Self {
    v.get_orientation()
  }
}

impl From<&Orientation> for UnitVector {
  fn from(o: &Orientation) -> Self {
    o.get_vector()
  }
}

impl From<&Vector> for UnitVector {
  fn from(v: &Vector) -> Self {
    let scale = 1.0 / (v * v).sqrt();
    UnitVector { _protected: (), x: scale * v.x, y: scale * v.y, z: scale * v.z }
  }
}

impl Index<usize> for UnitVector {
  type Output = f32;

  fn index(&self, index: usize) -> &f32 {
    match index {
      0 => &self.x,
      1 => &self.y,
      2 => &self.z,
      _ => panic!("Index out of bounds for Vector (length 3): {}", index)
    }
  }
}

impl From<(i32, i32, i32)> for Vector {
  fn from(p: (i32, i32, i32)) -> Vector {
    Self::new(p.0 as f32, p.1 as f32, p.2 as f32)
  }
}

impl From<(f32, f32, f32)> for Vector {
  fn from(p: (f32, f32, f32)) -> Vector {
    Self::new(p.0, p.1, p.2)
  }
}

impl From<[f32; 3]> for Vector {
  fn from(p: [f32; 3]) -> Vector {
    Self::new(p[0], p[1], p[2])
  }
}

impl Index<usize> for Vector {
  type Output = f32;

  fn index(&self, index: usize) -> &f32 {
    match index {
      0 => &self.x,
      1 => &self.y,
      2 => &self.z,
      _ => panic!("Index out of bounds for Vector (length 3): {}", index)
    }
  }
}

impl From<UnitVector> for FastVector {
  fn from(v: UnitVector) -> FastVector {
    [v.x, v.y, v.z]
  }
}
