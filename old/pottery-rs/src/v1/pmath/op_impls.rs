use std::ops::*;

use super::structures::*;

////////////////////////////////////////////////////////////////////////////////
// PURE VECTOR OPERATIONS
////////////////////////////////////////////////////////////////////////////////

impl<T> Add<&T> for &UnitVector
where T: VectorLike {
  type Output = Vector;

  fn add(self, rhs: &T) -> Self::Output {
    Vector::new(self[0] + rhs[0], self[1] + rhs[1], self[2] + rhs[2])
  }

}

impl<T> Add<&T> for &Vector
where T: VectorLike {
  type Output = Vector;

  fn add(self, rhs: &T) -> Self::Output {
    Vector::new(self.x + rhs[0], self.y + rhs[1], self.z + rhs[2])
  }

}

impl<T> Mul<&T> for &UnitVector
where T: VectorLike {
  type Output = f32;

  fn mul(self, rhs: &T) -> Self::Output {
    self[0] * rhs[0] + self[1] * rhs[1] + self[2] * rhs[2]
  }

}

impl<T> Mul<&T> for &Vector
where T: VectorLike {
  type Output = f32;

  fn mul(self, rhs: &T) -> Self::Output {
    self.x * rhs[0] + self.y * rhs[1] + self.z * rhs[2]
  }

}

impl Neg for &UnitVector {
  type Output = UnitVector;

  fn neg(self) -> Self::Output {
    UnitVector { _protected: (), x: -self.x, y: -self.y, z: -self.z }
  }

}

impl Neg for &Vector {
  type Output = Vector;

  fn neg(self) -> Self::Output {
    Vector { x: -self.x, y: -self.y, z: -self.z }
  }

}

impl<T> Sub<&T> for &UnitVector
where T: VectorLike {
  type Output = Vector;

  fn sub(self, rhs: &T) -> Self::Output {
    Vector::new(self[0] - rhs[0], self[1] - rhs[1], self[2] - rhs[2])
  }
}

impl<T> Sub<&T> for &Vector
where T: VectorLike {
  type Output = Vector;

  fn sub(self, rhs: &T) -> Self::Output {
    Vector::new(self.x - rhs[0], self.y - rhs[1], self.z - rhs[2])
  }
}

////////////////////////////////////////////////////////////////////////////////
// PURE MATRIX OPERATIONS
////////////////////////////////////////////////////////////////////////////////

impl Add<&Matrix> for &Matrix {

  type Output = Matrix;

  fn add(self, rhs: &Matrix) -> Self::Output {
    let mut result = [[0.0; 3]; 3];
    for i in 0..3 {
      for j in 0.. 3 {
        result[i][j] = self._data[i][j] + rhs._data[i][j]
      }
    }
    result.into()
  }

}

impl Mul<&Matrix> for &Matrix {

  type Output = Matrix;

  fn mul(self, rhs: &Matrix) -> Self::Output {
    let mut result = [[0.0; 3]; 3];
    for i in 0..3 {
      for j in 0.. 3 {
        result[i][j] = (0..3).map(|k| self._data[i][k] * rhs._data[k][j]).sum();
      }
    }
    result.into()
  }

}

impl Neg for &Matrix {

  type Output = Matrix;

  fn neg(self) -> Self::Output {
    let mut result = Matrix::empty();
    for i in 0..3 {
      for j in 0..3 {
        result._data[i][j] = - self._data[i][j];
      }
    }
    result
  }

}

impl Sub<&Matrix> for &Matrix {

  type Output = Matrix;

  fn sub(self, rhs: &Matrix) -> Self::Output {
    let mut result = [[0.0; 3]; 3];
    for i in 0..3 {
      for j in 0.. 3 {
        result[i][j] = self._data[i][j] - rhs._data[i][j]
      }
    }
    result.into()
  }
}

////////////////////////////////////////////////////////////////////////////////
// MIXED TYPE OPERATIONS
////////////////////////////////////////////////////////////////////////////////

impl Add<f32> for &Matrix {
  type Output = Matrix;

  fn add(self, rhs: f32) -> Self::Output {
    let mut result = Matrix::empty();
    for i in 0..3 {
      for j in 0..3 {
        result._data[i][j] = rhs + self._data[i][j];
      }
    }
    result
  }
}
impl Add<&Matrix> for f32 {
  type Output = Matrix;
  fn add(self, rhs: &Matrix) -> Self::Output {
    rhs + self
  }
}

impl Mul<f32> for &Matrix {
  type Output = Matrix;

  fn mul(self, rhs: f32) -> Self::Output {
    let mut result = Matrix::empty();
    for i in 0..3 {
      for j in 0..3 {
        result._data[i][j] = rhs * self._data[i][j];
      }
    }
    result
  }
}
impl Mul<&Matrix> for f32 {
  type Output = Matrix;
  fn mul(self, rhs: &Matrix) -> Self::Output {
    rhs * self
  }
}

impl Sub<f32> for &Matrix {

  type Output = Matrix;

  fn sub(self, rhs: f32) -> Self::Output {
    let mut result = Matrix::empty();
    for i in 0..3 {
      for j in 0..3 {
        result._data[i][j] = self._data[i][j] - rhs;
      }
    }
    result
  }
}

impl Add<f32> for &UnitVector {
  type Output = Vector;

  fn add(self, rhs: f32) -> Self::Output {
    Vector { x: self.x + rhs, y: self.y + rhs, z: self.z + rhs}
  }
}
impl Add<&UnitVector> for f32 {
  type Output = Vector;
  fn add(self, rhs: &UnitVector) -> Self::Output {
    rhs + self
  }
}

impl Add<f32> for &Vector {
  type Output = Vector;

  fn add(self, rhs: f32) -> Self::Output {
    Vector { x: self.x + rhs, y: self.y + rhs, z: self.z + rhs}
  }
}
impl Add<&Vector> for f32 {
  type Output = Vector;
  fn add(self, rhs: &Vector) -> Self::Output {
    rhs + self
  }
}

impl Mul<f32> for &UnitVector {
  type Output = Vector;

  fn mul(self, rhs: f32) -> Self::Output {
    Vector { x: self.x * rhs, y: self.y * rhs, z: self.z * rhs}
  }
}
impl Mul<&UnitVector> for f32 {
  type Output = Vector;
  fn mul(self, rhs: &UnitVector) -> Self::Output {
    rhs * self
  }
}
impl Mul<f32> for &Vector {
  type Output = Vector;

  fn mul(self, rhs: f32) -> Self::Output {
    Vector { x: self.x * rhs, y: self.y * rhs, z: self.z * rhs}
  }
}
impl Mul<&Vector> for f32 {
  type Output = Vector;
  fn mul(self, rhs: &Vector) -> Self::Output {
    rhs * self
  }
}

impl Sub<f32> for &UnitVector {
  type Output = Vector;

  fn sub(self, rhs: f32) -> Self::Output {
    Vector { x: self.x - rhs, y: self.y - rhs, z: self.z - rhs}
  }
}
impl Sub<f32> for &Vector {
  type Output = Vector;

  fn sub(self, rhs: f32) -> Self::Output {
    Vector { x: self.x - rhs, y: self.y - rhs, z: self.z - rhs}
  }
}

impl<T> Mul<&T> for &Matrix
where T: VectorLike {
  type Output = Vector;

  fn mul(self, rhs: &T) -> Self::Output {
    let mut result = [0.0; 3];
    for i in 0..3 {
      result[i] = (0..3).map(|j| self._data[i][j] * rhs[j]).sum();
    }
    result.into()
  }
}

impl Mul<(f32, f32, f32)> for &Matrix {
  type Output = Vector;

  fn mul(self, (x, y, z): (f32, f32, f32)) -> Self::Output {
    let mut result = [0.0; 3];
    for i in 0..3 {
      result[i] = self._data[i][0]*x + self._data[i][1]*y + self._data[i][2]*z;
    }
    result.into()
  }
}
