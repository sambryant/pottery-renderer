use crate::v1::{
  pmath::*,
  light_ray::LightRay
};

#[allow(unused_variables)]
pub fn interpolate(x_samples: &Vec<f32>, y_samples: &Vec<f32>, x_value: f32) -> Option<f32> {
  // // First check if its the height bounds
  // if center.z + radius < self.base.z {
  //   false
  // } else if center.z - radius > self.base.z + self.height {
  //   false
  todo!()
}

#[allow(unused_variables)]
pub fn point_in_box(p: &Point, c: &Point, radius: f32) -> bool {
  todo!()
}

#[allow(unused_variables)]
pub fn ray_through_box(ray: &LightRay, c: &Point, radius: f32) -> bool {
  todo!()
}

/// Checks if the given point lies within the given triangle assuming that all z-coordinates are
/// zero.
#[allow(non_snake_case)]
pub fn check_point_in_triangle2D(p: &Point, a: &Point, b: &Point, c: &Point) -> bool {
  let as_x = p.x - a.x;
  let as_y = p.y - a.y;
  let p_ab = ((b.x - a.x) * as_y - (b.y - a.y) * as_x) > 0.0;
  if ((c.x - a.x) * as_y - (c.y - a.y) * as_x > 0.0) == p_ab {
    false
  } else if ((c.x - b.x) * (p.y - b.y) - (c.y - b.y)*(p.x - b.x) > 0.0) != p_ab {
    false
  } else {
    true
  }
}

/// Given a point and a direction, computes (x, y) where z=0 along the corresponding line.
/// If the direction points away from z=0 or is parallel to z=0 plane, returns None.
pub fn get_xy_intercept(p: &Point, dir: &UnitVector) -> Option<Point> {
  // First check that direction points *towards* z=0 by looking at signs
  if p.z * dir.z >= 0.0 {
    None
  } else {
    // Figure out parametric value giving z=0
    let t = - p.z / dir.z; // will be well-defined and strictly positive b/c of above sign check.
    Some(Point {
      x: t * dir.x + p.x,
      y: t * dir.y + p.y,
      z: 0.0 // t * dir.z + p.z
    })
  }
}


