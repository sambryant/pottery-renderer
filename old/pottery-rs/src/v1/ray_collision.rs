use crate::v1::{
  color::Color,
  light_ray::LightRay,
  pmath::*,
};

/// Represents a light ray traced from eye to viewport to a point on an object.
pub struct RayCollision {
  /// Point on object surface where light ray collides
  pub point: Point,
  /// Orientation of normal vector of object surface at point of collision
  pub normal: Orientation,
  /// Unshaded color of object at point of collision.
  pub color: Color,
  /// Reference to collided object
  // pub object: &'a T,
  /// Distance from the eye to the point of collision
  pub distance: f32,
  /// Directional vector from collision point to camera eye.
  pub direction: UnitVector,
  /// Directional vector of light ray from collision to reflected light ray.
  pub direction_reflected: Option<UnitVector>,
}

/// Represents a light ray traced from eye to viewport to a point on an object.
pub struct FastRayCollision {
  /// Point on object surface where light ray collides
  pub point: FastPoint,
  /// Orientation of normal vector of object surface at point of collision
  pub normal: FastVector,
  /// Unshaded color of object at point of collision.
  pub color: Color,
}

impl RayCollision {

  pub fn new(point: Point, normal: Orientation, color: Color, light_ray: &LightRay) -> RayCollision {
    let displaced = &point - &light_ray.position;
    let distance = (&displaced * &displaced).sqrt();
    let direction = -&light_ray.direction;

    RayCollision {
      point, normal, color, distance, direction, direction_reflected: None
    }
  }

}

