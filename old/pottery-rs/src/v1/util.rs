use crate::v1::color::Color;

use image::{ImageBuffer, Rgb};

pub fn save_array_as_image(data: &Vec<Vec<Color>>, fname: &'static str) {
  let height = data.len();
  let width = data[0].len();

  let mut image = ImageBuffer::new(width as u32, height as u32);

  for i in 0..height {
    for j in 0..width {
      image.put_pixel(j as u32, (height - i - 1) as u32, Rgb::from(data[i][j]))
    }
  }

  image.save(fname).unwrap();
  println!("Saved image: {}", fname);
}

