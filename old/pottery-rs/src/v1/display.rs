use crate::v1::{
  pmath::structures::*,
  object::*,
  ray_collision::RayCollision
};

impl std::fmt::Debug for RayCollision {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "Ray Collision Information\n").and_then(|_| {
      write!(f, "  collision point           : {:?}\n", self.point)
    }).and_then(|_| {
      write!(f, "  surface normal            : {:?}\n", self.normal)
    }).and_then(|_| {
      write!(f, "  light ray vector          : {:?}\n", self.direction)
    }).and_then(|_| {
      write!(f, "  light ray orientation     : {:?}\n", self.direction.get_orientation())
    }).and_then(|_| {
      write!(f, "  reflected ray vector      : {:?}\n", self.direction_reflected)
    }).and_then(|_| {
      write!(f, "  reflected ray orientation : {:?}\n", self.direction_reflected.as_ref().map(|dr| dr.get_orientation()))
    })
  }
}

impl std::fmt::Display for Triangle {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "Triangle with color: {:?}\n", self.color)
      .and_then(|_| {
        write!(f, "  A = ( {:+.1}, {:+.1}, {:+.1} )\n", self.a[0], self.a[1], self.a[2])
      })
      .and_then(|_| {
        write!(f, "  B = ( {:+.1}, {:+.1}, {:+.1} )\n", self.b[0], self.b[1], self.b[2])
      })
      .and_then(|_| {
        write!(f, "  C = ( {:+.1}, {:+.1}, {:+.1} )\n", self.c[0], self.c[1], self.c[2])
      })
      .and_then(|_| {
        write!(f, "  normal {}", self.normal)
      })
  }
}

impl std::fmt::Display for VectorPoint {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "VectorPoint\n")
      .and_then(|_| {
        write!(f, "  position  = ( {:+.1}, {:+.1}, {:+.1} )\n", self.position[0], self.position[1], self.position[2])
      })
      .and_then(|_| {
        write!(f, "  direction = ( {:+.1}, {:+.1}, {:+.1} )\n", self.direction[0], self.direction[1], self.direction[2])
      })
  }
}

impl std::fmt::Display for Matrix {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "⎡ {:+.1}, {:+.1}, {:+.1} ⎤\n", self._data[0][0], self._data[0][1], self._data[0][2])
      .and_then(|_| {
        write!(f, "⎢ {:+.1}, {:+.1}, {:+.1} ⎥\n", self._data[1][0], self._data[1][1], self._data[1][2])
      })
      .and_then(|_| {
        write!(f, "⎣ {:+.1}, {:+.1}, {:+.1} ⎦", self._data[2][0], self._data[2][1], self._data[2][2])
      })
  }
}

impl std::fmt::Display for Orientation {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "theta = {:.1}°, phi = {:.1}°", (180.0/PI) * self.theta, (180.0/PI) * self.phi)
  }
}

impl std::fmt::Debug for Orientation {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "theta = {:.1}°, phi = {:.1}°", (180.0/PI) * self.theta, (180.0/PI) * self.phi)
  }
}

impl std::fmt::Display for UnitVector {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "⎛{:+.1}⎞\n⎜{:+.1}⎟\n⎝{:+.1}⎠", self.x, self.y, self.z)
  }
}

impl std::fmt::Display for Vector {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "⎛{:+.1}⎞\n⎜{:+.1}⎟\n⎝{:+.1}⎠", self.x, self.y, self.z)
  }
}

impl std::fmt::Debug for VectorPoint {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "position = {:?} direction = {:?}", self.position, self.direction)
  }
}

impl std::fmt::Debug for UnitVector {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{{{:+.1}, {:+.1}, {:+.1}}}", self.x, self.y, self.z)
  }
}

impl std::fmt::Debug for Vector {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{{{:+.1}, {:+.1}, {:+.1}}}", self.x, self.y, self.z)
  }
}
