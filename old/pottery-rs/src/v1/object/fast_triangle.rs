use crate::v1::{
  pmath::*,
  color::Color,
  light_ray::LightRay,
  ray_collision::{FastRayCollision, RayCollision},
};

use super::object::*;

pub struct FastTriangle {
  /// Unshaded color of surface of triangle.
  pub color: Color,
  /// First vertex in real coordinates.
  pub a: FastVector,
  /// Second vertex in real coordinates.
  pub b: FastVector,
  /// Third vertex in real coordinates.
  pub c: FastVector,
  /// Angle of vector normal to surface.
  pub normal: FastVector,
  /// Second vertex in translated coordinate system (given by trans).
  rb: FastVector,
  /// Third vertex in translated coordinate system (given by trans).
  rc: FastVector,

  /// Matrix performing rotations which align normal vector with +z axis.
  rotate_fast: FastMatrix,
  full_fast: FastMatrix,
  full_fast_inverse: FastMatrix,
}

impl FastTriangle {

  pub fn from_refs(a: &FastVector, b: &FastVector, c: &FastVector, color: &Color) -> FastTriangle {
    Self::new(a.clone(), b.clone(), c.clone(), color.clone())
  }

  pub fn new(a: FastVector, b: FastVector, c: FastVector, color: Color) -> FastTriangle {
    let normal = fast_vec_normalize(fast_cross_product(&fast_sub(&b, &a), &fast_sub(&c, &a)));
    let theta = normal[2].acos();
    let phi = normal[1].atan2(normal[0]);

    let rotate_z_for = convert_matrix_to_fast(&Matrix::rotation_z(-phi));
    let rotate_z_inv = convert_matrix_to_fast(&Matrix::rotation_z( phi));
    let rotate_y_for = convert_matrix_to_fast(&Matrix::rotation_y(-theta));
    let rotate_y_inv = convert_matrix_to_fast(&Matrix::rotation_y( theta));
    let translat_for = make_translation_matrix(-a[0], -a[1], -a[2]);
    let translat_inv = make_translation_matrix( a[0],  a[1],  a[2]);

    let rotate_fast = fast_matrix_mul(&rotate_y_for, &rotate_z_for);
    let full_fast = fast_matrix_mul(&rotate_fast, &translat_for);
    let inv_rotate_fast = fast_matrix_mul(&rotate_z_inv, &rotate_y_inv);
    let full_fast_inverse = fast_matrix_mul(&translat_inv, &inv_rotate_fast);

    // Compute vertices of triangle in translated coordinate system.
    let rb = fast_matrix_mul_vec(&full_fast, &b);
    let rc = fast_matrix_mul_vec(&full_fast, &c);

    FastTriangle {
      color, a, b, c, rb, rc, normal, rotate_fast, full_fast, full_fast_inverse
    }
  }

  fn detect_collision_3(&self, p0: &FastPoint, d0: &FastVector) -> Option<FastRayCollision> {
    // Transform ray to coordinate system where vertex A is origin and normal is +z axis.
    let p1 = fast_matrix_mul_pt(&self.full_fast, p0);
    let d1 = fast_matrix_mul_vec(&self.rotate_fast, d0);

    // Check that ray is pointed towards plane:
    if p1[2] * d1[2] >= 0.0 {
      None
    } else {
      // Find projection of eye point onto plane of triangle.
      let t = - p1[2] / d1[2];
      let p2 = [
        t * d1[0] + p1[0],
        t * d1[1] + p1[1],
        0.0,
        1.0
      ];
      // Determine if that 2D projection lies within the triangle
      let (b, c) = (&self.rb, &self.rc);
      let p_ab = (b[0] * p2[1] - b[1] * p2[0]) > 0.0;
      if (c[0] * p2[1] - c[1] * p2[0] > 0.0) == p_ab {
        None
      } else if ((c[0] - b[0]) * (p2[1] - b[1]) - (c[1] - b[1])*(p2[0] - b[0]) > 0.0) != p_ab {
        None
      } else {
        // Project intersection point back to real coordinates
        let p3 = fast_matrix_mul_pt(&self.full_fast_inverse, &p2);

        Some(FastRayCollision {
          point: p3,
          normal: self.normal.clone(),
          color: self.color.clone(),
        })
      }
    }
  }
}

impl Object for FastTriangle {

  fn fast_detect_collision(&self, p0: &FastPoint, d0: &FastVector) -> Option<FastRayCollision> {
    self.detect_collision_3(p0, d0)
  }

  fn detect_collision<'a>(&'a self, light_ray: &LightRay) -> Option<RayCollision> {
    todo!()
  }

  fn debug_detect_collision<'a>(&'a self, light_ray: &LightRay, test: bool) -> Option<RayCollision> {
    todo!()
  }
}
