use crate::v1::{
  color::Color,
  light_ray::LightRay,
  pmath::algorithms,
  pmath::*,
  ray_collision::{FastRayCollision, RayCollision},
};

use super::object::*;

pub struct Triangle {
  /// Unshaded color of surface of triangle.
  pub color: Color,
  /// First vertex in real coordinates.
  pub a: Point,
  /// Second vertex in real coordinates.
  pub b: Point,
  /// Third vertex in real coordinates.
  pub c: Point,
  /// Angle of vector normal to surface.
  pub normal: Orientation,
  /// First vertex in translated coordinate system (given by trans) (i.e. 0 vector).
  ra: Point,
  /// Second vertex in translated coordinate system (given by trans).
  rb: Point,
  /// Third vertex in translated coordinate system (given by trans).
  rc: Point,

  /// Matrix performing rotations which align normal vector with +z axis.
  trans: Matrix,
  trans_inverse: Matrix,
  rotate_fast: FastMatrix,
  full_fast: FastMatrix,
  full_fast_inverse: FastMatrix,
  /// Matrix used to obtain the direction of light reflected off the surface of the object.
  /// First does PI rotation about z-axis in coordinate system where triangle is in XY plane.
  /// Then inverts the transformation back to original coordinates.
  /// Finally it flips the direction of the ray so it will point outward.
  reflect_trans_inverse: Matrix,
}

impl Triangle {

  pub fn from_pts(mut pts: Vec<Point>, color: Color) -> Triangle {
    if pts.len() != 3 {
      panic!("Expected exactly three points to be passed to triangle");
    } else {
      Self::new(pts.pop().unwrap(), pts.pop().unwrap(), pts.pop().unwrap(), color)
    }
  }

  pub fn from_refs(a: &Point, b: &Point, c: &Point, color: &Color) -> Triangle {
    Self::new(a.clone(), b.clone(), c.clone(), color.clone())
  }

  pub fn new(a: Point, b: Point, c: Point, color: Color) -> Triangle {
    let normal = (&(&b - &a)).cross(&(&c - &a)).normalize().get_orientation();

    // We must compute the transformation matrices used to do ray tracing.
    // The transformation we need is the one that aligns the normal vector with the +z axis.
    //  1. Rotate about z axis so normal lies in XZ plane (no y component).
    let rotate_xy = Matrix::rotation_z(- normal.phi);
    //  2. Rotate about y axis so normal is aligned with z-axis.
    let rotate_zx = Matrix::rotation_y(- normal.theta);
    let trans = (&rotate_zx) * (&rotate_xy);
    // We also need the inverse operation, which we can get by doing steps in reverse.
    let rotate_zx = Matrix::rotation_y(  normal.theta);
    let rotate_xy = Matrix::rotation_z(  normal.phi);
    let trans_inverse = (&rotate_xy) * (&rotate_zx);
    let reflect_trans_inverse = -&(&trans_inverse * &Matrix::rotation_z(PI));

    // Compute vertices of triangle in translated coordinate system.
    let ra = Point::new(0.0, 0.0, 0.0);
    let rb = &trans * &(&b - &a);
    let rc = &trans * &(&c - &a);

    // Fast stuff
    let translate_fast = make_translation_matrix(-a.x, -a.y, -a.z);
    let rotate_fast = convert_matrix_to_fast(&trans);
    let full_fast = fast_matrix_mul(&rotate_fast, &translate_fast);
    // Fast reverse stuff
    let inv_translate_fast = make_translation_matrix(a.x, a.y, a.z);
    let inv_rotate_fast = convert_matrix_to_fast(&trans_inverse);
    let full_fast_inverse = fast_matrix_mul(&inv_translate_fast, &inv_rotate_fast);


    Triangle {
      color, a, b, c, ra, rb, rc, normal, trans, trans_inverse, reflect_trans_inverse, rotate_fast, full_fast, full_fast_inverse
    }
  }

  /// Detect if the given light ray collides with this triangle and if so, return info.
  ///
  /// How this algorithm works:
  ///  - We transform everything to coordinates where vertex A is the origin and the normal vector
  ///    is +z axis. In this system, the 2D triangle lies entirely in XY plane.
  ///  - We then calculate the (x,y) coordinate where the line defined by the light ray intercepts
  ///    z=0, this is a 2D point on XY plane.
  ///  - We then calculate if this 2D point lies within the 2D triangle.
  ///  - If it does, we invert our coordinate transformation to get the intersection point in the
  ///    original coordinate system.
  fn detect_collision_1<'a>(&'a self, light_ray: &LightRay) -> Option<RayCollision> {
    use algorithms::{get_xy_intercept, check_point_in_triangle2D};
    // Transform ray to coordinate system where vertex A is origin and normal is +z axis.
    let p0 = (&self.trans) * &((&light_ray.position) - (&self.a));
    let ray_direction = ((&self.trans) * (&light_ray.direction)).is_unit_vector();

    // Ray source (eye) projected onto plane of triangle.
    if let Some(p0_xy_projection) = get_xy_intercept(&p0, &ray_direction) {
      if check_point_in_triangle2D(&p0_xy_projection, &self.ra, &self.rb, &self.rc) {
        // We now have to translate the collision back to real coordinates.
        let point = &(&self.trans_inverse * &p0_xy_projection) + (&self.a);
        Some(RayCollision::new(
          point,
          self.normal.clone(),
          self.color.clone(),
          light_ray
        ))
      } else {
        None
      }
    } else {
      None
    }
  }

  fn detect_collision_2(&self, light_ray: &LightRay) -> Option<RayCollision> {
    // Transform ray to coordinate system where vertex A is origin and normal is +z axis.
    let p1 = (&self.trans) * &((&light_ray.position) - (&self.a));
    let d1 = ((&self.trans) * (&light_ray.direction)).is_unit_vector();

    // Check that ray is pointed towards plane:
    if p1.z * d1.z >= 0.0 {
      None
    } else {
      // Find projection of eye point onto plane of triangle.
      let t = - p1.z / d1.z;
      let x2 = t * d1.x + p1.x;
      let y2 = t * d1.y + p1.y;

      // Determine if that 2D projection lies within the triangle
      let (b, c) = (&self.rb, &self.rc);
      let p_ab = (b.x * y2 - b.y * x2) > 0.0;
      if (c.x * y2 - c.y * x2 > 0.0) == p_ab {
        None
      } else if ((c.x - b.x) * (y2 - b.y) - (c.y - b.y)*(x2 - b.x) > 0.0) != p_ab {
        None
      } else {
        // Project intersection point back to real coordinates
        let point = &(&self.trans_inverse * (x2, y2, 0.0)) + (&self.a);
        Some(RayCollision::new(
          point,
          self.normal.clone(),
          self.color.clone(),
          light_ray
        ))
      }
    }
  }

  fn detect_collision_3(&self, p0: &FastPoint, d0: &FastVector) -> Option<FastRayCollision> {
    // Transform ray to coordinate system where vertex A is origin and normal is +z axis.
    let p1 = fast_matrix_mul_pt(&self.full_fast, p0);
    let d1 = fast_matrix_mul_vec(&self.rotate_fast, d0);

    // Check that ray is pointed towards plane:
    if p1[2] * d1[2] >= 0.0 {
      None
    } else {
      // Find projection of eye point onto plane of triangle.
      let t = - p1[2] / d1[2];
      let p2 = [
        t * d1[0] + p1[0],
        t * d1[1] + p1[1],
        0.0,
        1.0
      ];
      // Determine if that 2D projection lies within the triangle
      let (b, c) = (&self.rb, &self.rc);
      let p_ab = (b.x * p2[1] - b.y * p2[0]) > 0.0;
      if (c.x * p2[1] - c.y * p2[0] > 0.0) == p_ab {
        None
      } else if ((c.x - b.x) * (p2[1] - b.y) - (c.y - b.y)*(p2[0] - b.x) > 0.0) != p_ab {
        None
      } else {
        // Project intersection point back to real coordinates
        let p3 = fast_matrix_mul_pt(&self.full_fast_inverse, &p2);

        Some(FastRayCollision {
          point: p3,
          normal: self.normal.get_vector().into(),
          color: self.color.clone(),
        })
      }
    }
  }
}

impl Object for Triangle {

  fn fast_detect_collision(&self, p0: &FastPoint, d0: &FastVector) -> Option<FastRayCollision> {
    self.detect_collision_3(p0, d0)
  }

  fn detect_collision<'a>(&'a self, light_ray: &LightRay) -> Option<RayCollision> {
    self.detect_collision_2(light_ray)
  }

  fn debug_detect_collision<'a>(&'a self, light_ray: &LightRay, test: bool) -> Option<RayCollision> {
    use algorithms::{get_xy_intercept, check_point_in_triangle2D};
    // Transform ray to coordinate system where vertex A is origin and normal is +z axis.
    let p0 = (&self.trans) * &((&light_ray.position) - (&self.a));
    let ray_direction = ((&self.trans) * (&light_ray.direction)).is_unit_vector();

    // Ray source (eye) projected onto plane of triangle.
    if let Some(p0_xy_projection) = get_xy_intercept(&p0, &ray_direction) {
      if check_point_in_triangle2D(&p0_xy_projection, &self.ra, &self.rb, &self.rc) {
        let displaced = &p0 - &p0_xy_projection;
        let distance = (&displaced * &displaced).sqrt();

        // Rotate incoming ray in XY plane by PI (reflection) and then invert original
        // transformation to get the direction of the light ray reflected off the surface.
        let direction_reflected = (&self.reflect_trans_inverse * &ray_direction).is_unit_vector();

        let trans_outgoing = &Matrix::rotation_z(PI) * &ray_direction;
        let outgoing = (&self.trans_inverse * &trans_outgoing).is_unit_vector();

        if test {
          println!("Information in transformed coordinates");

          println!("AA = {:?};", &self.a - &self.a);
          println!("BB = {:?};", &self.b - &self.a);
          println!("CC = {:?};", &self.c - &self.a);
          println!("mid = (AA + BB + CC)/3.0;");
          println!("normal = {:?};", self.normal.get_vector());
          println!("incoming = {:?};", -&light_ray.direction);
          println!("outgoing = {:?};", -&outgoing);

          println!("transA = {:?};", self.ra);
          println!("transB = {:?};", self.rb);
          println!("transC = {:?};", self.rc);
          println!("transmid = (transA + transB + transC)/3.0;");
          println!("transnormal = {:?};", &self.trans * &self.normal.get_vector());
          println!("transincoming = {:?};", -&ray_direction);
          println!("transoutgoing = {:?};", -&(&Matrix::rotation_z(PI) * &ray_direction));
        }

        // We now have to translate the collision back to real coordinates.
        let point = &(&self.trans_inverse * &p0_xy_projection) + (&self.a);
        Some(RayCollision::new(
          point,
          self.normal.clone(),
          self.color.clone(),
          light_ray
        ))
        // Some(RayCollision {
        //   point,
        //   distance,
        //   normal: self.normal.clone(),
        //   color: self.color.clone(),
        //   direction: -(&light_ray.direction),
        //   direction_reflected: Some(-&outgoing),
        // })
      } else {
        None
      }
    } else {
      None
    }
  }
}
