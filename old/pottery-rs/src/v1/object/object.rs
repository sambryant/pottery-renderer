use crate::v1::{
  pmath::*,
  light_ray::LightRay,
  ray_collision::*,
};

pub trait Object {

  fn detect_collision<'a>(&'a self, light_ray: &LightRay) -> Option<RayCollision>;

  fn fast_detect_collision(&self, p0: &FastPoint, d0: &FastVector) -> Option<FastRayCollision>;

  fn debug_detect_collision<'a>(&'a self, light_ray: &LightRay, _test: bool) -> Option<RayCollision> {
    self.detect_collision(light_ray)
  }

}

