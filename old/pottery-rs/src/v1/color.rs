pub type Color = [u8; 3];

pub const RED: Color = [193, 56, 48];
pub const BLUE: Color = [77, 142, 211];
pub const YELLOW: Color = [242, 214, 77];
pub const PURPLE: Color = [153, 114, 149];
pub const GRAY: Color = [50, 50, 50];
pub const WHITE: Color = [255, 255, 255];
pub const BLACK: Color = [0, 0, 0];

pub fn shade_color(color: &Color, light: f32) -> Color {
  [
    (color[0] as f32 * light) as u8,
    (color[1] as f32 * light) as u8,
    (color[2] as f32 * light) as u8,
  ]
}
