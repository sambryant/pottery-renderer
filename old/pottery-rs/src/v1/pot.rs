use crate::v1::{
  color::Color,
  object::{FastTriangle, Triangle},
  pmath::*,
};

pub struct PotMaker {
  pub n_ths: usize,
  pub n_zs: usize,
}

fn get_pts(height: f32, n_ths: u32, n_zs: u32) -> Vec<Vec<(f32, f32)>> {
  let z_inc = height / (n_zs as f32);
  let th_inc = 2.0 * PI / (n_ths as f32);
  (0..n_zs)
    .map(|i| z_inc * (i as f32))
    .map(|z| (0..n_ths)
      .map(|j| (z, th_inc * (j as f32)))
      .collect())
    .collect()
}

impl PotMaker {


  pub fn triangle_coloration(&self, ncols: u32, nrows: u32, height: f32, c1: Color, c2: Color) -> Vec<Vec<Color>> {
    let col_inc = 2.0 * PI / (ncols as f32);
    let row_inc = height / (nrows as f32);



    todo!()
  }


  pub fn make_pot_mesh(&self, base: Point, height: f32, color: Color, radius: fn(f32) -> f32) -> Vec<Triangle> {
    let z_inc = height / (self.n_zs as f32);
    let th_inc = 2.0 * PI / (self.n_ths as f32);

    let vs: Vec<Vec<Point>> = (0..self.n_zs)
      .map(|i| z_inc * (i as f32))
      .map(|z| (z, radius(z)))
      .map(|(z, r)| (0..self.n_ths)
        .map(|j| th_inc * (j as f32))
        .map(|th| Point::new(base.x + r * th.cos(), base.y + r * th.sin(), base.z + z))
        .collect()
      ).collect();

    println!("Computing mesh with {} x {} = {} vertices", self.n_zs, self.n_ths, self.n_zs * self.n_ths);

    let mut triangles = Vec::new();

    for i in 0..(self.n_zs - 1) {
      for j in 0..(self.n_ths) {
        let j2 = if j == self.n_ths - 1 {
          0
        } else {
          j + 1
        };
        triangles.push(Triangle::from_refs(&vs[i][j], &vs[i][j2], &vs[i+1][j2], &color));
        triangles.push(Triangle::from_refs(&vs[i][j], &vs[i+1][j], &vs[i+1][j2], &color));
      }
    }
    triangles
  }

  pub fn make_colored_pot_mesh(&self, base: Point, height: f32, color: fn(f32) -> [u8; 3], radius: fn(f32) -> f32) -> Vec<Triangle> {
    let z_inc = height / (self.n_zs as f32);
    let th_inc = 2.0 * PI / (self.n_ths as f32);

    let vs: Vec<Vec<(Color, Point)>> = (0..self.n_zs)
      .map(|i| z_inc * (i as f32))
      .map(|z| (z, radius(z)))
      .map(|(z, r)| (0..self.n_ths)
        .map(|j| th_inc * (j as f32))
        .map(|th| (
          color(th),
          Point::new(base.x + r * th.cos(), base.y + r * th.sin(), base.z + z)))
        .collect()
      ).collect();

    println!("Computing mesh with {} x {} = {} vertices", self.n_zs, self.n_ths, self.n_zs * self.n_ths);

    let mut triangles = Vec::new();

    for i in 0..(self.n_zs - 1) {
      for j in 0..(self.n_ths) {
        let j2 = if j == self.n_ths - 1 {
          0
        } else {
          j + 1
        };
        // if j < self.n_ths / 2 {
        //   continue
        // }
        triangles.push(Triangle::from_refs(&vs[i][j].1, &vs[i][j2].1, &vs[i+1][j2].1, &vs[i][j].0));
        triangles.push(Triangle::from_refs(&vs[i][j].1, &vs[i+1][j].1, &vs[i+1][j2].1, &vs[i][j].0));
      }
    }
    triangles
  }

  pub fn make_fast_colored_pot_mesh(&self, base: FastVector, height: f32, color: fn(f32) -> [u8; 3], radius: fn(f32) -> f32) -> Vec<FastTriangle> {
    let z_inc = height / (self.n_zs as f32);
    let th_inc = 2.0 * PI / (self.n_ths as f32);

    let vs: Vec<Vec<(Color, FastVector)>> = (0..self.n_zs)
      .map(|i| z_inc * (i as f32))
      .map(|z| (z, radius(z)))
      .map(|(z, r)| (0..self.n_ths)
        .map(|j| th_inc * (j as f32))
        .map(|th| (
          color(th),
          [base[0] + r * th.cos(), base[1] + r * th.sin(), base[2] + z]))
        .collect()
      ).collect();

    println!("Computing mesh with {} x {} = {} vertices", self.n_zs, self.n_ths, self.n_zs * self.n_ths);

    let mut triangles = Vec::new();

    for i in 0..(self.n_zs - 1) {
      for j in 0..(self.n_ths) {
        let j2 = if j == self.n_ths - 1 {
          0
        } else {
          j + 1
        };
        // if j < self.n_ths / 2 {
        //   continue
        // }
        triangles.push(FastTriangle::from_refs(&vs[i][j].1, &vs[i][j2].1, &vs[i+1][j2].1, &vs[i][j].0));
        triangles.push(FastTriangle::from_refs(&vs[i][j].1, &vs[i+1][j].1, &vs[i+1][j2].1, &vs[i][j].0));
      }
    }
    triangles
  }

}
