use std::collections::HashMap;
use crate::{
  scene::*,
  pmath::*,
  color::*,
};

pub struct ComplexRenderer {
  pub n_viewport_cols: u32,
  pub n_viewport_rows: u32,
  col_size: f32,
  row_size: f32,
}

pub struct ComplexRenderResult {
  pub colors: Vec<Vec<Color>>,
  pub flip_x: bool,
  pub flip_y: bool,
}

impl ComplexRenderer {
  pub fn new(scene: &Scene, n_viewport_cols: u32, n_viewport_rows: u32) -> Self {
    let col_size = scene.viewport[0] / (n_viewport_cols as f32);
    let row_size = scene.viewport[1] / (n_viewport_rows as f32);
    Self {
      n_viewport_cols,
      n_viewport_rows,
      col_size,
      row_size,
    }
  }
}

type SubBox = (u32, u32);

type TriangleIndex = usize;

impl ComplexRenderer {

  /// Given a vertex projected onto the viewport, determines which sub-box it lies in.
  fn determine_vertex_box(&self, scene: &Scene, projected: &Vec2) -> SubBox {
    let (w, h) = (scene.viewport[0], scene.viewport[1]);
    let (px, py) = (projected[0] + w / 2.0, projected[1] + h / 2.0);
    let (i, j) = ((px / self.col_size) as u32, (py / self.row_size) as u32);
    (i, j)
  }

  /// Determines the indices of the sub-boxes which fully encapsulate a given triangle.
  /// These are the bounds of the sub-boxes which need to check for the triangle to be present.
  fn determine_triangle_bounds(&self, scene: &Scene, projected: &[Vec2; 3]) -> [[u32; 2]; 2] {
    let mut xs = [0; 3];
    let mut ys = [0; 3];
    for i in 0..3 {
      let (x, y) = self.determine_vertex_box(scene, &projected[i]);
      xs[i] = x;
      ys[i] = y;
    }
    fn get_min_max(arr: [u32; 3]) -> [u32; 2] {
      let (mut min, mut max) = (arr[0], arr[0]);
      for i in 1..3 {
        if arr[i] > max {
          max = arr[i];
        }
        if arr[i] < min {
          min = arr[i];
        }
      }
      [min, max]
    }
    [get_min_max(xs), get_min_max(ys)]
  }

  fn build_box_map(&self, box_bounds: Vec<Option<[[u32; 2]; 2]>>) -> HashMap<(u32, u32), Vec<usize>> {
    let mut map = HashMap::new();
    for i in 0..self.n_viewport_cols {
      for j in 0..self.n_viewport_rows {
        map.insert((i, j), Vec::new());
      }
    }
    for (ind, bounds_opt) in box_bounds.into_iter().enumerate() {
      if let Some(bounds) = bounds_opt {
        for i in bounds[0][0]..(bounds[0][1]+1) {
          for j in bounds[1][0]..(bounds[1][1]+1) {
            if let Some(b) = map.get_mut(&(i,j)) {
              b.push(ind)
            }
          }
        }
      }
    }
    map
  }

  fn get_box_index(&self, scene: &Scene, i: u32, j: u32) -> (u32, u32) {
    (i * self.n_viewport_cols / scene.screen[0], j * self.n_viewport_rows / scene.screen[1])
  }

  fn get_viewport_point(&self, scene: &Scene, i: u32, j: u32) -> Vec2 {
    [
      ((i as f32) / (scene.screen[0] as f32)) * scene.viewport[0] - scene.viewport[0]/2.0,
      ((j as f32) / (scene.screen[1] as f32)) * scene.viewport[1] - scene.viewport[1]/2.0
    ]
  }

  pub fn render_point(&self, scene: &Scene, shading: &Option<Vec<f32>>, mut collisions: Vec<(usize, Vec3)>) -> Option<Color> {
    if collisions.len() == 0 {
      None
    } else if collisions.len() == 1 {
      Some(scene.objects[collisions[0].0].color.clone())
    } else {
      let mut min_distance = None;
      let mut min_collision = None;
      while let Some((ind, pt)) = collisions.pop() {
        let dis = pmath_distance2_vec3_vec3(&scene.camera.pos, &pt);
        if let Some(prev_min) = min_distance {
          if dis < prev_min {
            min_distance = Some(dis);
            min_collision = Some(ind);
          }
        } else {
          min_distance = Some(dis);
          min_collision = Some(ind);
        }
      }
      min_collision.map(|ind| {
        if let Some(shade) = shading {
          let light = (0.5 * ( 1.0 + shade[ind])).max(scene.ambient_light);
          shade_color(&scene.objects[ind].color, light)
        } else {
          scene.objects[ind].color.clone()
        }
      })
    }
  }

  fn compute_shading(&self, scene: &Scene) -> Option<Vec<f32>> {
    if let Some(light_source) = scene.light_source {
      // Compute cosine theta for angle between surface normal and light vector to midpoint;
      // This may have to be inverted depending on which face is rendered.
      Some(scene.objects.iter()
        .map(|tri| {
          // Vector pointing from midpoint of triangle to light source
          let tri_light_vec = pmath_vec3_normalize(&pmath_sub3(&light_source, &tri.mid));

          // Vector pointing from midpoint of triangle to camera
          let tri_camera_vec = pmath_sub3(&scene.camera.pos, &tri.mid);

          // Lighting on positive side of triangle given by dot product of v1 and normal
          let light = pmath_dot_vec3_vec3(&tri_light_vec, &tri.normal);

          // Determine which side of the triangle we are looking at by dotting v2 and normal
          let check_tri_side = pmath_dot_vec3_vec3(&tri.normal, &tri_camera_vec);
          if check_tri_side > 0.0 {
            light
          } else {
            -light
          }
        })
        .collect())
    } else {
      None
    }
  }

  /// Given a triangle in 3D, its projection onto VP, and a projected point P' in the viewport this
  /// computes the point P lying on the triangle in 3D whose projection is P'.
  /// This returns None when the point does not lie in the triangle.
  fn reverse_project_detect(&self, triangle: &[Vec3; 3], proj_triangle: &[Vec2; 3], proj_point: Vec2) -> Option<Vec3> {
    fn sub2(v1: &Vec2, v2: &Vec2) -> Vec2 {
      [v1[0] - v2[0], v1[1] - v2[1]]
    }
    fn sub3(v1: &Vec3, v2: &Vec3) -> Vec3 {
      [v1[0] - v2[0], v1[1] - v2[1], v1[2] - v2[2]]
    }
    fn add3(v1: &Vec3, v2: &Vec3) -> Vec3 {
      [v1[0] + v2[0], v1[1] + v2[1], v1[2] + v2[2]]
    }
    fn mul3(a: f32, v2: &Vec3) -> Vec3 {
      [a * v2[0], a * v2[1], a * v2[2]]
    }

    let (ap, bp, cp) = (proj_triangle[0], proj_triangle[1], proj_triangle[2]);
    let (rel_bp, rel_cp) = (sub2(&bp, &ap), sub2(&cp, &ap));
    let rel_pp = sub2(&proj_point, &ap);

    let denom = rel_bp[1] * rel_cp[0] - rel_bp[0] * rel_cp[1];
    let t1 = -(rel_pp[0]*rel_cp[1] - rel_pp[1]*rel_cp[0]) / denom;
    let t2 = (rel_pp[0]*rel_bp[1] - rel_pp[1]*rel_bp[0]) / denom;

    if t1 < 0.0 || t2 < 0.0 || (t1 + t2) > 1.0 {
      None
    } else {
      let (a, b, c) = (triangle[0], triangle[1], triangle[2]);
      Some(add3(&a, &add3(&mul3(t1, &sub3(&b, &a)), &mul3(t2, &sub3(&c, &a)))))
    }
  }

  // pub fn render_unoptimized(&self, scene: &Scene) -> ComplexRenderResult {

  //   // Step 1:
  //   // Transform system to coordinates where the viewport is z=0 plane.
  //   let triangles_trans: Vec<[Vec3; 3]> = scene.objects.iter()
  //     .map(|tri| scene.transform_triangle(&tri.vs))
  //     .collect();

  //   // Step 2:
  //   // For each triangle, get its vertices projected onto the viewport with respect to camera eye.
  //   let projected_triangles: Vec<Option<[Vec2; 3]>> = triangles_trans.iter()
  //     .map(|trans| scene.project_transformed_triangle(trans))
  //     .collect();

  //   // println!("Within: {:?}", self.get_viewport_point(&scene, 190, 190));
  //   // println!("RPD: {:?}", self.reverse_project_detect(&triangles_trans[0], &projected_triangles[0].unwrap(), [-0.05, -0.05]));
  //   // todo!();
  //   // self.reverse_project_detect(&triangles_trans[0], &projected_triangles[0].unwrap(), [])

  //   // Step 5: Ray tracing
  //   //  - Iterate through each point in the screen.
  //   //  - Determine which sub box we are in.
  //   //  - Check for collisions for each triangle in that subbox
  //   let mut rendered: Vec<Vec<Color>> = (0..scene.screen[0])
  //     .map(|_| (0..scene.screen[1])
  //       .map(|_| scene.background.clone()).collect()
  //     ).collect();
  //   for i in 0..scene.screen[0] {
  //     for j in 0..scene.screen[1] {
  //       let point = self.get_viewport_point(&scene, i, j);
  //       let mut collisions: Vec<(usize, Vec3)> = Vec::new();
  //       for k in 0..scene.objects.len() {
  //         let tri = &scene.objects[k];
  //         if let Some(tri_proj) = &projected_triangles[k] {
  //           let collision_point = self.reverse_project_detect(&tri.vs, tri_proj, point.clone());
  //           if let Some(point) = collision_point {
  //             collisions.push((k, point));
  //           }
  //         }
  //       }
  //       if let Some(color) = self.render_point(&scene, &None, collisions) {
  //         rendered[i as usize][j as usize] = color;
  //       }
  //     }
  //   }
  //   ComplexRenderResult {
  //     colors: rendered,
  //     flip_x: true,
  //     flip_y: true,
  //   }
  // }

  /// For each triangle, computes the projection of each of its vertices onto the viewport screen.
  fn compute_projected_triangles(scene: &Scene) -> Vec<Option<[Vec2; 3]>> {
    // Transform system to coordinates where the viewport is z=0 plane.
    let triangles_trans: Vec<[Vec3; 3]> = scene.objects.iter()
      .map(|tri| scene.transform_triangle(&tri.vs))
      .collect();

    // For each triangle, get its vertices projected onto the viewport with respect to camera eye.
    triangles_trans.iter()
      .map(|trans| scene.project_transformed_triangle(trans))
      .collect()
  }

  /// For each subbox on the viewport, determines list of triangles which might intersect it.
  fn compute_triangle_subboxes(&self, scene: &Scene, projected_triangles: &Vec<Option<[Vec2; 3]>>) -> Option<HashMap<SubBox, Vec<TriangleIndex>>> {
    match (self.n_viewport_cols, self.n_viewport_rows) {
      (1, 1) => None,
      _ => {
        // Determine the set of bounding sub-boxes for each triangle. When ray tracing within this
        // subregion, we must check that triangle.
        let triangle_box_bounds: Vec<Option<[[u32; 2]; 2]>> = projected_triangles.iter()
          .map(|t| t.map(|tri| self.determine_triangle_bounds(&scene, &tri)))
          .collect();
        // Build a map from each sub-box on the viewport to the triangles which may intersect it.
        Some(self.build_box_map(triangle_box_bounds))
      }
    }
  }

  pub fn render(&self, scene: &Scene) -> ComplexRenderResult {
    let mut rendered: Vec<Vec<Color>> = (0..scene.screen[0])
      .map(|_| (0..scene.screen[1])
        .map(|_| scene.background.clone()).collect()
      ).collect();

    // Step 1: Project all objects onto viewport screen in coordinate system where viewport at z=0
    let proj_tris: Vec<Option<[Vec2; 3]>> = Self::compute_projected_triangles(scene);

    // Step 2: Compute shading for each object in scene.
    let shading = self.compute_shading(&scene);

    // Step 3: For each subbox on the viewport, compute which triangles might intersect it.
    let triangle_subboxes = self.compute_triangle_subboxes(&scene, &proj_tris);

    // Step 4: Ray tracing
    //  - Iterate through each point in the screen.
    //  - Determine which sub box we are in.
    //  - Check for collisions for each triangle in that subbox
    for i in 0..scene.screen[0] {
      for j in 0..scene.screen[1] {
        let objects_to_check: Vec<TriangleIndex> = if let Some(ref subbox_map) = triangle_subboxes {
          let box_ind = self.get_box_index(&scene, i, j);
          subbox_map.get(&box_ind).unwrap().clone()
        } else {
          (0..scene.objects.len()).collect()
        };

        let point = self.get_viewport_point(&scene, i, j);
        let mut collisions: Vec<(TriangleIndex, Vec3)> = Vec::new();
        for ind in objects_to_check.into_iter() {
          let tri = &scene.objects[ind];
          if let Some(pt) = self.reverse_project_detect(&tri.vs, &proj_tris[ind].unwrap(), point.clone()) {
            collisions.push((ind, pt));
          }
        }
        if let Some(color) = self.render_point(&scene, &shading, collisions) {
          rendered[i as usize][j as usize] = color;
        }
      }
    }
    ComplexRenderResult {
      colors: rendered,
      flip_x: true,
      flip_y: true,
    }
  }
}
