use crate::scene::*;


/// Renders a scene of triangles by just projecting their vertices. Does not draw faces, or shading.
pub struct SimpleRenderer {}


/// Result of calling simple render. This is just all the vertices projected onto the viewport.
/// Keep in mind that *both* x and y should be flipped in the final image.
pub struct SimpleRenderResult {
  pub viewport_bounds: [[f32; 2]; 2],
  pub vertices: Vec<[f32; 2]>,
  pub flip_x: bool,
  pub flip_y: bool,
  pub screen: [u32; 2],
}

impl SimpleRenderer {

  pub fn render(&self, scene: &Scene) -> SimpleRenderResult {
    // For each triangle, get its vertices projected onto the viewport with respect to camera eye.
    let projected_triangles: Vec<[[f32; 2]; 3]> = scene.objects.iter()
      .filter_map(|tri| scene.get_viewport_projection_triangle(tri))
      .collect();

    let mut vertices = Vec::new();
    for t in projected_triangles.into_iter() {
      for i in 0..3 {
        vertices.push(t[i]);
      }
    }

    SimpleRenderResult {
      viewport_bounds: [
        [-scene.viewport[0]/2.0, scene.viewport[0]/2.0],
        [-scene.viewport[1]/2.0, scene.viewport[1]/2.0]
      ],
      vertices,
      flip_x: true,
      flip_y: true,
      screen: scene.screen.clone(),
    }
  }
}
