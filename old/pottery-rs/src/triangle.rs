use super::pmath::*;
use super::color::*;

pub struct Triangle {
  pub vs: [Vec3; 3],
  pub rel_vs: [Vec3; 2],
  pub color: Color,
  pub mid: Vec3,
  pub normal: Vec3,
}

impl Triangle {

  pub fn new(vs: [Vec3; 3]) -> Self {
    let mut mid = [0.0; 3];
    for i in 0..3 {
      mid[i] = (0..3).map(|j| vs[j][i]).sum::<f32>() / 3.0;
    }

    let ba = pmath_sub3(&vs[1], &vs[0]);
    let ca = pmath_sub3(&vs[2], &vs[0]);
    let normal = pmath_vec3_normalize(&pmath_cross_vec3_vec3(&ba, &ca));
    Self { vs, color: BLUE, normal, mid, rel_vs: [ba, ca] }
  }

  pub fn report(&self) {
    println!("Triangle");
    println!("  a = {};", fmt_vec3(&self.vs[0]));
    println!("  b = {};", fmt_vec3(&self.vs[1]));
    println!("  c = {};", fmt_vec3(&self.vs[2]));
  }

  pub fn color(mut self, color: Color) -> Self {
    self.color = color;
    self
  }
}