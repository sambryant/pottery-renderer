import numpy as np

from initial.core import *
from initial.pmath import *

class ShadingInfo():
  incoming: NormVector
  outgoing: NormVector
  light: NormVector
  shading_fraction: float

  def __init__(self, incoming: NormVector, outgoing: NormVector, light: NormVector, shading_fraction: float):
    self.incoming = incoming
    self.outgoing = outgoing
    self.light = light
    self.shading_fraction = shading_fraction

def compute_shading(eye: Point, obj: Point, normal: Point, light_source: Point):
  """
  Shading is defined on scale of 0 - 1.

  Tentatively, it's the cosine of the angle formed between two vectors. The first is the vector from
  the object point to the light source. The second is the angle of the outgoing vector. The outgoing
  vector is the vector from the eye to the object point reflected off the normal of the surface.
  """
  print(eye)
  print(obj)
  print(eye - obj)
  sys.exit(0)
  incoming = NormVector(eye - obj)

  outgoing = NormVector(compute_reflection(incoming, normal))

  light_vector = NormVector(light_source - obj)

  fraction = 0.5 * (1 + np.dot(outgoing, light_vector))

  return ShadingInfo(incoming, outgoing, light_vector, fraction)

