from __future__ import annotations
from abc import ABC, abstractmethod
from typing import Union, List

import numpy as np
import warnings

from initial.graphics import *
from initial.core import *
from initial.pmath import *

class ContactInfo():
  pt: Point # xyz point where ray intersects surface
  normal: Point # Normal vector of surface ray intersects
  incoming: Point # Orientation vector pointing from contact point to ray source
  distance: Point # Distance from eye to point on surface
  color: List[int] # Base color of object (ignores lighting effects)
  obj: Object

  def __init__(self, obj, pt, distance, incoming, normal, color):
    self.obj = obj
    self.pt = pt
    self.distance = distance
    self.incoming = incoming
    self.normal = normal
    self.color = np.array(color, dtype=int)

  @classmethod
  def get_closest(cls, intersects: List[ContactInfo]):
    mind = None
    minc = None
    for inter in intersects:
      if mind is None or inter.distance < mind:
        mind = inter.distance
        minc = inter
    return minc

class Object():
  _object_counter: int = 0

  def __init__(self):
    self.object_id = Object._object_counter
    Object._object_counter += 1

  @abstractmethod
  def render_point(eye_pt: Point, screen_pt: Point) -> Union[None, ContactInfo]:
    pass

def plot_scene(eye, screen, pts, normal=None):
  ax = setupax()

  print(eye)

  ax.plot(*eye, '.', ms=10, label='eye')
  ax.plot(*screen, '.', ms=8, label='screen')
  plot_triangle(pts, ax=ax)

  if normal is not None:
    ax.plot([pts[0][0], pts[0][0] + normal[0]], [pts[0][1], pts[0][1] + normal[1]], [pts[0][2], pts[0][2] + normal[2]], color='black')
  # ax.plot(pts[:,0], pts[:,1], pts[:,2], label='object')
  return ax

class Triangle(Object):
  pts: List[Point]
  color: str

  def __init__(self, pts, color=Color.BLUE):
    super().__init__()
    self.pts = pts
    self.rel_pts = np.array([[0, 0, 0], pts[1] - pts[0], pts[2] - pts[0]])
    self.normal = normalize(np.cross(self.rel_pts[1], self.rel_pts[2]))
    self.color = color

    self._compute_matrices()

  def __str__(self):
    return 'Triangle (id = %d)' % self.object_id

  def _compute_matrices(self):
    th, ph = get_vector_angles(self.normal)
    ph_rot = array([[cos(ph), sin(ph), 0], [-sin(ph), cos(ph), 0], [0, 0, 1]])
    th_rot = array([[cos(th), 0, -sin(th)], [0, 1, 0], [sin(th), 0, cos(th)]])
    self.T1 = np.einsum('ij,jk->ik', th_rot, ph_rot)
    self.T1inv = np.linalg.inv(self.T1)
    self.pts_2 = np.einsum('ij,kj->ki', self.T1, self.rel_pts)
    self.normal_2 = np.einsum('ij,j->i', self.T1, self.normal)

  def plot(self):
    ax = setupax()
    plot_triangle(self.pts, ax=ax)
    ax.plot([self.pts[0][0], self.pts[0][0] + self.normal[0]], [self.pts[0][1], self.pts[0][1] + self.normal[1]], [self.pts[0][2], self.pts[0][2] + self.normal[2]], color='black')

  def render_point(self, eye_pt: Point, screen_pt: Point) -> Union[None, ContactInfo]:
    incoming = normalize(eye_pt - screen_pt)
    costh = np.dot(self.normal, incoming)
    if costh < 0:
      normal = - self.normal
    elif costh > 0:
      normal = self.normal
    else:
      return None

    # Translate points so first triangle vertex is origin
    T0 = self.pts[0]
    p0_2 = np.einsum('ij,j->i', self.T1, eye_pt - T0)
    p1_2 = np.einsum('ij,j->i', self.T1, screen_pt - T0)

    # Compute equation for line connecting eye and scene
    contact_point = get_xy_intercept(p0_2, p1_2)

    if contact_point is None:
      return None

    if not check_pt_in_triangle2D(contact_point, self.pts_2):
      return None

    contact_point = np.einsum('ij,j->i', self.T1inv, contact_point) + T0
    distance = np.linalg.norm(contact_point - eye_pt)
    return ContactInfo(self, contact_point, distance, incoming, self.normal, self.color)

if __name__ == '__main__':
  eye_pt = array([0, 0, 1])
  screen_pt = array([0.2, -0.3, 0.4])
  triangle = Triangle(array([
    [1.0, -1.0, -0.7],
    [-1.0, -1.0, -1.1],
    [0.5, 1.0, -1.0]
  ]))
  # eye_pt = np.array([0, -1, 0])
  # screen_pt = np.array([0.3, 0.0, 0.2])
  # triangle = Triangle(np.array([[-1, 1.1, -0.1], [0.7, 0.8, 0.8], [1.1, 1.3, -0.7]]))
  triangle.render_point(eye_pt, screen_pt)
  # triangle.plot()


