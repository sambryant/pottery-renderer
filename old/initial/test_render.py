from __future__ import annotations
from abc import ABC, abstractmethod
from typing import Union
import sys

import numpy as np
import scipy
from PIL import Image, ImageDraw
from numpy import array, pi

from initial.graphics import *
from initial.core import *
from initial.pmath import *
from initial.object import *
from initial.renderer import *



def triangle():
  vp_width = 10
  vp_height = 5
  vp_distance = 5
  vp = Viewport(vp_width, vp_height, vp_distance)

  pixels_ratio = 50 # 500 * 400
  screen = Screen(int(pixels_ratio * vp_width), int(pixels_ratio * vp_height))

  print('screen: ', screen)

  eye_pt = array([0, -5, 0])
  eye_ph = 0
  eye_th = pi/2
  eye = Eye(eye_pt, eye_th, eye_ph)

  scene = Scene(vp, eye)

  scene.add_object(Triangle(array([[-1, 5.0, -1], [0, 5, 1], [1, 5, -1]])))
  imgmat = scene.render(screen)

  img = Image.new('RGB', (screen.n_pixels_x, screen.n_pixels_y))
  dr = ImageDraw.Draw(img)
  for j in range(len(imgmat)):
    for i in range(len(imgmat[0])):
      dr.point((j, i), fill=tuple(imgmat[j][i]))

  img.show()


def tetrahedron():
  vp_width = 10
  vp_height = 5
  vp_distance = 8
  vp = Viewport(vp_width, vp_height, vp_distance)

  pixels_ratio = 50 # 500 * 400
  screen = Screen(int(pixels_ratio * vp_width), int(pixels_ratio * vp_height))

  print('screen: ', screen)

  # eye_pt = array([-5, -5, 0])
  # eye_ph = pi/4
  # eye_th = pi/2
  # eye = Eye(eye_pt, eye_th, eye_ph)

  eye_pt = array([0, 0, 10])
  eye_ph = 0
  eye_th = 0
  eye = Eye(eye_pt, eye_th, eye_ph)

  scene = Scene(vp, eye)


  pts = [
    [+1.0, +0.0, 0.0],
    [-0.5, +0.866, 0.0],
    [-0.5, -0.866, 0.0],
    [0.0, 0.0, 1.0],
  ]
  colors = [
    (77, 142, 211),
    (153, 114, 149),
    (242, 214, 77),
    (193, 56, 48),
  ]
  offsets = [array([-3, 0, 0]), array([0, 0, 0]), array([3, 0, 0])]
  for p0 in offsets:
    triangles = []
    for i in range(4):
      tmp_pts = []
      for j in range(4):
        if j == i:
          continue
        tmp_pts.append(pts[j] + p0)
      scene.add_object(Triangle(array(tmp_pts), color=colors[i]))

  scene.add_object(Triangle(array([[-4, -2, -1], [4, 2, -1], [-4, 2, -1]]), color=(210, 210, 210)))
  scene.add_object(Triangle(array([[-4, -2, -1], [4, 2, -1], [+4, -2, -1]]), color=(210, 210, 210)))

  imgmat = scene.render(screen)

  img = Image.new('RGB', (screen.n_pixels_x, screen.n_pixels_y))
  dr = ImageDraw.Draw(img)
  for j in range(len(imgmat)):
    for i in range(len(imgmat[0])):
      dr.point((j, i), fill=tuple(imgmat[j][i]))

  img.show()


def tetrahedron_lit():
  vp_width = 10
  vp_height = 5
  vp_distance = 8
  vp = Viewport(vp_width, vp_height, vp_distance)

  pixels_ratio = 50 # 500 * 400
  screen = Screen(int(pixels_ratio * vp_width), int(pixels_ratio * vp_height))

  print('screen: ', screen)

  light = LightSource(array([3, 0, 8]))

  eye_pt = array([0, 0, 10])
  eye_ph = 0
  eye_th = 0
  eye = Eye(eye_pt, eye_th, eye_ph)

  scene = Scene(vp, eye)


  pts = [
    [+1.0, +0.0, 0.0],
    [-0.5, +0.866, 0.0],
    [-0.5, -0.866, 0.0],
    [0.0, 0.0, 1.0],
  ]
  colors = [
    (77, 142, 211),
    (153, 114, 149),
    (242, 214, 77),
    (193, 56, 48),
  ]
  offsets = [array([-3, 0, 0]), array([0, 0, 0]), array([3, 0, 0])]
  for p0 in offsets:
    triangles = []
    for i in range(4):
      tmp_pts = []
      for j in range(4):
        if j == i:
          continue
        tmp_pts.append(pts[j] + p0)
      scene.add_object(Triangle(array(tmp_pts), color=colors[i]))

  scene.add_object(Triangle(array([[-4, -2, -1], [4, 2, -1], [-4, 2, -1]]), color=(210, 210, 210)))
  scene.add_object(Triangle(array([[-4, -2, -1], [4, 2, -1], [+4, -2, -1]]), color=(210, 210, 210)))
  scene.set_light(light)

  imgmat = scene.render(screen)

  img = Image.new('RGB', (screen.n_pixels_x, screen.n_pixels_y))
  dr = ImageDraw.Draw(img)
  for j in range(len(imgmat)):
    for i in range(len(imgmat[0])):
      dr.point((j, i), fill=tuple(imgmat[j][i]))

  img.show()

def lighting():
  vp_width = 10
  vp_height = 5
  vp_distance = 8
  vp = Viewport(vp_width, vp_height, vp_distance)

  pixels_ratio = 50 # 500 * 400
  screen = Screen(int(pixels_ratio * vp_width), int(pixels_ratio * vp_height))

  light = LightSource(array([200, 0, 200]))

  eye_pt = array([0, 0, 10])
  eye_ph = 0
  eye_th = 0
  eye = Eye(eye_pt, eye_th, eye_ph)

  scene = Scene(vp, eye)

  scene.add_object(Triangle(array([[-5, -3, -1], [5, 3, -1], [-5, 3, -1]]), color=(210, 210, 210)))
  scene.add_object(Triangle(array([[-5, -3, -1], [5, 3, -1], [+5, -3, -1]]), color=(210, 210, 210)))
  scene.set_light(light)

  scene.add_object(Triangle(array([[-3, -2, 1], [-3, 2, 1], [-4, 0, 1]]), color=(77, 142, 211)))
  scene.add_object(Triangle(array([[+3, -2, 1], [+3, 2, 1], [+4, 0, 1]]), color=(77, 142, 211)))

  imgmat = scene.render(screen)

  img = Image.new('RGB', (screen.n_pixels_x, screen.n_pixels_y))
  dr = ImageDraw.Draw(img)
  for j in range(len(imgmat)):
    for i in range(len(imgmat[0])):
      dr.point((j, i), fill=tuple(imgmat[j][i]))

  img.show()


if __name__ == '__main__':
  # triangle()
  # tetrahedron_lit()
  lighting()
