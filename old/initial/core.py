import numpy as np

class Color():
  RED = '#C13830'
  ORANGE = '#E5A050'
  YELLOW = '#F2D64D'
  GREEN = '#9EAA26'
  BLUE = (77, 142, 211)
  TEAL = '#008080'
  VIOLET = '#C62483'
  PURPLE = '#997295'
  BROWN = '#964B00'
  GREY = '#E6EAED'
  BLACK = '#181916'

  class Alt():
    RED = '#B35B59'
    ORANGE = '#D29165'
    YELLOW = '#D7B467'
    GREEN = '#A2AA5C'
    BLUE = '#7392A7'
    VIOLET = '#9D84A5'
    GREY = '#F4F4F4'
    BLACK = '#232420'
    ALL = [RED, ORANGE, YELLOW, GREEN, BLUE, VIOLET]

class NormVector(np.array):

  def __init__(self, data):
    super().__init__(data)

  @property
  def x(self):
    return self[0]

  @property
  def y(self):
    return self[1]

  @property
  def z(self):
    return self[2]

Point = np.ndarray # 3 dim (x, y, z)

Orientation = np.ndarray # 2 dim (th, ph)

def fmt_pt(pt):
  return '%+.1f %+.1f %.1f' % tuple(pt)

# def compute_reflection_vector(incoming: Orientation, normal: Orientation):



