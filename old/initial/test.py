import numpy as np
import matplotlib.pyplot as plt
from numpy import sin, cos, pi, array
import sys

from initial.core import *
from initial.pmath import *

def setupax():
  fig = plt.figure()
  ax = fig.add_subplot(projection='3d')
  ax.set_xlim(-1.1, 1.1)
  ax.set_ylim(-1.1, 1.1)
  ax.set_zlim(-1.1, 1.1)
  ax.set_xticks([-1, 0, 1])
  ax.set_yticks([-1, 0, 1])
  ax.set_zticks([-1, 0, 1])
  ax.set_xlabel('X')
  ax.set_ylabel('Y')
  ax.set_zlabel('Z')
  ax.set_aspect('equal')
  ax.plot([0], [0], [0], '.', color='black', ms=20)
  return ax

def plot_ori(pt, ax=None, label=None, color=None):
  if ax is None:
    ax = setupax()
  xs = [0, pt[0]]
  ys = [0, pt[1]]
  zs = [0, pt[2]]
  if color:
    ax.plot(xs, ys, zs, label=label, color=color)
  else:
    ax.plot(xs, ys, zs, label=label)
  return ax

def plot_reflection(normal, incoming, outgoing=None, ax=None, title=None):
  if ax is None:
    ax = setupax()
  if title is not None:
    ax.set_title(title)
  plot_surface(normal, ax=ax, color=Color.Alt.BLUE)
  plot_ori(normal, ax=ax, label='Normal', color=Color.BLUE)
  plot_ori(incoming, ax=ax, label='Incoming', color=Color.RED)
  if outgoing is not None:
    plot_ori(outgoing, label='Outgoing', ax=ax, color=Color.ORANGE)
  ax.legend()
  return ax

# def compute_reflection(incoming, normal):
#   """
#   1. Perform rotation about z-axis so that normal vector lies in XZ plane (y component is zero).
#   2. Perform rotation about y-axis so that normal vector is aligned to z-axis.
#   3. Perform rotation about z-axis so that incoming vector lies in XZ plane (y component is zero).
#   4. Reflect x-coordinate of incoming vector
#   5. Reverse transformations done in step 1-3
#   """
#   incoming = normalize(incoming)
#   normal = normalize(normal)
#   th, ph = ori_to_angles(normal)
#   ph_rot = array([[cos(ph), sin(ph), 0], [-sin(ph), cos(ph), 0], [0, 0, 1]])
#   th_rot = array([[cos(th), 0, -sin(th)], [0, 1, 0], [sin(th), 0, cos(th)]])
#   T1 = np.einsum('ij,jk->ik', th_rot, ph_rot)
#   incoming2 = np.einsum('ij,j->i', T1, incoming)

#   th2, ph2, = ori_to_angles(incoming2)
#   T2 = array([[cos(ph2), sin(ph2), 0], [-sin(ph2), cos(ph2), 0], [0, 0, 1]])
#   incoming3 = np.einsum('ij,j->i', T2, incoming2)

#   reflect = array([[-1, 0, 0], [0, 1, 0], [0, 0, 1]])
#   incoming4 = np.einsum('ij,j->i', reflect, incoming3)

#   inverted = np.einsum('ij,jk->ik', T2, T1)
#   inverted = np.linalg.inv(inverted)
#   return np.einsum('ij,j->i', inverted, incoming4)

def compute_reflection_test(incoming, normal):
  incoming = normalize(incoming)
  normal = normalize(normal)
  outgoing = compute_reflection(incoming, normal)

  test = np.cross(incoming, normal)

  print(incoming)
  print(normal)
  print(outgoing)

  ax = plot_reflection(normal, incoming, outgoing=outgoing, title='Reflected vector')
  plot_ori(test, ax=ax, label='Cross', color=Color.PURPLE)
  plt.show()
  sys.exit(0)



  th, ph = ori_to_angles(normal)
  ph_rot = array([[cos(ph), sin(ph), 0], [-sin(ph), cos(ph), 0], [0, 0, 1]])
  th_rot = array([[cos(th), 0, -sin(th)], [0, 1, 0], [sin(th), 0, cos(th)]])


  normal = np.einsum('ij,j->i', ph_rot, normal)
  incoming = np.einsum('ij,j->i', ph_rot, incoming)
  plot_reflection(normal, incoming, title='2: After ph rotation')

  normal = np.einsum('ij,j->i', th_rot, normal)
  incoming = np.einsum('ij,j->i', th_rot, incoming)
  plot_reflection(normal, incoming, title='3: After th rotation')
  th2, ph2, = ori_to_angles(incoming)
  ph_rot = array([[cos(ph2), sin(ph2), 0], [-sin(ph2), cos(ph2), 0], [0, 0, 1]])
  normal = np.einsum('ij,j->i', ph_rot, normal)
  incoming = np.einsum('ij,j->i', ph_rot, incoming)
  plot_reflection(normal, incoming, title='4: After ph (incoming) rotation')
  plt.show()
  sys.exit()

def normalize(a):
  return a / np.sqrt(np.dot(a, a))

def plot_surface(pt, ax=None, color=None):
  if ax is None:
    ax = setupax()
  xs = np.linspace(-1, 1, 100)
  ys = np.linspace(-1, 1, 100)
  zs = 1 - xs - ys

  a, b, c = pt
  pts = np.zeros((100 * 100, 3))
  for i, x in enumerate(np.linspace(-1, 1, 100)):
    for j, y in enumerate(np.linspace(-1, 1, 100)):
      z = (-a * x - b * y) / c
      pts[100 * i + j,:] = [x, y, z]

  if color:
    ax.plot(pts[:,0], pts[:,1], pts[:,2], color=color)
  else:
    ax.plot(pts[:,0], pts[:,1], pts[:,2])
  return ax

def ori_to_angles(ori):
  ori /= np.sqrt(np.dot(ori, ori))
  th = np.arccos(ori[2])
  if sin(th) == 0:
    return 0, 0
  ph = np.arccos(ori[0]/np.sin(th))
  return th, ph

def test4():
  normal = np.array([0, 0, 1])
  incoming = np.array([0.5, 0, 0.5])
  compute_reflection_test(incoming, normal)

def test3():
  incoming = [-1.0, 0.8, 2.0]
  normal = np.array([0.5, 0.8, 2.0])
  compute_reflection_test(incoming, normal)

def test2():
  normal = [0.5, 1, 0]
  print(180 / (pi) * ori_to_angles(normal)[1])
  ax = setupax()
  plot_surface(normal, ax=ax)
  plot_ori(normal, ax=ax)
  plt.show()

def test1():
  plot_ori([0, 1, 1])
  plot_surface([0, 1, 1])
  plt.show()

if __name__ == '__main__':
  test3()
  # test1()
  # test2()


