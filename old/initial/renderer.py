from __future__ import annotations
from abc import ABC, abstractmethod
from typing import Union, List

import numpy as np
from numpy import array, pi

from initial.graphics import *
from initial.core import *
from initial.pmath import *
from initial.object import *
from initial.shader import *

class Viewport():
  width: float
  height: float
  distance: float

  def __init__(self, width, height, distance):
    self.width = width
    self.height = height
    self.distance = distance

class Eye():
  pt: Point
  th: float
  ph: float

  def __init__(self, pt, th, ph):
    self.pt = pt
    self.th = th
    self.ph = ph

class Screen():
  n_pixels_x: int
  n_pixels_y: int
  def __init__(self, n_pixels_x, n_pixels_y):
    self.n_pixels_x = n_pixels_x
    self.n_pixels_y = n_pixels_y

class LightSource():
  pt: Point

  def __init__(self, pt):
    self.pt = pt

class Scene():
  light: LightSource
  viewport: Viewport
  eye: Eye
  objects: List[Object]

  def __init__(self, viewport, eye):
    self.viewport = viewport
    self.eye = eye
    self.light = None
    self.objects = []

  def set_light(self, light):
    self.light = light

  def add_object(self, object):
    self.objects.append(object)

  def render(self, screen):
    print('test')

    # Eye rotation matrices
    th, ph = self.eye.th, self.eye.ph
    rot_ph = array([[cos(ph), sin(ph), 0], [-sin(ph), cos(ph), 0], [0, 0, 1]])
    rot_th = array([[1, 0, 0], [0, cos(th), -sin(th)], [0, sin(th), cos(th)]])
    # rot_th = array([[cos(th), 0, -sin(th)], [0, 1, 0], [sin(th), 0, cos(th)]])
    trans_eye = np.einsum('ij,jk->ik', rot_th, rot_ph)

    x_per_pixel = self.viewport.width / screen.n_pixels_x
    y_per_pixel = self.viewport.height / screen.n_pixels_y

    rendered_scene = np.zeros((screen.n_pixels_x, screen.n_pixels_y, 3), dtype=int)

    # top left corner of viewport in frame where eye looks in negative z direction
    p0 = array([-self.viewport.width / 2, +self.viewport.height / 2, -self.viewport.distance])
    for j in range(screen.n_pixels_x):
      for i in range(screen.n_pixels_y):

        vp_pt = p0 + array([x_per_pixel * j, -y_per_pixel * i, 0])
        vp_ptt = np.einsum('ij,j->i', trans_eye, vp_pt) + self.eye.pt

        if i == 0 or i == screen.n_pixels_y - 1:
          if j == 0 or j == screen.n_pixels_x - 1:
            print('viewport point (real): ', ', '.join(['%.1f' % i for i in vp_ptt]))

        intersects = []
        for obj in self.objects:
          contact = obj.render_point(self.eye.pt, vp_ptt)
          if contact is not None:
            intersects.append(contact)

        intersect = ContactInfo.get_closest(intersects)
        if intersect is not None:
          if self.light is None:
            rendered_scene[j][i][:] = intersect.color
          else:
            shading = compute_shading(vp_ptt, intersect.pt, intersect.normal, self.light.pt)

            if i % 20 == 0 and j % 20 == 0:
              print('Test point: %d, %d' % (j, i))
              print('  eye point        : %s' % fmt_pt(self.eye.pt))
              print('  logical viewport point: %s' % fmt_pt(vp_pt))
              print('  actual  viewport point: %s' % fmt_pt(vp_ptt))
              print('  intersected obj  : %s' % intersect.obj)
              print('  intersect pt     : %s' % fmt_pt(intersect.pt))
              print('  eye-ray vector   : %s' % fmt_pt(normalize(self.eye.pt - vp_ptt)))
              print('  incoming         : %s' % fmt_pt(normalize(intersect.incoming)))
              print('  incoming (shader): %s' % fmt_pt(shading.incoming))
              print('  object normal    : %s' % fmt_pt(intersect.normal))
              print('  outgoing         : %s' % fmt_pt(shading.outgoing))
              print('  object-light ray : %s' % fmt_pt(shading.light))
              print('  shading          : %.2f' % shading.shading_fraction)
              color = (255, 255, 255)
            else:
              color = [shading.shading_fraction * intersect.color[0], shading * intersect.color[1], shading * intersect.color[2]]
            rendered_scene[j][i][:] = color
    return rendered_scene











