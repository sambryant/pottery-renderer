import sys
import numpy as np
from numpy import sin, cos, pi, array

from initial.core import *

def get_xy_intercept(p0: Point, p1: Point):
  x1, y1, z1 = p0
  x2, y2, z2 = p1
  if z2 == z1:
    return None
  if z1 > 0:
    # Indicates plane is behind eye
    if z2 < 0:
      return None
    # Indicates plane is between eye and screen
    if z2 > z1:
      return None
  if z1 < 0:
    if z2 > 0:
      return None
    if z2 < z1:
      return None

  if x1 == x2:
    x0 = x1
  else:
    dxdz = (x2 - x1) / (z2 - z1)
    x0 = x1 - dxdz * z1
  if y1 == y2:
    y0 = y1
  else:
    dydz = (y2 - y1) / (z2 - z1)
    y0 = y1 - dydz * z1
  return array([x0, y0, 0])

def check_pt_in_triangle2D(pt, vertices):
  # We assume first vertex is origin and all points are relative to it
  a, b, c = vertices
  s = pt

  as_x = s[0] - a[0]
  as_y = s[1] - a[1]
  s_ab = ((b[0] - a[0]) * as_y - (b[1] - a[1]) * as_x) > 0
  if (((c[0] - a[0]) * as_y - (c[1] - a[1]) * as_x > 0) == s_ab):
    return False
  if (((c[0] - b[0]) * (s[1] - b[1]) - (c[1] - b[1])*(s[0] - b[0]) > 0) != s_ab):
    return False
  return True


  # as_x = pt[0] - a[0]
  # as_y = pt[1] - a[1]
  # s_ab = (b[0] - a[0]) * as_y - (b[1] - a[1]) * as_x > 0
  # if ((c[0] - a[0]) * as_y - (c[1] - a[1]) * as_x > 0) == s_ab:
  #   return False
  # if ((c[0] - b[0]) * (pt[1] - b[1]) - (c[1] - b[1]) * (pt[0] - b[0])) > 0 != s_ab:
  #   return False
  # return True

def test_check_pt_in_triangle2D():
  a = [-1, -1]
  b = [1, -1]
  c = [0, 1]

  cases = [
    ([0, 0], True),
    ([-1, 1], False),
    ([1, 1], False),
    ([0, 0.7], True),
    ([-0.5, -0.9], True),
    ([+0.5, -0.9], True)
  ]
  for i, (pt, expected) in enumerate(cases):
    actual = check_pt_in_triangle2D(pt, (a, b, c))
    print('Case: %d: %s' % (i, actual == expected))

def compute_reflection(incoming, normal):
  """
  Takes an incoming vector and normal surface vector and computes the reflection of the incoming
  vector at the surface.

  This method is slow, but explicit about its methodology. There is likely a much much more efficient
  way of doing this.

  0. Check that angle between normal and incoming is less than 90, if not, invert the normal
  1. Perform rotation about z-axis so that normal vector lies in XZ plane (y component is zero).
  2. Perform rotation about y-axis so that normal vector is aligned to z-axis.
  3. Perform rotation about z-axis so that incoming vector lies in XZ plane (y component is zero).
  4. Reflect x-coordinate of incoming vector
  5. Reverse transformations done in step 1-3
  """
  incoming = normalize(incoming)
  normal = normalize(normal)

  costh0 = np.dot(incoming, normal)
  if costh0 < 0:
    normal = - normal

  th, ph = get_vector_angles(normal)
  ph_rot = array([[cos(ph), sin(ph), 0], [-sin(ph), cos(ph), 0], [0, 0, 1]])
  th_rot = array([[cos(th), 0, -sin(th)], [0, 1, 0], [sin(th), 0, cos(th)]])
  T1 = np.einsum('ij,jk->ik', th_rot, ph_rot)
  incoming2 = np.einsum('ij,j->i', T1, incoming)

  th2, ph2, = get_vector_angles(incoming2)
  T2 = array([[cos(ph2), sin(ph2), 0], [-sin(ph2), cos(ph2), 0], [0, 0, 1]])
  incoming3 = np.einsum('ij,j->i', T2, incoming2)

  reflect = array([[-1, 0, 0], [0, 1, 0], [0, 0, 1]])
  incoming4 = np.einsum('ij,j->i', reflect, incoming3)

  inverted = np.einsum('ij,jk->ik', T2, T1)
  inverted = np.linalg.inv(inverted)
  return np.einsum('ij,j->i', inverted, incoming4)

def get_vector_angles(ori):
  """
  Returns theta, phi for a given vector.
  """
  ori = normalize(ori)
  th = np.arccos(ori[2])
  ph = np.arctan2(ori[1], ori[0])
  return th, ph

def normalize(a):
  return a / np.sqrt(np.dot(a, a))

if __name__ == '__main__':
  test_check_pt_in_triangle2D()