import numpy as np
import matplotlib.pyplot as plt
from numpy import sin, cos, pi, array
import sys

from initial.core import *
from initial.pmath import *

def setupax():
  fig = plt.figure(figsize=(8, 8))
  ax = fig.add_subplot(projection='3d')
  ax.set_xlim(-1.1, 1.1)
  ax.set_ylim(-1.1, 1.1)
  ax.set_zlim(-1.1, 1.1)
  ax.set_xticks([-1, 0, 1])
  ax.set_yticks([-1, 0, 1])
  ax.set_zticks([-1, 0, 1])
  ax.set_xlabel('X')
  ax.set_ylabel('Y')
  ax.set_zlabel('Z')
  ax.set_aspect('equal')
  ax.plot([0], [0], [0], '.', color='black', ms=20)
  return ax

def plot_ori(pt, ax=None, label=None, color=None):
  if ax is None:
    ax = setupax()
  xs = [0, pt[0]]
  ys = [0, pt[1]]
  zs = [0, pt[2]]
  if color:
    ax.plot(xs, ys, zs, label=label, color=color)
  else:
    ax.plot(xs, ys, zs, label=label)
  return ax

def plot_triangle(pts, ax=None, label=None, color=None):
  if ax is None:
    ax = setupax()
  xs = list(pts[:, 0])
  ys = list(pts[:, 1])
  zs = list(pts[:, 2])
  xs.append(xs[0])
  ys.append(ys[0])
  zs.append(zs[0])
  if color:
    ax.plot(xs, ys, zs, label=label, color=color)
  else:
    ax.plot(xs, ys, zs, label=label)
  return ax

def plot_surface(pt, ax=None, color=None):
  if ax is None:
    ax = setupax()
  xs = np.linspace(-1, 1, 100)
  ys = np.linspace(-1, 1, 100)
  zs = 1 - xs - ys

  a, b, c = pt
  pts = np.zeros((100 * 100, 3))
  for i, x in enumerate(np.linspace(-1, 1, 100)):
    for j, y in enumerate(np.linspace(-1, 1, 100)):
      z = (-a * x - b * y) / c
      pts[100 * i + j,:] = [x, y, z]

  if color:
    ax.plot(pts[:,0], pts[:,1], pts[:,2], color=color)
  else:
    ax.plot(pts[:,0], pts[:,1], pts[:,2])
  return ax