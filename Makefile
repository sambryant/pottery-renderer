PROJ_NAME=main

CPP_FILES=$(shell find src -name "*.cc")

S_OBJ=$(patsubst src/%.cc, obj/%.o, $(CPP_FILES))
S_DEP=$(patsubst src/%.cc, dep/%.d, $(CPP_FILES))

CXX=clang++
CXXFLAGS=-std=c++17 -Wall -Wno-c++11-extensions -O3 -g
SDL_HEAD=`pkg-config --cflags-only-I sdl2`
SDL_LINK=`pkg-config --libs --cflags sdl2`

all: $(PROJ_NAME)
	@echo Compiling application with $(CXX)

$(PROJ_NAME): $(S_OBJ)
	@echo Linking objects $(S_OBJ)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(SDL_LINK)

-include $(S_DEP)

# This rule updates the dependency file whenever its .cc or .h (if it exist) changes.
dep/%.d: src/%.cc $(wildcard src/%.h)
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $(SDL_HEAD) -MM -MT '$(patsubst src/%.cc,obj/%.o,$<)' $< -MF $@

# This rule updates the object file whenever its dep file changes.
obj/%.o: src/%.cc dep/%.d $(wildcard src/%.h)
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $(SDL_HEAD) -o $@ -c $<

clean:
	@echo Removing secondary things
	@rm -r -f objects $(S_OBJ) $(PROJ_NAME) $(S_DEP)
	@echo Done!
